package com.isofh.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
@PropertySource(value = {"classpath:application.properties", "classpath:image.properties", "classpath:rabbitmq.service.properties", "classpath:hospital.service.properties", "classpath:hospital.list.properties", "classpath:facebook" +
        ".service.properties"})
@EntityScan("com.isofh.service.model")
public class ServiceApplication {
	public static void main(String[] args) {
		SpringApplication.run(ServiceApplication.class, args);
	}
}
