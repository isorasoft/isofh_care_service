package com.isofh.service.rabbit.subcriber;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.isofh.service.constant.AppConst;
import com.isofh.service.model.WalletMessage;
import com.isofh.service.rabbit.publisher.PublisherWallet;
import com.isofh.service.service.BookingService;
import com.isofh.service.utils.DigitalSignatureTokenUtils;
import com.isofh.service.utils.GsonUtils;

@Component
public class PayWalletConsumer {

    @Autowired
    DigitalSignatureTokenUtils tokenService;

    @Autowired
    BookingService bookingService;
    
    @Autowired
    PublisherWallet publisherWallet;

    private static final Logger LOGGER = LoggerFactory.getLogger(SubcriberWalletVendor.class);

    @SuppressWarnings("unchecked")
    @RabbitListener(queues = "${jsa.rabbitmq.queue.wallet.vendor.pay}")
    public void recievedMessage(byte[] msg) {
        try {
            LOGGER.info("Recieved Message: {}", msg);
            String str = new String(msg);
            WalletMessage entity = GsonUtils.toObject(str, WalletMessage.class);
            LOGGER.info("Recieved Message String: {}", str);
            String data = tokenService.verifyTokenWallet(entity.getData(), AppConst.DATA);
            Map<String, String> mapData = GsonUtils.toObject(data, Map.class);
            String bookingId = mapData.get(AppConst.ODER_ID);
            String status = mapData.get(AppConst.STATUS);
            if (status.equals(AppConst.SUCCESS)) {
                String rs = bookingService.isPayWallet(Long.valueOf(bookingId));
                if (!rs.equals("True")) {
                    Map<String, String> mapResult = new HashMap<String, String>();
                    mapResult.put(AppConst.ODER_ID, bookingId);
                    mapResult.put(AppConst.REJECT, rs);
                    WalletMessage dataSend = new WalletMessage();
                    String mess = tokenService.generateToken(AppConst.DATA, GsonUtils.toString(mapResult));
                    dataSend.setData(mess);
                    publisherWallet.produceMsg(GsonUtils.toString(dataSend));
                }
            }

        } catch (Exception e) {
            LOGGER.error("ERROR: store drug {}", e);
        }

    }
}
