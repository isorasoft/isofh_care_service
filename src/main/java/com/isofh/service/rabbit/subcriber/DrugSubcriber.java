package com.isofh.service.rabbit.subcriber;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import com.isofh.service.utils.GsonUtils;

@Component
public class DrugSubcriber {

	private static final Logger LOGGER = LoggerFactory.getLogger(DrugSubcriber.class);


	@RabbitListener(queues = "${jsa.rabbitmq.queue}")
	public void recievedMessage(String msg) {
		try {
			LOGGER.info("Recieved Message: {}", msg);
			Object entity = GsonUtils.toObject(msg, Object.class);
			LOGGER.info("store sucess drug: {}", entity);
			LOGGER.info("store sucess drug: {}", entity);
		} catch (Exception e) {
			LOGGER.error("ERROR: store drug {}", e);
		}

	}
}
