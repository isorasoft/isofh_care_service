package com.isofh.service.rabbit.subcriber;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import com.isofh.service.model.MessageRabbitProfile;
import com.isofh.service.utils.GsonUtils;

@Component
public class ProfileSubcriber {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProfileSubcriber.class);


//	@RabbitListener(queues = "${jsa.rabbitmq.routingkey.profile}")
	public void recievedMessage(String msg) {
		try {
			LOGGER.info("Recieved Message: {}", msg);
			MessageRabbitProfile entity = GsonUtils.toObject(msg, MessageRabbitProfile.class);
			LOGGER.info("store sucess drug: {}", entity);
			LOGGER.info("store sucess drug: {}", entity);
		} catch (Exception e) {
			LOGGER.error("ERROR: store drug {}", e);
		}

	}
}
