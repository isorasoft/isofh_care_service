package com.isofh.service.rabbit.subcriber;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import com.isofh.service.model.HospitalEntity;
import com.isofh.service.model.MessageRabbitProfile;
import com.isofh.service.model.ProfileEntity;
import com.isofh.service.service.BaseService;
import com.isofh.service.utils.GsonUtils;

@Component
public class SubcriberWalletVendor extends BaseService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SubcriberWalletVendor.class);

	@RabbitListener(queues = "${jsa.rabbitmq.queue.wallet.vendor}")
    public void recievedMessage(byte[] msg) {
        try {
            LOGGER.info("Recieved Message: {}", msg);
            String str = new String(msg);
            LOGGER.info("Recieved Message String: {}", str);
            MessageRabbitProfile entity = GsonUtils.toObject(str, MessageRabbitProfile.class);
            HospitalEntity hospital = getHospital(Long.valueOf(entity.getData().getIsofh_care_id()));
            if(entity.getData().getWallet().getStatus().equals("ACTIVE")) {
                hospital.setActiveWallet(1);
            } else {
                hospital.setActiveWallet(0);
            }
            ProfileEntity profile = hospital.getUserAdmin().getProfile();
            profile.setWalletId(entity.getData().getWallet().getId());
            save(hospital);
            save(profile);
            LOGGER.info("store sucess drug: {}", entity);
        } catch (Exception e) {
            LOGGER.error("ERROR: store drug {}", e);
        }

    }
}
