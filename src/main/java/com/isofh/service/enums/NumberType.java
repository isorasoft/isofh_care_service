package com.isofh.service.enums;

public enum NumberType {
    COMMENT(1),
    POST(2),
    REVIEW(3);
    private final int value;

    NumberType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
