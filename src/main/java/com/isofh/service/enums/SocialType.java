/*
 * *******************************************************************
 * Copyright (c) 2018 Isofh.com to present.
 * All rights reserved.
 *
 * Author: tuanld
 * ******************************************************************
 *
 */

package com.isofh.service.enums;

/**
 * 1 Username/Password<br>
 * 2 FB<br>
 * 4 GG<br>
 */
public enum SocialType {
    DEFAULT(1),
    FACEBOOK(2),
    GOOGLE(4);


    private final Integer value;

    SocialType(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }
}
