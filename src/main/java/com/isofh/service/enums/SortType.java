package com.isofh.service.enums;

public enum SortType {

    ALPHA_B(1),
    DECS(2),
    ASC(3);


    private final int value;

    SortType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }


}
