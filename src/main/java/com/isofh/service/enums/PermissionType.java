/*
 * *******************************************************************
 * Copyright (c) 2018 Isofh.com to present.
 * All rights reserved.
 *
 * Author: tuanld
 * ******************************************************************
 */

package com.isofh.service.enums;

/**
 * Phân loại thuốc: int <br>
 * +) 1 = view tk quan tri <br>
 * +) 2 = them tk quan tri<br>
 * +) 4 = sua tk quan tri<br>
 * +) 8 = set password<br>
 * +) 16 = view tk user<br>
 * +) 32 = them tk user<br>
 * +) 64 = sua tk user<br>
 * +) 128 = view tk doctor<br>
 * +) 256 = them tk doctor<br>
 * +) 512 = sua tk doctor<br>
 * +) 1024 = view role<br>
 * +) 2048 = them role<br>
 * +) 4096 = sua role<br>
 * +) 8192 = xoa role<br>
 * +) 16384 = xem post<br>
 * +) 32768 = sua post<br>
 * +) 65536 = xem hospital<br>
 * +) 131072 = them hospital<br>
 * +) 262144 = sua hospital<br>
 * +) 524288 = xem chuyen khoa<br>
 * +) 1048576 = them chuyen khoa<br>
 * +) 2097152 = sua chuyen khoa<br>
 * +) 4194304 = xem dich vu<br>
 * +) 8388608 = them dich vu<br>
 * +) 16777216 = sua dich vu<br>
 */
public enum PermissionType {
    VIEW_ADMIN(1),
    ADD_ADMIN(1 << 1),
    UPDATE_ADMIN(1 << 2),
    SET_PASS_ADMIN(1 << 3),
    VIEW_USER(1 << 4),
    ADD_USER(1 << 5),
    UPDATE_USER(1 << 6),
    VIEW_DOCTOR(1 << 7),
    ADD_DOCTOR(1 << 8),
    UPDATE_DOCTOR(1 << 9),
    VIEW_ROLE(1<<10),
    ADD_ROLE(1<<11),
    UPDATE_ROLE(1<<12),
    DELETE_ROLE(1<<13),
    VIEW_POST(1<<14),
    UPDATE_POST(1<<15),
    VIEW_HOSPITAL(1<<16),
    ADD_HOSPITAL(1<<17),
    UPDATE_HOSPITAL(1<<18),
    VIEW_SPECIALIST(1<<19),
    ADD_SPECIALIST(1<<20),
    UPDATE_SPECIALIST(1<<21),
    VIEW_SERVICE(1<<22),
    ADD_SERVICE(1<<23),
    UPDATE_SERVICE(1<<24);
    

    private final Integer value;

    PermissionType(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }
}
