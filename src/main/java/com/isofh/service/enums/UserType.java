/*
 * *******************************************************************
 * Copyright (c) 2018 Isofh.com to present.
 * All rights reserved.
 *
 * Author: tuanld
 * ******************************************************************
 *
 */

package com.isofh.service.enums;

/**
 * Phan quyen cua nguoi dung <br>
 * 1: user, dai bieu<br>
 * 4: admin <br>
 * 8: admin co so y te <br>
 */
public enum UserType {

    USER(1),
    DOCTOR(2),
    ADMIN(4),
    ADMIN_FACILITY(8);

    private final int value;

    UserType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
