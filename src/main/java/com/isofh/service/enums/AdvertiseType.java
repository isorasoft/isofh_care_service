/*
 * *******************************************************************
 * Copyright (c) 2018 Isofh.com to present.
 * All rights reserved.
 *
 * Author: tuanld
 * ******************************************************************
 *
 */

package com.isofh.service.enums;

public enum AdvertiseType {
    NOTIFICATION(1),
    PROMOTION(2),
    NEW_FEATURES(4);


    private Integer value;

    AdvertiseType(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }
}
