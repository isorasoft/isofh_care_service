/*
 * *******************************************************************
 * Copyright (c) 2018 Isofh.com to present.
 * All rights reserved.
 *
 * Author: tuanld
 * ******************************************************************
 */

package com.isofh.service.enums;

/**
 * Phân loại thuốc: int <br>
 * +) 1 = Thuốc thường <br>
 * +) 2 = Dịch truyền<br>
 * +) 4 = Độc<br>
 * +) 8 = Hóa chất<br>
 * +) 16 = Hướng thần<br>
 * +) 32 = máu<br>
 * +) 64 = Nghiên cứu khoa học<br>
 * +) 128 = Thực phẩm chức năng<br>
 * +) 256 = Thuốc gây nghiện<br>
 * +) 512 = Vật tư tiêu hao<br>
 * +) 1024 = Hàm lượng<br>
 */
public enum DrugCategoryType {

    THUOC_THUONG(1),
    DICH_TRUYEN(1 << 1),
    DOC(1 << 2),
    HOA_CHAT(1 << 3),
    HUONG_THAN(1 << 4),
    MAU(1 << 5),
    NGHIEN_CUU_KHOA_HOC(1 << 6),
    THUC_PHAM_CHUC_NANG(1 << 7),
    VAT_TU_TIEU_HAO(1 << 8),
    HAM_LUONG(1 << 9);

    private final Integer value;

    DrugCategoryType(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }
}
