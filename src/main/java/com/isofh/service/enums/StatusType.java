package com.isofh.service.enums;

/**
 * status type post <br>
 * 1: chua duyet<br>
 * 2: da duyet<br>
 * 3: da tra loi<br>
 * 4: khong hop le<br>
 * 5: da tu choi<br>
 * 6: da danh gia<br>
 * 7: ket thuc phien<br>
 */
public enum StatusType {

    CHUA_DUYET(1),
    DA_DUYET(2),
    DA_TRA_LOI(3),
    DA_TU_CHOI(5),
    KHONG_HOP_LE(4),
    KET_THUC_PHIEN(7),
    DA_DANH_GIA(6);

    private final int value;

    StatusType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
