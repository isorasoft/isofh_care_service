/*
 * *******************************************************************
 * Copyright (c) 2018 Isofh.com to present.
 * All rights reserved.
 *
 * Author: tuanld
 * ******************************************************************
 */

package com.isofh.service.enums;

public enum HospitalProfileType {

    BVDHY(1);

    private final Integer value;

    HospitalProfileType(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }

    public static HospitalProfileType fromValue(int value) {
        if (Integer.valueOf(0).equals(value)) {
            return BVDHY;
        }

        return BVDHY;
    }
}
