/*
 * *******************************************************************
 * Copyright (c) 2018 Isofh.com to present.
 * All rights reserved.
 *
 * Author: tuanld
 * ******************************************************************
 */

package com.isofh.service.enums;

/**
 * 1 them lich lam viec<br>
 * 2 sua lich lam viec<br>
 * 4 xoa lich lam viec<br>
 * 8 them lich kham<br>
 * 16 sua lich kham<br>
 * 32 xoa lich kham <br>
 * 64 tao tai khoan<br>
 * 128 khoa tai khoan<br>
 * 256 reset mat khau<br>
 * 512 gui tin nhan quang ba<br>
 * 1024 mo khoa tai khoan<br>
 */
public enum LogType {

    ADD_SCHEDULE(1),
    UPDATE_SCHEDULE(1 << 1),
    DELETE_SCHEDULE(1 << 2),
    CREATE_BOOKING(1 << 3),
    UPDATE_BOOKING(1 << 4),
    CANCEL_BOOKING(1 << 5),
    CREATE_ACCOUNT(1 << 6),
    BLOCK_ACCOUNT(1 << 7),
    RESET_PASSWORD(1 << 8),
    SEND_ADV(1 << 9),
    UNBLOCK_ACCOUNT(1 << 10);

    private final Integer value;

    LogType(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }
}
