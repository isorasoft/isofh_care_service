package com.isofh.service.enums;

public enum DiseaseHistoryType {

    HEART(1),
    CHOLESTEROL(2),
    BLOOD_PRESSURE(4),
    HIV(8),
    RESPIRATORY(16),
    STOMACH(32),
    OSTEOARTHRITIS(64),
    LACK_OF_SUBSTANCE(128);
    private final int value;

    DiseaseHistoryType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
