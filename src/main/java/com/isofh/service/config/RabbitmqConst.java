/*
 * *******************************************************************
 * Copyright (c) 2018 Isofh.com to present.
 * All rights reserved.
 *
 * Author: tuanld
 * ******************************************************************
 */

package com.isofh.service.config;

public class RabbitmqConst {
    public static final String HOST = "123.24.206.9";
    public static final String USER_NAME = "isofh";
    public static final String PASSWORD = "isofh@123";
    public static final int PORT = 5672;
}
