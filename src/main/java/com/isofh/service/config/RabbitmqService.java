/*
 * *******************************************************************
 * Copyright (c) 2018 Isofh.com to present.
 * All rights reserved.
 *
 * Author: tuanld
 * ******************************************************************
 */

package com.isofh.service.config;

public class RabbitmqService {
    public static final String QUEUE_NEW_DRUG = "NEW_DRUG";
    public static final String QUEUE_UPDATED_DRUG = "UPDATED_DRUG";
    public static final String QUEUE_DELETED_DRUG = "DELETED_DRUG";
}
