package com.isofh.service.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.isofh.service.constant.AppConst;
import com.isofh.service.constant.TableConst;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity(name = TableConst.BOOKING)
public class BookingEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true)
    private long id;


    @JsonFormat(pattern = AppConst.DATE_TIME_FORMAT)
    public LocalDateTime createdDate;

    @JsonFormat(pattern = AppConst.DATE_TIME_FORMAT)
    public LocalDateTime updatedDate;

    @PrePersist
    public void prePersist() {
        createdDate = LocalDateTime.now();
        updatedDate = LocalDateTime.now();
    }

    /**
     * Ngay va gio dat kham
     */
    @JsonFormat(pattern = AppConst.DATE_TIME_FORMAT)
    private LocalDateTime bookingTime;

    /**
     * Ghi chu
     */
    private String note;

    /**
     * So thu tu dat kham
     */
    private Long sequenceNo;

    /**
     * ma lich su vao vien cua dat kham nay
     */
    private Long hisPatientHistoryId;

    /**
     * Trang thai cau dat kham
     */
    private Integer status;

    /**
     * Schedule of booking
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "schedule_id")
    @JsonIgnore
    @NotFound(
            action = NotFoundAction.IGNORE)
    private ScheduleEntity schedule;

    /**
     * Schedule of booking
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "profile_id")
    @JsonIgnore
    @NotFound(
            action = NotFoundAction.IGNORE)
    private ProfileEntity profile;


    /**
     * Department of Schedule
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "department_id")
    @JsonIgnore
    @NotFound(
            action = NotFoundAction.IGNORE)
    private DepartmentEntity department;

    /**
     * Room of Schedule
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "room_id")
    @JsonIgnore
    @NotFound(
            action = NotFoundAction.IGNORE)
    private RoomEntity room;

    /**
     * Service of Schedule
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "service_id")
    @JsonIgnore
    @NotFound(
            action = NotFoundAction.IGNORE)
    private ServiceEntity service;

    /**
     * Specialist of Schedule
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "specialist_id")
    @JsonIgnore
    @NotFound(
            action = NotFoundAction.IGNORE)
    private SpecialistEntity specialist;

    /**
     * Specialist of Schedule
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "doctor_id")
    @JsonIgnore
    @NotFound(
            action = NotFoundAction.IGNORE)
    private DoctorEntity doctor;

    /**
     * Trang thai xoa cua lich
     */
    private int deleted;

    @JsonIgnore
    @Column(columnDefinition = "TEXT")
    private String checkInResult;

    @Column(columnDefinition = "TEXT")
    private String reminder;

    /**
     * Trang thai da den benh vien
     */
    private int arrival;

    /**
     * admin dat kham, neu la nguoi dung thi de trong
     */
    private String admin;

    /**
     * Nguoi benh co yeu cau chinh xac la bac si nay kham khong
     */
    @Column(columnDefinition = "int(11) default 0")
    private int requestDoctor;

    /**
     * So dien thoai cua nguoi goi dien den dat kham
     */
    private String phoneCall;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "col_hospital_id")
    @NotFound(
            action = NotFoundAction.IGNORE)
    @JsonIgnore
    private HospitalEntity hospital;
    
    @Column(columnDefinition = "TEXT")
    private String hash;

    @PreUpdate
    public void preUpdate() {
        updatedDate = LocalDateTime.now();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public LocalDateTime getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(LocalDateTime updatedDate) {
        this.updatedDate = updatedDate;
    }

    public LocalDateTime getBookingTime() {
        return bookingTime;
    }

    public void setBookingTime(LocalDateTime bookingTime) {
        this.bookingTime = bookingTime;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Long getSequenceNo() {
        return sequenceNo;
    }

    public void setSequenceNo(Long sequenceNo) {
        this.sequenceNo = sequenceNo;
    }

    public Long getHisPatientHistoryId() {
        return hisPatientHistoryId;
    }

    public void setHisPatientHistoryId(Long hisPatientHistoryId) {
        this.hisPatientHistoryId = hisPatientHistoryId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public ScheduleEntity getSchedule() {
        return schedule;
    }

    public void setSchedule(ScheduleEntity schedule) {
        this.schedule = schedule;
    }

    public ProfileEntity getProfile() {
        return profile;
    }

    public void setProfile(ProfileEntity profile) {
        this.profile = profile;
    }

    public DepartmentEntity getDepartment() {
        return department;
    }

    public void setDepartment(DepartmentEntity department) {
        this.department = department;
    }

    public RoomEntity getRoom() {
        return room;
    }

    public void setRoom(RoomEntity room) {
        this.room = room;
    }

    public ServiceEntity getService() {
        return service;
    }

    public void setService(ServiceEntity service) {
        this.service = service;
    }

    public SpecialistEntity getSpecialist() {
        return specialist;
    }

    public void setSpecialist(SpecialistEntity specialist) {
        this.specialist = specialist;
    }

    public DoctorEntity getDoctor() {
        return doctor;
    }

    public void setDoctor(DoctorEntity doctor) {
        this.doctor = doctor;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public String getCheckInResult() {
        return checkInResult;
    }

    public void setCheckInResult(String checkInResult) {
        this.checkInResult = checkInResult;
    }

    public String getReminder() {
        return reminder;
    }

    public void setReminder(String reminder) {
        this.reminder = reminder;
    }

    public int getArrival() {
        return arrival;
    }

    public void setArrival(int arrival) {
        this.arrival = arrival;
    }

    public String getAdmin() {
        return admin;
    }

    public void setAdmin(String admin) {
        this.admin = admin;
    }

    public int getRequestDoctor() {
        return requestDoctor;
    }

    public void setRequestDoctor(int requestDoctor) {
        this.requestDoctor = requestDoctor;
    }

    public String getPhoneCall() {
        return phoneCall;
    }

    public void setPhoneCall(String phoneCall) {
        this.phoneCall = phoneCall;
    }

    public HospitalEntity getHospital() {
        return hospital;
    }

    public void setHospital(HospitalEntity hospital) {
        this.hospital = hospital;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }
}
