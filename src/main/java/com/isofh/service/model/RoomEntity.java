package com.isofh.service.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.isofh.service.constant.AppConst;
import com.isofh.service.constant.TableConst;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity(name = TableConst.ROOM)
public class RoomEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true)
    private long id;


    @JsonFormat(pattern = AppConst.DATE_TIME_FORMAT)
    public LocalDateTime createdDate;

    @JsonFormat(pattern = AppConst.DATE_TIME_FORMAT)
    public LocalDateTime updatedDate;

    @PrePersist
    public void prePersist() {
        createdDate = LocalDateTime.now();
        updatedDate = LocalDateTime.now();
    }

    /**
     * Id cua phong tren HIS
     */
    private String hisRoomId;
    /**
     * Loai phong tren HIS
     */
    private String hisRoomType;

    /**
     * Ten hien thi cua phong
     */
    private String name;

    /**
     * Ma phong
     */
    private String code;

    /**
     * trang thai xoa
     */
    private int deleted = 0;

    /**
     * Nước sản xuất: lấy trên danh mục của HIS
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "col_hospital_id")
    @NotFound(
            action = NotFoundAction.IGNORE)
    @JsonIgnore
    private HospitalEntity hospital;

    @PreUpdate
    public void preUpdate() {
        updatedDate = LocalDateTime.now();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public LocalDateTime getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(LocalDateTime updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getHisRoomId() {
        return hisRoomId;
    }

    public void setHisRoomId(String hisRoomId) {
        this.hisRoomId = hisRoomId;
    }

    public String getHisRoomType() {
        return hisRoomType;
    }

    public void setHisRoomType(String hisRoomType) {
        this.hisRoomType = hisRoomType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public HospitalEntity getHospital() {
        return hospital;
    }

    public void setHospital(HospitalEntity hospital) {
        this.hospital = hospital;
    }
}
