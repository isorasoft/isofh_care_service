package com.isofh.service.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.isofh.service.constant.AppConst;
import com.isofh.service.constant.TableConst;
import com.isofh.service.enums.SocialType;
import com.isofh.service.enums.UserType;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

/**
 * Tai khoan dang nhap va quan ly co so y te
 */
@Entity(name = TableConst.USER)
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true)
    private long id;


    @JsonFormat(pattern = AppConst.DATE_TIME_FORMAT)
    private LocalDateTime createdDate;

    @JsonFormat(pattern = AppConst.DATE_TIME_FORMAT)
    private LocalDateTime updatedDate;

    /**
     * nguoi tao
     */
    @OneToOne()
    @JoinColumn(name = "user_create_id")
    @NotFound(action = NotFoundAction.IGNORE)
    @JsonIgnore
    private UserEntity userCreate;

    /**
     * nguoi tao
     */
    @OneToOne()
    @JoinColumn(name = "user_update_id")
    @NotFound(action = NotFoundAction.IGNORE)
    @JsonIgnore
    private UserEntity userUpdate;

    @PrePersist
    public void prePersist() {
        createdDate = LocalDateTime.now();
        updatedDate = LocalDateTime.now();
    }

    @PreUpdate
    public void preUpdate() {
        updatedDate = LocalDateTime.now();
    }

    /**
     * Ten tai khoan
     */
    private String name;

    /**
     * Ten dang nhap
     */
    private String username;

    /**
     * Dia chi email
     */
    private String email;

    /**
     * Gioi tinh
     *
     * @see com.isofh.service.enums.GenderType
     */
    private int gender;

    /**
     * Ten cong ty
     */
    private String company;

    /**
     * Mat khau dang nhap
     */
    @JsonIgnore
    private String password;

    /**
     * so dien thoai
     */
    private String phone;

    /**
     * dia chi
     */
    private String address;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getIntroduct() {
        return introduct;
    }

    public void setIntroduct(String introduct) {
        this.introduct = introduct;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    /**
     * gioi thieu
     */
    private String introduct;

    /**
     * ma benh nhan, day tu his sang
     */
    private String patientCode;

    /**
     * Trang thai block cua lai khoan
     */
    private int blocked;

    private int verify = 0;

    /**
     * Max xac nhan tai khoan
     */
    @JsonIgnore
    private String verifyCode;

    /**
     * Ghi chu
     */
    @Column(columnDefinition = "TEXT")
    private String note;

    /**
     * Token sinh ra khi login tai khoan
     */
    @Column(columnDefinition = "TEXT")
    private String loginToken;

    @JsonIgnore
    private String resetPasswordCode;

    /**
     * Loai tai khoan
     *
     * @see com.isofh.service.enums.SocialType
     */
    private int socialType = SocialType.DEFAULT.getValue();

    /**
     * Phan quyen nguoi dung, mac dinh la user
     *
     * @see UserType
     */
    private int role;

    /**
     * danh sach cac thiet bi ma nguoi dung nay dang nhap vao
     */
    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "user")
    @JsonIgnore
    @NotFound(
            action = NotFoundAction.IGNORE)
    private Set<DeviceEntity> devices = new HashSet<>();

    /**
     * danh sach cac co so y te ma bac si lam viec
     */
    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "doctor")
    @JsonIgnore
    @NotFound(
            action = NotFoundAction.IGNORE)
    private Set<HospitalEntity> hospitals = new HashSet<>();

    /**
     * anh dai dien
     */
    private String avatar;

    public Set<HospitalEntity> getHospitals() {
        return hospitals;
    }

    public void setHospitals(Set<HospitalEntity> hospitals) {
        this.hospitals = hospitals;
    }

    /**
     * anh dai dien nho
     */
    private String thumbnail;

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    /**
     * ngay sinh
     */
    @JsonFormat(pattern = AppConst.DATE_TIME_FORMAT)
    private LocalDateTime dob;

    /**
     * ngay sinh
     */
    @JsonIgnore
    private String emailToken;

    /**
     * Trang thai cua lai khoan
     */
    private int active =1;

    /**
     * Id social khi dang nhap bang google
     */
    private String googleId;

    /**
     * Id social khi dang nhap bang facebook
     */
    private String facebookId;

    /**
     * Lan cuoi cung login cua user nay
     */
    private LocalDateTime lastLogin;


    public String getCertificateCode() {
        return certificateCode;
    }

    public void setCertificateCode(String certificateCode) {
        this.certificateCode = certificateCode;
    }

    /**
     * chung chi cua bac si
     */
    private String certificateCode;

    /**
     * chung vu cua bac si
     */
    private Integer title;

    /**
     * Hoc ham, hoc vi
     */
    private Integer degree;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    /**
     * hinh anh
     */
    private String image;
    /**
     * Danh sach cac facility ma nguoi dung nay la admin
     */
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true,
            fetch = FetchType.LAZY,
            mappedBy = "userAdmin")
    @JsonIgnore
    @NotFound(
            action = NotFoundAction.IGNORE)
    private Set<FacilityEntity> facilities = new HashSet<>();

    /**
     * Department ma user thuoc ve
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "col_doctor_department_id")
    @JsonIgnore
    @NotFound(
            action = NotFoundAction.IGNORE)
    private DoctorDepartmentEntity doctorDepartment;


    public Integer getTitle() {
        return title;
    }

    public void setTitle(Integer title) {
        this.title = title;
    }

    public Integer getDegree() {
        return degree;
    }

    public void setDegree(Integer degree) {
        this.degree = degree;
    }

    public CommonSpecialistEntity getSpecialist() {
        return specialist;
    }

    public void setSpecialist(CommonSpecialistEntity specialist) {
        this.specialist = specialist;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "specialist_id")
    @JsonIgnore
    @NotFound(
            action = NotFoundAction.IGNORE)
    private CommonSpecialistEntity specialist;

    public RolesEntity getRoleUser() {
        return roleUser;
    }

    public void setRoleUser(RolesEntity roleUser) {
        this.roleUser = roleUser;
    }

    /**
     * quyen cua nguoi dung
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "role_id")
    @JsonIgnore
    @NotFound(
            action = NotFoundAction.IGNORE)
    private RolesEntity roleUser;
    /**
     * uid cua nguoi dung
     */
    private String uid;

    /**
     * cmt cua nguoi dung
     */
    private String passport;

    /**
     * ngay cap cmt
     */
    private LocalDateTime dateRangePassPort;

    /**
     * noi cap
     */
    private String placePassPort;

    @Column(columnDefinition = "LONGTEXT")
    private String jsonPost;

    /**
     * so luong cau hoi bi reject cua 1 bs
     */
    private Integer numberRejectPost = 0;
    
    /**
     * so luong cau hoi bi reject cua 1 bs
     */
    private Integer numberAnsweredPost = 0;
    
    /**
     * so luong cau hoi bi reject cua 1 bs
     */
    private Integer numberWaitingPost = 0;

    /**
     * User co duy nhat 1 profile nay
     */
    @OneToOne(fetch = FetchType.LAZY,
            cascade = CascadeType.ALL,
            mappedBy = "user")
    @JsonIgnore
    @NotFound(
            action = NotFoundAction.IGNORE)
    private ProfileEntity profile;

    /**
     * Department ma user thuoc ve
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "col_hospital_id")
    @JsonIgnore
    @NotFound(
            action = NotFoundAction.IGNORE)
    private HospitalEntity hospital;
    
    /**
     * ma so thue
     */
    @Column(name = "col_tax_code")
    private String taxCode;
    
    /**
     * check yeu cau doi pass lan dau login<br>
     * 1: phai doi<br>
     * 0: k phai doi <br>
     */
    private Integer flag = 0;
    
    private Integer source;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public LocalDateTime getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(LocalDateTime updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPatientCode() {
        return patientCode;
    }

    public void setPatientCode(String patientCode) {
        this.patientCode = patientCode;
    }

    public int getBlocked() {
        return blocked;
    }

    public void setBlocked(int blocked) {
        this.blocked = blocked;
    }

    public int getVerify() {
        return verify;
    }

    public void setVerify(int verify) {
        this.verify = verify;
    }

    public String getVerifyCode() {
        return verifyCode;
    }

    public void setVerifyCode(String verifyCode) {
        this.verifyCode = verifyCode;
    }

    public String getLoginToken() {
        return loginToken;
    }

    public void setLoginToken(String loginToken) {
        this.loginToken = loginToken;
    }

    public Set<DeviceEntity> getDevices() {
        return devices;
    }

    public void setDevices(Set<DeviceEntity> devices) {
        this.devices = devices;
    }

    public int getSocialType() {
        return socialType;
    }

    public void setSocialType(int socialType) {
        this.socialType = socialType;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public String getAvartar() {
        return avatar;
    }

    public void setAvartar(String avatar) {
        this.avatar = avatar;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public LocalDateTime getDob() {
        return dob;
    }

    public void setDob(LocalDateTime dob) {
        this.dob = dob;
    }

    public String getEmailToken() {
        return emailToken;
    }

    public void setEmailToken(String emailToken) {
        this.emailToken = emailToken;
    }

    public String getGoogleId() {
        return googleId;
    }

    public void setGoogleId(String googleId) {
        this.googleId = googleId;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public LocalDateTime getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(LocalDateTime lastLogin) {
        this.lastLogin = lastLogin;
    }

    public String getResetPasswordCode() {
        return resetPasswordCode;
    }

    public void setResetPasswordCode(String resetPasswordCode) {
        this.resetPasswordCode = resetPasswordCode;
    }

    public Set<FacilityEntity> getFacilities() {
        return facilities;
    }

    public void setFacilities(Set<FacilityEntity> facilities) {
        this.facilities = facilities;
    }

    public DoctorDepartmentEntity getDoctorDepartment() {
        return doctorDepartment;
    }

    public void setDoctorDepartment(DoctorDepartmentEntity doctorDepartment) {
        this.doctorDepartment = doctorDepartment;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getPassport() {
        return passport;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }

    public LocalDateTime getDateRangePassPort() {
        return dateRangePassPort;
    }

    public void setDateRangePassPort(LocalDateTime dateRangePassPort) {
        this.dateRangePassPort = dateRangePassPort;
    }

    public String getPlacePassPort() {
        return placePassPort;
    }

    public void setPlacePassPort(String placePassPort) {
        this.placePassPort = placePassPort;
    }

    public UserEntity getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(UserEntity userCreate) {
        this.userCreate = userCreate;
    }

    public UserEntity getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(UserEntity userUpdate) {
        this.userUpdate = userUpdate;
    }

    public String getJsonPost() {
        return jsonPost;
    }

    public void setJsonPost(String jsonPost) {
        this.jsonPost = jsonPost;
    }

    public Integer getNumberRejectPost() {
        return numberRejectPost;
    }

    public void setNumberRejectPost(Integer numberRejectPost) {
        this.numberRejectPost = numberRejectPost;
    }

    public ProfileEntity getProfile() {
        return profile;
    }

    public void setProfile(ProfileEntity profile) {
        this.profile = profile;
    }

    public Integer getNumberAnsweredPost() {
        return numberAnsweredPost;
    }

    public void setNumberAnsweredPost(Integer numberAnsweredPost) {
        this.numberAnsweredPost = numberAnsweredPost;
    }

    public Integer getNumberWaitingPost() {
        return numberWaitingPost;
    }

    public void setNumberWaitingPost(Integer numberWaitingPost) {
        this.numberWaitingPost = numberWaitingPost;
    }

    public String getTaxCode() {
        return taxCode;
    }

    public void setTaxCode(String taxCode) {
        this.taxCode = taxCode;
    }

    public HospitalEntity getHospital() {
        return hospital;
    }

    public void setHospital(HospitalEntity hospital) {
        this.hospital = hospital;
    }

    public Integer getSource() {
        return source;
    }

    public void setSource(Integer source) {
        this.source = source;
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }
    
}
