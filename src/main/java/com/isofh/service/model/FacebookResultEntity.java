package com.isofh.service.model;

public class FacebookResultEntity {

    String id;

    PhoneFacebook phone;

    ApplicationFacebook application;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public PhoneFacebook getPhone() {
        return phone;
    }

    public void setPhone(PhoneFacebook phone) {
        this.phone = phone;
    }

    public ApplicationFacebook getApplication() {
        return application;
    }

    public void setApplication(ApplicationFacebook application) {
        this.application = application;
    }


}
