package com.isofh.service.model;

public class PatientHistoryHisEntity {

    public String patientHistoryId;
    public String date;
    
    public String getPatientHistoryId() {
        return patientHistoryId;
    }
    public void setPatientHistoryId(String patientHistoryId) {
        this.patientHistoryId = patientHistoryId;
    }
    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }
    
}
