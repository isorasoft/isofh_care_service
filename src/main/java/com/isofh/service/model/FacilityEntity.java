package com.isofh.service.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.isofh.service.constant.AppConst;
import com.isofh.service.constant.TableConst;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity(name = TableConst.FACILITY)
public class FacilityEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true)
    private long id;


    @JsonFormat(pattern = AppConst.DATE_TIME_FORMAT)
    public LocalDateTime createdDate;

    @JsonFormat(pattern = AppConst.DATE_TIME_FORMAT)
    public LocalDateTime updatedDate;

    @PrePersist
    public void prePersist() {
        createdDate = LocalDateTime.now();
        updatedDate = LocalDateTime.now();
    }

    @PreUpdate
    public void preUpdate() {
        updatedDate = LocalDateTime.now();
    }

    /**
     * Tên CSYT: String
     */
    private String name;


    /**
     * Ma co so y te
     */
    private String code;

    /**
     * Loai co so y te
     * +) 1 = Bệnh viện
     * +) 2 = Phòng khám
     * +) 4 = Trung tâm y tế
     * +) 8 = Nha thuoc
     *
     * @see com.isofh.service.enums.FacilityType
     */
    private int type;


    /**
     * Tuyen cua benh vien<br>
     * 1 Tuyen 1<br>
     * 2 Tuyen 2<br>
     * 4 Tuyen 3<br>
     * 8 Tuyen 4<br>
     *
     * @see com.isofh.service.enums.FacilityLevelType
     */
    private long levelId;


    /**
     * Hang cua benh vien <br>
     * 1 Hang dac biet<br>
     * 2 Hang 1<br>
     * 4 Hang 2<br>
     * 8 Hang 3<br>
     * 16 Hang 4<br>
     *
     * @see com.isofh.service.enums.FacilityRankType
     */
    private long rankId;


    /**
     * Website: String
     */
    private String website;


    /**
     * Danh gia
     */
    private float review = 0;

    private long reviewCount = 0;

    /**
     * Cai co so y te nay thuoc ve isofh k?
     * +) 1 = Có
     * +) 0 = Không
     */
    private int belongIsofh;

    /**
     * Dia chi
     */
    private String address;

    /**
     * SĐT: String
     */
    private String phone;


    /**
     * Gioi thieu
     */
    @Column(columnDefinition = "TEXT")
    private String introduction;


    /**
     * Trang thai phe duyet
     */
    private int approval;

    /**
     * Kinh do
     */
    private float longitude = 0;

    /**
     * Vi do
     */
    private float latitude = 0;

    /**
     * danh sach cac chuyen khoa cua facility
     */
    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })
    @JsonIgnore
    @JoinTable(name = "facility_specialist",
            joinColumns = {@JoinColumn(name = "facility_id")},
            inverseJoinColumns = {@JoinColumn(name = "specialist_id")})
    private Set<CommonSpecialistEntity> specialists = new HashSet<>();

    /**
     * Danh sach cac image of facility
     */
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true,
            fetch = FetchType.LAZY,
            mappedBy = "facility")
    @JsonIgnore
    @NotFound(
            action = NotFoundAction.IGNORE)
    private Set<ImageEntity> images = new HashSet<>();

    /**
     * Thành phố
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "province_id")
    @JsonIgnore
    @NotFound(
            action = NotFoundAction.IGNORE)
    private ProvinceEntity province;


    /**
     * So cap giay phep cua nha thuoc
     */
    private String licenseNumber;

    /**
     * Tiêu chuẩn GPP: int (0,1) cua nha thuoc
     */
    private int gpp;

    /**
     * Duoc si phu trach nha thuoc nay
     */
    private String pharmacist;


    /**
     * logo cua co so y te
     */
    private String logo;

    /**
     * So dien thoai kham cap
     */
    private String emergencyContact;

    /**
     * Nguoi tao ra facility nay
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    @JsonIgnore
    @NotFound(
            action = NotFoundAction.IGNORE)
    private UserEntity user;

    /**
     * Nguoi co trach nhiem quan ly, tra loi cac cau hoi cua facility nay
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_admin_id")
    @JsonIgnore
    @NotFound(
            action = NotFoundAction.IGNORE)
    private UserEntity userAdmin;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public LocalDateTime getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(LocalDateTime updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public float getReview() {
        return review;
    }

    public void setReview(float review) {
        this.review = review;
    }

    public int getBelongIsofh() {
        return belongIsofh;
    }

    public void setBelongIsofh(int belongIsofh) {
        this.belongIsofh = belongIsofh;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public long getLevelId() {
        return levelId;
    }

    public void setLevelId(long levelId) {
        this.levelId = levelId;
    }

    public long getRankId() {
        return rankId;
    }

    public void setRankId(long rankId) {
        this.rankId = rankId;
    }

    public Set<CommonSpecialistEntity> getSpecialists() {
        return specialists;
    }

    public void setSpecialists(Set<CommonSpecialistEntity> specialists) {
        this.specialists = specialists;
    }

    public Set<ImageEntity> getImages() {
        return images;
    }

    public void setImages(Set<ImageEntity> images) {
        this.images = images;
    }

    public ProvinceEntity getProvince() {
        return province;
    }

    public void setProvince(ProvinceEntity province) {
        this.province = province;
    }

    public int getApproval() {
        return approval;
    }

    public void setApproval(int approval) {
        this.approval = approval;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public long getReviewCount() {
        return reviewCount;
    }

    public void setReviewCount(long reviewCount) {
        this.reviewCount = reviewCount;
    }

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    public int getGpp() {
        return gpp;
    }

    public void setGpp(int gpp) {
        this.gpp = gpp;
    }

    public String getPharmacist() {
        return pharmacist;
    }

    public void setPharmacist(String pharmacist) {
        this.pharmacist = pharmacist;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getEmergencyContact() {
        return emergencyContact;
    }

    public void setEmergencyContact(String emergencyContact) {
        this.emergencyContact = emergencyContact;
    }

    public UserEntity getUserAdmin() {
        return userAdmin;
    }

    public void setUserAdmin(UserEntity userAdmin) {
        this.userAdmin = userAdmin;
    }
}
