package com.isofh.service.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.isofh.service.constant.AppConst;

public class ProfileMessage {

    public ProfileMessage() {
        super();
    }

//    public ProfileMessage(String name, Integer gender, String birthday, String passport, String phone, String address,
//            Long isofh_care_id, String status, String tax_id) {
//        super();
//        this.name = name;
//        this.gender = gender;
//        this.birthday = birthday;
//        this.passport = passport;
//        this.phone = phone;
//        this.address = address;
//        this.isofh_care_id = isofh_care_id;
//        this.status = status;
//        this.tax_id = tax_id;
//    }
    
    public ProfileMessage(String name, Integer gender, String birthday, String passport, String phone, String address,
            String isofh_care_id, String status) {
        super();
        this.name = name;
        this.gender = gender;
        this.birthday = birthday;
        this.passport = passport;
        this.phone = phone;
        this.address = address;
        this.isofh_care_id = isofh_care_id;
        this.status = status;
    }
    
    public ProfileMessage(String name, String tax_id, String address, String status,
            String isofh_care_id, WalletEntity wallet) {
        super();
        this.name = name;
        this.tax_id = tax_id;
        this.address = address;
        this.status = status;
        this.isofh_care_id = isofh_care_id;
        this.wallet = wallet;
    }

    private String name;
    private Integer gender;
    @JsonFormat(pattern = AppConst.DATE_FORMAT)
    private String birthday;
    private String passport;
    private String phone;
    private String address;
    private String id;
    private Integer idWallet;
    private String isofh_care_id;
    private String tax_id;

    private WalletEntity wallet;
    /**
     * status: active
     */
    private String status;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getPassport() {
        return passport;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getIdWallet() {
        return idWallet;
    }

    public void setIdWallet(Integer idWallet) {
        this.idWallet = idWallet;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIsofh_care_id() {
        return isofh_care_id;
    }

    public void setIsofh_care_id(String isofh_care_id) {
        this.isofh_care_id = isofh_care_id;
    }

    public String getTax_id() {
        return tax_id;
    }

    public void setTax_id(String tax_id) {
        this.tax_id = tax_id;
    }

    public WalletEntity getWallet() {
        return wallet;
    }

    public void setWallet(WalletEntity wallet) {
        this.wallet = wallet;
    }

}
