package com.isofh.service.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.isofh.service.constant.AppConst;
import com.isofh.service.constant.TableConst;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity(name = TableConst.DISEASE)
public class DiseaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true)
    private long id;


    @JsonFormat(pattern = AppConst.DATE_TIME_FORMAT)
    private LocalDateTime createdDate;

    @JsonFormat(pattern = AppConst.DATE_TIME_FORMAT)
    private LocalDateTime updatedDate;

    @PrePersist
    public void prePersist() {
        createdDate = LocalDateTime.now();
        updatedDate = LocalDateTime.now();
    }

    @PreUpdate
    public void preUpdate() {
        updatedDate = LocalDateTime.now();
    }

    /**
     * Tên bệnh: String
     */
    private String name;
    /**
     * Giới tính: int
     * +) 1 = Nữ
     * +) 2 = Nam
     * +) 3 = Cả 2
     */
    private int gender;

    /**
     * Nguyên nhân:String
     */
    @Column(columnDefinition = "TEXT")
    private String reason;

    /**
     * Tổng quan:String
     */
    @Column(columnDefinition = "TEXT")
    private String generalInfo;

    /**
     * Chuyên khoa
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "specialist_id")
    @NotFound(
            action = NotFoundAction.IGNORE)
    @JsonIgnore
    private CommonSpecialistEntity specialist;

    /**
     * Tuổi(from age):
     */
    private int fromAge;


    /**
     * Tuổi(to age):int
     */
    private int toAge;

    /**
     * Hướng điều trị:String
     */
    @Column(columnDefinition = "TEXT")
    private String treatment;

    /**
     * So lan dc tim kiem cua trieu trung
     */
    private long viewCount = 0;


    /**
     * Danh sach cac image of facility
     */
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true,
            fetch = FetchType.LAZY,
            mappedBy = "disease")
    @JsonIgnore
    @NotFound(
            action = NotFoundAction.IGNORE)
    private Set<ImageEntity> images = new HashSet<>();


    /**
     * danh sach cac chuyen khoa cua facility
     */
    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })
    @JoinTable(name = "disease_symptom",
            joinColumns = {@JoinColumn(name = "disease_id")},
            inverseJoinColumns = {@JoinColumn(name = "symptom_id")})
    @JsonIgnore
    private Set<SymptomEntity> symptoms = new HashSet<>();


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getGeneralInfo() {
        return generalInfo;
    }

    public void setGeneralInfo(String generalInfo) {
        this.generalInfo = generalInfo;
    }

    public CommonSpecialistEntity getSpecialist() {
        return specialist;
    }

    public void setSpecialist(CommonSpecialistEntity specialist) {
        this.specialist = specialist;
    }

    public int getFromAge() {
        return fromAge;
    }

    public void setFromAge(int fromAge) {
        this.fromAge = fromAge;
    }

    public int getToAge() {
        return toAge;
    }

    public void setToAge(int toAge) {
        this.toAge = toAge;
    }

    public String getTreatment() {
        return treatment;
    }

    public void setTreatment(String treatment) {
        this.treatment = treatment;
    }

    public Set<ImageEntity> getImages() {
        return images;
    }

    public void setImages(Set<ImageEntity> images) {
        this.images = images;
    }

    public Set<SymptomEntity> getSymptoms() {
        return symptoms;
    }

    public void setSymptoms(Set<SymptomEntity> symptoms) {
        this.symptoms = symptoms;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public LocalDateTime getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(LocalDateTime updatedDate) {
        this.updatedDate = updatedDate;
    }

    public long getViewCount() {
        return viewCount;
    }

    public void setViewCount(long viewCount) {
        this.viewCount = viewCount;
    }
}
