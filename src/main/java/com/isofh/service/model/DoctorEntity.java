package com.isofh.service.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.gson.annotations.Expose;
import com.isofh.service.constant.AppConst;
import com.isofh.service.constant.TableConst;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity(name = TableConst.DOCTOR)
public class DoctorEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true)
    private long id;


    @JsonFormat(pattern = AppConst.DATE_TIME_FORMAT)
    public LocalDateTime createdDate;

    @JsonFormat(pattern = AppConst.DATE_TIME_FORMAT)
    public LocalDateTime updatedDate;

    @PrePersist
    public void prePersist() {
        createdDate = LocalDateTime.now();
        updatedDate = LocalDateTime.now();
    }

    /**
     * Email cua bac si
     */
    private String email;

    /**
     * Id cua bac si tren HIS
     */
    private String hisDoctorId;

    /**
     * Value his day sang, chua su dung
     */
    private String value;

    /**
     * Ten hien thi day du
     */
    private String fullname;

    /**
     * Ma hoc vi cua bac si
     */
    @Expose
    private String hisAcademicRankId;

    /**
     * Hoc ham, hoc vi cua bac si
     */
    @Expose
    private String academicRank;

    /**
     * Hoc ham hoc vi viet tat
     */
    @Expose
    private String academicRankShortName;

    /**
     * Chuc vu cua bac si
     */
    @Expose
    private String position;

    /**
     * Giai thuong ma bi si day co
     */
    @Expose
    private String award;

    /**
     * Avatar his tra ve
     */
    @Expose
    private String avatar;

    @Expose
    private String name;

    /**
     * trang thai xoa
     */
    private int deleted = 0;

    /**
     * Nước sản xuất: lấy trên danh mục của HIS
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "col_hospital_id")
    @NotFound(
            action = NotFoundAction.IGNORE)
    @JsonIgnore
    private HospitalEntity hospital;

    @PreUpdate
    public void preUpdate() {
        updatedDate = LocalDateTime.now();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public LocalDateTime getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(LocalDateTime updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHisDoctorId() {
        return hisDoctorId;
    }

    public void setHisDoctorId(String hisDoctorId) {
        this.hisDoctorId = hisDoctorId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getHisAcademicRankId() {
        return hisAcademicRankId;
    }

    public void setHisAcademicRankId(String hisAcademicRankId) {
        this.hisAcademicRankId = hisAcademicRankId;
    }

    public String getAcademicRank() {
        return academicRank;
    }

    public void setAcademicRank(String academicRank) {
        this.academicRank = academicRank;
    }

    public String getAcademicRankShortName() {
        return academicRankShortName;
    }

    public void setAcademicRankShortName(String academicRankShortName) {
        this.academicRankShortName = academicRankShortName;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getAward() {
        return award;
    }

    public void setAward(String award) {
        this.award = award;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public HospitalEntity getHospital() {
        return hospital;
    }

    public void setHospital(HospitalEntity hospital) {
        this.hospital = hospital;
    }
}
