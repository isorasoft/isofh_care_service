/*
 * *******************************************************************
 * Copyright (c) 2018 Isofh.com to present.
 * All rights reserved.
 *
 * Author: tuanld
 * ******************************************************************
 *
 */

package com.isofh.service.model;


public class QueueEntity {

    /**
     * {@link com.isofh.service.constant.type.QueueType}
     * Loai queue can thuc hien
     */
    private Integer type;
    private Object data;

    public QueueEntity(Object object, int type) {
        this.type = type;
        this.data = object;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
