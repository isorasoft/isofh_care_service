package com.isofh.service.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.isofh.service.constant.AppConst;
import com.isofh.service.constant.TableConst;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity(name = TableConst.DOCTOR_INF)
public class DoctorInfEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true)
    private long id;


    @JsonFormat(pattern = AppConst.DATE_TIME_FORMAT)
    public LocalDateTime createdDate;

    @JsonFormat(pattern = AppConst.DATE_TIME_FORMAT)
    public LocalDateTime updatedDate;

    @PrePersist
    public void prePersist() {
        createdDate = LocalDateTime.now();
        updatedDate = LocalDateTime.now();
    }

    /**
     * ten bs dang ky
     */
    @Column(name = "col_name")
    private String name;

    /**
     * email bs dang ky
     */
    @Column(name = "col_email")
    private String email;

    /**
     * sdt bs dang ky
     */
    @Column(name = "col_phone")
    private String phone;

    /**
     * trang thai active tai khoan bs
     */
    @Column(name = "col_active")
    private Integer active =0;

    public String getEmail() {
        return email;
    }

    public Integer getActive() {
        return active;
    }

    public void setActive(Integer active) {
        this.active = active;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }


    public String getCertificateCode() {
        return certificateCode;
    }

    public CommonSpecialistEntity getSpecialist() {
        return specialist;
    }

    public void setSpecialist(CommonSpecialistEntity specialist) {
        this.specialist = specialist;
    }

    public void setCertificateCode(String certificateCode) {
        this.certificateCode = certificateCode;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }


    /**
     * chung chi hanh nghe bs dang ky
     */
    @Column(name = "col_certificate_code")
    private String certificateCode;

    /**
     * link anh bs dang ky
     */
    @Column(name = "col_image")
    private String image;

    @PreUpdate
    public void preUpdate() {
        updatedDate = LocalDateTime.now();
    }


    /**
     * chuyen khoa bs dang ky
     */

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "specialist_id")
    @JsonIgnore
    @NotFound(
            action = NotFoundAction.IGNORE)
    private CommonSpecialistEntity specialist;
    
    /**
     * block doctor info <br>
     * 0: un block <br>
     * 1: block
     */
    private Integer block = 0;

    @Column(columnDefinition = "LONGTEXT")
    private String reject;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public LocalDateTime getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(LocalDateTime updatedDate) {
        this.updatedDate = updatedDate;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getBlock() {
        return block;
    }

    public void setBlock(Integer block) {
        this.block = block;
    }

    public String getReject() {
        return reject;
    }

    public void setReject(String reject) {
        this.reject = reject;
    }
    
}
