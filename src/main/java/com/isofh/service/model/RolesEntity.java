package com.isofh.service.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.isofh.service.constant.AppConst;
import com.isofh.service.constant.TableConst;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity(name = TableConst.ROLES)
public class RolesEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true)
    private long id;


    @JsonFormat(pattern = AppConst.DATE_TIME_FORMAT)
    public LocalDateTime createdDate;

    @JsonFormat(pattern = AppConst.DATE_TIME_FORMAT)
    public LocalDateTime updatedDate;

    @PrePersist
    public void prePersist() {
        createdDate = LocalDateTime.now();
        updatedDate = LocalDateTime.now();
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    @Column(name = "col_deleted", nullable = false, columnDefinition = "int(11) default 0")
    public int deleted = 0;

    @Column(name = "col_name")
    private String name;

    public Long getValueRole() {
        return valueRole;
    }

    public void setValueRole(Long valueRole) {
        this.valueRole = valueRole;
    }

    public Set<PermissionEntity> getPermissionEntities() {
        return permissionEntities;
    }

    public void setPermissionEntities(Set<PermissionEntity> permissionEntities) {
        this.permissionEntities = permissionEntities;
    }

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })
    @JoinTable(name = "permission_roles",
            joinColumns = {@JoinColumn(name = "role_id")},
            inverseJoinColumns = {@JoinColumn(name = "permission_id")})
    @JsonIgnore
    private Set<PermissionEntity> permissionEntities = new HashSet<>();

    @Column(name = "col_code")
    private String code;

    @Column(name = "col_valueRole")
    private Long valueRole;

    @Column(name = "col_blocked")
    private Integer blocked = 0;

    @OneToOne()
    @JoinColumn(name = "col_create_person_id")
    @NotFound(
            action = NotFoundAction.IGNORE)
    @JsonIgnore
    private UserEntity createPerson;

    @OneToOne()
    @JoinColumn(name = "col_update_person_id")
    @NotFound(
            action = NotFoundAction.IGNORE)
    @JsonIgnore
    private UserEntity updatePerson;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public UserEntity getCreatePerson() {
        return createPerson;
    }

    public void setCreatePerson(UserEntity createPerson) {
        this.createPerson = createPerson;
    }

    public UserEntity getUpdatePerson() {
        return updatePerson;
    }

    public void setUpdatePerson(UserEntity updatePerson) {
        this.updatePerson = updatePerson;
    }


    public Set<UserEntity> getUsers() {
        return users;
    }

    public void setUsers(Set<UserEntity> users) {
        this.users = users;
    }

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true,
            fetch = FetchType.LAZY,
            mappedBy = "role")
    @JsonIgnore
    @NotFound(
            action = NotFoundAction.IGNORE)
    private Set<UserEntity> users = new HashSet<>();

    @PreUpdate
    public void preUpdate() {
        updatedDate = LocalDateTime.now();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public LocalDateTime getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(LocalDateTime updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Integer getBlocked() {
        return blocked;
    }

    public void setBlocked(Integer blocked) {
        this.blocked = blocked;
    }
}
