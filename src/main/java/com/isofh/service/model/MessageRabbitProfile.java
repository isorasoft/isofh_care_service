package com.isofh.service.model;

public class MessageRabbitProfile {

    private String action;
    private ProfileMessage data;
    
    public String getAction() {
        return action;
    }
    public void setAction(String action) {
        this.action = action;
    }
    public ProfileMessage getData() {
        return data;
    }
    public void setData(ProfileMessage data) {
        this.data = data;
    }
    
}
