/*
 * *******************************************************************
 * Copyright (c) 2018 Isofh.com to present.
 * All rights reserved.
 *
 * Author: tuanld
 * ******************************************************************
 *
 */

package com.isofh.service.model;


public class ResultHisEntity {

    private Integer code;
    private String message;
    private HisEntity data;


    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public HisEntity getData() {
        return data;
    }

    public void setData(HisEntity data) {
        this.data = data;
    }
}
