package com.isofh.service.model;

public class WalletEntity {

    
    public WalletEntity() {
        super();
    }
    public WalletEntity(String id) {
        super();
        this.id = id;
    }
    private String id;
    private String owner_type;
    private String owner_id;
    private String status;
    private String balance;
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getOwner_type() {
        return owner_type;
    }
    public void setOwner_type(String owner_type) {
        this.owner_type = owner_type;
    }
    public String getOwner_id() {
        return owner_id;
    }
    public void setOwner_id(String owner_id) {
        this.owner_id = owner_id;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public String getBalance() {
        return balance;
    }
    public void setBalance(String balance) {
        this.balance = balance;
    }
    
    
}
