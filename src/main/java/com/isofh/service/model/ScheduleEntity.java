package com.isofh.service.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.isofh.service.constant.AppConst;
import com.isofh.service.constant.TableConst;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity(name = TableConst.SCHEDULE)
public class ScheduleEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true)
    private long id;


    @JsonFormat(pattern = AppConst.DATE_TIME_FORMAT)
    public LocalDateTime createdDate;

    @JsonFormat(pattern = AppConst.DATE_TIME_FORMAT)
    public LocalDateTime updatedDate;

    @PrePersist
    public void prePersist() {
        createdDate = LocalDateTime.now();
        updatedDate = LocalDateTime.now();
    }

    /**
     * Thoi gian bat dau lam viec tinh theo don vi la phu tu luc 0h sang<br>
     * 8h tuong ung 8 * 60 = 480
     */
    private Long startWorking;

    /**
     * Thoi gian ket thuc lam viec lam viec tinh theo don vi la phu tu luc 0h sang <br>
     * 8h tuong ung 8 * 60 = 480
     */
    private Long endWorking;

    /**
     * so luong lich trong vong 30p
     */
    private Integer numberCase;

    /**
     * Thoi gian bat dau co hieu luc
     */
    @JsonFormat(pattern = AppConst.DATE_FORMAT)
    private LocalDate scheduleDate;

    /**
     * Ngay lam viec trong tuan cua bac si <br>
     * t2=1 cn = 7
     */
    private Integer dayOfWeek;


    /**
     * Ca lam viec cua bac si<br>
     * 0 ca sang <br>
     * 1 ca chieu
     */
    private Integer timeOfDay;


    /**
     * So ngay cho phep dat lich truoc do
     */
    private Integer numAllowedDate;

    /**
     * Id de luu thong tin cac lich tao cung 1 lan, de chinh sua va update
     */
    private String scheduleChain;

    private int deleted = 0;

    /**
     * Department of Schedule
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "department_id")
    @JsonIgnore
    @NotFound(
            action = NotFoundAction.IGNORE)
    private DepartmentEntity department;

    /**
     * Room of Schedule
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "room_id")
    @JsonIgnore
    @NotFound(
            action = NotFoundAction.IGNORE)
    private RoomEntity room;

    /**
     * Service of Schedule
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "service_id")
    @JsonIgnore
    @NotFound(
            action = NotFoundAction.IGNORE)
    private ServiceEntity service;

    /**
     * Specialist of Schedule
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "specialist_id")
    @JsonIgnore
    @NotFound(
            action = NotFoundAction.IGNORE)
    private SpecialistEntity specialist;

    /**
     * Specialist of Schedule
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "doctor_id")
    @JsonIgnore
    @NotFound(
            action = NotFoundAction.IGNORE)
    private DoctorEntity doctor;


    /**
     * Danh sach cac booking cua schedule nay
     */
    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "schedule")
    @JsonIgnore
    @NotFound(
            action = NotFoundAction.IGNORE)
    private Set<BookingEntity> bookings = new HashSet<>();

    /**
     * Nước sản xuất: lấy trên danh mục của HIS
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "col_hospital_id")
    @NotFound(
            action = NotFoundAction.IGNORE)
    @JsonIgnore
    private HospitalEntity hospital;

    @PreUpdate
    public void preUpdate() {
        updatedDate = LocalDateTime.now();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public LocalDateTime getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(LocalDateTime updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Long getStartWorking() {
        return startWorking;
    }

    public void setStartWorking(Long startWorking) {
        this.startWorking = startWorking;
    }

    public Long getEndWorking() {
        return endWorking;
    }

    public void setEndWorking(Long endWorking) {
        this.endWorking = endWorking;
    }

    public Integer getNumberCase() {
        return numberCase;
    }

    public void setNumberCase(Integer numberCase) {
        this.numberCase = numberCase;
    }

    public LocalDate getScheduleDate() {
        return scheduleDate;
    }

    public void setScheduleDate(LocalDate scheduleDate) {
        this.scheduleDate = scheduleDate;
    }

    public Integer getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(Integer dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public Integer getTimeOfDay() {
        return timeOfDay;
    }

    public void setTimeOfDay(Integer timeOfDay) {
        this.timeOfDay = timeOfDay;
    }

    public Integer getNumAllowedDate() {
        return numAllowedDate;
    }

    public void setNumAllowedDate(Integer numAllowedDate) {
        this.numAllowedDate = numAllowedDate;
    }

    public String getScheduleChain() {
        return scheduleChain;
    }

    public void setScheduleChain(String scheduleChain) {
        this.scheduleChain = scheduleChain;
    }

    public DepartmentEntity getDepartment() {
        return department;
    }

    public void setDepartment(DepartmentEntity department) {
        this.department = department;
    }

    public RoomEntity getRoom() {
        return room;
    }

    public void setRoom(RoomEntity room) {
        this.room = room;
    }

    public ServiceEntity getService() {
        return service;
    }

    public void setService(ServiceEntity service) {
        this.service = service;
    }

    public SpecialistEntity getSpecialist() {
        return specialist;
    }

    public void setSpecialist(SpecialistEntity specialist) {
        this.specialist = specialist;
    }

    public DoctorEntity getDoctor() {
        return doctor;
    }

    public void setDoctor(DoctorEntity doctor) {
        this.doctor = doctor;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public Set<BookingEntity> getBookings() {
        return bookings;
    }

    public void setBookings(Set<BookingEntity> bookings) {
        this.bookings = bookings;
    }

    public HospitalEntity getHospital() {
        return hospital;
    }

    public void setHospital(HospitalEntity hospital) {
        this.hospital = hospital;
    }
}
