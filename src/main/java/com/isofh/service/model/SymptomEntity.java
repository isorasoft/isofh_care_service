package com.isofh.service.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.isofh.service.constant.AppConst;
import com.isofh.service.constant.TableConst;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity(name = TableConst.SYMPTOM)
public class SymptomEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true)
    private long id;


    @JsonFormat(pattern = AppConst.DATE_TIME_FORMAT)
    public LocalDateTime createdDate;

    @JsonFormat(pattern = AppConst.DATE_TIME_FORMAT)
    public LocalDateTime updatedDate;

    @PrePersist
    public void prePersist() {
        createdDate = LocalDateTime.now();
        updatedDate = LocalDateTime.now();
    }

    @PreUpdate
    public void preUpdate() {
        updatedDate = LocalDateTime.now();
    }

    /**
     * Khi day len luu y:
     * Phia truoc  : 1
     * phia sau : 2
     * ca 2 phia: 3
     *
     * 4 Nu
     * 8 Nam
     * 12 Ca 2
     *
     */


    /**
     * đầu
     */
    private long head;
    /**
     * cổ
     */
    private long neck;

    /**
     * vai
     */
    private long shoulder;

    /**
     * ngực
     */
    private long chest;

    /**
     * cánh tay
     */
    private long arm;

    /**
     * cánh tay
     */
    private long hand;

    /**
     * Bụng
     */
    private long abdomen;

    /**
     * hông
     */
    private long flank;


    /**
     * hông
     */
    private long pelvis;

    /**
     * chân
     */
    private long leg;

    /**
     * chân
     */
    private long foot;

    /**
     * lưng
     */
    private long back;

    /**
     * thắt lưng
     */
    private long waist;

    /**
     * thắt lưng
     */
    private long ass;

    private String name;

    /**
     * So lan dc tim kiem cua trieu trung
     */
    private long viewCount = 0;

//    /**
//     * Danh sach cac benh co trieu chung nay
//     */
//    @ManyToMany(fetch = FetchType.LAZY,
//            cascade = {
//                    CascadeType.PERSIST,
//                    CascadeType.MERGE
//            }, mappedBy = "symptoms")
//    @JsonIgnore
//    private Set<DiseaseEntity> diseases = new HashSet<>();

    /**
     * danh sach cac chuyen khoa cua facility
     */
    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })
    @JoinTable(name = "disease_symptom",
            joinColumns = {@JoinColumn(name = "symptom_id")},
            inverseJoinColumns = {@JoinColumn(name = "disease_id")})
    @JsonIgnore
    private Set<DiseaseEntity> diseases = new HashSet<>();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getHead() {
        return head;
    }

    public void setHead(long head) {
        this.head = head;
    }

    public long getNeck() {
        return neck;
    }

    public void setNeck(long neck) {
        this.neck = neck;
    }

    public long getShoulder() {
        return shoulder;
    }

    public void setShoulder(long shoulder) {
        this.shoulder = shoulder;
    }

    public long getChest() {
        return chest;
    }

    public void setChest(long chest) {
        this.chest = chest;
    }

    public long getArm() {
        return arm;
    }

    public void setArm(long arm) {
        this.arm = arm;
    }

    public long getAbdomen() {
        return abdomen;
    }

    public void setAbdomen(long abdomen) {
        this.abdomen = abdomen;
    }

    public long getFlank() {
        return flank;
    }

    public void setFlank(long flank) {
        this.flank = flank;
    }

    public long getPelvis() {
        return pelvis;
    }

    public void setPelvis(long pelvis) {
        this.pelvis = pelvis;
    }

    public long getLeg() {
        return leg;
    }

    public void setLeg(long leg) {
        this.leg = leg;
    }

    public long getFoot() {
        return foot;
    }

    public void setFoot(long foot) {
        this.foot = foot;
    }

    public long getBack() {
        return back;
    }

    public void setBack(long back) {
        this.back = back;
    }

    public long getWaist() {
        return waist;
    }

    public void setWaist(long waist) {
        this.waist = waist;
    }

    public long getAss() {
        return ass;
    }

    public void setAss(long ass) {
        this.ass = ass;
    }

    public Set<DiseaseEntity> getDiseases() {
        return diseases;
    }

    public void setDiseases(Set<DiseaseEntity> diseases) {
        this.diseases = diseases;
    }

    public long getHand() {
        return hand;
    }

    public void setHand(long hand) {
        this.hand = hand;
    }

    public long getViewCount() {
        return viewCount;
    }

    public void setViewCount(long viewCount) {
        this.viewCount = viewCount;
    }
}
