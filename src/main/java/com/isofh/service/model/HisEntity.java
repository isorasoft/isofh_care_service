package com.isofh.service.model;

public class HisEntity {

    DepartmentEntity[] departments;
    SpecialistEntity[] specialists;
    DoctorEntity[] doctors;
    RoomEntity[] rooms;
    ServiceEntity[] services;
    PatientHistoryHisEntity[] patientHistoryIds;
    public DepartmentEntity[] getDepartments() {
        return departments;
    }
    public void setDepartments(DepartmentEntity[] departments) {
        this.departments = departments;
    }
    public SpecialistEntity[] getSpecialists() {
        return specialists;
    }
    public void setSpecialists(SpecialistEntity[] specialists) {
        this.specialists = specialists;
    }
    public DoctorEntity[] getDoctors() {
        return doctors;
    }
    public void setDoctors(DoctorEntity[] doctors) {
        this.doctors = doctors;
    }
    public RoomEntity[] getRooms() {
        return rooms;
    }
    public void setRooms(RoomEntity[] rooms) {
        this.rooms = rooms;
    }
    public ServiceEntity[] getServices() {
        return services;
    }
    public void setServices(ServiceEntity[] services) {
        this.services = services;
    }
    public PatientHistoryHisEntity[] getPatientHistoryIds() {
        return patientHistoryIds;
    }
    public void setPatientHistoryIds(PatientHistoryHisEntity[] patientHistoryIds) {
        this.patientHistoryIds = patientHistoryIds;
    }
}
