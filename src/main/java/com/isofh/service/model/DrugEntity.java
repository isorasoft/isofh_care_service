package com.isofh.service.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.isofh.service.constant.AppConst;
import com.isofh.service.constant.TableConst;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity(name = TableConst.DRUG)
public class DrugEntity {

    public DrugEntity() {

    }


    public DrugEntity(String name, String activeSubstances, long category, String component, long methodUse, CountryEntity country, String standard, String useness, String avoidUseness, String manufacturer, Long price, String images,
                      FacilityEntity facility) {
        this.name = name;
        this.activeSubstances = activeSubstances;
        this.category = category;
        this.component = component;
        this.methodUse = methodUse;
        this.country = country;
        this.standard = standard;
        this.useness = useness;
        this.avoidUseness = avoidUseness;
        this.manufacturer = manufacturer;
        this.price = price;
        this.images = images;
        this.facility = facility;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true)
    private long id;


    @JsonFormat(pattern = AppConst.DATE_TIME_FORMAT)
    private LocalDateTime createdDate;

    @JsonFormat(pattern = AppConst.DATE_TIME_FORMAT)
    private LocalDateTime updatedDate;

    @PrePersist
    public void prePersist() {
        createdDate = LocalDateTime.now();
        updatedDate = LocalDateTime.now();
    }

    @PreUpdate
    public void preUpdate() {
        updatedDate = LocalDateTime.now();
    }

    private String name;

    /**
     * Hoạt chất
     */
    private String activeSubstances;

    /**
     * Phân loại thuốc: int
     * +) 1 = Thuốc thường
     * +) 2 = Dịch truyền
     * +) 4 = Độc
     * +) 8 = Hóa chất
     * +) 16 = Hướng thần
     * +) 32 = máu
     * +) 64 = Nghiên cứu khoa học
     * +) 128 = Thực phẩm chức năng
     * +) 256 = Thuốc gây nghiện
     * +) 512 = Vật tư tiêu hao
     * +) 1024 = Hàm lượng
     */
    private long category;

    /**
     * Hàm lượng : String
     */
    @Column(columnDefinition = "TEXT")
    private String component;

    /**
     * Cách sử dụng dùng: lấy trên danh mục của HIS
     */
    private long methodUse;

    /**
     * Nước sản xuất: lấy trên danh mục của HIS
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "country_id")
    @NotFound(
            action = NotFoundAction.IGNORE)
    @JsonIgnore
    private CountryEntity country;

    /**
     * Quy cách: String
     */
    @Column(columnDefinition = "TEXT")
    private String standard;

    /**
     * Chỉ định: String
     */
    @Column(columnDefinition = "TEXT")
    private String useness;

    /**
     * Chống chỉ định: String
     */
    @Column(columnDefinition = "TEXT")
    private String avoidUseness;

    /**
     * Hãng sản xuất: String
     */
    private String manufacturer;

    /**
     * Giá tham khảo: String
     */
    private Long price;

    /**
     * So luong luot xem
     */
    private long viewCount;

    /**
     * danh sach cac chuyen khoa cua facility
     */
    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })
    @JoinTable(name = "drug_disease",
            joinColumns = {@JoinColumn(name = "disease_id")},
            inverseJoinColumns = {@JoinColumn(name = "drug_id")})
    @JsonIgnore
    private Set<DiseaseEntity> diseases = new HashSet<>();

    @Column(columnDefinition = "TEXT")
    private String images;

    /**
     * Id cua nha thuoc chua thuoc nay, neu facility_id = 0 thi thuoc nay la thuoc chung
     */

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "facility_id")
    @JsonIgnore
    @NotFound(
            action = NotFoundAction.IGNORE)
    private FacilityEntity facility;

    /**
     * Id cua thang thuoc ma minh copy ra
     */
    private long originId;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public long getCategory() {
        return category;
    }

    public void setCategory(long category) {
        this.category = category;
    }

    public long getMethodUse() {
        return methodUse;
    }

    public void setMethodUse(long methodUse) {
        this.methodUse = methodUse;
    }

    public CountryEntity getCountry() {
        return country;
    }

    public void setCountry(CountryEntity country) {
        this.country = country;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public String getActiveSubstances() {
        return activeSubstances;
    }

    public void setActiveSubstances(String activeSubstances) {
        this.activeSubstances = activeSubstances;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public String getStandard() {
        return standard;
    }

    public void setStandard(String standard) {
        this.standard = standard;
    }

    public String getUseness() {
        return useness;
    }

    public void setUseness(String useness) {
        this.useness = useness;
    }

    public String getAvoidUseness() {
        return avoidUseness;
    }

    public void setAvoidUseness(String avoidUseness) {
        this.avoidUseness = avoidUseness;
    }

    public Set<DiseaseEntity> getDiseases() {
        return diseases;
    }

    public void setDiseases(Set<DiseaseEntity> diseases) {
        this.diseases = diseases;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public LocalDateTime getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(LocalDateTime updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Long getViewCount() {
        return viewCount;
    }

    public void setViewCount(Long viewCount) {
        this.viewCount = viewCount;
    }

    public void setViewCount(long viewCount) {
        this.viewCount = viewCount;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public FacilityEntity getFacility() {
        return facility;
    }

    public void setFacility(FacilityEntity facility) {
        this.facility = facility;
    }

    public long getOriginId() {
        return originId;
    }

    public void setOriginId(long originId) {
        this.originId = originId;
    }
}
