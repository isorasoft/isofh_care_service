package com.isofh.service.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.isofh.service.constant.TableConst;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;

@Entity(name = TableConst.DEVICE)
public class DeviceEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true)
    private long id;

    /**
     * @see com.isofh.service.enums.DeviceType
     */
    private int os;

    /**
     * Id cua device
     */
    private String deviceId;

    /**
     * Token cua nguoi dung
     */
    private String token;

    @OneToOne()
    @JoinColumn(name = "user_id")
    @NotFound(
            action = NotFoundAction.IGNORE)
    @JsonIgnore
    private UserEntity user;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getOs() {
        return os;
    }

    public void setOs(int os) {
        this.os = os;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }
}
