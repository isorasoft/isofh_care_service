package com.isofh.service.model;

import javax.persistence.PreUpdate;
import java.time.LocalDateTime;

public class DrugTestEntity {

    private long id;


    private LocalDateTime createdDate;

    private LocalDateTime updatedDate;

    public void prePersist() {
        createdDate = LocalDateTime.now();
        updatedDate = LocalDateTime.now();
    }

    @PreUpdate
    public void preUpdate() {
        updatedDate = LocalDateTime.now();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public LocalDateTime getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(LocalDateTime updatedDate) {
        this.updatedDate = updatedDate;
    }

    private String ten;
    private String anh;
    private String chidinh;
    private String chongchidinh;
    private String tacdungngoaiy;
    private String sodangky;
    private String congty;
    private String cachdonggoi;
    private String lieuluong;
    private String chuydephong;

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getAnh() {
        return anh;
    }

    public void setAnh(String anh) {
        this.anh = anh;
    }

    public String getChidinh() {
        return chidinh;
    }

    public void setChidinh(String chidinh) {
        this.chidinh = chidinh;
    }

    public String getChongchidinh() {
        return chongchidinh;
    }

    public void setChongchidinh(String chongchidinh) {
        this.chongchidinh = chongchidinh;
    }

    public String getTacdungngoaiy() {
        return tacdungngoaiy;
    }

    public void setTacdungngoaiy(String tacdungngoaiy) {
        this.tacdungngoaiy = tacdungngoaiy;
    }

    public String getSodangky() {
        return sodangky;
    }

    public void setSodangky(String sodangky) {
        this.sodangky = sodangky;
    }

    public String getCongty() {
        return congty;
    }

    public void setCongty(String congty) {
        this.congty = congty;
    }

    public String getCachdonggoi() {
        return cachdonggoi;
    }

    public void setCachdonggoi(String cachdonggoi) {
        this.cachdonggoi = cachdonggoi;
    }

    public String getLieuluong() {
        return lieuluong;
    }

    public void setLieuluong(String lieuluong) {
        this.lieuluong = lieuluong;
    }

    public String getChuydephong() {
        return chuydephong;
    }

    public void setChuydephong(String chuydephong) {
        this.chuydephong = chuydephong;
    }
}
