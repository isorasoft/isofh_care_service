package com.isofh.service.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.gson.annotations.Expose;
import com.isofh.service.constant.AppConst;
import com.isofh.service.constant.TableConst;
import com.isofh.service.enums.GenderType;
import com.isofh.service.utils.StrUtils;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity(name = TableConst.PROFILE)
public class ProfileEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true)
    private long id;


    @JsonFormat(pattern = AppConst.DATE_TIME_FORMAT)
    public LocalDateTime createdDate;

    @JsonFormat(pattern = AppConst.DATE_TIME_FORMAT)
    public LocalDateTime updatedDate;

    @PrePersist
    public void prePersist() {
        createdDate = LocalDateTime.now();
        updatedDate = LocalDateTime.now();
        uid = UUID.randomUUID().toString();
    }

    /**
     * User co profile nay
     */
    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    @JsonIgnore
    @NotFound(
            action = NotFoundAction.IGNORE)
    private UserEntity user;

    /**
     * User co Country
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "country_id")
    @JsonIgnore
    @NotFound(
            action = NotFoundAction.IGNORE)
    private CountryEntity country;

    /**
     * User co Country
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "province_id")
    @JsonIgnore
    @NotFound(
            action = NotFoundAction.IGNORE)
    private ProvinceEntity province;

    /**
     * User co Country
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "district_id")
    @JsonIgnore
    @NotFound(
            action = NotFoundAction.IGNORE)
    private DistrictEntity district;

    /**
     * User co Country
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "zone_id")
    @JsonIgnore
    @NotFound(
            action = NotFoundAction.IGNORE)
    private ZoneEntity zone;

    /**
     * Ten benh nhan
     */
    private String name;

    /**
     * Gioi tinh
     *
     * @see com.isofh.service.enums.GenderType
     */
    private int gender = GenderType.FEMALE.getType();

    /**
     * Ngay sinh
     */
    @JsonFormat(pattern = AppConst.DATE_TIME_FORMAT)
    private LocalDateTime dob;

    /**
     * ho chieu hoac chung minh thu, cccd
     */
    private String passport;

    /**
     * so dien thoai
     */
    private String phone;

    /**
     * Dia chi
     */
    private String address;

    /**
     * Ten nguoi bao ho
     */
    private String guardianName;

    /**
     * so dien thoai nguoi bao ho
     */
    private String guardianPhone;

    /**
     * So ho chieu ng bao ho
     */
    private String guardianPassport;

    // Neu profile duoc tao tu dong thi da active isActive = 0
    @Expose
    private int active = 0;

    /**
     * Danh dau profile nay duoc tao tu dong sau khi tao tai khoan, goi tu his
     * 1 - Tao tu dong, 0- Tao binh thuong
     */
    @Expose
    private int autoCreated = 0;

    /**
     * Danh dau la ho so tu hop dong lao dong
     */
    private boolean isContract = false;


    /**
     * Trang thai xoa cua profile
     */
    private int deleted = 0;
    
    /**
     * ma so thue
     */
    @Column(name = "col_tax_code")
    private String taxCode;

    /**
     * Danh sach patient histories Id cach nhau boi dau ,
     */
    @Column(columnDefinition = "TEXT")
    private String patientHistoryIds;
    
    private String walletId;
    
    private String uid;

/**
     * His su dung truong nay, khong dung hisPatientId nua
     */
    @Expose
    private String hisPatientId;

    @PreUpdate
    public void preUpdate() {
        updatedDate = LocalDateTime.now();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public LocalDateTime getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(LocalDateTime updatedDate) {
        this.updatedDate = updatedDate;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public LocalDateTime getDob() {
        return dob;
    }

    public void setDob(LocalDateTime dob) {
        this.dob = dob;
    }

    public String getPassport() {
        return passport;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGuardianName() {
        return guardianName;
    }

    public void setGuardianName(String guardianName) {
        this.guardianName = guardianName;
    }

    public String getGuardianPhone() {
        return guardianPhone;
    }

    public void setGuardianPhone(String guardianPhone) {
        this.guardianPhone = guardianPhone;
    }

    public String getGuardianPassport() {
        return guardianPassport;
    }

    public void setGuardianPassport(String guardianPassport) {
        this.guardianPassport = guardianPassport;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public int getAutoCreated() {
        return autoCreated;
    }

    public void setAutoCreated(int autoCreated) {
        this.autoCreated = autoCreated;
    }

    public CountryEntity getCountry() {
        return country;
    }

    public void setCountry(CountryEntity country) {
        this.country = country;
    }

    public ProvinceEntity getProvince() {
        return province;
    }

    public void setProvince(ProvinceEntity province) {
        this.province = province;
    }

    public DistrictEntity getDistrict() {
        return district;
    }

    public void setDistrict(DistrictEntity district) {
        this.district = district;
    }

    public ZoneEntity getZone() {
        return zone;
    }

    public void setZone(ZoneEntity zone) {
        this.zone = zone;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public void updateAddress() {
        String add = "";
        if (zone != null) {
            if (!StrUtils.isNullOrWhiteSpace(add)) {
                add += ", ";
            }
            add += zone.getName();
        }

        if (district != null) {
            if (!StrUtils.isNullOrWhiteSpace(add)) {
                add += ", ";
            }
            add += district.getName();
        }

        if (province != null) {
            if (!StrUtils.isNullOrWhiteSpace(add)) {
                add += ", ";
            }
            add += province.getName();
        }

        if (country != null) {
            if (!StrUtils.isNullOrWhiteSpace(add)) {
                add += ", ";
            }
            add += country.getName();
        }

        address = add;
    }

    public boolean isContract() {
        return isContract;
    }

    public void setContract(boolean contract) {
        isContract = contract;
    }

    public String getPatientHistoryIds() {
        return patientHistoryIds;
    }

    public void setPatientHistoryIds(String patientHistoryIds) {
        this.patientHistoryIds = patientHistoryIds;
    }

    public String getTaxCode() {
        return taxCode;
    }

    public void setTaxCode(String taxCode) {
        this.taxCode = taxCode;
    }

    public String getWalletId() {
        return walletId;
    }

    public void setWalletId(String walletId) {
        this.walletId = walletId;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getHisPatientId() {
        return hisPatientId;
    }

    public void setHisPatientId(String hisPatientId) {
        this.hisPatientId = hisPatientId;
    }
}
