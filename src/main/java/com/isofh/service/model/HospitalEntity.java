package com.isofh.service.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.isofh.service.constant.AppConst;
import com.isofh.service.constant.TableConst;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity(name = TableConst.HOSPITAL)
public class HospitalEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true)
    private long id;


    @JsonFormat(pattern = AppConst.DATE_TIME_FORMAT)
    public LocalDateTime createdDate;

    @JsonFormat(pattern = AppConst.DATE_TIME_FORMAT)
    public LocalDateTime updatedDate;

    @PrePersist
    public void prePersist() {
        createdDate = LocalDateTime.now();
        updatedDate = LocalDateTime.now();
    }

    @OneToOne()
    @JoinColumn(name = "col_create_person_id")
    @NotFound(
            action = NotFoundAction.IGNORE)
    @JsonIgnore
    private UserEntity createdPerson;

    @OneToOne()
    @JoinColumn(name = "col_update_person_id")
    @NotFound(
            action = NotFoundAction.IGNORE)
    @JsonIgnore
    private UserEntity updatedPerson;

    @OneToOne()
    @JoinColumn(name = "col_admin_id")
    @NotFound(
            action = NotFoundAction.IGNORE)
    @JsonIgnore
    private UserEntity userAdmin;

    /**
     * Ten cua benh vien
     */
    @Column(name = "col_name")
    private String name;

    /**
     * ma so thue
     */
    @Column(name = "col_tax_code")
    private String taxCode;

    /**
     * trang thai active cua tk admin cua csyt
     * 1: da active
     * 0: chua active
     */
    @Column(name = "col_active")
    private Integer active=1;

    @Column(name = "col_service_url")
    private String serviceUrl;

    /**
     * dia chi csyt
     */
    @Column(name = "col_address")
    private String address;


    /**
     * ma tai khoan vi
     */
    @Column(name = "col_wallet_code")
    private String walletCode;

    /**
     * trang thai active vi cua csyt
     * 1: da active
     * 0: chua active
     */
    @Column(name = "col_active_wallet")
    private Integer activeWallet=0;

    /**
     * bac si cua hospital
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "col_doctor_id")
    @JsonIgnore
    @NotFound(
            action = NotFoundAction.IGNORE)
    private UserEntity doctor;
    
    /**
     * uid cua hospital
     */
    private String uid = UUID.randomUUID().toString();
    
    /**
     * time load ybdt theo phut
     */
    private Integer timeInit;
    
    private String logo;
    
    @Column(columnDefinition = "LONGTEXT")
    private String images;

    @PreUpdate
    public void preUpdate() {
        updatedDate = LocalDateTime.now();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public UserEntity getDoctor() {
        return doctor;
    }

    public void setDoctor(UserEntity doctor) {
        this.doctor = doctor;
    }

    public UserEntity getCreatedPerson() {
        return createdPerson;
    }

    public void setCreatedPerson(UserEntity createdPerson) {
        this.createdPerson = createdPerson;
    }

    public UserEntity getUpdatedPerson() {
        return updatedPerson;
    }

    public void setUpdatedPerson(UserEntity updatedPerson) {
        this.updatedPerson = updatedPerson;
    }


    public Integer getActiveWallet() {
        return activeWallet;
    }

    public void setActiveWallet(Integer activeWallet) {
        this.activeWallet = activeWallet;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public LocalDateTime getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(LocalDateTime updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getServiceUrl() {
        return serviceUrl;
    }

    public void setServiceUrl(String serviceUrl) {
        this.serviceUrl = serviceUrl;
    }

    public void setWalletCode(String walletCode) {
        this.walletCode = walletCode;
    }

    public UserEntity getUserAdmin() {
        return userAdmin;
    }

    public void setUserAdmin(UserEntity userAdmin) {
        this.userAdmin = userAdmin;
    }

    public String getTaxCode() {
        return taxCode;
    }

    public void setTaxCode(String taxCode) {
        this.taxCode = taxCode;
    }

    public Integer getActive() {
        return active;
    }

    public void setActive(Integer active) {
        this.active = active;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getWalletCode() {
        return walletCode;
    }

    public Integer getTimeInit() {
        return timeInit;
    }

    public void setTimeInit(Integer timeInit) {
        this.timeInit = timeInit;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }
    
    
}
