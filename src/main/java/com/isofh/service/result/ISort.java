package com.isofh.service.result;

import org.springframework.data.domain.Sort;

public interface ISort {
    /**
     * sort by id giam dan
     *
     * @return
     */
    default Sort getSortDESC() {
        Sort.Order order = new Sort.Order(Sort.Direction.DESC, "created_date");
        return Sort.by(order);
    }


    /**
     * Sort by Id tang dan
     *
     * @return
     */
    default Sort getSortASC() {
        Sort.Order order = new Sort.Order(Sort.Direction.ASC, "created_date");
        return Sort.by(order);
    }
}
