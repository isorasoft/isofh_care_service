package com.isofh.service.result;

import com.isofh.service.model.ResultEntity;
import com.isofh.service.utils.CustomException;
import com.isofh.service.utils.LogUtils;

public interface IErrorResult {
    /**
     * Create response when exception
     */
    default ResultEntity error(Exception ex) {
        LogUtils.getInstance().error(ex);
        ResultEntity resultEntity = new ResultEntity();
        resultEntity.setMessage(ex.getMessage());
        if (ex instanceof CustomException) {
            CustomException customException = (CustomException) ex;
            resultEntity.setCode(customException.getCode());
            resultEntity.setData(customException.getData());
        } else {
            resultEntity.setCode(500);
            resultEntity.setMessage("Internal Server Error " + ex.getMessage());
        }
        return resultEntity;
    }
}
