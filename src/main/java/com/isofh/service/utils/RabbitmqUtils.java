/*
 * *******************************************************************
 * Copyright (c) 2018 Isofh.com to present.
 * All rights reserved.
 *
 * Author: tuanld
 * ******************************************************************
 *
 */

package com.isofh.service.utils;

import com.isofh.service.config.RabbitmqConst;
import com.isofh.service.constant.type.QueueType;
import com.isofh.service.model.QueueEntity;
import com.isofh.service.result.IGetResult;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeoutException;

public class RabbitmqUtils {

    /**
     * @param queue
     * @param content
     * @return True if success
     * @throws IOException
     * @throws TimeoutException
     */
    private static void sendMessage(String queue, String content, IGetResult... results) {
        System.out.println("start");
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(RabbitmqConst.HOST);
        factory.setUsername(RabbitmqConst.USER_NAME);
        factory.setPassword(RabbitmqConst.PASSWORD);
        factory.setPort(RabbitmqConst.PORT);

        IGetResult result = null;
        if (results != null && results.length > 0) {
            result = results[0];
        }

        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {
            channel.queueDeclare(queue, true, false, false, null);

            String message = content;
            AMQP.BasicProperties pros = new AMQP.BasicProperties.Builder()
                    .contentType("text/plain")
                    .deliveryMode(2)
                    .priority(1)
                    .build();

            channel.basicPublish("", queue, pros, message.getBytes());
            System.out.println(" [x] Sent '" + message + "'");
            if (result != null)
                result.setResult(true);
        } catch (Exception ex) {
            ex.printStackTrace();
            if (result != null)
                result.setResult(false);
        }
        System.out.println("end");
    }


    private static void send(String queueName, QueueEntity queue) {
        RabbitmqUtils.sendMessage(queueName, GsonUtils.toString(queue), result -> {
            System.out.println(result);
        });
    }

    public static void sendMessages(String queueName, List<QueueEntity> queues) {
        ExecutorService pool = Executors.newFixedThreadPool(10);
        for (int i = 0; i < queues.size(); i++) {
            System.out.println("send " + i);
            QueueEntity queue = queues.get(i);
            Runnable r = () -> send(queueName, queue);
            pool.execute(r);
        }
    }

    public static QueueEntity getCreateQueue(Object entity) {
        return new QueueEntity(entity, QueueType.CREATE.getValue());
    }

    public static QueueEntity getUpdateQueue(Object entity) {
        return new QueueEntity(entity, QueueType.UPDATE.getValue());
    }

    public static QueueEntity getDeleteQueue(Object entity) {
        return new QueueEntity(entity, QueueType.DELETE.getValue());
    }
}
