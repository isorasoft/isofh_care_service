/*
 * *******************************************************************
 * Copyright (c) 2018 Isofh.com to present.
 * All rights reserved.
 *
 * Author: tuanld
 * ******************************************************************
 */

package com.isofh.service.utils;

import com.isofh.service.constant.NotiConst;
import com.isofh.service.constant.ServiceConst;
import com.isofh.service.enums.DeviceType;
import com.isofh.service.enums.HttpMethodType;
import com.isofh.service.model.DeviceEntity;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class NotiUtils {

    /**
     * Gui thong bao
     *
     * @param devices danh sach cac devices ma nguoi dung nay da dang nhap
     * @param id      id cua thong bao
     */
//    public static void sendNotification(Collection<DeviceEntity> devices, Long id, String type, String title) {
//        sendNotification(devices, id, 0);
//    }

    /**
     * Gui thong bao
     *
     * @param type > 0 la tin nhan quang ba, null hoac = 0 la notification
     */


    public static void sendNotification(Collection<DeviceEntity> devices, Long id, String type, String title) {
        for (DeviceEntity deviceEntity : devices) {
            if (deviceEntity.getOs() == (DeviceType.WEB.getValue()) || StrUtils.isNullOrWhiteSpace(deviceEntity.getToken())) {
                continue;
            }

            Thread thread = new Thread(() -> {
                try {
                    Map<String, Object> mapData = new HashMap<>();
                    mapData.put("id", id);
                    mapData.put("type", type);

                    Map<String, Object> mapNoti = new HashMap<>();
                    mapNoti.put("body", NotiConst.DEFAULT_BODY_NOTIFICATION);
                    mapNoti.put("title", title);
                    mapNoti.put("sound", NotiConst.DEFAULT_SOUND);

                    Map<String, Object> mapBodys = new HashMap<>();
                    mapBodys.put("to", deviceEntity.getToken());
                    mapBodys.put("data", mapData);
                    mapBodys.put("notification", mapNoti);

                    Map<String, Object> mapHeaders = new HashMap<>();
                    mapHeaders.put("Authorization", "key=" + NotiConst.FIREBASE_KEY);

                    String result = NetworkUtils.callService(ServiceConst.FIREBASE_URL, HttpMethodType.POST, mapHeaders, mapBodys);
                    LogUtils.getInstance().info("send noti result " + result);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            });
            thread.start();
        }
    }


}

