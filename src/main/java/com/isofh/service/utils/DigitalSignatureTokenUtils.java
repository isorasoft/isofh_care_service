package com.isofh.service.utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class DigitalSignatureTokenUtils {

    @Value("${file.upload-dir}")
    private String filePath;
    
	private String publicKeyString;
	private String privateKeyString;
	private String publicKeyWalletString;

	private PrivateKey privateKey;
	private PublicKey publicKey;
	private PublicKey publicKeyWallet;

//	public static void main(String[] args) throws InvalidKeyException, IOException, Exception {
//
// 		initKey();
//		String token = generateToken(privateKey, "2009");
//		System.out.println("Generated Token:\n" + token);
//
//		verifyToken(token, publicKey);
//
//	}

	@SuppressWarnings("resource")
	private void initKey() throws Exception {
		if (privateKey == null || publicKey == null || publicKeyWallet == null) {
			try {
			    FileReader publicKeyFile = new FileReader(filePath + "/publicKey");
                FileReader privateKeyFile = new FileReader(filePath + "/privateKey");
                FileReader publicKeyWalletFile = new FileReader(filePath + "/publicKeyWallet");

				BufferedReader bufferpublicKeyFile = new BufferedReader(publicKeyFile);
				BufferedReader bufferprivateKeyFile = new BufferedReader(privateKeyFile);
				BufferedReader bufferpublicKeyWaleltFile = new BufferedReader(publicKeyWalletFile);

				publicKeyString = bufferpublicKeyFile.readLine();
				privateKeyString = bufferprivateKeyFile.readLine();
				publicKeyWalletString = bufferpublicKeyWaleltFile.readLine();

				byte[] publicKeyByte = java.util.Base64.getDecoder().decode(publicKeyString.getBytes());
				byte[] privateKeyByte = java.util.Base64.getDecoder().decode(privateKeyString.getBytes());
				byte[] publicKeyWalletByte = java.util.Base64.getDecoder().decode(publicKeyWalletString.getBytes());

				X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(publicKeyByte);
				X509EncodedKeySpec publicKeyWalletSpec = new X509EncodedKeySpec(publicKeyWalletByte);
				PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(privateKeyByte);
				KeyFactory kf = KeyFactory.getInstance("RSA");
				publicKey = kf.generatePublic(publicKeySpec);
				privateKey = kf.generatePrivate(privateKeySpec);
				publicKeyWallet = kf.generatePublic(publicKeyWalletSpec);
			} catch (Exception e) {
			    LogUtils.getInstance().info(e);
				System.out.println("ERROR:" + e.getMessage());
			}

		}
	}

	public String generateToken(Long id, Integer type, Long vendorId) throws Exception {
		initKey();
		String token = null;
		try {
			Map<String, Object> claims = new HashMap<String, Object>();

			// put your information into claim
			claims.put("userId", id);
			claims.put("iss", "isofhCare");
			claims.put("type", type);
			claims.put("vendorId", vendorId);
			claims.put("created", new Date());
			claims.put("viewWallet", 1);
			claims.put("viewTransaction", 1);

			token = Jwts.builder().setClaims(claims).signWith(SignatureAlgorithm.RS256, privateKey).compact();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return token;
	}
	
	public String generateToken(String key, String value) throws Exception {
        initKey();
        String token = null;
        try {
            Map<String, Object> claims = new HashMap<String, Object>();

            // put your information into claim
            claims.put(key, value);

            token = Jwts.builder().setClaims(claims).signWith(SignatureAlgorithm.RS256, privateKey).compact();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return token;
    }

	private String verifyToken(String token, String key) {
		Claims claims;
		try {
		    initKey();
			claims = Jwts.parser().setSigningKey(publicKey).parseClaimsJws(token).getBody();

			return String.valueOf(claims.get(key));
		} catch (Exception e) {

			claims = null;
			return null;
		}
		
	}
	
	public Long getUserIdFromToken(String token) {
        try {
            return Long.valueOf(verifyToken(token, "userId"));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
	
	public Integer getTypeFromToken(String token) {
        try {
            return Integer.valueOf(verifyToken(token, "type"));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
	
	public String verifyTokenWallet(String token, String key) {
        Claims claims;
        try {
            initKey();
            claims = Jwts.parser().setSigningKey(publicKeyWallet).parseClaimsJws(token).getBody();

            return String.valueOf(claims.get(key));
        } catch (Exception e) {

            claims = null;
            return null;
        }
        
    }
}
