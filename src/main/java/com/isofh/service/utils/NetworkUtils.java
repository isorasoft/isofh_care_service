/*
 * *******************************************************************
 * Copyright (c) 2018 Isofh.com to present.
 * All rights reserved.
 *
 * Author: tuanld
 * ******************************************************************
 *
 */

package com.isofh.service.utils;

import com.isofh.service.constant.AppConst;
import com.isofh.service.enums.HttpMethodType;
import com.isofh.service.model.ResultEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

public class NetworkUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(NetworkUtils.class);

    public static String callService(String url, HttpMethodType methodType, Map<String, ?> headers, Object body,
            Map<String, ?> queryParams, Map<String, ?> urlVariables) {
        HttpHeaders httpHeaders = new HttpHeaders();
        if (!MapUtils.isNullOrEmpty(headers)) {
            headers.forEach((s, object) -> httpHeaders.set(s, object.toString()));
        }
        if (!httpHeaders.containsKey(HttpHeaders.CONTENT_TYPE)) {
            httpHeaders.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE);
        }
        HttpMethod method = HttpMethod.GET;
        switch (methodType) {
        case GET:
            method = HttpMethod.GET;
            break;
        case POST:
            method = HttpMethod.POST;
            break;
        case DELETE:
            method = HttpMethod.DELETE;
            break;
        }

        if (urlVariables == null) {
            urlVariables = new HashMap<>();
        }

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url);
        if (queryParams != null) {
            for (Map.Entry<String, ?> entry : queryParams.entrySet()) {
                Object obj = entry.getValue();
                String value;
                if (obj instanceof LocalDate) {
                    LocalDate dateValue = (LocalDate) obj;
                    value = dateValue.format(AppConst.DATE_FORMATTER);
                } else if (obj instanceof LocalDateTime) {
                    LocalDateTime dateValue = (LocalDateTime) obj;
                    value = dateValue.format(AppConst.DATE_TIME_FORMATTER);
                } else {
                    value = obj.toString();
                }
                builder.queryParam(entry.getKey(), value);
            }
        }
        HttpEntity<Object> entity = new HttpEntity<>(GsonUtils.toString(body), httpHeaders);
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> response = restTemplate.exchange(builder.build(urlVariables), method, entity,
                String.class);
        return response.getBody();
    }

    public static String callService(String url, HttpMethodType methodType, Map<String, Object> headers, Object body,
            Map<String, Object> queryParams) {
        return callService(url, methodType, headers, body, queryParams, null);
    }

    public static String callService(String url, HttpMethodType methodType, Map<String, Object> headers, Object body) {
        return callService(url, methodType, headers, body, null);
    }

    public static String callService(String url, HttpMethodType methodType, Map<String, Object> headers) {
        return callService(url, methodType, headers, null);
    }

    public static String callService(String url, HttpMethodType methodType) {
        return callService(url, methodType, null);
    }

    public static ResultEntity callServiceBvy(String url, HttpMethodType methodType, Map<String, ?> headers,
            Object body, Map<String, ?> queryParams, Map<String, ?> uriVariables) {
        String result = callService(url, methodType, headers, body, queryParams, uriVariables);
        return GsonUtils.toObject(result, ResultEntity.class);
    }

    public static ResultEntity callServiceBvy(String url, HttpMethodType methodType, Map<String, Object> headers,
            Object body, Map<String, Object> queryParams) {
        return callServiceBvy(url, methodType, headers, body, queryParams, null);
    }
}
