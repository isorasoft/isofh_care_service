/*
 * *******************************************************************
 * Copyright (c) 2018 Isofh.com to present.
 * All rights reserved.
 *
 * Author: tuanld
 * ******************************************************************
 *
 */

package com.isofh.service.utils;

public class ThreadUtils {

    /**
     * Run a runnable Object in other thread
     *
     * @param runnable
     */
    public static void runInThread(Runnable runnable) {
        try {
            Thread thread = new Thread(runnable::run);
            thread.start();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }
}
