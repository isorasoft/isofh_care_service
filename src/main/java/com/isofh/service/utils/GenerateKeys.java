package com.isofh.service.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;

import org.apache.commons.codec.binary.Base64;

public class GenerateKeys {

	private KeyPairGenerator keyGen;
	private KeyPair pair;
	private PrivateKey privateKey;
	private PublicKey publicKey;

	public GenerateKeys(int keylength) throws NoSuchAlgorithmException, NoSuchProviderException {

		this.keyGen = KeyPairGenerator.getInstance("RSA");
		this.keyGen.initialize(keylength);
	}

	public void createKeys() {
		this.pair = this.keyGen.generateKeyPair();
		this.privateKey = pair.getPrivate();
		this.publicKey = pair.getPublic();
	}

	public PrivateKey getPrivateKey() {
		return this.privateKey;
	}

	public PublicKey getPublicKey() {
		return this.publicKey;
	}

	public void writeToFile(String path, byte[] key) throws IOException {

		File f = new File(path);
		f.getParentFile().mkdirs();

		FileOutputStream fos = new FileOutputStream(f);
		fos.write(key);
		fos.flush();
		fos.close();

	}

	public void writeToFile(String path, String key) throws IOException {

		File f = new File(path);
		f.getParentFile().mkdirs();

		PrintWriter writer = new PrintWriter(f);
		writer.println(key);
		writer.close();

	}

	public static void main(String[] args)
			throws NoSuchAlgorithmException, NoSuchProviderException, IOException, InvalidKeySpecException {
		GenerateKeys myKeys = new GenerateKeys(2048);
//        
		myKeys.createKeys();
		myKeys.writeToFile("MyKeys/publicKey", Base64.encodeBase64String(myKeys.getPublicKey().getEncoded()));
		myKeys.writeToFile("MyKeys/privateKey", Base64.encodeBase64String(myKeys.getPrivateKey().getEncoded()));
	}
}
