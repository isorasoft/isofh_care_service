/*
 * *******************************************************************
 * Copyright (c) 2018 Isofh.com to present.
 * All rights reserved.
 *
 * Author: tuanld
 * ******************************************************************
 *
 */

package com.isofh.service.utils;

import com.isofh.service.constant.AppConst;
import com.isofh.service.constant.EmailConst;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

public class EmailUtils {


    static Session getMailSession;
    static MimeMessage mimeMessage;

    public static synchronized void sendEmail(String email, String subject, String content) {
        try {
            LogUtils.getInstance().info("start send EMAIL " + email);

            String mailServerPORT = EmailConst.MAIL_SERVER_PORT;
            String mailServer = EmailConst.MAIL_SERVER;
            String mailServerPassword = EmailConst.MAIL_SERVER_PASSWORD;
            String mailServerSTMP = EmailConst.MAIL_SERVER_STMP;
            String personalInfo = EmailConst.PERSONAL;

            Properties mailServerProperties = System.getProperties();

            mailServerProperties.put("mail.smtp.port", mailServerPORT);
            mailServerProperties.put("mail.smtp.auth", "true");
            mailServerProperties.put("mail.smtp.starttls.enable", "true");

            getMailSession = Session.getDefaultInstance(mailServerProperties, null);
            mimeMessage = new MimeMessage(getMailSession);
            mimeMessage.setFrom(new InternetAddress(mailServer));
            mimeMessage.setHeader("Content-Type", "text/plain; charset=UTF-8");
            mimeMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(email, personalInfo));

            mimeMessage.setSubject(subject, "utf-8");
            mimeMessage.setText(content, "utf-8", "html");
            // Step3
            Transport transport = getMailSession.getTransport("smtp");


            transport.connect(mailServerSTMP, mailServer, mailServerPassword);
            transport.sendMessage(mimeMessage, mimeMessage.getAllRecipients());
            transport.close();
            LogUtils.getInstance().info("end send EMAIL");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Create content of EMAIL forgot password
     *
     * @param name
     * @param link
     * @return
     */
    public static String getBodyResetPassword(String name, String link, String code) {
        String appName = AppConst.APP_NAME;
        String contactEmail = AppConst.CONTACT_EMAIL;
        String linkAndroid = AppConst.LINK_ANDROID;
        String linkIOS = AppConst.LINK_IOS;
        String emailContent = FileUtils.getFileContent("email_reset_password.html");

        String keyUserName = "$USER_NAME$";
        String keyAppName = "$APP_NAME$";
        String keyHref = "$HREF$";
        String keyContactEmail = "$CONTACT_EMAIL$";
        String keyLinkAndroid = "$LINK_ANDROID$";
        String keyLinkIOS = "$LINK_IOS$";
        String keyCodePhone = "$CODE_PHONE$";
        emailContent = emailContent.replace(keyUserName, name);
        emailContent = emailContent.replace(keyAppName, appName);
        emailContent = emailContent.replace(keyHref, link);
        emailContent = emailContent.replace(keyContactEmail, contactEmail);
        emailContent = emailContent.replace(keyLinkAndroid, linkAndroid);
        emailContent = emailContent.replace(keyLinkIOS, linkIOS);
        emailContent = emailContent.replace(keyCodePhone, code);
        return emailContent;
    }

    /**
     * Email body when user want to change EMAIL
     *
     * @param name
     * @param link
     * @return
     */
    public static String getBodyVerifyEmail(String name, String username, String link) {
        String appName = AppConst.APP_NAME;
        String contactEmail = AppConst.CONTACT_EMAIL;
        String linkAndroid = AppConst.LINK_ANDROID;
        String linkIOS = AppConst.LINK_IOS;
        String emailContent = FileUtils.getFileContent("email_verify_email.html");

        String keyName = "$NAME$";
        String keyUserName = "$USER_NAME$";
        String keyAppName = "$APP_NAME$";
        String keyHref = "$HREF$";
        String keyContactEmail = "$CONTACT_EMAIL$";
        String keyLinkAndroid = "$LINK_ANDROID$";
        String keyLinkIOS = "$LINK_IOS$";
        emailContent = emailContent.replace(keyName, name);
        emailContent = emailContent.replace(keyUserName, username);
        emailContent = emailContent.replace(keyAppName, appName);
        emailContent = emailContent.replace(keyHref, link);
        emailContent = emailContent.replace(keyContactEmail, contactEmail);
        emailContent = emailContent.replace(keyLinkAndroid, linkAndroid);
        emailContent = emailContent.replace(keyLinkIOS, linkIOS);
        return emailContent;
    }

    public static String getBodySendAccountInfo(String name, String link, String email, String password) {
        String appName = AppConst.APP_NAME;
        String contactEmail = AppConst.CONTACT_EMAIL;
        String linkAndroid = AppConst.LINK_ANDROID;
        String linkIOS = AppConst.LINK_IOS;
        String emailContent = FileUtils.getFileContent("email_send_account_info.html");

        String keyUserName = "$USER_NAME$";
        String keyAppName = "$APP_NAME$";
        String keyHref = "$HREF$";
        String keyContactEmail = "$CONTACT_EMAIL$";
        String keyPassword = "$PASSWORD$";
        String keyEmail = "$EMAIL$";
        String keyLinkAndroid = "$LINK_ANDROID$";
        String keyLinkIOS = "$LINK_IOS$";
        emailContent = emailContent.replace(keyUserName, name);
        emailContent = emailContent.replace(keyAppName, appName);
        emailContent = emailContent.replace(keyHref, link);
        emailContent = emailContent.replace(keyContactEmail, contactEmail);
        emailContent = emailContent.replace(keyLinkAndroid, linkAndroid);
        emailContent = emailContent.replace(keyLinkIOS, linkIOS);
        emailContent = emailContent.replace(keyEmail, email);
        emailContent = emailContent.replace(keyPassword, password);
        return emailContent;
    }

}
