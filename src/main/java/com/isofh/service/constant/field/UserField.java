package com.isofh.service.constant.field;

public class UserField {
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final String EMAIL = "email";
}
