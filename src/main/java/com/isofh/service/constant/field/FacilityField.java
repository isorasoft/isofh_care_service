package com.isofh.service.constant.field;

public class FacilityField {

    private FacilityField() {
    }

    public static final String APPROVAL = "approval";
    public static final String REVIEW = "review";
}
