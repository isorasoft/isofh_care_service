package com.isofh.service.constant.field;

public class CommonField {

    private CommonField() {
    }

    public static final String PAGE = "page";
    public static final String SIZE = "size";
}
