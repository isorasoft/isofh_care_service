/*
 * *******************************************************************
 * Copyright (c) 2018 Isofh.com to present.
 * All rights reserved.
 *
 * Author: tuanld
 * ******************************************************************
 *
 */

package com.isofh.service.constant;

import java.time.format.DateTimeFormatter;

public class AppConst {

    private AppConst() {
    }

    /**
     * 1 hour to milisecond
     */
    public static final Integer ONE_HOUR = 60 * 60 * 1000;

    /**
     * 1 day to milisecond
     */
    public static final Integer ONE_DAY = 24 * ONE_HOUR;

    public static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_FORMAT = "yyyy-MM-dd";

    public static final String WEB_ADDRESS = "http://test.isofhcare.com:8482";

    public static final String APP_NAME = "ISOFH CARE";

    public static final String CONTACT_EMAIL = "isofhcare@gmail.com";

    /**
     * Link ung dung tren google store
     */
    public static final String LINK_ANDROID = "https://play.google.com/store";

    /**
     * Link ung dugn tren apple store
     */
    public static final String LINK_IOS = "https://www.apple.com/lae/ios/app-store";

    public static final String DEFAULT_AVATAR = "images/default.png";

    public static String FILE_DIR = "/opt/tomcat8382_isofh_care_service_test/files";

    public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern(AppConst.DATE_TIME_FORMAT);
    public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern(AppConst.DATE_FORMAT);
    public static final String AUTHORIZATION = "Authorization";
    public static final long BVY_ID = 1;
    
    public static final String STATUS = "status";
    public static final String DATA = "data";
    public static final String BOOKING_ID = "booking_ref_id";
    public static final String SUCCESS = "SUCCESS";
    public static final String ODER_ID = "order_ref_id";
    public static final String REJECT = "reject";
}