/*
 * *******************************************************************
 * Copyright (c) 2018 Isofh.com to present.
 * All rights reserved.
 *
 * Author: tuanld
 * ******************************************************************
 *
 */

package com.isofh.service.constant;

public class ServiceConst {

    public static final int CODE_OK = 0;
    public static final String FIREBASE_URL = "https://fcm.googleapis.com/fcm/send";

    private ServiceConst() {
    }

    /**
     * fake ip and port bvy
     */
    public static final String API = "http://10.0.0.86:8001/isofh/services/isofhcare";
    
    /**
     * sync service
     */
    public static final String SYNC_DEPARTMENT = "/list/get-all-departments";

    /**
     * sync doctors
     */
    public static final String SYNC_DOCTOR = "/list/get-all-doctors";

    /**
     * sync specialist
     */
    public static final String SYNC_SPECIALIST = "/list/get-all-specialists";

    /**
     * sync room
     */
    public static final String SYNC_ROOM = "/list/get-all-rooms";

    /**
     * sync service
     */
    public static final String SYNC_SERVICE = "/list/get-all-services";

    /**
     * Dong bo country
     */
    public static final String SYNC_COUNTRY = "/list/getCountrys";

    /**
     * Dong bo province
     */
    public static final String SYNC_PROVINCE = "/list/getProvinces";

    /**
     * Dong bo district
     */
    public static final String SYNC_DISTRICT = "/list/getDistricts";

    /**
     * Dong bo zone
     */
    public static final String SYNC_ZONE = "/list/getZones";

    // Checkin thong tin dat kham day len vien
    public static final String CHECK_IN = "/patient/checkIn";

    /**
     * Lay danh sach lich su vao vien theo value(HisPatientId cu)
     */
    public static final String GET_BY_VALUE = "/patient/get-by-value/{isofhCareValue}";

    /**
     * Lay chi tiet theo lich su vao vien
     */
    public static final String GET_DETAIL_PATIENT_HISTORY = "/patient/get-detail-patient-history/{patientHistoryId}";

    /**
     * lay ket qua theo lich su vao vien
     */
    public static final String GET_RESULT_PATIENT_HISTORY = "/patient/get-result-patient-history/{patientHistoryId}";
}
