package com.isofh.service.constant.type;

/**
 * 1 tao moi<br>
 * 2 cap nhap<br>
 * 4 xoa<br>
 */
public enum QueueType {
    CREATE(1),
    UPDATE(2),
    DELETE(4);

    private final int value;

    QueueType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
