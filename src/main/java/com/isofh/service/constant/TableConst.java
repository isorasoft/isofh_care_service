/*
 * *******************************************************************
 * Copyright (c) 2018 Isofh.com to present.
 * All rights reserved.
 *
 * Author: tuanld
 * ******************************************************************
 *
 */

package com.isofh.service.constant;

public class TableConst {
    private TableConst() {
    }

    public static final String USER = "user";
    public static final String PROVINCE = "province";
    public static final String DEVICE = "device";
    public static final String IMAGE = "image";
    public static final String SCHEDULE = "schedule";
    public static final String ADVERTISE = "advertise";
    public static final String FILE = "file";
    public static final String COMMON_SPECIALIST = "common_specialist";
    public static final String DISEASE = "disease";
    public static final String SYMPTOM = "symptom";
    public static final String FACILITY = "facility";
    public static final String COUNTRY = "country";
    public static final String DRUG = "drug";
    public static final String METHOD_USE = "method_use";
    public static final String MENU = "menu";
    public static final String SMS = "sms";
    public static final String TOKEN_PASSWORD = "token_password";
    public static final String SLIDE_ITEM = "slide_item";
    public static final String SLIDE = "slide";
    public static final String SLIDE_PLACE = "slide_place";
    public static final String EMAIL_TOKEN = "email_token";
    public static final String EMAIL = "email";
    public static final String HOSPITAL_PROFILE = "hospital_profile";
    public static final String HOSPITAL = "hospital";
    public static final String API_LOG = "tbl_api_log";
    public static final String POST = "tbl_post";
    public static final String COMMENT = "tbl_comment";
    public static final String DOCTOR_DEPARTMENT = "tbl_doctor_department";
    public static final String ROLES = "tbl_role";
    public static final String PERMISSION = "tbl_permission";
    public static final String DOCTOR_INF = "tbl_doctor_inf";
    public static final String NOTIFICATION = "tbl_notification";

    public static final String DOCTOR = "doctor";
    public static final String ROOM = "room";
    public static final String SERVICE = "service";
    public static final String DEPARTMENT = "department";
    public static final String LOG = "log";
    public static final String BOOKING = "booking";
    public static final String PROFILE = "profile";
    public static final String SPECIALIST = "specialist";

    public static final String ZONE = "zone";
    public static final String DISTRICT = "district";
    public static final String CACHE_HIS = "cache_his";
    public static final String PATIENT_HISTORY = "patient_history";
}
