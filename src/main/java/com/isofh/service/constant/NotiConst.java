/*
 * *******************************************************************
 * Copyright (c) 2018 Isofh.com to present.
 * All rights reserved.
 *
 * Author: tuanld
 * ******************************************************************
 */

package com.isofh.service.constant;

public class NotiConst {

    private NotiConst() {
    }

    public static final String DEFAULT_BODY_NOTIFICATION = "Bạn có một thông báo mới";
    public static final String DEFAULT_TITLE_NOTIFICATION = "Hội nghị Nhất Minh";
    public static final String DEFAULT_SOUND = "default";
    public static final String TOPIC_NAME = "nmcevent_test";
    public static final String URL_SEND_MESSAGE = "https://fcm.googleapis.com/v1/projects/cong-dong-y-te/messages:send";
    public static final String FIREBASE_KEY = "AIzaSyCirN-manB9oqf9of1iAnJGIMJ_3PyJqZQ";
    public static final String BAC_SI = "Bác sĩ";
    public static final String NGUOI_HOI = "Người hỏi";

}
