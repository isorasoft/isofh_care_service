package com.isofh.service.service;

import com.isofh.service.model.*;
import com.isofh.service.result.IMapData;
import com.isofh.service.utils.CollectionUtils;
import com.isofh.service.utils.RabbitmqUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.*;

@Service
public class DrugService extends BaseService implements IMapData<DrugEntity> {

    @Override
    public Map<String, Object> getMapData(DrugEntity entity) {
        Map<String, Object> mapData = new HashMap<>();
        mapData.put(DRUG, entity);
        return mapData;
    }

    @Transactional
    public ResultEntity create(Object object) throws Exception {
        init(object);
        DrugEntity drugEntity = getObject(DRUG, DrugEntity.class);

        //kiem tra xem co bi trung ten khong
        if (drugRepository.checkName(drugEntity.getName()) != null) {
            throw getException(2, "Thuoc nay da co trong nha thuoc");
        }

        //kiểm tra nước sản xuất
        long countryId = getInteger("countryId", -1);
        if (countryId != -1) {
            CountryEntity entity = getCountry(countryId);
            drugEntity.setCountry(entity);
        }

        // đọc mảng diseaseId, lấy object từ mảng diseaseId truyền vào
        Long[] diseaseIds = getObject("diseaseIds", Long[].class);
        if (diseaseIds != null && diseaseIds.length > 0) {
            for (Long diseaseId : diseaseIds) {
                DiseaseEntity diseaseEntity = getDisease(diseaseId);
                drugEntity.getDiseases().add(diseaseEntity);
            }
        }
        // lưu drugEntity vào danh mục thuốc chung
        drugRepository.save(drugEntity);

        // đọc giá trị facilityId đẩy lên và lấy object facility từ facilityId
        Long facilityId = getLong("facilityId");
        if (facilityId != null) {
            FacilityEntity facilityEntity = getFacility(facilityId);
            DrugEntity newDrugEntity = cloneDrug(drugEntity, facilityEntity, drugEntity.getId());
            sendCreatedToQueue(newDrugEntity);
            return ok(DRUG, newDrugEntity);
        } else {
            sendCreatedToQueue(drugEntity);
            return ok(DRUG, drugEntity);
        }
    }

    //copy các thông tin cơ bản sang 1 entity mới
    private DrugEntity copyDrug(DrugEntity drugEntity) {
        return new DrugEntity(drugEntity.getName()
                , drugEntity.getActiveSubstances()
                , drugEntity.getCategory()
                , drugEntity.getComponent()
                , drugEntity.getMethodUse()
                , drugEntity.getCountry()
                , drugEntity.getStandard()
                , drugEntity.getUseness()
                , drugEntity.getAvoidUseness()
                , drugEntity.getManufacturer()
                , drugEntity.getPrice()
                , drugEntity.getImages()
                , drugEntity.getFacility());
    }

    //
    private DrugEntity cloneDrug(DrugEntity drugEntity, FacilityEntity facilityEntity, Long drugId) {
        // copy cac thong tin co ban
        DrugEntity entity = copyDrug(drugEntity);
        entity.setFacility(facilityEntity);
        entity.setOriginId(drugId);
        //lưu vào cơ sở dữ liệu chung có các thông tin của thuốc và nhà thuốc đã tạo ra nó
        save(entity);
        return entity;
    }

    @Transactional
    public ResultEntity update(Object object, Long id) throws Exception {
        init(object);
        DrugEntity data = getObject(DRUG, DrugEntity.class);
        DrugEntity entity = getDrug(id);
        entity.setName(data.getName());
        entity.setActiveSubstances(data.getActiveSubstances());
        entity.setCategory(data.getCategory());
        entity.setComponent(data.getComponent());
        entity.setMethodUse(data.getMethodUse());
        entity.setManufacturer(data.getManufacturer());
        entity.setStandard(data.getStandard());
        entity.setUseness(data.getUseness());
        entity.setAvoidUseness(data.getAvoidUseness());
        entity.setPrice(data.getPrice());
        entity.setImages(data.getImages());
        Long countryId = getLong("countryId", 0L);
        CountryEntity countryEntity = getCountry(countryId);
        entity.setCountry(countryEntity);


        List<Long> diseaseIds = Arrays.asList(getObject("diseaseIds", Long[].class));
        entity.getDiseases().clear();
        if (!CollectionUtils.isNullOrEmpty(diseaseIds)) {
            for (Long symptomId : diseaseIds) {
                DiseaseEntity disease = getDisease(symptomId);
                entity.getDiseases().add(disease);
            }
        }
        drugRepository.save(entity);
        sendUpdatedToQueue(entity);
        return ok(Arrays.asList(DRUG, COUNTRY, DISEASES), Arrays.asList(entity, entity.getCountry(), entity.getDiseases()));
    }

    @Transactional
    public ResultEntity delete(Long id) throws Exception {
        init();
        drugRepository.deleteById(id);
        sendDeletedToQueue(id);
        return ok();
    }

    @Transactional
    public ResultEntity search(Integer page, Integer size, String name, String activeSubstances, Long category, Long methodUse, String standard, String manufacturer, Long fromPrice, Long toPrice, LocalDate fromUpdatedDate,
                               LocalDate toUpdatedDate, Integer sortType, Integer facilityId) throws Exception {
        init();
        Sort sort = Sort.by(Sort.Direction.DESC, "updated_date");
        switch (sortType) {
            case 1:
                sort = Sort.by(Sort.Direction.DESC, "view_count");
                break;
        }
        Page<DrugEntity> drugs = drugRepository.search(name, activeSubstances, category, methodUse, standard, manufacturer, fromPrice, toPrice, fromUpdatedDate, toUpdatedDate, facilityId, getPaging(page, size, sort));
        List<Map<String, Object>> mapResults = new ArrayList<>();
        for (DrugEntity drugEntity : drugs) {
            Map<String, Object> mapResult = new HashMap<>();
            mapResults.add(mapResult);
            mapResult.put(DISEASES, drugEntity.getDiseases());
            mapResult.put(COUNTRY, drugEntity.getCountry());
            mapResult.put(DRUG, drugEntity);
        }
        return ok(drugs.getTotalElements(), mapResults);
    }

    @Transactional
    public ResultEntity updateViewCount(Long id) throws Exception {
        init();
        DrugEntity entity = getDrug(id);
        entity.setViewCount(entity.getViewCount() + 1);
        drugRepository.save(entity);
        sendUpdatedToQueue(entity);
        return ok(Arrays.asList(DISEASE, COUNTRY, DISEASES, IMAGES), Arrays.asList(entity, entity.getCountry(), entity.getDiseases(), entity.getImages()));
    }

    @Transactional
    public ResultEntity importCsv(Object object) throws Exception {
        init(object);
        JSONArray drugs = getJSONArray("drugs");
        Iterable<CountryEntity> countries = countryRepository.findAll();
        Map<String, CountryEntity> mapData = new HashMap<>();
        for (CountryEntity country : countries) {
            mapData.put(country.getName(), country);
        }

        Iterable<MethodUseEntity> methodUseEntities = methodUseRepository.findAll();
        Map<String, Long> mapMethodUse = new HashMap<>();
        for (MethodUseEntity methodUse : methodUseEntities) {
            mapMethodUse.put(methodUse.getName(), methodUse.getId());
        }
        List<DrugEntity> listDrug = new ArrayList<>();
        if (drugs != null) {
            for (int i = 0; i < drugs.length(); i++) {
                JSONObject drug = drugs.getJSONObject(i);
                DrugEntity drugEntity = new DrugEntity();
                drugEntity.setName(drug.getString("Name"));
                drugEntity.setActiveSubstances(drug.getString("activeSubstances"));

                String strMethodUse = drug.getString("Methoduser");
                if (mapMethodUse.containsKey(strMethodUse)) {
                    drugEntity.setMethodUse(mapMethodUse.get(strMethodUse));
                }

                drugEntity.setComponent(drug.getString("component"));
                drugEntity.setStandard(drug.getString("standard"));
                drugEntity.setManufacturer(drug.getString("manufacturer"));
                String country = drug.getString("country");

                CountryEntity countryEntity = mapData.get(country);
                drugEntity.setCountry(countryEntity);
                drugRepository.save(drugEntity);
                sendCreatedToQueue(drugEntity);
//                    listDrug.add(drugEntity);
            }
        }

        return ok("drugs", listDrug);
    }

    @Transactional
    public ResultEntity getDetail(long id) {
        init();
        DrugEntity drugEntity = getDrug(id);
        Map<String, Object> mapResult = new HashMap<>();
        mapResult.put(DRUG, drugEntity);
        mapResult.put(DISEASES, drugEntity.getDiseases());
        mapResult.put(COUNTRY, drugEntity.getCountry());
        return ok(mapResult);
    }

    @Transactional
    public ResultEntity syncData() throws Exception {
        init();
        Iterable<DrugEntity> drugList = drugRepository.findAll();
        List<QueueEntity> queues = new ArrayList<>();
        for (DrugEntity drug : drugList) {
            queues.add(RabbitmqUtils.getCreateQueue(drug));
        }
        sendMessages(queues);
        return ok();
    }

    @Transactional
    public ResultEntity addDrugToFacility(Object object) throws Exception {
        init(object);
        Long facilityId = getLong("facilityId");
        Long drugId = getLong("drugId");

        DrugEntity checkEntity = drugRepository.findByFacilityDrug(facilityId, drugId);
        if (checkEntity != null) {
            throw getException(2, "Thuoc nay da them vao nha thuoc");
        }

        FacilityEntity facilityEntity = getFacility(facilityId);
        DrugEntity drugEntity = getDrug(drugId);
        cloneDrug(drugEntity, facilityEntity, drugEntity.getId());
        save(drugEntity);
        return ok();
    }

    @Transactional
    public ResultEntity importThuocNet(Object object) throws Exception {
        init(object);
        List<DrugEntity> drugEntities = new ArrayList<>();
        DrugTestEntity[] drugs = getObject("data", DrugTestEntity[].class);
        for (DrugTestEntity entity : drugs) {
            DrugEntity drugEntity = new DrugEntity();
            drugEntity.setName(entity.getTen());
            drugEntity.setStandard(entity.getCachdonggoi());
            drugEntity.setUseness(entity.getChidinh());
            drugEntity.setAvoidUseness(entity.getChongchidinh());
            drugEntities.add(drugEntity);
        }
        drugRepository.saveAll(drugEntities);
        return ok("drugs", drugEntities.size());
    }

    /**
     * day thong tin tao moivao Rbmq xu ly
     *
     * @param entity
     */
    private void sendCreatedToQueue(DrugEntity entity) {
        QueueEntity queue = RabbitmqUtils.getCreateQueue(entity);
        sendMessage(queue);
    }


    private void sendMessage(QueueEntity queue) {
        sendMessages(Arrays.asList(queue));
    }

    private void sendMessages(List<QueueEntity> queues) {
        RabbitmqUtils.sendMessages(drugQueue, queues);
    }

    /**
     * day thong tin cap nhap vao Rbmq xu ly
     *
     * @param entity
     */
    private void sendUpdatedToQueue(DrugEntity entity) {
        QueueEntity queue = RabbitmqUtils.getUpdateQueue(entity);
        sendMessage(queue);
    }

    /**
     * day thong tin tao xoa vao Rbmq xu ly
     *
     * @param id
     */
    private void sendDeletedToQueue(long id) {
        Map<String, Object> mapId = new HashMap<>();
        mapId.put("id", id);
        QueueEntity queue = RabbitmqUtils.getDeleteQueue(mapId);
        sendMessage(queue);
    }

    @Value("${drug.queue}")
    private String drugQueue;

}
