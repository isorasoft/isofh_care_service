package com.isofh.service.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.isofh.service.enums.PermissionType;
import com.isofh.service.model.PermissionEntity;
import com.isofh.service.model.ResultEntity;
import com.isofh.service.model.RolesEntity;
import com.isofh.service.model.UserEntity;
import com.isofh.service.result.IMapData;
import com.isofh.service.utils.CustomException;

@Service
public class RolesService extends BaseService implements IMapData<RolesEntity> {

    @Override
    public Map<String, Object> getMapData(RolesEntity entity) {
        Map<String, Object> mapData = new HashMap<>();
        mapData.put(ROLES, entity);
        mapData.put(PERMISSION, entity.getPermissionEntities());
        mapData.put(CREATE_PERSON, entity.getCreatePerson());
        mapData.put(UPDATE_PERSON, entity.getUpdatePerson());
        return mapData;
    }

    @Override
    public List<Map<String, Object>> getMapDatas(Iterable<RolesEntity> roles) {
        List<Map<String, Object>> mapResults = new ArrayList<>();
        for (RolesEntity role : roles) {
            mapResults.add(getMapData(role));
        }
        return mapResults;
    }

    @Transactional
    public ResultEntity create(Object object) throws Exception {
        init(object);
        validateUser(PermissionType.ADD_ROLE.getValue());
        RolesEntity entity = getObject(ROLES, RolesEntity.class);
        RolesEntity check = rolesRepository.checkCode(entity.getCode());
        if (check != null) {
            throw getException(2, "da ton tai ma role");
        }
        Long[] permissionIds = getObject("permissionIds", Long[].class);
        entity.setValueRole(0l);
        if(permissionIds!=null) {
            for (Long permissionId : permissionIds) {
                PermissionEntity permissionEntity = getPermission(permissionId);
                if (permissionEntity != null) {
                    entity.getPermissionEntities().add(permissionEntity);
                    entity.setValueRole(entity.getValueRole()+permissionEntity.getValue());
                }
            }
        }
        

        UserEntity userCreate = getCurrentUser();
        entity.setCreatePerson(userCreate);

        entity.setUpdatePerson(userCreate);
        save(entity);
        return ok(getMapData(entity));
    }

    @Transactional
    public ResultEntity update(Object object, long id) throws Exception {
        init(object);
        validateUser(PermissionType.UPDATE_ROLE.getValue());
        RolesEntity data = getObject(ROLES, RolesEntity.class);
        UserEntity userUpdate = getCurrentUser();
        RolesEntity entity = getRoles(id);
        RolesEntity check = rolesRepository.checkCode(data.getCode());
        if (check != null && !check.getCode().equals(entity.getCode()) && check.getId() != entity.getId()) {
            throw getException(2, "da ton tai ma role");
        }

        entity.setName(data.getName());
        entity.setCode(data.getCode());
        entity.setValueRole(0l);
        entity.setUpdatePerson(userUpdate);
        entity.setBlocked(data.getBlocked());

        entity.getPermissionEntities().clear();
        Long[] permissionIds = getObject("permissionIds", Long[].class);
        for (Long permissionId : permissionIds) {
            PermissionEntity permissionEntity = getPermission(permissionId);
            if (permissionEntity != null) {
                entity.getPermissionEntities().add(permissionEntity);
                entity.setValueRole(entity.getValueRole()+permissionEntity.getValue());
            }
        }
        save(entity);
        return ok(getMapData(entity));
    }

    @Transactional
    public ResultEntity delete(long id) throws Exception {
        init();
        //validateUser(PermissionType.DELETE_ROLE.getValue());
        RolesEntity rolesEntity = getRoles(id);
        UserEntity checkRoles = userRepository.checkRoles(id);
        if (checkRoles != null) {
            throw getException(2, "role nay da duoc gan vao 1 hay nhieu user nao do");
        }
        rolesEntity.setDeleted(1);
        save(rolesEntity);
        return ok();
    }

    @Transactional
    public ResultEntity search(Integer page, Integer size, String stringQuery, Long value, int type, int blocked) throws Exception {
        init();
        validateUser(PermissionType.VIEW_ROLE.getValue());
        /**
         * 1: sort theo ho ten
         * 2: ngay tao
         */
        Pageable pageable = null;
        switch (type) {
            case 1:
                pageable = PageRequest.of(page - 1, size, Sort.by("col_name").descending());
                break;

            case 2:
                pageable = PageRequest.of(page - 1, size, Sort.by("created_date").descending());
                break;

            case 3:
                pageable = PageRequest.of(page - 1, size, Sort.by("created_date").ascending());
                break;

            case 4:
                pageable = PageRequest.of(page - 1, size, Sort.by("col_name").ascending());
                break;

            case 6:
                pageable = PageRequest.of(page - 1, size, Sort.by("col_code").descending());
                break;

            case 7:
                pageable = PageRequest.of(page - 1, size, Sort.by("col_code").ascending());
                break;
            default:
                pageable = getDefaultPage(page, size);
        }
        Page<RolesEntity> entity = rolesRepository.search(stringQuery, value, blocked, pageable);
        return ok(entity.getTotalElements(), getMapDatas(entity));
    }

    @Transactional
    public ResultEntity getDetail(long id) throws CustomException {
        init();
        validateUser(PermissionType.VIEW_ROLE.getValue());
        RolesEntity entity = getRoles(id);
        return ok(getMapData(entity));
    }

    @Transactional
    public ResultEntity block(Object object, long id) throws Exception {
        init(object);
        Integer data = getInteger("blocked");
        RolesEntity entity = getRoles(id);
        entity.setBlocked(data);
        save(entity);
        return ok(getMapData(entity));
    }
    
    @Transactional
    public ResultEntity updateRoleHR(long id) throws Exception {
        init();
        RolesEntity entity = getEntityById(rolesRepository, id);
        Long value = 0L;
        entity.getPermissionEntities().removeAll(entity.getPermissionEntities());
        List<PermissionEntity> roleList = (List<PermissionEntity>) permissionRepository.findAll();
        
        for(PermissionEntity role : roleList) {
            entity.getPermissionEntities().add(role);
            value = value + role.getValue();
        }
        entity.setValueRole(value);
        save(entity);
        return ok(getMapData(entity));
    }

}
