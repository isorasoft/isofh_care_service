package com.isofh.service.service;

import com.isofh.service.model.ResultEntity;
import com.isofh.service.model.SlideItemEntity;
import com.isofh.service.result.IMapData;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SlideItemService extends BaseService implements IMapData<SlideItemEntity> {

    @Override
    public Map<String, Object> getMapData(SlideItemEntity entity) {
        Map<String, Object> mapData = new HashMap<>();
        mapData.put(SLIDE_ITEM, entity);
        return mapData;
    }

    @Transactional
    public ResultEntity create(Object object) throws Exception {
        init(object);
        SlideItemEntity entity = getObject(SLIDE_ITEM, SlideItemEntity.class);
        slideItemRepository.save(entity);
        return ok(SLIDE_ITEM, entity);
    }

    @Transactional
    public ResultEntity update(Object object, long id) throws Exception {
        init(object);
        SlideItemEntity data = getObject(SLIDE_ITEM, SlideItemEntity.class);
        SlideItemEntity entity = getSlideItem(id);
        entity.setHref(data.getHref());
        entity.setImage(data.getImage());
        entity.setName(data.getName());
        slideItemRepository.save(entity);
        return ok(SLIDE_ITEM, entity);
    }

    @Transactional
    public ResultEntity delete(long id) throws Exception {
        init();
        slideItemRepository.deleteById(id);
        return ok();
    }

    @Transactional
    public ResultEntity search(Integer page, Integer size, Long slideId, String name) throws Exception {
        init();
        Page<SlideItemEntity> queryResults = slideItemRepository.search(slideId, name, getDefaultPage(page, size));
        List<Map<String, Object>> mapResults = new ArrayList<>();
        for (SlideItemEntity entity : queryResults.getContent()) {
            Map<String, Object> mapResult = new HashMap<>();
            mapResults.add(mapResult);
            mapResult.put(SLIDE_ITEM, entity);
        }
        return ok(queryResults.getTotalElements(), mapResults);
    }
}
