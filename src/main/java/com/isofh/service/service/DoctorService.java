package com.isofh.service.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.springframework.stereotype.Service;

import com.isofh.service.constant.ServiceConst;
import com.isofh.service.enums.HttpMethodType;
import com.isofh.service.model.DoctorEntity;
import com.isofh.service.model.HospitalEntity;
import com.isofh.service.model.ResultEntity;
import com.isofh.service.model.ResultHisEntity;
import com.isofh.service.model.ScheduleEntity;
import com.isofh.service.result.IMapData;
import com.isofh.service.utils.CollectionUtils;
import com.isofh.service.utils.GsonUtils;
import com.isofh.service.utils.NetworkUtils;

@Service
public class DoctorService extends BaseService implements IMapData<String> {

    @Override
    public Map<String, Object> getMapData(String entity) {
        Map<String, Object> mapData = new HashMap<>();
        return mapData;
    }

    public ResultEntity sync(long hospitalId) throws Exception {
        try {
            init();

            List<Long> oldDoctorIds = new ArrayList<>();
            Iterable<DoctorEntity> doctors = doctorRepository.getAll(hospitalId);
            for (DoctorEntity room : doctors) {
                oldDoctorIds.add(room.getId());
            }
            HospitalEntity hospitalEntity = getHospital(hospitalId); 
            if(hospitalEntity != null) {
                String queryData = NetworkUtils.callService(hospitalEntity.getServiceUrl() + ServiceConst.SYNC_DOCTOR, HttpMethodType.GET);
                ResultHisEntity data = GsonUtils.toObject(queryData, ResultHisEntity.class);
                if (data == null || data.getCode() != 0) {
                    throw getException(1, "His tra ve loi ");
                }

                for (int i = 0; i < data.getData().getDoctors().length; i++) {
                    try {
                        DoctorEntity entity = data.getData().getDoctors()[i];
                        entity.setHospital(hospitalEntity);
                        if (Objects.isNull(entity.getHisDoctorId())) {
                            continue;
                        }
                        
                        DoctorEntity doctorEntity = doctorRepository.findByHisDoctorId(entity.getHisDoctorId(), hospitalId);
                        if (doctorEntity != null) {
                            entity = doctorEntity;
                            oldDoctorIds.remove(doctorEntity.getId());
                        }

                        // Ma hoc ham hoc vi tren his
                        if(Integer.valueOf(entity.getAvatar()).equals(0)) {
                            entity.setAvatar(hospitalEntity.getServiceUrl() + entity.getAvatar());
                        } else {
                            entity.setAvatar("");
                        }
                        save(entity);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }

                for (Long id : oldDoctorIds) {
                    DoctorEntity doctorEntity = getDoctor(id);
                    doctorEntity.setDeleted(1);
                    save(doctorEntity);

                    List<ScheduleEntity> deletedSchedules = scheduleRepository.findByDoctor(id);
                    if (!CollectionUtils.isNullOrEmpty(deletedSchedules)) {
                        for (ScheduleEntity schedule : deletedSchedules) {
                            schedule.setDeleted(1);
                        }
                        scheduleRepository.saveAll(deletedSchedules);
                    }
                }
            } else {
                throw getException(2, "Nhap sai id hospital");
            }
            
            return ok();
        } catch (Exception ex) {
            throw ex;
        }
    }

    public ResultEntity getAll(long hospitalId) throws Exception {
        try {
            init();
            Iterable<DoctorEntity> doctors = doctorRepository.getAll(hospitalId);
            return ok(DOCTORS, doctors);
        } catch (Exception ex) {
            throw ex;
        }
    }

    public ResultEntity getByDepartmentSpecialist(Long departmentId, Long specialistId) throws Exception {
        try {
            init();
            List<DoctorEntity> doctors = doctorRepository.findByDepartmentSpecialist(departmentId, specialistId);
            return ok(DOCTORS, doctors);
        } catch (Exception ex) {
            throw ex;
        }
    }
}
