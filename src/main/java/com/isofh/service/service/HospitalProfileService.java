package com.isofh.service.service;

import com.isofh.service.model.HospitalEntity;
import com.isofh.service.model.HospitalProfileEntity;
import com.isofh.service.model.ResultEntity;
import com.isofh.service.model.UserEntity;
import com.isofh.service.result.IMapData;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class HospitalProfileService extends BaseService implements IMapData<HospitalProfileEntity> {

    @Override
    public Map<String, Object> getMapData(HospitalProfileEntity entity) {
        Map<String, Object> mapData = new HashMap<>();
        mapData.put(HOSPITAL, entity.getHospital());
        return mapData;
    }

    @Transactional
    public ResultEntity create(Object object) throws Exception {
        init(object);
        HospitalProfileEntity entity = getObject(USER, HospitalProfileEntity.class);
        Long hospitalId = getLong("hospitalId");
        HospitalProfileEntity checkEntity = hospitalProfileRepository.findByProfileHospital(hospitalId,
                getCurrentUser().getProfile().getId());
        if (checkEntity == null) {
            HospitalEntity hospitalEntity = getHospital(hospitalId);
            entity.setHospital(hospitalEntity);
            save(entity);
        }
        return ok();
    }

    public ResultEntity getHospitalByProfile() throws Exception {
        try {
            init();
            UserEntity user = getCurrentUser();
            List<HospitalProfileEntity> patientHistoryList = hospitalProfileRepository.findByProfile(user.getProfile().getId());
            return ok(getMapDatas(patientHistoryList));
        } catch (Exception ex) {
            throw ex;
        }
    }
}
