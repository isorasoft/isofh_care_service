package com.isofh.service.service;

import com.google.gson.JsonArray;
import com.isofh.service.constant.ServiceConst;
import com.isofh.service.enums.HttpMethodType;
import com.isofh.service.model.*;
import com.isofh.service.result.IMapData;
import com.isofh.service.utils.FileUtils;
import com.isofh.service.utils.NetworkUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

@Service
public class CountryService extends BaseService implements IMapData<AdvertiseEntity> {

    @Override
    public Map<String, Object> getMapData(AdvertiseEntity entity) {
        Map<String, Object> mapData = new HashMap<>();
        mapData.put(COUNTRY, entity);
        return mapData;
    }

    @Transactional
    public ResultEntity getAll() throws Exception {
        init();
        Iterable<CountryEntity> coutries = countryRepository.findAll();
        return ok("countries", coutries);
    }
}
