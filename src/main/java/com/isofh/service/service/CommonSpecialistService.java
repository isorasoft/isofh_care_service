package com.isofh.service.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.isofh.service.model.CommonSpecialistEntity;
import com.isofh.service.model.ResultEntity;
import com.isofh.service.result.IMapData;

@Service
public class CommonSpecialistService extends BaseService implements IMapData<CommonSpecialistEntity> {

    @Override
    public Map<String, Object> getMapData(CommonSpecialistEntity entity) {
        Map<String, Object> mapData = new HashMap<>();
        mapData.put(SPECIALIST, entity);
        return mapData;
    }

    @Transactional
    public ResultEntity create(Object object) throws Exception {
        init(object);
        CommonSpecialistEntity entity = getObject(SPECIALIST, CommonSpecialistEntity.class);
        CommonSpecialistEntity checkEntity = commonSpecialistRepository.findFirstByName(entity.getName());
        if (checkEntity != null) {
            throw getException(2, "Ten chuyen khoa da ton tai");
        }
        commonSpecialistRepository.save(entity);
        return ok(SPECIALIST, entity);
    }

    @Transactional
    public ResultEntity update(Object object, Long id) throws Exception {
        init(object);
        CommonSpecialistEntity data = getObject(SPECIALIST, CommonSpecialistEntity.class);
        CommonSpecialistEntity entity = getCommonSpecialist(id);

        if (!data.getName().equals(entity.getName())) {
            // neu co thay doi ten chuyen khoa
            CommonSpecialistEntity checkEntity = commonSpecialistRepository.findFirstByName(data.getName());
            if (checkEntity != null) {
                throw getException(2, "Ten chuyen khoa da ton tai");
            }
        }
        entity.setName(data.getName());
        commonSpecialistRepository.save(entity);
        return ok(SPECIALIST, entity);
    }

    @Transactional
    public ResultEntity delete(Long id) throws Exception {
        init();
        commonSpecialistRepository.deleteById(id);
        return ok();
    }

    @Transactional
    public ResultEntity search(String name, Integer page, Integer size, Integer sortType, LocalDate fromUpdatedDate, LocalDate toUpdatedDate) throws Exception {
        init();
        Sort sort = Sort.by(Sort.Direction.DESC, "updated_date");
        switch (sortType) {
            case 1:
                sort = Sort.by(Sort.Direction.DESC, "view_count");
                break;
        }
        Page<CommonSpecialistEntity> specialists = commonSpecialistRepository.search(name, fromUpdatedDate, toUpdatedDate, getPaging(page, size, sort));
        List<Map<String, Object>> mapResults = new ArrayList<>();
        for (CommonSpecialistEntity entity : specialists.getContent()) {
            Map<String, Object> mapResult = new HashMap<>();
            mapResult.put(SPECIALIST, entity);
            mapResults.add(mapResult);
        }
        return ok(specialists.getTotalElements(), mapResults);
    }

    @Transactional
    public ResultEntity updateViewCount(Long id) throws Exception {
        init();
        CommonSpecialistEntity entity = getCommonSpecialist(id);
        entity.setViewCount(entity.getViewCount() + 1);
        commonSpecialistRepository.save(entity);
        return ok(SPECIALIST, entity);
    }
    
    @Transactional
    public ResultEntity importSp(Object object) throws Exception {
        init(object);
        String message = jsonData.get(SPECIALIST).toString();
        List<CommonSpecialistEntity> objectList = stringToArray(message, CommonSpecialistEntity[].class);

        commonSpecialistRepository.saveAll(objectList);
        return ok(SPECIALIST, objectList.get(0));
    }

    public static <T> List<T> stringToArray(String s, Class<T[]> clazz) {
        T[] arr = new Gson().fromJson(s, clazz);
        return Arrays.asList(arr); // or return Arrays.asList(new Gson().fromJson(s, clazz)); for a one-liner
    }
}
