package com.isofh.service.service;

import com.isofh.service.model.*;
import com.isofh.service.result.IMapData;
import com.isofh.service.utils.ArrayUtils;
import com.isofh.service.utils.CollectionUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DiseaseService extends BaseService implements IMapData<DiseaseEntity> {

    @Override
    public Map<String, Object> getMapData(DiseaseEntity entity) {
        Map<String, Object> mapData = new HashMap<>();
        mapData.put(DISEASE, entity);
        mapData.put(SPECIALIST, entity.getSpecialist());
        mapData.put(IMAGES, entity.getImages());
        return mapData;
    }

    private Map<String, Object> getMapDataDetail(DiseaseEntity entity) {
        Map<String, Object> mapData = getMapData(entity);
        mapData.put(SYMPTOMS, entity.getSymptoms());
        return mapData;
    }

    @Transactional
    public ResultEntity create(Object object) throws Exception {
        init(object);
        DiseaseEntity entity = getObject(DISEASE, DiseaseEntity.class);

        Long specialistId = getLong("specialistId", 0L);
        CommonSpecialistEntity commonSpecialistEntity = getCommonSpecialist(specialistId);
        entity.setSpecialist(commonSpecialistEntity);

        Long[] symptomIds = getObject("symptomIds", Long[].class);
        if (!ArrayUtils.isNullOrEmpty(symptomIds)) {
            for (Long symptomId : symptomIds) {
                SymptomEntity symptom = getSymptom(symptomId);
                if (symptom != null) {
                    entity.getSymptoms().add(symptom);
                }
            }
        }

        String[] imageIds = getObject("imageUrls", String[].class);
        if (!ArrayUtils.isNullOrEmpty(imageIds)) {
            for (String image : imageIds) {
                ImageEntity imageEntity = new ImageEntity();
                imageEntity.setUrl(image);
                imageEntity.setDisease(entity);
                entity.getImages().add(imageEntity);
            }
        }
        diseaseRepository.save(entity);
        return ok(Arrays.asList(DISEASE, SPECIALIST, "symptoms", "images"), Arrays.asList(entity, entity.getSpecialist(), entity.getSymptoms(), entity.getImages()));
    }

    @Transactional
    public ResultEntity update(Object object, Long id) throws Exception {
        init(object);
        DiseaseEntity data = getObject(DISEASE, DiseaseEntity.class);
        DiseaseEntity entity = getDisease(id);
        entity.setName(data.getName());
        entity.setGender(data.getGender());
        entity.setReason(data.getReason());
        entity.setGeneralInfo(data.getGeneralInfo());
        entity.setFromAge(data.getFromAge());
        entity.setToAge(data.getToAge());
        entity.setTreatment(data.getTreatment());

        Long specialistId = getLong("specialistId", 0L);
        CommonSpecialistEntity commonSpecialistEntity = getCommonSpecialist(specialistId);
        data.setSpecialist(commonSpecialistEntity);


        List<Long> symptomIds = Arrays.asList(getObject("symptomIds", Long[].class));
        entity.getImages().clear();
        entity.getSymptoms().clear();
        if (!CollectionUtils.isNullOrEmpty(symptomIds)) {
            for (Long symptomId : symptomIds) {
                SymptomEntity symptomEntity = getSymptom(symptomId);
                entity.getSymptoms().add(symptomEntity);
            }
        }


        String[] imageIds = getObject("imageUrls", String[].class);
        entity.getImages().clear();
        if (!ArrayUtils.isNullOrEmpty(imageIds)) {
            for (String image : imageIds) {
                ImageEntity imageEntity = new ImageEntity();
                imageEntity.setUrl(image);
                imageEntity.setDisease(entity);
                entity.getImages().add(imageEntity);
            }
        }
        diseaseRepository.save(entity);
        return ok(Arrays.asList(DISEASE, SPECIALIST, SYMPTOMS, IMAGES), Arrays.asList(entity, entity.getSpecialist(), entity.getSymptoms(), entity.getImages()));
    }

    @Transactional
    public ResultEntity delete(Long id) throws Exception {
        init();
        diseaseRepository.deleteById(id);
        return ok();
    }

    @Transactional
    public ResultEntity search(Integer page, Integer size, String name, Integer fromAge, Integer toAge, Integer specialistId, Integer gender, LocalDate fromUpdatedDate, LocalDate toUpdatedDate, Integer sortType) throws Exception {
        init();
        Sort sort = Sort.by(Sort.Direction.DESC, "updated_date");
        switch (sortType) {
            case 1:
                sort = Sort.by(Sort.Direction.DESC, "view_count");
                break;
        }

        Page<DiseaseEntity> diseases = diseaseRepository.search(name, fromAge, toAge, specialistId, gender, fromUpdatedDate, toUpdatedDate, getPaging(page, size, sort));
        List<Map<String, Object>> mapResults = getMapDatas(diseases.getContent());
        return ok(diseases.getTotalElements(), mapResults);
    }

    @Transactional
    public ResultEntity searchByDiseaseSymptom(Integer page, Integer size, String name, Integer sortType) throws Exception {
        init();

        Sort sort = Sort.by(Sort.Direction.DESC, "updated_date");
        switch (sortType) {
            case 1:
                sort = Sort.by(Sort.Direction.DESC, "view_count");
                break;
        }
        Page<DiseaseEntity> diseases = diseaseRepository.searchByDiseaseSymptom(name, getPaging(page, size, sort));
        List<Map<String, Object>> mapResults = getMapDatas(diseases.getContent());

        return ok(diseases.getTotalElements(), mapResults);
    }

    @Transactional
    public ResultEntity updateViewCount(Long id) throws Exception {
        init();
        DiseaseEntity entity = getDisease(id);
        entity.setViewCount(entity.getViewCount() + 1);
        save(entity);
        return ok(DISEASE, entity);
    }

    @Transactional
    public ResultEntity getDetail(long id) {
        init();
        DiseaseEntity entity = getDisease(id);
        return ok(getMapDataDetail(entity));
    }

    @Transactional
    public ResultEntity searchDiseaseBySymptom(Integer page, Integer size, Integer sortType, long symptomId) throws Exception {
        init();

        Sort sort = Sort.by(Sort.Direction.DESC, "created_date");
        switch (sortType) {
            case 1:
                sort = Sort.by(Sort.Direction.DESC, "view_count");
                break;
        }
        Page<DiseaseEntity> diseases = diseaseRepository.searchDiseaseBySymptom(symptomId, getPaging(page, size, sort));
        List<Map<String, Object>> mapResults = getMapDatas(diseases.getContent());

        return ok(diseases.getTotalElements(), mapResults);
    }
}
