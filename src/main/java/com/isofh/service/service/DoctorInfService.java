package com.isofh.service.service;


import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import com.isofh.service.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.isofh.service.ServiceConnector;
import com.isofh.service.enums.UserType;
import com.isofh.service.model.CommonSpecialistEntity;
import com.isofh.service.model.DoctorInfEntity;
import com.isofh.service.model.ResultEntity;
import com.isofh.service.model.UserEntity;

import com.isofh.service.result.IMapData;
import com.isofh.service.utils.RandomUtils;
import com.isofh.service.utils.StrUtils;

@Service
public class DoctorInfService extends BaseService implements IMapData<DoctorInfEntity> {

    @Autowired
    UserService userService;

    @Autowired
    ServiceConnector serviceConnector;

    @Override
    public Map<String, Object> getMapData(DoctorInfEntity entity) {
        Map<String, Object> mapData = new HashMap<>();
        mapData.put(DOCTOR_INF, entity);
        mapData.put(SPECIALIST, entity.getSpecialist());
        return mapData;
    }

    @Transactional
    public ResultEntity create(Object object) throws Exception {
        init(object);
        DoctorInfEntity entity = getObject(DOCTOR_INF, DoctorInfEntity.class);
        Long specialistId = getLong("specialistId");
        CommonSpecialistEntity commonSpecialistEntity = getCommonSpecialist(specialistId);
        entity.setSpecialist(commonSpecialistEntity);
        UserEntity checkEmailDoctor = userRepository.findByEmail(UserType.DOCTOR.getValue(), entity.getEmail());
        DoctorInfEntity doctorInfEntity = doctorInfRepository.checkEmail(entity.getEmail());

        UserEntity checkPhoneDoctor = userRepository.findByPhone(UserType.DOCTOR.getValue(), entity.getPhone());
        DoctorInfEntity checkPhoneDoctor1 = doctorInfRepository.checkPhone(entity.getPhone());

        UserEntity checkCertificateCodeDoctor = userRepository.findByCertificateCode(entity.getCertificateCode());
        DoctorInfEntity checkCertificateCodeDoctor1 = doctorInfRepository
                .checkcertificateCode(entity.getCertificateCode());
        if (checkCertificateCodeDoctor != null || checkCertificateCodeDoctor1 != null) {
            if (checkEmailDoctor != null || doctorInfEntity != null) {
                if (checkPhoneDoctor != null || checkPhoneDoctor1 != null) {
                    throw getException(9,
                            "Số điện thoại, email và chứng chỉ đã được sử dụng trong hệ thống. Vui lòng nhập lại.");
                } else {
                    throw getException(6, "Email và chứng chỉ đã được sử dụng trong hệ thống. Vui lòng nhập lại.");
                }
            } else if (checkPhoneDoctor != null || checkPhoneDoctor1 != null) {
                throw getException(8, "Số điện thoại và chứng chỉ đã được sử dụng trong hệ thống. Vui lòng nhập lại.");
            } else {
                throw getException(4, "Số chứng chỉ đã được sử dụng trong hệ thống. Vui lòng nhập lại.");
            }
        } else if (checkPhoneDoctor != null || checkPhoneDoctor1 != null) {
            if (checkEmailDoctor != null || doctorInfEntity != null) {
                throw getException(7, "Email và số điện thoại đã được sử dụng trong hệ thống. Vui lòng nhập lại.");
            } else {
                throw getException(2, "Số điện thoại đã được sử dụng trong hệ thống. Vui lòng nhập lại.");
            }
        } else if (checkEmailDoctor != null || doctorInfEntity != null) {
            throw getException(3, "Email đã được sử dụng trong hệ thống. Vui lòng nhập lại.");
        }
        save(entity);
        return ok(getMapData(entity));
    }

    @Transactional
    public ResultEntity active(Long id) throws Exception {
        init();
        DoctorInfEntity doctorInfEntity = getEntityById(doctorInfRepository, id);
        doctorInfEntity.setActive(1);
        Map<String, Object> initData = new HashMap<String, Object>();
        String password = RandomUtils.getRandomPasswordNumber();
        String encryptPassword = StrUtils.toMD5(password);
        initData = initUser(initData, doctorInfEntity, encryptPassword);

        ObjectMapper mapper = new ObjectMapper();
        String jsonFromMap = mapper.writeValueAsString(initData);

        ResultEntity result = userService.create(new Gson().fromJson(jsonFromMap, JsonObject.class));
        ResultEntity check = serviceConnector.sendSMS(doctorInfEntity.getPhone(),
                "ISOFHCare xin tran trong thong bao ban da duoc tao tai khoan bac si. Vui long dang nhap bang mat khau tam thoi: "
                        + password + ". Xin cam on.");
//        save(entity);
        return ok(result);
    }

    private Map<String, Object> initUser(Map<String, Object> data, DoctorInfEntity doctorInfEntity,
            String encryptPassword) {
        Map<String, Object> user = new HashMap<String, Object>();
        user.put("name", doctorInfEntity.getName());
        user.put("email", doctorInfEntity.getEmail());
        user.put("phone", doctorInfEntity.getPhone());
        user.put("role", UserType.DOCTOR.getValue());
        user.put("flag", 1);
        user.put("password", encryptPassword);
        user.put("certificateCode", doctorInfEntity.getCertificateCode());
        user.put("image", doctorInfEntity.getImage());
        data.put("specialistId",
                doctorInfEntity.getSpecialist() != null ? doctorInfEntity.getSpecialist().getId() : "");
        data.put("user", user);
        return data;
    }


    @Transactional
    public ResultEntity reject(Long id, Object object) throws Exception {
        init(object);
        DoctorInfEntity doctorInfEntity = getEntityById(doctorInfRepository, id);
        String reject = getString("reject");

        doctorInfEntity.setBlock(1);
        doctorInfEntity.setReject(reject);
        serviceConnector.sendSMS(doctorInfEntity.getPhone(),
                "ISOFHCare xin thong bao yeu cau dang ky của ban khong duoc chap nhan. Vui long lien he truc tiep 08 888 222 88 neu ban co thac mac. Xin cam on.");
        save(doctorInfEntity);
        return ok(getMapData(doctorInfEntity));
    }


    public ResultEntity search(Integer page, Integer size,String queryString, Long specialistId, Integer active, Integer type) throws Exception {
        init();
        Pageable pageable = null;
        switch (type) {
            case 1:
                pageable = PageRequest.of(page - 1, size, Sort.by("col_name").descending());
                break;

            case 2:
                pageable = PageRequest.of(page - 1, size, Sort.by("col_email").descending());
                break;

            case 3:
                pageable = PageRequest.of(page - 1, size, Sort.by("col_name").ascending());
                break;

            case 4:
                pageable = PageRequest.of(page - 1, size, Sort.by("col_email").ascending());
                break;
            case 5:
                pageable = PageRequest.of(page - 1, size, Sort.by("created_date").ascending());
                break;
            case 6:
                pageable = PageRequest.of(page - 1, size, Sort.by("created_date").descending());
                break;
            default:
                pageable = getDefaultPage(page, size);
        }
        Page<DoctorInfEntity> results = doctorInfRepository.search(queryString, specialistId,active, pageable);
        ResultEntity result = ok(results.getTotalElements(), getMapDatas(results));
        return result;
    }


}
