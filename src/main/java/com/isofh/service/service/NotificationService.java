package com.isofh.service.service;

import com.isofh.service.model.NotificationEntity;
import com.isofh.service.model.ResultEntity;
import com.isofh.service.model.UserEntity;
import com.isofh.service.result.IMapData;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class NotificationService extends BaseService implements IMapData<NotificationEntity> {
    @Override
    public Map<String, Object> getMapData(NotificationEntity entity) {
        Map<String, Object> mapData = new HashMap<>();
        mapData.put(NOTIFICATION, entity);
        mapData.put(USER, getMapDataUser(entity.getUser()));
        return mapData;
    }
    
    public Map<String, Object> getMapDataUser(UserEntity entity) {
        Map<String, Object> mapData = new HashMap<>();
        mapData.put("name", entity.getName());
        mapData.put("id", entity.getId());
        mapData.put("avatar", entity.getAvatar());
        return mapData;
    }

    @Override
    public List<Map<String, Object>> getMapDatas(Iterable<NotificationEntity> notifications) {
        List<Map<String, Object>> mapResults = new ArrayList<>();
        for (NotificationEntity notification : notifications) {
            mapResults.add(getMapData(notification));
        }
        return mapResults;
    }

    @Transactional
    public ResultEntity search(Integer watched, Integer page, Integer size) throws Exception {
        init();
        Page<NotificationEntity> entity = notificationRepository.search(userId, watched, getDefaultPage(page, size));
        return ok(getMapDatas(entity));
    }

    @Transactional
    public ResultEntity getDetail(long id) {
        init();
        NotificationEntity entity = getNotification(id);
        return ok(getMapData(entity));
    }

    @Transactional
    public ResultEntity activeWatched(long id) {
        init();
        NotificationEntity entity = getNotification(id);
        entity.setWatched(1);
        save(entity);
        return ok(getMapData(entity));
    }

    @Transactional
    public ResultEntity deleteAll() {
        init();
        try {
            List<NotificationEntity> notification = notificationRepository.findByUserDelete(userId);
            notificationRepository.deleteAll(notification);
            return ok(true);
        } catch (Exception e) {
            return ok(false);
        }

    }

    @Transactional
    public ResultEntity countNotification() {
        init();
        try {
            List<NotificationEntity> notificationList = notificationRepository.findByUser(userId);
            Map<String, Object> data = new HashMap<String, Object>();
            data.put("total", notificationList.size());
            return ok(data);
        } catch (Exception e) {
            return ok(false);
        }

    }
}
