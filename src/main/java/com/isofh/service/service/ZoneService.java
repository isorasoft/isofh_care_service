package com.isofh.service.service;

import com.isofh.service.model.DistrictEntity;
import com.isofh.service.model.ResultEntity;
import com.isofh.service.model.ZoneEntity;
import com.isofh.service.result.IMapData;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

@Service
public class ZoneService extends BaseService implements IMapData<String> {

    @Override
    public Map<String, Object> getMapData(String entity) {
        Map<String, Object> mapData = new HashMap<>();
        return mapData;
    }

    public ResultEntity getByDistrict(Long districtId) throws Exception {
        init();
        DistrictEntity districtEntity = getDistrict(districtId);
        Iterable<ZoneEntity> zones = zoneRepository.findByDistrictCode(districtEntity.getCode());
        return ok(ZONES, zones);
    }
}
