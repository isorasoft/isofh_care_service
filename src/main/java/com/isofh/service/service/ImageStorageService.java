package com.isofh.service.service;

import com.isofh.service.controller.ImageController;
import com.isofh.service.exception.FileStorageException;
import com.isofh.service.utils.FileUtils;
import com.isofh.service.utils.RandomUtils;
import com.isofh.service.utils.StrUtils;
import org.imgscalr.Scalr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.Map;

@Service
public class ImageStorageService {

    private static final Logger log = LoggerFactory.getLogger(ImageController.class);

    @Value("${image.upload-dir}")
    private String imagePath;

    @Value("${image.thumbnail.with}")
    private int thumbnailWidth;

    @Value("${image.thumbnail.height}")
    private int thumbnailHeight;

    public Map<String, String> storeImage(MultipartFile file) {

        File f = new File(imagePath);
        if (!f.exists()) {
            f.mkdirs();
        }

        Path imageLocation = Paths.get(imagePath);
        log.info("file " + file.getOriginalFilename());
        Map<String, String> images = new HashMap<>();
        // Normalize file name
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

        String fileExt = "";
        if (fileName.contains(".")) {
            fileExt = fileName.substring(fileName.lastIndexOf('.'));
            fileName = fileName.substring(0, fileName.lastIndexOf('.'));
            fileName = StrUtils.convertToUnsignedLowerCase(fileName);
            fileName = StrUtils.removeSpecialCharactor(fileName);
        }

        log.info("file Ext " + fileExt);
        if (StrUtils.isNullOrWhiteSpace(fileExt)) {
            fileExt = fileExt + ".png";
        }
        log.info("file Ext " + fileExt);
        try {
            // Check if the file's name contains invalid characters
            if (fileName.contains("..")) {
                fileName = "image";
            }

            String image = fileName + "_" + RandomUtils.getRandomId() + fileExt;
            images.put("image", "/images/" + image);

            // Copy file to the target location (Replacing existing file with the same name)
            Path targetLocation = imageLocation.resolve(image);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

            try {
                String imageThumbnail = fileName + "_" + RandomUtils.getRandomId() + ".png";
                File fileThumbnail = FileUtils.createFile(imageLocation.toString() + "/" + imageThumbnail);
                BufferedImage img2 = ImageIO.read(imageLocation.resolve(image).toFile());
                BufferedImage scaledImg = Scalr.resize(img2, Scalr.Method.QUALITY,
                        thumbnailWidth, thumbnailHeight, Scalr.OP_ANTIALIAS);
                ImageIO.write(scaledImg, "png", fileThumbnail);

                images.put("thumbnail", "/images/" + imageThumbnail);
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            return images;
        } catch (Exception ex) {
            log.error(ex.getMessage());
            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
        }
    }
}
