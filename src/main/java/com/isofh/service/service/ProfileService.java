package com.isofh.service.service;

import com.isofh.service.model.*;
import com.isofh.service.result.IMapData;
import com.isofh.service.utils.CustomException;
import com.isofh.service.utils.GsonUtils;
import com.isofh.service.utils.JWTokenUtils;
import com.isofh.service.utils.StrUtils;
import org.json.JSONObject;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class ProfileService extends BaseService implements IMapData<ProfileEntity> {

    @Override
    public Map<String, Object> getMapData(ProfileEntity entity) {
        Map<String, Object> mapData = new HashMap<>();
        mapData.put(PROFILE, entity);
        mapData.put(COUNTRY, entity.getCountry());
        mapData.put(PROVINCE, entity.getProvince());
        mapData.put(DISTRICT, entity.getDistrict());
        mapData.put(ZONE, entity.getZone());
        return mapData;
    }

    @Override
    public List<Map<String, Object>> getMapDatas(Iterable<ProfileEntity> entities) {
        List<Map<String, Object>> mapResults = new ArrayList<>();
        for (ProfileEntity entity : entities) {
            mapResults.add(getMapData(entity));
        }
        return mapResults;
    }

    public ResultEntity create(Object object) throws Exception {
        init(object);
        Long userId = getLong("userId");
        UserEntity user = getUser(userId);
        if (user == null) {
            throw getException(2, "userId = %d khong ton tai ", userId);
        }
        ProfileEntity profile = getObject(PROFILE, ProfileEntity.class);
        profile.setActive(1);
        profile.setUser(user);

        Long countryId = getLong("countryId");
        CountryEntity country = getCountry(countryId);
        profile.setCountry(country);

        Long provinceId = getLong("provinceId");
        ProvinceEntity province = getProvince(provinceId);
        profile.setProvince(province);

        Long districtId = getLong("districtId");
        DistrictEntity district = getDistrict(districtId);
        profile.setDistrict(district);

        Long zoneId = getLong("zoneId");
        ZoneEntity zone = getZone(zoneId);
        profile.setZone(zone);
        profile.updateAddress();
        if (!StrUtils.isNullOrWhiteSpace(profile.getPhone())) {
            profile.setPhone(StrUtils.removeSpaces(profile.getPhone()));
        }
        save(profile);
        if (!StrUtils.isNullOrWhiteSpace(profile.getPhone()) && StrUtils.isNullOrWhiteSpace(user.getPhone())) {
            user.setPhone(profile.getPhone());
            save(user);
        }
        return ok(PROFILE, profile);
    }

    public ResultEntity getByUser(long userId) throws Exception {
        init();
        validateUser();
        List<ProfileEntity> profiles = profileRepository.findByUserId(userId);
        return ok(PROFILES, getMapDatas(profiles));
    }

    public ResultEntity getDetail(long id) {
        init();
        ProfileEntity profile = getProfile(id);
        return ok(getMapData(profile));
    }
}
