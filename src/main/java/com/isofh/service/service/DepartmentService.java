package com.isofh.service.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.springframework.stereotype.Service;

import com.isofh.service.constant.ServiceConst;
import com.isofh.service.enums.HttpMethodType;
import com.isofh.service.model.DepartmentEntity;
import com.isofh.service.model.HospitalEntity;
import com.isofh.service.model.ResultEntity;
import com.isofh.service.model.ResultHisEntity;
import com.isofh.service.model.ScheduleEntity;
import com.isofh.service.result.IMapData;
import com.isofh.service.utils.CollectionUtils;
import com.isofh.service.utils.GsonUtils;
import com.isofh.service.utils.NetworkUtils;

@Service
public class DepartmentService extends BaseService implements IMapData<String> {

    @Override
    public Map<String, Object> getMapData(String entity) {
        Map<String, Object> mapData = new HashMap<>();
        return mapData;
    }

    public ResultEntity sync(long hospitalId) throws Exception {
        try {
            init();
            List<Long> oldDepartmentIds = new ArrayList<>();
            Iterable<DepartmentEntity> departments = departmentRepository.getAll(hospitalId);
            for (DepartmentEntity room : departments) {
                oldDepartmentIds.add(room.getId());
            }
            HospitalEntity hospitalEntity = getHospital(hospitalId);

            String queryData = NetworkUtils.callService(hospitalEntity.getServiceUrl() + ServiceConst.SYNC_DEPARTMENT,
                    HttpMethodType.GET);
            ResultHisEntity data = GsonUtils.toObject(queryData, ResultHisEntity.class);
            if (data == null || data.getCode() != 0) {
                throw getException(1, "His tra ve loi ");
            }

            for (int i = 0; i < data.getData().getDepartments().length; i++) {
                try {
                    DepartmentEntity entity = data.getData().getDepartments()[i];
                    entity.setHospital(hospitalEntity);
                    if (Objects.isNull(entity.getHisDepartmentId())) {
                        continue;
                    }
                    DepartmentEntity departmentEntity = departmentRepository.findByHisDepartmentId(entity.getHisDepartmentId(),
                            hospitalId);
                    if (departmentEntity != null) {
                        entity = departmentEntity;
                        oldDepartmentIds.remove(departmentEntity.getId());
                    }
                    save(entity);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            for (Long id : oldDepartmentIds) {
                DepartmentEntity departmentEntity = getDepartment(id);
                departmentEntity.setDeleted(1);
                save(departmentEntity);
                List<ScheduleEntity> deletedSchedules = scheduleRepository.findByDepartment(id);
                if (!CollectionUtils.isNullOrEmpty(deletedSchedules)) {
                    for (ScheduleEntity schedule : deletedSchedules) {
                        schedule.setDeleted(1);
                    }
                    scheduleRepository.saveAll(deletedSchedules);
                }
            }
            return ok();
        } catch (Exception ex) {
            throw ex;
        }
    }

    public ResultEntity getAll(long hospitalId) throws Exception {
        try {
            init();
            Iterable<DepartmentEntity> departments = departmentRepository.getAll(hospitalId);
            return ok(DEPARTMENTS, departments);
        } catch (Exception ex) {
            throw ex;
        }
    }

    public ResultEntity getDetail(long id) throws Exception {
        try {
            init();
            DepartmentEntity departmentEntity = getDepartment(id);
            return ok(DEPARTMENT, departmentEntity);
        } catch (Exception ex) {
            throw ex;
        }
    }
}
