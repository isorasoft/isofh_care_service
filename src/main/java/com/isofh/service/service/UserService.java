package com.isofh.service.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

import com.isofh.service.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.isofh.service.ServiceConnector;
import com.isofh.service.constant.AppConst;
import com.isofh.service.constant.field.SlideField;
import com.isofh.service.constant.field.UserField;
import com.isofh.service.enums.PermissionType;
import com.isofh.service.enums.SocialType;
import com.isofh.service.enums.UserSourceType;
import com.isofh.service.enums.UserType;
import com.isofh.service.result.IMapData;
import com.isofh.service.utils.ArrayUtils;
import com.isofh.service.utils.CollectionUtils;
import com.isofh.service.utils.CustomException;
import com.isofh.service.utils.EmailUtils;
import com.isofh.service.utils.GsonUtils;
import com.isofh.service.utils.JWTokenUtils;
import com.isofh.service.utils.RandomUtils;
import com.isofh.service.utils.StrUtils;

@Service
public class UserService extends BaseService implements IMapData<UserEntity> {

    @Autowired
    ServiceConnector serviceConnector;

    @Override
    public Map<String, Object> getMapData(UserEntity entity) {
        Map<String, Object> mapData = new HashMap<>();
        mapData.put(USER, entity);
        mapData.put(ROLE, entity.getRoleUser());
        mapData.put(SPECIALIST, entity.getSpecialist());
        mapData.put(USER_CREATE, entity.getUserCreate());
        mapData.put(USER_UPDATE, entity.getUserUpdate());
        mapData.put("hospitalByAdmin", entity.getHospital());
        mapData.put(PROFILE, entity.getProfile());
        mapData.put("hospitalByDoctor", entity.getHospitals());
        return mapData;
    }

    @Transactional
    public ResultEntity create(Object object) throws CustomException {
        init(object);
        UserEntity user = getObject(USER, UserEntity.class);
        if (user.getRole() == UserType.USER.getValue()) {
            validateUser(PermissionType.ADD_USER.getValue());
        } else if (user.getRole() == UserType.DOCTOR.getValue()) {
            validateUser(PermissionType.ADD_DOCTOR.getValue());
        } else if (user.getRole() == UserType.ADMIN.getValue()) {
            validateUser(PermissionType.ADD_ADMIN.getValue());
        }

        if (!StringUtils.isEmpty(user.getPhone())) {
            if (userRepository.findByPhone(user.getRole(), user.getPhone()) != null) {
                throw getException(2, "Phone da ton tai");
            }
        }

        // version demo
        if (user.getRole() == UserType.USER.getValue()) {
            user.setEmail(createEmailByUser(user.getPhone()));
        }
        // Neu co gui thong tin email, ktra xem email co bi trung khong
        if (!StringUtils.isEmpty(user.getEmail())) {
            if (userRepository.findByEmail(user.getRole(), user.getEmail()) != null) {
                throw getException(3, "Email da ton tai");
            }
        }
        // Neu co gui thong tin email, ktra xem email co bi trung khong
        if (!StringUtils.isEmpty(user.getCertificateCode())) {
            if (userRepository.findByCertificateCode(user.getCertificateCode()) != null) {
                throw getException(4, "Chung chi BS da ton tai");
            }
        }

        Long roleId = getLong("roleId");
        if (roleId != null) {
            RolesEntity role = getEntityById(rolesRepository, roleId);
            user.setRoleUser(role);
        }

        Long specialistId = getLong("specialistId");
        if (specialistId != null) {
            CommonSpecialistEntity specialist = getEntityById(commonSpecialistRepository, specialistId);
            user.setSpecialist(specialist);
        }

        if (StringUtils.isEmpty(user.getUsername())) {
            String username = createUsername(user.getName());
            user.setUsername(username);
        }

        if (userRepository.findByUsername(user.getUsername()) != null) {
            throw getException(5, "user name da ton tai");
        }

        user.setVerify(1);
        UserEntity userCreate = getCurrentUser();
        user.setUserCreate(userCreate);
        user.setUserUpdate(userCreate);
        save(user);

        Long[] facilityIds = getObject("facilityIds", Long[].class);
        if (!ArrayUtils.isNullOrEmpty(facilityIds)) {
            for (Long facilityId : facilityIds) {
                FacilityEntity facilityEntity = getFacility(facilityId);
                facilityEntity.setUserAdmin(user);
                save(facilityEntity);
                user.getFacilities().add(facilityEntity);
            }
        }
        Thread thread = new Thread(() -> {
            try {
                actionProfileUser(user, true);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
        thread.start();

        return ok(getMapData(user));
    }

    private String createUsername(String name) {
        int index = 0;
        String nickName = StrUtils.convertToUnsignedLowerCase(name).replace(" ", "");
        while (userRepository.findByUsername(nickName + (index == 0 ? "" : "" + index)) != null) {
            index++;
        }
        return nickName + (index == 0 ? "" : "" + index);
    }

    private String createEmailByUser(String phone) {
        String phoneMail = phone.replace(" ", "");

        return phoneMail + "@cdyt.com";
    }

    @Transactional
    public ResultEntity delete(Long id) throws Exception {
        init();
        userRepository.deleteById(id);
        return ok();
    }

    @Transactional
    public ResultEntity search(String queryString, int active, Long specialistId, int type, int style, Long userId,
                               Integer page, Integer size) throws Exception {
        init();

        // type 1 search doctor
        // type 2 search paintent
        Page<UserEntity> users = null;
        Pageable pageable = null;
        switch (style) {
            case 1:
                pageable = PageRequest.of(page - 1, size, Sort.by("name").descending());
                break;

            case 2:
                pageable = PageRequest.of(page - 1, size, Sort.by("created_date").descending());
                break;

            case 3:
                pageable = PageRequest.of(page - 1, size, Sort.by("created_date").ascending());
                break;

            case 4:
                pageable = PageRequest.of(page - 1, size, Sort.by("name").ascending());
                break;
            case 5:
                pageable = PageRequest.of(page - 1, size, Sort.by("username").ascending());
                break;
            case 6:
                pageable = PageRequest.of(page - 1, size, Sort.by("username").descending());
                break;
            case 7:
                pageable = PageRequest.of(page - 1, size, Sort.by("number_waiting_post").ascending());
                break;
            default:
                pageable = getDefaultPage(page, size);
        }
        if (type == 1) {
            validateUser(PermissionType.VIEW_DOCTOR.getValue());
            users = userRepository.searchByDoctor(queryString, active, specialistId, userId, pageable);
        } else if (type == 2) {
            validateUser(PermissionType.VIEW_USER.getValue());
            users = userRepository.searchByUser(queryString, active, userId, pageable);
        } else if (type == 3) {
            validateUser(PermissionType.VIEW_ADMIN.getValue());
            users = userRepository.searchByAdmin(queryString, active, userId, pageable);
        } else {
            throw getException(3, "Yêu cầu xác nhận type search");
        }

        List<Map<String, Object>> mapResults = new ArrayList<>();
        for (UserEntity user : users.getContent()) {
            mapResults.add(getMapData(user));
        }
        return ok(users.getTotalElements(), mapResults);
    }

    @Transactional
    public ResultEntity block(Object object, Long id) throws Exception {
        init(object);
        UserEntity userEntity = getUser(id);
        Integer blocked = getInteger("block");
        userEntity.setBlocked(blocked);
        userRepository.save(userEntity);
        return ok(USER, userEntity);
    }

    @Transactional
    public ResultEntity login(Object object) throws Exception {
        init(object);
        String emailOrPhone = getString("emailOrPhone");
        String password = getString(UserField.PASSWORD);
        DeviceEntity device = getObject(DEVICE, DeviceEntity.class);

        UserEntity user = userRepository.login(emailOrPhone, password, roleBase);
        if (user == null) {
            throw getException(3, "Email/Số điện thoại hoặc Mật khẩu không hợp lệ");
        } else if ((Objects.equals(user.getBlocked(), 1)) || (Objects.equals(user.getActive(), 0))) {
            throw getException(2, "Tài khoản đã bị khóa. Vui lòng liên hệ với Admin của Isofhcare");
        } else if (!Objects.equals(user.getVerify(), 1)) {
            throw getException(4, "Tài khoản chưa được kick hoạt. Vui lòng liên hệ với Admin của Isofhcare");
        }

        saveDeviceInfo(user);
//        user.setLoginToken(JWTokenUtils.generateToken(user.getId()));
        if (device == null) {
            user.setLoginToken(digitalSignature.generateToken(user.getId(), 0,
                    user.getHospital() != null ? user.getHospital().getId() : null));
        } else {
            user.setLoginToken(digitalSignature.generateToken(user.getId(), 1,
                    user.getHospital() != null ? user.getHospital().getId() : null));

        }

        return ok(getMapData(user));
    }

    private void saveDeviceInfo(UserEntity user) {
        DeviceEntity device = getObject(DEVICE, DeviceEntity.class);
        if (device != null && !StrUtils.isNullOrWhiteSpace(device.getDeviceId())) {
            deleteDeviceInfo(device.getDeviceId());
            device.setUser(user);
            deviceRepository.save(device);
        }
    }

    private void deleteDeviceInfo(String deviceId) {
        try {
            deviceRepository.deleteByDeviceId(deviceId);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Transactional
    public ResultEntity resetPassword(Object object, Long id) throws Exception {
        init(object);
        UserEntity userEntity = getUser(id);
        userEntity.setPassword(StrUtils.generateDefaultPassword());
        userRepository.save(userEntity);
        return ok(USER, userEntity);
    }

    @Transactional
    public ResultEntity register(Object object) throws Exception {
        init(object);
        // kiem tra ten dang nhap
        UserEntity entity = getObject(USER, UserEntity.class);

        String socialId = getString("socialId");

        String accessToken = getString("accessToken");

        String applicationId = getString("applicationId");

        FacebookResultEntity facebookToken = serviceConnector.accessTokenFB(accessToken);
        if (!applicationId.equals(facebookToken.getApplication().getId())
                || !facebookToken.getPhone().getNumber().replace("+84", "0").equals(entity.getPhone())) {
            throw getException(4, "validate token false");
        }
        int socialType = getInteger("socialType");
        if (socialType != SocialType.FACEBOOK.getValue() && socialType != SocialType.GOOGLE.getValue()) {

            if (!StringUtils.isEmpty(entity.getEmail())) {
                UserEntity checkEmail = userRepository.findByEmail(entity.getRole(), entity.getEmail());

                if (checkEmail != null) {
                    throw getException(6, "Email đã được sử dụng trong hệ thống. Vui lòng nhập lại.");
                }
            }

            if (!StringUtils.isEmpty(entity.getPhone())) {
                UserEntity checkPhone = userRepository.findByPhone(entity.getRole(), entity.getPhone());
                if (checkPhone != null) {
                    throw getException(9, "Số điện thoại đã được sử dụng trong hệ thống. Vui lòng nhập lại.");
                }
            }

            if (StringUtils.isEmpty(entity.getPhone()) && StringUtils.isEmpty(entity.getEmail())) {
                throw getException(2, "Các trường bắt buộc không được để trống!");
            }

            // version demo
            if (entity.getRole() == UserType.USER.getValue()) {
                entity.setEmail(createEmailByUser(entity.getPhone()));
            }

            entity.setSocialType(socialType);
            entity.setVerifyCode(RandomUtils.getRandomPasswordNumber());
            if (StringUtils.isEmpty(entity.getUsername())) {
                String username = createUsername(entity.getName());
                entity.setUsername(username);
            }
            if (userRepository.findByUsername(entity.getUsername()) != null) {
                throw getException(5, "user name da ton tai");
            }
            saveDeviceInfo(entity);
            entity.setVerify(1);
            save(entity);
//            entity.setLoginToken(JWTokenUtils.generateToken(entity.getId()));
            entity.setLoginToken(digitalSignature.generateToken(entity.getId(), 1,
                    entity.getHospital() != null ? entity.getHospital().getId() : null));
            Thread thread = new Thread(() -> {
                try {
                    actionProfileUser(entity, true);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            });
            thread.start();
            return ok(USER, entity);
        } else {
            if (!StringUtils.isEmpty(entity.getEmail())) {
                UserEntity checkEmail = userRepository.findByEmail(entity.getRole(), entity.getEmail());

                if (checkEmail != null) {
                    throw getException(6, "Email đã được sử dụng trong hệ thống. Vui lòng nhập lại.");
                }
            }

            if (!StringUtils.isEmpty(entity.getPhone())) {
                UserEntity checkPhone = userRepository.findByPhone(entity.getRole(), entity.getPhone());
                if (checkPhone != null) {
                    throw getException(9, "Số điện thoại đã được sử dụng trong hệ thống. Vui lòng nhập lại.");
                }
            }

            if (StringUtils.isEmpty(entity.getPhone()) && StringUtils.isEmpty(entity.getEmail())) {
                throw getException(2, "bat buoc nhap 1 trong 2 truong");
            }

            // version demo
            if (entity.getRole() == UserType.USER.getValue()) {
                entity.setEmail(createEmailByUser(entity.getPhone()));
            }
            // version demo
            if (entity.getRole() == UserType.USER.getValue()) {
                entity.setEmail(createEmailByUser(entity.getPhone()));
            }

            if (Objects.equals(socialType, SocialType.FACEBOOK.getValue())) {
                entity.setFacebookId(socialId);
            } else if (Objects.equals(socialType, SocialType.GOOGLE.getValue())) {
                entity.setGoogleId(socialId);
            }
            entity.setSocialType(socialType);
            entity.setVerifyCode(RandomUtils.getRandomPasswordNumber());
            if (StringUtils.isEmpty(entity.getUsername())) {
                String username = createUsername(entity.getName());
                entity.setUsername(username);
            }
            if (userRepository.findByUsername(entity.getUsername()) != null) {
                throw getException(5, "user name da ton tai");
            }
            saveDeviceInfo(entity);
            entity.setVerify(1);
            save(entity);
//            entity.setLoginToken(JWTokenUtils.generateToken(entity.getId()));
            entity.setLoginToken(digitalSignature.generateToken(entity.getId(), 1,
                    entity.getHospital() != null ? entity.getHospital().getId() : null));
            Thread thread = new Thread(() -> {
                try {
                    actionProfileUser(entity, true);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            });
            thread.start();
            return ok(USER, entity);
        }
    }

    @Transactional
    public ResultEntity active(Object object, long id) throws Exception {
        init(object);
        UserEntity entity = getUser(id);

        String code = getString("verifyCode");
        if (entity != null && Objects.equals(entity.getVerifyCode(), code)) {
            entity.setVerify(1);
        } else {
            throw getException(2, "verifyCode khong dung");
        }
        userRepository.save(entity);
        return ok(Arrays.asList(USER), Arrays.asList(entity));
    }

    @Transactional
    public ResultEntity activeEmail(String token) throws Exception {
        init();
        if (StringUtils.isEmpty(token)) {
            throw getException(2, "token khong de trong");
        }
        UserEntity entity = userRepository.findByEmailToken(token);
        if (entity == null) {
            throw getException(3, "token khong hop le");
        }
        entity.setVerify(1);
        entity.setEmailToken("");
        userRepository.save(entity);
        return ok(Arrays.asList(USER), Arrays.asList(entity));
    }

    @Transactional
    public ResultEntity loginSocial(Object object) throws Exception {
        init(object);
        String socialId = getString("socialId");
        String name = getString("name");
        int socialType = getInteger("socialType");
        if (socialType != SocialType.FACEBOOK.getValue() && socialType != SocialType.GOOGLE.getValue()) {
            throw getException(1, "SocialType is not valid");
        }

        if (StrUtils.isNullOrWhiteSpace(socialId, name)) {
            throw getException(2, "socialId and Fullname is not valid");
        }

        UserEntity entity = null;
        if (Objects.equals(socialType, SocialType.GOOGLE.getValue())) {
            entity = userRepository.findByGoogleIdAndRole(socialId, roleBase);
        }

        if (Objects.equals(socialType, SocialType.FACEBOOK.getValue())) {
            entity = userRepository.findByFacebookIdAndRole(socialId, roleBase);
        }

        /**
         * social nay chua dang nhap lan nao
         */
        if (entity == null) {
            throw getException(3, "Tai khoan nay chua duoc tao");

        } else {
            if (Objects.equals(1, entity.getBlocked())) {
                throw getException(3, "Tai khoan nay da bi khoa");
            } else if (!Objects.equals(entity.getVerify(), 1)) {
                throw getException(4, "tai khoan chua duoc kich hoat");
            }
            entity.setLastLogin(LocalDateTime.now());
//            entity.setLoginToken(JWTokenUtils.generateToken(entity.getId()));
            entity.setLoginToken(digitalSignature.generateToken(entity.getId(), 1,
                    entity.getHospital() != null ? entity.getHospital().getId() : null));
            userRepository.save(entity);
            saveDeviceInfo(entity);
            return ok(USER, entity);
        }
    }

    @Transactional
    public ResultEntity forgetPassword(Object object) throws Exception {
        init(object);
        String emailOrPhone = getString("emailOrPhone");
        Integer type = getInteger("type");
        if (StringUtils.isEmpty(emailOrPhone)) {
            throw getException(2, "emailOrPhone is empty");
        }

        if (Objects.equals(type, 1)) {
            sendEmailForgetPassword(emailOrPhone, false);
        } else if (Objects.equals(type, 2)) {
            sendSmsForgetPassword(emailOrPhone);
        } else if (Objects.equals(type, 3)) {
            sendEmailForgetPassword(emailOrPhone, true);
        } else {
            throw getException(2, "type invalid");
        }
        return ok();
    }

    @Transactional
    public ResultEntity refreshPasswordByToken(Object object) throws Exception {
        init(object);

        String phone = getString("phone");

        String accessToken = getString("accessToken");

        String applicationId = getString("applicationId");

        String newPassword = getString("newPassword");

        FacebookResultEntity facebookToken = serviceConnector.accessTokenFB(accessToken);
        if (!applicationId.equals(facebookToken.getApplication().getId())
                || !facebookToken.getPhone().getNumber().replace("+84", "0").equals(phone)) {
            throw getException(4, "validate token false");
        }

        UserEntity user = userRepository.findByPhone(roleBase, phone);
        user.setPassword(newPassword);

        save(user);
        return ok(USER, user);
    }

    @Transactional
    public ResultEntity refreshPhoneByToken(Object object) throws Exception {
        init(object);

        String phone = getString("phone");

        String accessToken = getString("accessToken");

        String applicationId = getString("applicationId");

        String newPhone = getString("newPhone");

        FacebookResultEntity facebookToken = serviceConnector.accessTokenFB(accessToken);
        if (!applicationId.equals(facebookToken.getApplication().getId())
                || !facebookToken.getPhone().getNumber().replace("+84", "0").equals(phone)) {
            throw getException(4, "validate token false");
        }

        UserEntity user = userRepository.findByPhone(roleBase, phone);
        user.setPassword(newPhone);

        save(user);
        return ok(USER, user);
    }

    private void sendSmsForgetPassword(String phone) {
        UserEntity user = userRepository.findByPhone(roleBase, phone);
        String code = RandomUtils.getRandomPasswordNumber();
        SmsEntity smsEntity = new SmsEntity();
        smsEntity.setPhone(phone);
        smsEntity.setContent(code + " là mã xác xác minh khoản ISOFH_CARE của bạn");
        user.setResetPasswordCode(code);
        smsRepository.save(smsEntity);
        userRepository.save(user);
    }

    private void sendEmailForgetPassword(String email, boolean isAdmin) throws CustomException {
        UserEntity user = userRepository.findByEmail(roleBase, email);
        if (user == null) {
            throw getException(2, "Not found user with email: ");
        }
        String code = RandomUtils.getRandomPasswordNumber();
        String token = RandomUtils.getRandomId();
        TokenPasswordEntity tokenPasswordEntity = new TokenPasswordEntity();
        tokenPasswordEntity.setExpiredDate(LocalDateTime.now().plusDays(3));
        tokenPasswordEntity.setToken(token);
        tokenPasswordEntity.setUser(user);
        user.setResetPasswordCode(code);
        tokenPasswordRepository.save(tokenPasswordEntity);
        userRepository.save(user);
        String link = AppConst.WEB_ADDRESS + String.format("/user/recovery/%s", tokenPasswordEntity.getToken());
        String appName = AppConst.APP_NAME;
        String subject = String.format("Thông báo tài khoản - Từ ứng dụng %s", appName);
        if (isAdmin) {
            EmailUtils.sendEmail(email, subject, EmailUtils.getBodyResetPassword(user.getName(), link, null));
        } else {
            EmailUtils.sendEmail(email, subject, EmailUtils.getBodyResetPassword(user.getName(), link, code));
        }

    }

    @Transactional
    public ResultEntity addFacility(Object object, long id) throws Exception {
        init(object);
        UserEntity entity = getUser(id);
        Long[] facilityIds = getObject("facilityIds", Long[].class);
        if (!ArrayUtils.isNullOrEmpty(facilityIds)) {
            for (Long facilityId : facilityIds) {
                FacilityEntity facilityEntity = getFacility(facilityId);
                facilityEntity.setUserAdmin(entity);
                save(facilityEntity);
            }
        }
        return ok(Arrays.asList(USER, FACILITIES), Arrays.asList(entity, entity.getFacilities()));
    }

    @Transactional
    public ResultEntity removeFacility(Object object, long id) throws Exception {
        init(object);
        Long[] facilityIds = getObject("facilityIds", Long[].class);
        if (!ArrayUtils.isNullOrEmpty(facilityIds)) {
            for (Long facilityId : facilityIds) {
                FacilityEntity facilityEntity = getFacility(facilityId);
                if (facilityEntity != null && facilityEntity.getUserAdmin() != null
                        && Objects.equals(id, facilityEntity.getUserAdmin().getId())) {
                    facilityEntity.setUserAdmin(null);
                    save(facilityEntity);
                }
            }
        }
        return ok();
    }

    @Transactional
    public ResultEntity getDetail(Long id) throws Exception {
        init();
        UserEntity userEntity = getUser(id);
        System.out.println(GsonUtils.toString(userEntity));
        return ok(getMapData(userEntity));
    }

    @Transactional
    public ResultEntity clearEmail(String email) throws Exception {
        init();
        UserEntity userEntity = userRepository.findByEmail(roleBase, email);
        userEntity.setEmail("");
        save(userEntity);
        return ok();
    }

    @Transactional
    public ResultEntity checkUsedPhone(String phone) throws Exception {
        init();
        ResultEntity result = new ResultEntity();
        if (userRepository.findByPhone(roleBase, phone) != null) {
            result.setCode(2);
            result.setData(true);
            result.setMessage("used phone");
            return result;
        } else {
            result.setCode(2);
            result.setData(false);
            result.setMessage("no used phone");
            return result;
        }
    }

    @Transactional
    public ResultEntity confirmCode(Object object) throws Exception {
        init(object);
        String code = getString("code");
        String phoneOrMail = getString("phoneOrMail");
        // TODO
        UserEntity entity = userRepository.findByPhoneOrMail(phoneOrMail, roleBase);
        if (entity == null || !Objects.equals(code, entity.getResetPasswordCode())) {
            throw getException(2, "Ma xac thuc khong dung");

        }
        entity.setResetPasswordCode("");
        return ok(USER, entity);
    }

    @Transactional
    public ResultEntity updatePassword(long id, Object object) throws Exception {
        init(object);
        validateUser(PermissionType.SET_PASS_ADMIN.getValue());
        String passwordOld = getString("passwordOld");
        String passwordNew = getString("passwordNew");
        UserEntity entity = getUser(id);
        if (roleBase != UserType.ADMIN.getValue()) {
            if (!entity.getPassword().equals(passwordOld)) {
                throw getException(2, "Sai password cu");
            }
        }
        entity.setPassword(passwordNew);
        userRepository.save(entity);

        Thread thread = new Thread(() -> {
            List<TokenPasswordEntity> tokens = tokenPasswordRepository.findByUserId(id);
            if (!CollectionUtils.isNullOrEmpty(tokens)) {
                for (TokenPasswordEntity tokenEntity : tokens) {
                    tokenEntity.setExpiredDate(LocalDateTime.now());
                    tokenPasswordRepository.save(tokenEntity);
                }
            }
        });
        thread.start();
        // xoa cac token cu cua user nay di
        return ok(USER, entity);
    }

    @Transactional
    public ResultEntity verifySmsCode(Object object, Long id) throws Exception {
        init(object);
        UserEntity userEntity = getUser(id);
        String verifyCode = getString("verifyCode");
        if (StringUtils.isEmpty(verifyCode)) {
            throw getException(2, "ma xac thuc trong");
        }
        if (!verifyCode.equals(userEntity.getVerifyCode())) {
            throw getException(3, "ma xac thuc khong dung");
        }
        userEntity.setVerify(1);
        save(userEntity);
        return ok(USER, userEntity);
    }

    @Transactional
    public ResultEntity updateEmail(Object object, long id) throws Exception {
        init(object);
        UserEntity entity = getUser(id);
        String email = getString("email");
        UserEntity checkUser = userRepository.findByEmail(entity.getRole(), email);
        if (checkUser != null) {
            if (checkUser.getId() == entity.getId()) {
                throw getException(1, "Email dang la cua user nay");
            } else {
                throw getException(2, "Ton tai tai khoan chua email nay");
            }
        }
        // chua co user nao co email nay, tao email token
        EmailTokenEntity tokenEntity = new EmailTokenEntity();
        tokenEntity.setEmail(email);
        tokenEntity.setToken(RandomUtils.getRandomId());
        tokenEntity.setUser(entity);
        save(tokenEntity);

        String link = AppConst.WEB_ADDRESS + "/user/confirm-email/" + tokenEntity.getToken();
        String emailBody = EmailUtils.getBodyVerifyEmail(entity.getName(), entity.getUsername(), link);
        save(new EmailEntity(email, "Cập nhập thông tin Email", emailBody));
        return ok(USER, entity);
    }

    @Transactional
    public ResultEntity update(Object object, long id) throws Exception {
        init(object);
        UserEntity entity = getUser(id);
        if (entity.getRole() == UserType.USER.getValue()) {
            validateUser(PermissionType.UPDATE_USER.getValue());
        } else if (entity.getRole() == UserType.DOCTOR.getValue()) {
            validateUser(PermissionType.UPDATE_DOCTOR.getValue());
        } else if (entity.getRole() == UserType.ADMIN.getValue()) {
            validateUser(PermissionType.UPDATE_ADMIN.getValue());
        }

        UserEntity data = getObject(USER, UserEntity.class);
        Long specialistId = getLong("specialistId");
        entity.setName(data.getName());
        entity.setDob(data.getDob());
        entity.setGender(data.getGender());
        entity.setImage(data.getImage());
        entity.setAvartar(data.getAvartar());
        entity.setDegree(data.getDegree());
        if (specialistId != null) {
            CommonSpecialistEntity specialist = getEntityById(commonSpecialistRepository, specialistId);
            entity.setSpecialist(specialist);
        }
        Long roleId = getLong("roleId");
        if (roleId != null) {
            RolesEntity role = getEntityById(rolesRepository, roleId);
            entity.setRoleUser(role);
        } else {
            entity.setRoleUser(null);
        }
        entity.setAddress(data.getAddress());
        entity.setIntroduct(data.getIntroduct());
        entity.setTitle(data.getTitle());
        if (!StringUtils.isEmpty(data.getCertificateCode())) {
            UserEntity userCheckCertificate = userRepository.findByCertificateCode(data.getCertificateCode());
            if (userRepository.findByCertificateCode(data.getCertificateCode()) != null
                    && userCheckCertificate.getId() != id) {
                throw getException(4, "Chung chi BS da ton tai");
            } else {
                entity.setCertificateCode(data.getCertificateCode());
            }
        }

        entity.setPassport(data.getPassport());
        entity.setDateRangePassPort(data.getDateRangePassPort());
        entity.setPlacePassPort(data.getPlacePassPort());
        // version demo
        if (entity.getRole() != UserType.USER.getValue()) {
            UserEntity userCheckPhone = userRepository.findByPhone(entity.getRole(), data.getPhone());
            if (userCheckPhone != null && userCheckPhone.getId() != id) {
                throw getException(2, "phone da ton tai");
            } else {
                entity.setPhone(data.getPhone());
            }
        }
        UserEntity userCheckMail = userRepository.findByEmail(entity.getRole(), data.getEmail());
        if (userCheckMail != null && userCheckMail.getId() != id) {
            throw getException(3, "mail da ton tai");
        } else {
            entity.setEmail(data.getEmail());
        }

        UserEntity userUpdate = getCurrentUser();
        entity.setUserUpdate(userUpdate);
        entity.setNote(data.getNote());

        UserEntity userChecUsername = userRepository.findByUsername(data.getUsername());
        if (userRepository.findByUsername(data.getUsername()) != null && userChecUsername.getId() != id) {
            throw getException(5, "user name da ton tai");
        } else {
            entity.setUsername(data.getUsername());
        }
        save(entity);
        Thread thread = new Thread(() -> {
            try {
                actionProfileUser(entity, false);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
        thread.start();
        return ok(USER, entity);
    }

    @Transactional
    public ResultEntity token(Long userId) throws Exception {
        init();
        return ok("token", JWTokenUtils.generateToken(userId));
    }

// tao bac si

    @Transactional
    public ResultEntity createDoctor(Object object) throws CustomException {
        init(object);
        UserEntity entity = getObject(DOCTOR, UserEntity.class);

        if ((!StringUtils.isEmpty(entity.getPhone())) && (!StringUtils.isEmpty(entity.getEmail()))
                && (!StringUtils.isEmpty(entity.getCertificateCode()))) {
            UserEntity checkEmailDoctor = userRepository.findByEmail(UserType.DOCTOR.getValue(), entity.getEmail());
            UserEntity checkPhoneDoctor = userRepository.findByPhone(UserType.DOCTOR.getValue(), entity.getPhone());
            UserEntity checkCertificateCodeDoctor = userRepository.findByCertificateCode(entity.getCertificateCode());

            if (checkCertificateCodeDoctor != null) {
                if (checkEmailDoctor != null) {
                    if (checkPhoneDoctor != null) {
                        throw getException(9,
                                "Số điện thoại, email và chứng chỉ đã được sử dụng trong hệ thống. Vui lòng nhập lại.");
                    } else {
                        throw getException(6, "Email và chứng chỉ đã được sử dụng trong hệ thống. Vui lòng nhập lại.");
                    }
                } else if (checkPhoneDoctor != null) {
                    throw getException(8,
                            "Số điện thoại và chứng chỉ đã được sử dụng trong hệ thống. Vui lòng nhập lại.");
                } else {
                    throw getException(4, "Số chứng chỉ đã được sử dụng trong hệ thống. Vui lòng nhập lại.");
                }
            } else if (checkPhoneDoctor != null) {
                if (checkEmailDoctor != null) {
                    throw getException(7, "Email và số điện thoại đã được sử dụng trong hệ thống. Vui lòng nhập lại.");
                } else {
                    throw getException(2, "Số điện thoại đã được sử dụng trong hệ thống. Vui lòng nhập lại.");
                }
            } else if (checkEmailDoctor != null) {
                throw getException(3, "Email đã được sử dụng trong hệ thống. Vui lòng nhập lại.");
            }
        } else {
            throw getException(10, "Các trường bắt buộc không được để trống!");
        }

        if (userRepository.findByUsername(entity.getUsername()) != null) {
            throw getException(5, "user name da ton tai");
        }

        Long specialistId = getLong("specialistId");
        CommonSpecialistEntity commonSpecialistEntity = getCommonSpecialist(specialistId);
        entity.setSpecialist(commonSpecialistEntity);
        entity.setActive(1);
        save(entity);

        return ok(getMapData(entity));

    }

    @Transactional
    public ResultEntity updateDoctor(long id, Object object) throws CustomException {
        init(object);
        UserEntity data = getObject(DOCTOR, UserEntity.class);
        UserEntity entity = getUser(id);

        if ((!StringUtils.isEmpty(data.getPhone())) && (!StringUtils.isEmpty(data.getEmail()))
                && (!StringUtils.isEmpty(data.getCertificateCode()))) {
            UserEntity checkEmailDoctor = userRepository.findByEmail(UserType.DOCTOR.getValue(), data.getEmail());
            UserEntity checkPhoneDoctor = userRepository.findByPhone(UserType.DOCTOR.getValue(), data.getPhone());
            UserEntity checkCertificateCodeDoctor = userRepository.findByCertificateCode(data.getCertificateCode());

            if (checkCertificateCodeDoctor != null && checkCertificateCodeDoctor.getId() != id) {
                if (checkEmailDoctor != null && checkEmailDoctor.getId() != id) {
                    if (checkPhoneDoctor != null && checkPhoneDoctor.getId() != id) {
                        throw getException(9,
                                "Số điện thoại, email và chứng chỉ đã được sử dụng trong hệ thống. Vui lòng nhập lại.");
                    } else {
                        throw getException(6, "Email và chứng chỉ đã được sử dụng trong hệ thống. Vui lòng nhập lại.");
                    }
                } else if (checkPhoneDoctor != null && checkPhoneDoctor.getId() != id) {
                    throw getException(8,
                            "Số điện thoại và chứng chỉ đã được sử dụng trong hệ thống. Vui lòng nhập lại.");
                } else {
                    throw getException(4, "Số chứng chỉ đã được sử dụng trong hệ thống. Vui lòng nhập lại.");
                }
            } else if (checkPhoneDoctor != null && checkPhoneDoctor.getId() != id) {
                if (checkEmailDoctor != null && checkEmailDoctor.getId() != id) {
                    throw getException(7, "Email và số điện thoại đã được sử dụng trong hệ thống. Vui lòng nhập lại.");
                } else {
                    throw getException(2, "Số điện thoại đã được sử dụng trong hệ thống. Vui lòng nhập lại.");
                }
            } else if (checkEmailDoctor != null && checkEmailDoctor.getId() != id) {
                throw getException(3, "Email đã được sử dụng trong hệ thống. Vui lòng nhập lại.");
            }
        } else {
            throw getException(10, "Các trường bắt buộc không được để trống!");
        }
        entity.setName(data.getName());
        entity.setAddress(data.getAddress());
        entity.setGender(data.getGender());
        entity.setDob(data.getDob());
        entity.setImage(data.getImage());
        entity.setPhone(data.getPhone());
        entity.setRole(data.getRole());
        entity.setEmail(data.getEmail());
        entity.setDegree(data.getDegree());
        entity.setTitle(data.getTitle());
        entity.setIntroduct(data.getIntroduct());
        entity.setCertificateCode(data.getCertificateCode());

        Long specialistId = getLong("specialistId");
        CommonSpecialistEntity commonSpecialistEntity = getCommonSpecialist(specialistId);
        entity.setSpecialist(commonSpecialistEntity);
        save(entity);

        return ok(getMapData(entity));

    }

    @Transactional
    public ResultEntity getDetailDoctor(long id) {
        init();
        UserEntity dotorEntity = getUser(id);
        return ok(getMapData(dotorEntity));
    }

    @Transactional
    public ResultEntity setActive(Object object, long id) throws Exception {
        init(object);
        Integer active = getInteger(SlideField.ACTIVE);
        UserEntity entity = getUser(id);
        entity.setActive(active);
        userRepository.save(entity);
        return ok(getMapData(entity));
    }

    @Transactional
    public ResultEntity getListBySpecialist(long id, Integer page, Integer size, Integer type) throws Exception {
        init();
        Pageable pageable = null;
        switch (type) {
            case 1:
                pageable = PageRequest.of(page - 1, size, Sort.by("number_waiting_post").descending());
                break;

            case 2:
                pageable = PageRequest.of(page - 1, size, Sort.by("number_waiting_post").ascending());
                break;

            case 3:
                pageable = PageRequest.of(page - 1, size, Sort.by("u.name").descending());
                break;

            case 4:
                pageable = PageRequest.of(page - 1, size, Sort.by("u.name").ascending());
                break;

            default:
                pageable = getDefaultPage(page, size);
        }

        Page<UserEntity> users = userRepository.getListBySpecialist(id, pageable);
        List<Map<String, Object>> mapResults = new ArrayList<>();
        for (UserEntity user : users.getContent()) {
            mapResults.add(getMapData(user));
        }
        return ok(users.getTotalElements(), mapResults);

    }

    @SuppressWarnings("unchecked")
    @Transactional
    public ResultEntity createByHis(Object object) throws Exception {

        init(object);

        // value la ma benh nhan tren his
        String hisPatientId = getString("hisPatientId");
        HospitalEntity hospital = hospitalRepository.findUid(hospitalUid);

//        // benh nhan da ton tai
//        if (profile != null && profile.getUser() != null) { 
//            UserEntity user = profile.getUser();
//            if (StrUtils.isNullOrWhiteSpace(profile.getUid())) {
//                profile.setUid(UUID.randomUUID().toString());
//                save(profile);
//            }
////            serviceConnector.sendSMS(profile.getPhone(),
////                    "ISOFHCare xin trân trọng thông báo bạn đã có tài khoản trong ISOFHCare với tài khoản "
////                            + user.getUsername() + ". Xin cám ơn.");
//            HospitalProfileEntity hospitalProfile = hospitalProfileRepository.findByProfileHospital(profile.getId(),
//                    hospital.getId());
//            if (hospitalProfile == null) {
//                hospitalProfile = new HospitalProfileEntity();
//                hospitalProfile.setProfile(profile);
//                hospitalProfile.setHospital(hospital);
//                hospitalProfile.setHisPatientId(hisPatientId);
//                hospitalProfile.setEmail(user.getEmail());
//                save(hospitalProfile);
//            }
//            return (ok(Arrays.asList("username", "password", "isofhCareId"),
//                    Arrays.asList(user.getUsername(), user.getPassword(), profile.getUid())));
//        } else {
        // xoa profile cu neu k co user
//        if (profile != null) {
//            profileRepository.deleteById(profile.getId());
//        }
        // chua co profile nao voi value nay
        Map<String, Object> result = createUser(UserSourceType.HIS.getValue(), hisPatientId);
        HospitalProfileEntity hospitalProfile = hospitalProfileRepository.findByProfileHospital(
                Long.valueOf(((ProfileEntity) (result.get("profile"))).getId()), hospital.getId());
        if (hospitalProfile == null) {
            hospitalProfile = new HospitalProfileEntity();
            hospitalProfile.setProfile((ProfileEntity) (result.get("profile")));
            hospitalProfile.setHospital(hospital);
            hospitalProfile.setHisPatientId(hisPatientId);
            hospitalProfile.setEmail(((UserEntity) (result.get("user"))).getEmail());
            save(hospitalProfile);
        }
        Map<String, Object> returnResult = getResult(Arrays.asList("username", "password", "isofhCareId"),
                Arrays.asList(((UserEntity) (result.get("user"))).getUsername(), result.get("password"),
                        ((ProfileEntity) (result.get("profile"))).getUid()));
//            ProfileEntity profileCheck = (ProfileEntity) result.get("profile");
        return (ok(returnResult));

    }

    /**
     * Tao moi profile va tai khoan, tra ve thong tin dang nhap
     *
     * @param userSource
     * @return
     */
    private synchronized Map<String, Object> createUser(int userSource, String hisPatientId) throws Exception {

        log.info("create user userSource = " + userSource + " hisPatientId " + hisPatientId);
        // New new profile
        ProfileEntity profileData = getObject("profile", ProfileEntity.class);

        String phone = StrUtils.removeSpaces(profileData.getPhone());

        // kiem tra xem email nay da ton tai chua
        if (!StrUtils.isNullOrWhiteSpace(phone)) {
            UserEntity checkUser = userRepository.findByPhone(UserType.USER.getValue(), phone);
            if (checkUser != null) {

                ProfileEntity profile = checkUser.getProfile();
                if (profile == null) {
                    profile = profileData;
                    profile.setHisPatientId(hisPatientId);
                    profile.setAutoCreated(1);
                    profile.setUser(checkUser);
                    profile.setActive(1);

                    String countryCode = getString("countryCode");
                    CountryEntity country = countryRepository.findFirstByCode(countryCode);
                    profile.setCountry(country);

                    String provinceCode = getString("provinceCode");
                    ProvinceEntity province = provinceRepository.findFirstByCode(provinceCode);
                    profile.setProvince(province);

                    String districtCode = getString("districtCode");
                    DistrictEntity district = districtRepository.findFirstByCode(districtCode);
                    profile.setDistrict(district);

                    String zoneCode = getString("zoneCode");
                    ZoneEntity zone = zoneRepository.findFirstByCode(zoneCode);
                    profile.setZone(zone);
                    profile.updateAddress();

                    save(profileData);
                } else {
                    if (profile.getHisPatientId() == null) {
                        profile.setHisPatientId(hisPatientId);
                    }
                }

                if (profile.getUid() == null) {
                    profile.setUid(UUID.randomUUID().toString());
                }
                save(profile);
                serviceConnector.sendSMS(profile.getPhone(),
                        "ISOFHCare xin trân trọng thông báo bạn đã có tài khoản trong ISOFHCare với tài khoản "
                                + checkUser.getPhone() + ". Xin cám ơn.");
                Map<String, Object> mapResult = new HashMap<>();
                mapResult.put("user", checkUser);
                mapResult.put("password", checkUser.getPassword());
                mapResult.put("profile", profile);
                return mapResult;
            } else {
                UserEntity user = new UserEntity();
                user.setName(profileData.getName());
                user.setSource(userSource);
                String username = createUsername(user.getName());
                user.setUsername(username);
                user.setPhone(phone);
                user.setDob(profileData.getDob());
                user.setAddress(profileData.getAddress());
                user.setGender(profileData.getGender());
                user.setEmail(createEmailByUser(user.getPhone()));
//                String password = "12345678";
                String password = RandomUtils.getRandomPasswordNumber();
                String encryptPassword = StrUtils.toMD5(password);
                user.setPassword(encryptPassword);
                user.setRole(UserType.USER.getValue());
                user.setVerify(1);
                save(user);
                //
//                // chi tao tai khoan khi email khac empty va null
//                if (!StringUtils.isEmpty(user.getEmail()) && !user.getEmail().endsWith("@cdyt.vn")) {
//                    createIsofhCareAccount(user);
//                }

                // Add profile info
                profileData.setHisPatientId(hisPatientId);
                profileData.setAutoCreated(1);
                profileData.setUser(user);
                profileData.setActive(1);

                String countryCode = getString("countryCode");
                CountryEntity country = countryRepository.findFirstByCode(countryCode);
                profileData.setCountry(country);

                String provinceCode = getString("provinceCode");
                ProvinceEntity province = provinceRepository.findFirstByCode(provinceCode);
                profileData.setProvince(province);

                String districtCode = getString("districtCode");
                DistrictEntity district = districtRepository.findFirstByCode(districtCode);
                profileData.setDistrict(district);

                String zoneCode = getString("zoneCode");
                ZoneEntity zone = zoneRepository.findFirstByCode(zoneCode);
                profileData.setZone(zone);
                profileData.updateAddress();

                save(profileData);
                serviceConnector.sendSMS(profileData.getPhone(), "ISOFHCare xin trân trọng thông báo tài khoản "
                        + user.getPhone() + " có mật khẩu là " + password + ". Xin cám ơn.");
                Map<String, Object> mapResult = new HashMap<>();
                mapResult.put("user", user);
                mapResult.put("password", password);
                mapResult.put("profile", profileData);
                return mapResult;
            }
        } else {
            throw getException(2, "Tai khoan khong co số điện thoại.");
        }


    }

    private String createEmail(String phone) {
        int count = 0;
        String newEmail = phone + "@cdyt.vn";
        UserEntity checkUser = userRepository.findFirstByEmail(newEmail);
        // neu nguoi dung any van ton tai
        while (checkUser != null) {
            newEmail = phone + count + "@cdyt.vn";
            checkUser = userRepository.findFirstByEmail(newEmail);
        }
        return newEmail;
    }

    @Transactional
    public ResultEntity logout(long userId) throws Exception {
        init();
        UserEntity userEntity = getUser(userId);
        userEntity.setLoginToken("");
        List<DeviceEntity> device = deviceRepository.findAllByUser(userId);
        for (DeviceEntity i : device) {
            deleteDeviceInfo(i.getDeviceId());
        }

        return ok();
    }

    @Transactional
    public ResultEntity updateHospital(Object object, long userId) throws Exception {
        init(object);
        UserEntity entity = getUser(userId);
        entity.getHospitals().clear();
        Long[] hospitalIds = getObject("hospitalIds", Long[].class);
        for (Long hospitalId : hospitalIds) {
            HospitalEntity hospitalEntity = getHospital(hospitalId);
            if (hospitalEntity != null) {
                entity.getHospitals().add(hospitalEntity);
            }
        }
        save(entity);
        return ok(getMapData(entity));
    }

    public ResultEntity getHospitalByCurrentUser() throws Exception {
        init();
        UserEntity user = getCurrentUser();
        if (user.getProfile() == null) {
            throw getException(2, "khong co profile");
        }
        List<HospitalEntity> hospitals = hospitalRepository.findByProfileHospital(user.getProfile().getId());
        return ok("hospitals", hospitals);
    }
}
