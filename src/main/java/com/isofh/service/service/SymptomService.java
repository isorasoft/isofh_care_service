package com.isofh.service.service;

import com.isofh.service.model.ResultEntity;
import com.isofh.service.model.SymptomEntity;
import com.isofh.service.result.IMapData;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SymptomService extends BaseService implements IMapData<SymptomEntity> {

    @Override
    public Map<String, Object> getMapData(SymptomEntity entity) {
        Map<String, Object> mapData = new HashMap<>();
        mapData.put(SYMPTOM, entity);
        return mapData;
    }

    @Transactional
    public ResultEntity create(Object object) throws Exception {
        init(object);
        SymptomEntity data = getObject(SYMPTOM, SymptomEntity.class);
        symptomRepository.save(data);
        return ok(SYMPTOM, data);
    }

    @Transactional
    public ResultEntity update(Object object, Long id) throws Exception {
        init(object);
        SymptomEntity data = getObject(SYMPTOM, SymptomEntity.class);
        SymptomEntity entity = getSymptom(id);
        entity.setName(data.getName());
        entity.setHead(data.getHead());
        entity.setNeck(data.getNeck());
        entity.setShoulder(data.getShoulder());
        entity.setChest(data.getChest());
        entity.setArm(data.getArm());
        entity.setAbdomen(data.getAbdomen());
        entity.setFlank(data.getFlank());
        entity.setPelvis(data.getPelvis());
        entity.setLeg(data.getLeg());
        entity.setFoot(data.getFoot());
        entity.setBack(data.getBack());
        entity.setWaist(data.getWaist());
        entity.setAss(data.getAss());
        entity.setHand(data.getHand());
        symptomRepository.save(data);
        return ok(SYMPTOM, data);
    }

    @Transactional
    public ResultEntity delete(Long id) throws Exception {
        init();
        symptomRepository.deleteById(id);
        return ok();
    }

    @Transactional
    public ResultEntity search(String name, Integer head, Integer neck, Integer shoulder, Integer chest, Integer arm, Integer abdomen, Integer flank, Integer pelvis, Integer leg, Integer foot, Integer back, Integer waist, Integer ass,
                               Integer hand, Integer page, Integer size, Integer sortType, LocalDate fromUpdatedDate, LocalDate toUpdatedDate) throws Exception {
        init();
        Sort sort = Sort.by(Sort.Direction.DESC, "updated_date");
        switch (sortType) {
            case 1:
                sort = Sort.by(Sort.Direction.DESC, "view_count");
                break;
        }
        Page<SymptomEntity> symptomEntities = symptomRepository.search(name, head, neck, shoulder, chest, arm, abdomen, flank, pelvis, leg, foot, back, waist, ass, hand, fromUpdatedDate, toUpdatedDate, getPaging(page, size, sort));
        List<Map<String, Object>> mapResults = new ArrayList<>();
        for (SymptomEntity entity : symptomEntities.getContent()) {
            Map<String, Object> mapResult = new HashMap<>();
            mapResult.put(SYMPTOM, entity);
            mapResults.add(mapResult);
        }
        return ok(symptomEntities.getTotalElements(), mapResults);
    }

    @Transactional
    public ResultEntity updateViewCount(Long id) throws Exception {
        init();
        SymptomEntity entity = getSymptom(id);
        entity.setViewCount(entity.getViewCount() + 1);
        save(entity);
        return ok(SYMPTOM, entity);
    }
}
