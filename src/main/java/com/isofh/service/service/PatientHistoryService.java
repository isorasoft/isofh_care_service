package com.isofh.service.service;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.isofh.service.constant.ServiceConst;
import com.isofh.service.enums.HttpMethodType;
import com.isofh.service.model.DepartmentEntity;
import com.isofh.service.model.HospitalProfileEntity;
import com.isofh.service.model.PatientHistoryEntity;
import com.isofh.service.model.ProfileEntity;
import com.isofh.service.model.ResultEntity;
import com.isofh.service.model.ResultHisEntity;
import com.isofh.service.model.UserEntity;
import com.isofh.service.result.IMapData;
import com.isofh.service.utils.GsonUtils;
import com.isofh.service.utils.LogUtils;
import com.isofh.service.utils.NetworkUtils;
import com.isofh.service.utils.StrUtils;

@Service
public class PatientHistoryService extends BaseService implements IMapData<PatientHistoryEntity> {

    @Autowired
    BookingService booking;

    @Override
    public Map<String, Object> getMapData(PatientHistoryEntity entity) {
        Map<String, Object> mapData = new HashMap<>();
        mapData.put(PATIENT_HISTORY, entity);
        mapData.put(HOSPITAL, entity.getHospitalId());
        return null;
    }

    private List<Map<String, Object>> getMapDatas(List<PatientHistoryEntity> entities) {
        List<Map<String, Object>> mapResults = new ArrayList<>();
        for (PatientHistoryEntity entity : entities) {
            mapResults.add(getMapData(entity));
        }
        return mapResults;
    }

    @Transactional
    public ResultEntity sync(long hospitalId, String profileUid, Boolean isCheck) throws Exception {
        try {
            if (isCheck) {
                init();
            }

            ProfileEntity profile = profileRepository.findFirstByUid(profileUid);
            if (profile != null) {
                Long profileId = profile.getId();
                HospitalProfileEntity hospitalUser = hospitalProfileRepository.findByProfileHospital(profileId,
                        hospitalId);

                if (hospitalUser == null) {
                    log.info("Khong co so yte lien ket voi user");
                    return null;
                }
                String url = ServiceConst.GET_BY_VALUE;
                Map<String, Object> uriVariables = new HashMap<>();
                uriVariables.put("isofhCareValue", hospitalUser.getProfile().getUid());
                String result = NetworkUtils.callService(hospitalUser.getHospital().getServiceUrl() + url,
                        HttpMethodType.GET, null, null, null, uriVariables);
                // luu tru danh sach patientHistory
                try {
                    ResultHisEntity hisResult = GsonUtils.toObject(result, ResultHisEntity.class);
                    String patientHistoryIds = "";
                    PatientHistoryEntity patientHistory;
                    for (int i = 0; i < hisResult.getData().getPatientHistoryIds().length; i++) {
                        patientHistory = patientHistoryRepository.findByProfileHospitalPatientHistory(profileId,
                                hospitalId, hisResult.getData().getPatientHistoryIds()[i].getPatientHistoryId());
                        if (patientHistory == null) {
                            patientHistory = new PatientHistoryEntity();
                            patientHistory.setHospitalId(hospitalId);
                            patientHistory.setProfileId(profileId);
                            patientHistory.setPatientHistoryId(
                                    hisResult.getData().getPatientHistoryIds()[i].getPatientHistoryId());
                            if (!StrUtils.isNullOrWhiteSpace(
                                    hisResult.getData().getPatientHistoryIds()[i].getPatientHistoryId())) {
                                patientHistoryIds += ",";
                                ResultEntity rsPatien = booking.getObjectResultPatientHistory(
                                        Long.valueOf(
                                                hisResult.getData().getPatientHistoryIds()[i].getPatientHistoryId()),
                                        hospitalId);
                                patientHistory.setResult(GsonUtils.toString(rsPatien.getData()));

                                ResultEntity rsDetail = booking.getObjectDetailPatientHistory(
                                        Long.valueOf(
                                                hisResult.getData().getPatientHistoryIds()[i].getPatientHistoryId()),
                                        hospitalId);
                                patientHistory.setResultDetail(GsonUtils.toString(rsDetail.getData()));
                                JSONObject jsonObject = new JSONObject(patientHistory.getResultDetail());
                                if (jsonObject.has("Profile")) {
                                    JSONObject profileJson = jsonObject.getJSONObject("Profile");
                                    if (profileJson.has("TimeGoIn")) {
                                        Long timeGoIn = profileJson.getLong("TimeGoIn");
                                        LocalDateTime date = LocalDateTime.ofInstant(Instant.ofEpochMilli(timeGoIn),
                                                ZoneId.systemDefault());
                                        patientHistory.setTimeGoIn(date);
                                    }
                                }
                            }
                            save(patientHistory);
                        } else {
                            ResultEntity rsPatien = booking.getObjectResultPatientHistory(
                                    Long.valueOf(hisResult.getData().getPatientHistoryIds()[i].getPatientHistoryId()),
                                    hospitalId);
                            patientHistory.setResult(GsonUtils.toString(rsPatien.getData()));
                            save(patientHistory);
                        }
                        patientHistoryIds += String
                                .valueOf(hisResult.getData().getPatientHistoryIds()[i].getPatientHistoryId());
                    }
                    profile.setPatientHistoryIds(patientHistoryIds);
                    save(profile);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                return GsonUtils.toObject(result, ResultEntity.class);
            } else {
                throw getException(2, "Khong lay duoc profile");
            }

        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional
    public ResultEntity updateResult(long hospitalId, String patientHistoryId, Boolean isCheck) throws Exception {
        try {
            if (isCheck) {
                init();
            }
            PatientHistoryEntity entity = patientHistoryRepository.findByHospitalPatientHistory(hospitalId,
                    patientHistoryId);
            if (entity != null) {
                ResultEntity rsPatien = booking.getObjectResultPatientHistory(Long.valueOf(patientHistoryId),
                        hospitalId);
                ResultEntity rsDetail = booking.getObjectDetailPatientHistory(Long.valueOf(patientHistoryId),
                        hospitalId);
                entity.setResult(GsonUtils.toString(rsPatien.getData()));
                entity.setResultDetail(GsonUtils.toString(rsDetail.getData()));
                entity.setUpdatedDate(LocalDateTime.now());
                save(entity);
                return ok(getMapData(entity));
            } else {
                throw getException(2, "Khong co ma lich su vao vien");
            }

        } catch (Exception e) {
            throw e;
        }
    }

    public ResultEntity getAll(long hospitalId) throws Exception {
        try {
            init();
            Iterable<DepartmentEntity> departments = departmentRepository.getAll(hospitalId);
            return ok(DEPARTMENTS, departments);
        } catch (Exception ex) {
            throw ex;
        }
    }

    public ResultEntity getDetail(long id) throws Exception {
        try {
            init();
            PatientHistoryEntity patientHistory = getEntityById(patientHistoryRepository, id);
            return ok(PATIENT_HISTORY, patientHistory);
        } catch (Exception ex) {
            throw ex;
        }
    }

    

    public ResultEntity updateData() throws Exception {
        try {
            init();
            List<PatientHistoryEntity> patientList = (List<PatientHistoryEntity>) patientHistoryRepository.findAll();
            for (PatientHistoryEntity object : patientList) {
                JSONObject jsonObject = new JSONObject(object.getResultDetail());
                if (jsonObject.has("Profile")) {
                    JSONObject profileJson = jsonObject.getJSONObject("Profile");
                    if (profileJson.has("TimeGoIn")) {
                        Long timeGoIn = profileJson.getLong("TimeGoIn");
                        LocalDateTime date = LocalDateTime.ofInstant(Instant.ofEpochMilli(timeGoIn),
                                ZoneId.systemDefault());
                        object.setTimeGoIn(date);
                        save(object);
                    }
                }
            }
            return ok("patientHistorys", patientList);
        } catch (Exception ex) {
            throw ex;
        }
    }

    public ResultEntity sync(Long profileId) throws Exception {
        try {
            ProfileEntity profile = getEntityById(profileRepository, profileId);
            if (profile != null) {

                List<HospitalProfileEntity> hospitalUserList = hospitalProfileRepository.findByProfile(profile.getId());
                for (HospitalProfileEntity hospitalUser : hospitalUserList) {

                    String url = ServiceConst.GET_BY_VALUE;
                    Map<String, Object> uriVariables = new HashMap<>();
                    uriVariables.put("isofhCareValue", hospitalUser.getProfile().getUid());
                    String result = NetworkUtils.callService(hospitalUser.getHospital().getServiceUrl() + url,
                            HttpMethodType.GET, null, null, null, uriVariables);
                    // luu tru danh sach patientHistory
                    try {
                        ResultHisEntity hisResult = GsonUtils.toObject(result, ResultHisEntity.class);
                        String patientHistoryIds = "";
                        PatientHistoryEntity patientHistory;
                        for (int i = 0; i < hisResult.getData().getPatientHistoryIds().length; i++) {
                            try {
                                patientHistory = patientHistoryRepository.findByProfileHospitalPatientHistory(profileId,
                                        hospitalUser.getHospital().getId(),
                                        hisResult.getData().getPatientHistoryIds()[i].getPatientHistoryId());
                                if (patientHistory == null) {
                                    patientHistory = new PatientHistoryEntity();
                                    patientHistory.setHospitalId(hospitalUser.getHospital().getId());
                                    patientHistory.setProfileId(profileId);
                                    patientHistory.setPatientHistoryId(
                                            hisResult.getData().getPatientHistoryIds()[i].getPatientHistoryId());
                                    if (!StrUtils.isNullOrWhiteSpace(
                                            hisResult.getData().getPatientHistoryIds()[i].getPatientHistoryId())) {
                                        patientHistoryIds += ",";
                                        ResultEntity rsPatien = booking.getObjectResultPatientHistory(Long.valueOf(
                                                hisResult.getData().getPatientHistoryIds()[i].getPatientHistoryId()),
                                                hospitalUser.getHospital().getId());
                                        patientHistory.setResult(GsonUtils.toString(rsPatien.getData()));

                                        ResultEntity rsDetail = booking.getObjectDetailPatientHistory(Long.valueOf(
                                                hisResult.getData().getPatientHistoryIds()[i].getPatientHistoryId()),
                                                hospitalUser.getHospital().getId());
                                        patientHistory.setResultDetail(GsonUtils.toString(rsDetail.getData()));
                                        JSONObject jsonObject = new JSONObject(patientHistory.getResultDetail());
                                        if (jsonObject.has("Profile")) {
                                            JSONObject profileJson = jsonObject.getJSONObject("Profile");
                                            if (profileJson.has("TimeGoIn")) {
                                                Long timeGoIn = profileJson.getLong("TimeGoIn");
                                                LocalDateTime date = LocalDateTime.ofInstant(
                                                        Instant.ofEpochMilli(timeGoIn), ZoneId.systemDefault());
                                                patientHistory.setTimeGoIn(date);
                                            }
                                        }
                                    }
                                    save(patientHistory);
                                } else {
                                    ResultEntity rsPatien = booking.getObjectResultPatientHistory(Long.valueOf(
                                            hisResult.getData().getPatientHistoryIds()[i].getPatientHistoryId()),
                                            hospitalUser.getHospital().getId());
                                    patientHistory.setResult(GsonUtils.toString(rsPatien.getData()));
                                    ResultEntity rsDetail = booking.getObjectDetailPatientHistory(Long.valueOf(
                                            hisResult.getData().getPatientHistoryIds()[i].getPatientHistoryId()),
                                            hospitalUser.getHospital().getId());
                                    patientHistory.setResultDetail(GsonUtils.toString(rsDetail.getData()));
                                    save(patientHistory);
                                }
                                patientHistoryIds += String
                                        .valueOf(hisResult.getData().getPatientHistoryIds()[i].getPatientHistoryId());
                            } catch (Exception ex) {
                                throw ex;
                            }

                        }
                        profile.setPatientHistoryIds(patientHistoryIds);
                        save(profile);
                    } catch (Exception ex) {
                        LogUtils.getInstance().error(ex);
                    }

                }
                return ok("hospitals", hospitalUserList);
            } else {
                throw getException(2, "Khong lay duoc profile");
            }

        } catch (Exception e) {
            throw e;
        }
    }

}
