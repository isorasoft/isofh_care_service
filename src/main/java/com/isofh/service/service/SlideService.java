package com.isofh.service.service;

import com.isofh.service.constant.field.SlideField;
import com.isofh.service.model.ResultEntity;
import com.isofh.service.model.SlideEntity;
import com.isofh.service.model.SlideItemEntity;
import com.isofh.service.result.IMapData;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class SlideService extends BaseService implements IMapData<SlideEntity> {

    @Override
    public Map<String, Object> getMapData(SlideEntity entity) {
        Map<String, Object> mapData = new HashMap<>();
        mapData.put(SLIDE, entity);
        return mapData;
    }

    @Transactional
    public ResultEntity create(Object object) throws Exception {
        init(object);
        SlideEntity entity = getObject(SLIDE, SlideEntity.class);
        Long[] slideItemIds = getObject("slideItemIds", Long[].class);
        for (Long slideItemId : slideItemIds) {
            SlideItemEntity slideItemEntity = getSlideItem(slideItemId);
            if (slideItemEntity != null) {
                entity.getSlideItems().add(slideItemEntity);
            }
        }
        slideRepository.save(entity);
        return ok(Arrays.asList(SLIDE, SLIDE_ITEMS), Arrays.asList(entity, entity.getSlideItems()));
    }

    @Transactional
    public ResultEntity update(Object object, long id) throws Exception {
        init(object);
        SlideEntity data = getObject(SLIDE, SlideEntity.class);
        SlideEntity entity = getSlide(id);

        entity.setActive(data.getActive());
        entity.setAutoPlay(data.getAutoPlay());
        entity.setIntervalTime(data.getIntervalTime());
        entity.setName(data.getName());

        entity.getSlideItems().clear();
        Long[] slideItemIds = getObject("slideItemIds", Long[].class);
        for (Long slideItemId : slideItemIds) {
            SlideItemEntity slideItemEntity = getSlideItem(slideItemId);
            if (slideItemEntity != null) {
                entity.getSlideItems().add(slideItemEntity);
            }
        }
        slideRepository.save(entity);
        return ok(Arrays.asList(SLIDE, SLIDE_ITEM), Arrays.asList(entity, entity.getSlideItems()));
    }

    @Transactional
    public ResultEntity delete(long id) throws Exception {
        init();
        slideRepository.deleteById(id);
        return ok();
    }

    @Transactional
    public ResultEntity search(Integer page, Integer size, Integer active, Long intervalTime, Integer autoPlay, String name) throws Exception {
        init();
        Page<SlideEntity> queryResults = slideRepository.search(active, intervalTime, autoPlay, name, getDefaultPage(page, size));
        List<Map<String, Object>> mapResults = new ArrayList<>();
        for (SlideEntity entity : queryResults.getContent()) {
            Map<String, Object> mapResult = new HashMap<>();
            mapResults.add(mapResult);
            mapResult.put(SLIDE, entity);
            mapResult.put(SLIDE_ITEMS, entity.getSlideItems());
        }
        return ok(queryResults.getTotalElements(), mapResults);
    }

    @Transactional
    public ResultEntity setActive(Object object, long id) throws Exception {
        init(object);
        Integer active = getInteger(SlideField.ACTIVE);
        SlideEntity entity = getSlide(id);
        entity.setActive(active);
        slideRepository.save(entity);
        return ok(Arrays.asList(SLIDE, SLIDE_ITEMS), Arrays.asList(entity, entity.getSlideItems()));
    }
}
