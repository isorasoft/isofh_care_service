package com.isofh.service.service;

import com.isofh.service.enums.BookingStatusType;
import com.isofh.service.enums.LogType;
import com.isofh.service.model.*;
import com.isofh.service.result.IMapData;
import com.isofh.service.utils.CollectionUtils;
import com.isofh.service.utils.CustomException;
import com.isofh.service.utils.RandomUtils;
import com.isofh.service.utils.StrUtils;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.*;

@Service
public class ScheduleService extends BaseService implements IMapData<ScheduleEntity> {

    public Map<String, Object> getMapData(ScheduleEntity entity) {
        Map<String, Object> mapData = new HashMap<>();
        mapData.put(SCHEDULE, entity);
        mapData.put(ROOM, entity.getRoom());
        mapData.put(SPECIALIST, entity.getSpecialist());
        mapData.put(SERVICE, entity.getService());
        mapData.put(DOCTOR, entity.getDoctor());
        mapData.put(DEPARTMENT, entity.getDepartment());
        return mapData;
    }

    private List<Map<String, Object>> getMapDatas(List<ScheduleEntity> schedules) {
        List<Map<String, Object>> mapResults = new ArrayList<>();
        for (ScheduleEntity scheduleEntity : schedules) {
            mapResults.add(getMapData(scheduleEntity));
        }
        return mapResults;
    }

    public ResultEntity create(Object object, long hospitalId) throws Exception {
        try {
            init(object);
            String chain = RandomUtils.getRandomId();

            DepartmentEntity department = getDepartmentRequire();
            RoomEntity room = getRoomRequire();
            ServiceEntity service = getServiceRequire();
            SpecialistEntity specialist = getSpecialistRequire();
            DoctorEntity doctor = getDoctorRequire();
            HospitalEntity hospital = getHospital(hospitalId);

            ScheduleEntity schedule = getObject(BaseService.SCHEDULE, ScheduleEntity.class);

            LocalDate startDate = getDate("startDate");
            LocalDate endDate = getDate("endDate");
            if (startDate.isAfter(endDate)) {
                throw getException(1, "startDate must be before endDate");
            }
            LocalDate localDate = startDate;
            while (schedule.getDayOfWeek() != localDate.getDayOfWeek().getValue()) {
                localDate = localDate.plusDays(1);
            }
            validRoom(room.getId(), startDate, endDate, schedule.getDayOfWeek(), schedule.getTimeOfDay(), null);
            List<ScheduleEntity> schedules = new ArrayList<>();
            while (!localDate.isAfter(endDate)) {
                ScheduleEntity scheduleInsert = getObject(BaseService.SCHEDULE, ScheduleEntity.class);
                scheduleInsert.setScheduleDate(localDate);
                scheduleInsert.setScheduleChain(chain);
                scheduleInsert.setRoom(room);
                scheduleInsert.setService(service);
                scheduleInsert.setSpecialist(specialist);
                scheduleInsert.setDepartment(department);
                scheduleInsert.setDoctor(doctor);
                scheduleInsert.setHospital(hospital);
                scheduleInsert.setDayOfWeek(localDate.getDayOfWeek().getValue());
                schedules.add(scheduleInsert);
                localDate = localDate.plusDays(7);
            }
            scheduleRepository.saveAll(schedules);
            saveLog(LogType.ADD_SCHEDULE, doctor.getName(), "", "", startDate);
            saveLog(LogType.ADD_SCHEDULE, doctor.getName(), "", "", endDate);
            return ok(SCHEDULES, schedules);
        } catch (Exception ex) {
            throw ex;
        }
    }

    protected DepartmentEntity getDepartmentRequire() throws CustomException {
        Long departmentId = getLong("departmentId");
        if (departmentId == null) {
            throw getException("thieu truong departmentId");
        }

        DepartmentEntity department = getDepartment(departmentId);
        if (department == null) {
            throw getException(1, "khong tim thay department void id " + departmentId);
        }
        return department;
    }

    protected RoomEntity getRoomRequire() throws CustomException {
        Long roomId = getLong("roomId");
        if (roomId == null) {
            throw getException("thieu truong roomId");
        }

        RoomEntity room = getRoom(roomId);
        if (room == null) {
            throw getException(1, "khong tim thay room void id " + roomId);
        }
        return room;
    }

    protected DoctorEntity getDoctorRequire() throws CustomException {
        Long doctorId = getLong("doctorId");
        if (doctorId == null) {
            throw getException("thieu truong doctorId");
        }

        DoctorEntity doctor = getDoctor(doctorId);
        if (doctor == null) {
            throw getException(1, "khong tim thay doctor void id " + doctorId);
        }
        return doctor;
    }

    protected ServiceEntity getServiceRequire() throws CustomException {
        Long serviceId = getLong("serviceId");
        if (serviceId == null) {
            throw getException("thieu truong serviceId");
        }

        ServiceEntity service = getService(serviceId);
        if (service == null) {
            throw getException(1, "khong tim thay service void id " + serviceId);
        }
        return service;
    }

    protected SpecialistEntity getSpecialistRequire() throws CustomException {
        Long specialistId = getLong("specialistId");
        if (specialistId == null) {
            throw getException("thieu truong serviceId");
        }

        SpecialistEntity specialist = getSpecialist(specialistId);
        if (specialist == null) {
            throw getException(1, "khong tim thay specialist void id " + specialistId);
        }
        return specialist;
    }

    private void validRoom(long id, LocalDate startDate, LocalDate endDate, Integer dayOfWeeks, Integer timeOfDay, Long oldScheduleId) throws CustomException {
        // neu la ca sang
        if (timeOfDay == 0) {
            // kiem tra xem ca chieu co lich va da co dat kham chua
            List<ScheduleEntity> noonSchedules = scheduleRepository.findByRoom(id, startDate, endDate, dayOfWeeks, 1);

            if (!CollectionUtils.isNullOrEmpty(noonSchedules)) {
                List<BookingEntity> bookings = getBookingBySchedules(noonSchedules);
                if (!CollectionUtils.isNullOrEmpty(bookings)) {
                    throw getException(10, "Da ton tai lich lam viec co lich kham vao buoi chieu, se bi trung so neu tao lich nay");
                }
            }
        }

        List<ScheduleEntity> schedules = scheduleRepository.findByRoom(id, startDate, endDate, dayOfWeeks, timeOfDay);
        boolean duplicate = false;
        long duplicateId = 0;
        if (!CollectionUtils.isNullOrEmpty(schedules)) {
            if (oldScheduleId != null) {
                for (ScheduleEntity schedule : schedules) {
                    if (!Objects.equals(oldScheduleId, schedule.getId())) {
                        duplicate = true;
                        duplicateId = schedule.getId();
                    }
                }
            } else {
                duplicate = true;
                duplicateId = schedules.get(0).getId();
            }
        }
        if (duplicate) {
            CustomException exception = getException(2, "ton tai lich %d trong phong %d", duplicateId, id);
            Map<String, Object> mapError = new HashMap<>();
            mapError.put(SCHEDULES, schedules);
            exception.setData(mapError);
            throw exception;
        }
    }

    private List<BookingEntity> getBookingBySchedules(List<ScheduleEntity> schedules) {
        if (CollectionUtils.isNullOrEmpty(schedules)) {
            return new ArrayList<>();
        }
        List<Long> scheduleIds = new ArrayList<>();
        for (ScheduleEntity entity : schedules) {
            scheduleIds.add(entity.getId());
        }
        return getBookingByScheduleIds(scheduleIds);
    }

    private List<BookingEntity> getBookingByScheduleIds(List<Long> scheduleIds) {
        if (CollectionUtils.isNullOrEmpty(scheduleIds)) {
            return new ArrayList<>();
        }
        return bookingRepository.findBySchedules(scheduleIds);
    }

    public ResultEntity getByDepartment(Long departmentId, LocalDate startDate, LocalDate endDate) throws Exception {
        try {
            init();
            List<ScheduleEntity> scheduleQueries = scheduleRepository.findByDepartmentDate(startDate, endDate, departmentId);
            List<Map<String, Object>> mapResults = new ArrayList<>();
            long oldSpecialistId = 0;
            Map<String, Object> mapResult = new HashMap<>();
            SpecialistEntity specialist = null;
            List<Map<String, Object>> mapSchedules = new ArrayList<>();
//            List<ScheduleEntity> schedules = new ArrayList<>();
            for (int i = 0; i < scheduleQueries.size(); i++) {

                ScheduleEntity scheduleEntity = scheduleQueries.get(i);
                SpecialistEntity specialistEntity = scheduleEntity.getSpecialist();

                if (oldSpecialistId != specialistEntity.getId()) {
                    if (oldSpecialistId != 0) {
                        // khong phai la lan dau tien
                        // luu thong tin cu
                        mapResult.put(SPECIALIST, specialist);
                        mapResult.put(SCHEDULES, mapSchedules);
                        mapResults.add(mapResult);

                        oldSpecialistId = specialistEntity.getId();
                        mapResult = new HashMap<>();
                        specialist = specialistEntity;
                        mapSchedules = new ArrayList<>();
                    } else {
                        oldSpecialistId = specialistEntity.getId();
                        specialist = specialistEntity;
                    }
                }
                Map<String, Object> mapSchedule = getMapData(scheduleEntity);
                mapSchedules.add(mapSchedule);

                if (i == scheduleQueries.size() - 1) {
                    mapResult.put(SPECIALIST, specialist);
                    mapResult.put(SCHEDULES, mapSchedules);
                    mapResults.add(mapResult);
                }

            }
            return ok(SPECIALISTS, mapResults);
        } catch (Exception ex) {
            throw ex;
        }
    }

    public ResultEntity delete(long id) throws Exception {
        try {
            init();
            Integer chain = getInteger("chain", 0);
            ScheduleEntity scheduleEntity = getSchedule(id);
            if (chain == 1) {
                // neu chain is empty
                List<ScheduleEntity> chainSchedules = scheduleRepository.findByChainDate(scheduleEntity.getScheduleChain(), scheduleEntity.getScheduleDate());
                List<BookingEntity> bookings = getBookingBySchedules(chainSchedules);

                if (!CollectionUtils.isNullOrEmpty(bookings)) {
                    String bookingIds = "";
                    for (BookingEntity entity : bookings) {
                        bookingIds += "," + String.valueOf(entity.getId());
                    }
                    throw getException(3, "da ton tai dat kham cua schedule nay : " + StrUtils.trimTop(bookingIds));
                }

                for (ScheduleEntity entity : chainSchedules) {
                    entity.setDeleted(1);
                    entity.setScheduleChain(RandomUtils.getRandomId());
                    save(entity);
                }
            } else {

                List<BookingEntity> bookings = getBookingByScheduleIds(Arrays.asList(id));
                if (!CollectionUtils.isNullOrEmpty(bookings)) {
                    String bookingIds = "";
                    for (BookingEntity entity : bookings) {
                        bookingIds += "," + String.valueOf(entity.getId());
                    }
                    throw getException(3, "da ton tai dat kham cua schedule nay : " + StrUtils.trimTop(bookingIds));
                }

                scheduleEntity.setDeleted(1);
                scheduleEntity.setScheduleChain(RandomUtils.getRandomId());
                save(scheduleEntity);
            }
            saveLog(LogType.DELETE_SCHEDULE, scheduleEntity.getDoctor().getName(), null, "", scheduleEntity.getScheduleDate());
            return ok();
        } catch (Exception ex) {
            throw ex;
        }
    }

    public ResultEntity update(Object object, long id) throws Exception {
        try {
            init(object);
            Integer chain = getInteger("chain", 0);
            Integer status = getInteger("status", -1);
            ScheduleEntity entity = getSchedule(id);
            if (entity == null || entity.getDeleted() == 1) {
                throw getException(1, "ban ghi khong ton tai hoac da bi xoa");
            }
            ScheduleEntity scheduleData = getObject(SCHEDULE, ScheduleEntity.class);

            DepartmentEntity department = getDepartmentRequire();
            RoomEntity room = getRoomRequire();
            ServiceEntity service = getServiceRequire();
            SpecialistEntity specialist = getSpecialistRequire();
            DoctorEntity doctor = getDoctorRequire();

            boolean checkDuplicate = false;
            if (room.getId() != entity.getRoom().getId()
                    || !scheduleData.getDayOfWeek().equals(entity.getDayOfWeek())
                    || !scheduleData.getTimeOfDay().equals(entity.getTimeOfDay())) {
                checkDuplicate = true;
            }
            int scheduleStatus = getUpdateStatus(id);
            if (status == -1) {
                status = scheduleStatus;
            }
            if (scheduleStatus > status) {
                throw getException(3, "Da co them lich kham duoc dat trong qua trinh thay doi, vui long thu lai ");
            }

            List<ScheduleEntity> updatedList = new ArrayList<>();
            if (chain == 1 || StrUtils.isNullOrWhiteSpace(entity.getScheduleChain())) {

                // neu chain is empty
                List<ScheduleEntity> chainSchedules = scheduleRepository.findByChainDate(entity.getScheduleChain(), entity.getScheduleDate());

                for (ScheduleEntity item : chainSchedules) {
                    if (checkDuplicate) {
                        validRoom(room.getId(), item.getScheduleDate(), item.getScheduleDate(), scheduleData.getDayOfWeek(), scheduleData.getTimeOfDay(), item.getId());
                    }
                }

                String newChain = RandomUtils.getRandomId();
                for (ScheduleEntity item : chainSchedules) {
                    updateInfo(item, department, service, specialist, room, doctor, newChain, scheduleData);
                    updatedList.add(item);
                }
            } else {
                // update 1 ban ghi
                if (checkDuplicate) {
                    validRoom(room.getId(), entity.getScheduleDate(), entity.getScheduleDate(), scheduleData.getDayOfWeek(), scheduleData.getTimeOfDay(), entity.getId());
                }

                String newChain = RandomUtils.getRandomId();
                entity.setScheduleChain(newChain);

                // neu khong co dat kham trong ngay, cho phep tat ca cac thong tin
                if (status < 2) {
                    updateInfo(entity, department, service, specialist, room, doctor, newChain, scheduleData);
                } else if (status == 2) {
                    // chi cho phep update thong tin bac si
                    if (entity.getDoctor().getId() != doctor.getId()) {
                        entity.setDoctor(doctor);
                    }

                    Set<BookingEntity> bookings = entity.getBookings();
                    for (BookingEntity item : bookings) {
                        if (!item.getStatus().equals(BookingStatusType.CANCEL.getValue())) {
                            item.setDoctor(doctor);
                            save(item);
                        }
                    }

                }
                updatedList.add(entity);
            }
            saveLog(LogType.UPDATE_SCHEDULE, doctor.getName(), null, "", entity.getScheduleDate());
            return ok(SCHEDULES, updatedList);
        } catch (Exception ex) {
            throw ex;
        }
    }

    private int getUpdateStatus(long id) {
        int status = 0;

        ScheduleEntity scheduleEntity = getSchedule(id);
        List<BookingEntity> bookings = getBookingByScheduleIds(Arrays.asList(id));

        // Hom nay khong co lich
        if (bookings.size() == 0) {

            // kiem tra cac lich trong tuong lai co booking nao khong
            // Danh sach cac schedule sau schedule any, co cung scheduleChain()
            List<ScheduleEntity> nextChainSchedules = scheduleRepository.findByChainDate(scheduleEntity.getScheduleChain(), scheduleEntity.getScheduleDate().minusDays(-1));

            // Danh sach cac Booking trong nextChainSchedules
            List<BookingEntity> nextBookings = new ArrayList<>();

            if (nextChainSchedules.size() > 0) {
                nextBookings = getBookingBySchedules(nextChainSchedules);
            }

            // tuong lai khong co lich
            if (nextBookings.size() > 0) {
                status = 1;
            }
        }
        // Hom nay co lich
        else {
            // Khong yeu cau bac si
            status = 2;
            for (BookingEntity entity : bookings) {
                // neu co yeu cau dich danh bac si
                if (entity.getRequestDoctor() == 1) {
                    status = 3;
                    break;
                }
            }
        }
        return status;
    }

    private void updateInfo(ScheduleEntity entity, DepartmentEntity department, ServiceEntity service, SpecialistEntity specialist, RoomEntity room, DoctorEntity doctor, String newChain, ScheduleEntity scheduleData) {
        if (entity.getDepartment().getId() != department.getId()) {
            entity.setDepartment(department);
        }

        if (entity.getService().getId() != service.getId()) {
            entity.setService(service);
        }

        if (entity.getSpecialist().getId() != specialist.getId()) {
            entity.setSpecialist(specialist);
        }

        if (entity.getRoom().getId() != room.getId()) {
            entity.setRoom(room);
        }

        if (entity.getDoctor().getId() != doctor.getId()) {
            entity.setDoctor(doctor);
        }

        entity.setScheduleChain(newChain);
        entity.setDayOfWeek(scheduleData.getDayOfWeek());
        entity.setTimeOfDay(scheduleData.getTimeOfDay());
        entity.setStartWorking(scheduleData.getStartWorking());
        entity.setEndWorking(scheduleData.getEndWorking());
        entity.setNumAllowedDate(scheduleData.getNumAllowedDate());
        entity.setNumberCase(scheduleData.getNumberCase());
        save(entity);
    }

    public ResultEntity getByDoctor(Long doctorId, LocalDate startDate, LocalDate endDate) throws Exception {
        try {
            init();
            List<ScheduleEntity> scheduleQueries = scheduleRepository.findByDoctorDate(startDate, endDate, doctorId);
            List<Map<String, Object>> mapResults = new ArrayList<>();
            long odlDepartmentId = 0;
            Map<String, Object> mapResult = new HashMap<>();
            DepartmentEntity department = null;
            List<Map<String, Object>> mapSchedules = new ArrayList<>();
            for (int i = 0; i < scheduleQueries.size(); i++) {

                ScheduleEntity scheduleEntity = scheduleQueries.get(i);
                DepartmentEntity departmentEntity = scheduleEntity.getDepartment();

                if (odlDepartmentId != departmentEntity.getId()) {
                    if (odlDepartmentId != 0) {
                        // khong phai la lan dau tien
                        // luu thong tin cu
                        mapResult.put(DEPARTMENT, department);
                        mapResult.put(SCHEDULES, mapSchedules);
                        mapResults.add(mapResult);

                        odlDepartmentId = departmentEntity.getId();
                        mapResult = new HashMap<>();
                        department = departmentEntity;
                        mapSchedules = new ArrayList<>();
                    } else {
                        odlDepartmentId = departmentEntity.getId();
                        department = departmentEntity;
                    }
                }
                Map<String, Object> mapSchedule = getMapData(scheduleEntity);
                mapSchedules.add(mapSchedule);

                if (i == scheduleQueries.size() - 1) {
                    mapResult.put(DEPARTMENT, department);
                    mapResult.put(SCHEDULES, mapSchedules);
                    mapResults.add(mapResult);
                }

            }
            return ok(SCHEDULES, mapResults);
        } catch (Exception ex) {
            throw ex;
        }
    }

    public ResultEntity getByChainDate(String scheduleChain, LocalDate startDate) throws Exception {
        try {
            init();
            List<ScheduleEntity> scheduleQueries = scheduleRepository.findByChainDate(scheduleChain, startDate);
            List<Map<String, Object>> mapResults = new ArrayList<>();
            for (ScheduleEntity entity : scheduleQueries
            ) {
                Map<String, Object> mapResult = new HashMap<>();
                mapResult.put(SCHEDULE, entity);
                boolean hasBookings = entity.getBookings().stream().anyMatch(entity1 -> !Objects.equals(entity1.getStatus(), BookingStatusType.CANCEL.getValue()));
                mapResult.put("hasBooking", hasBookings ? 1 : 0);
                mapResults.add(mapResult);
            }

            return ok(SCHEDULES, mapResults);
        } catch (Exception ex) {
            throw ex;
        }
    }

    public ResultEntity getByDoctorSpecialistDepartment(Long doctorId, Long specialistId, Long departmentId, Integer dayOfWeek, LocalDate startDate, LocalDate endDate) throws Exception {
        try {
            init();
            List<ScheduleEntity> scheduleQueries = scheduleRepository.findByDoctorSpecialistDepartment(doctorId, specialistId, departmentId, startDate, endDate, dayOfWeek);
            return ok(SCHEDULES, getMapDatas(scheduleQueries));
        } catch (Exception ex) {
            throw ex;
        }
    }

    public ResultEntity checkBookingStatus(long id) throws Exception {
        try {
            init();
            return ok("status", getUpdateStatus(id));
        } catch (Exception ex) {
            throw ex;
        }
    }
}
