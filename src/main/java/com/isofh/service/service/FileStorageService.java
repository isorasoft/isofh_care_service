package com.isofh.service.service;

import com.isofh.service.exception.FileStorageException;
import com.isofh.service.utils.RandomUtils;
import com.isofh.service.utils.StrUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Service
public class FileStorageService {

    @Value("${file.upload-dir}")
    private String filePath;
    private Path fileStorageLocation;

    public String storeFile(MultipartFile file) {
        // Normalize file name
        File f = new File(filePath);
        if (!f.exists()) {
            f.mkdirs();
        }

        fileStorageLocation = Paths.get(filePath);
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        fileName = StrUtils.convertToUnsignedLowerCase(fileName);
        fileName = StrUtils.removeSpecialCharactor(fileName);
        String fileExt = "";
        if (fileName.contains(".")) {
            fileExt = fileName.substring(fileName.lastIndexOf("."));
            fileName = fileName.substring(0, fileName.lastIndexOf("."));
        }
        fileName = fileName + "_" + RandomUtils.getRandomId() + fileExt;
        try {
            // Check if the file's name contains invalid characters
            if (fileName.contains("..")) {
                throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
            }

            // Copy file to the target location (Replacing existing file with the same name)
            Path targetLocation = fileStorageLocation.resolve(fileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

            return "/files/" + fileName;
        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
        }
    }
}
