package com.isofh.service.service;

import com.isofh.service.model.DistrictEntity;
import com.isofh.service.model.ResultEntity;
import com.isofh.service.result.IMapData;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class DistrictService extends BaseService implements IMapData<String> {

    @Override
    public Map<String, Object> getMapData(String entity) {
        return null;
    }

    public ResultEntity getAll() throws Exception {
        init();
        Iterable<DistrictEntity> districts = districtRepository.findAll();
        return ok(DISTRICTS, districts);
    }
}
