package com.isofh.service.service;

import com.isofh.service.model.ResultEntity;
import com.isofh.service.model.TokenPasswordEntity;
import com.isofh.service.result.IMapData;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@Service
public class TokenPasswordService extends BaseService implements IMapData<TokenPasswordEntity> {

    @Override
    public Map<String, Object> getMapData(TokenPasswordEntity entity) {
        Map<String, Object> mapData = new HashMap<>();
        mapData.put(TOKEN_PASSWORD, entity);
        return mapData;
    }

    @Transactional
    public ResultEntity getDetail(String token) throws Exception {
        init();
        TokenPasswordEntity entity = tokenPasswordRepository.findFirstByToken(token);
        if (entity.getExpiredDate() != null && entity.getExpiredDate().isBefore(LocalDateTime.now())) {
            throw getException(2, "token da het han");
        }
        return ok("userId", entity.getUser().getId());
    }
}
