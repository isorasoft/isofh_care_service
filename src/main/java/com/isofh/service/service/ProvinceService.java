package com.isofh.service.service;

import com.google.common.collect.Lists;
import com.isofh.service.model.*;
import com.isofh.service.result.IMapData;
import com.isofh.service.utils.FileUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ProvinceService extends BaseService implements IMapData<ProvinceEntity> {

    @Override
    public Map<String, Object> getMapData(ProvinceEntity entity) {
        Map<String, Object> mapData = new HashMap<>();
        mapData.put(PROVINCE, entity);
        return mapData;
    }

    @Transactional
    public ResultEntity getAll() throws Exception {
        init();
        Iterable<ProvinceEntity> provinces = provinceRepository.findAll();
        return ok(PROVINCES, provinces);
    }
}
