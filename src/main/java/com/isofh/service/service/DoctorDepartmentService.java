package com.isofh.service.service;

import com.isofh.service.model.DoctorDepartmentEntity;
import com.isofh.service.model.ResultEntity;
import com.isofh.service.result.IMapData;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DoctorDepartmentService extends BaseService implements IMapData<DoctorDepartmentEntity> {

    @Override
    public Map<String, Object> getMapData(DoctorDepartmentEntity entity) {
        Map<String, Object> mapData = new HashMap<>();
        mapData.put(DOCTOR_DEPARTMENT, entity);
        return mapData;
    }

    @Transactional
    public ResultEntity create(Object object) throws Exception {
        init(object);
        DoctorDepartmentEntity newsEntity = getObject(DOCTOR_DEPARTMENT, DoctorDepartmentEntity.class);
        save(newsEntity);
        return ok(getMapData(newsEntity));
    }

    @Transactional
    public ResultEntity getAll() throws Exception {
        init();
        Iterable<DoctorDepartmentEntity> departments = doctorDepartmentRepository.findAll();
        return ok(DOCTOR_DEPARTMENT, getMapDatas(departments));
    }

    @Transactional
    public ResultEntity search(Integer page, Integer size, String linkAlias) throws Exception {
        init();
        Page<DoctorDepartmentEntity> results = doctorDepartmentRepository.search(linkAlias, getDefaultPage(page, size));
        return ok(results.getTotalElements(), results.getContent());
    }

    @Transactional
    public ResultEntity getListGroupByDepartment() throws Exception {
        init();
        List<DoctorDepartmentEntity> departments = doctorDepartmentRepository.getListGroupByDepartment();
        List<Map<String, Object>> mapResults = new ArrayList<>();
        for (DoctorDepartmentEntity department : departments) {
            Map<String, Object> mapDepartment = new HashMap<>();
            mapDepartment.put(DOCTOR_DEPARTMENT, department);
            mapDepartment.put(DOCTORS, department.getUsers());
            mapResults.add(mapDepartment);
        }
        return ok(DOCTOR_DEPARTMENTS, mapResults);
    }
}
