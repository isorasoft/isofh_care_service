package com.isofh.service.service;

import com.isofh.service.model.*;
import com.isofh.service.result.IMapData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.*;

@Service
public class HospitalService extends BaseService implements IMapData<HospitalEntity> {

    @Autowired
    UserService userService;
    @Override
    public Map<String, Object> getMapData(HospitalEntity entity) {
        Map<String, Object> mapData = new HashMap<>();
        mapData.put(HOSPITAL, entity);
        mapData.put("createdPerson", entity.getCreatedPerson());
        mapData.put("updatedPerson", entity.getUpdatedPerson());
        mapData.put("userAdmin", entity.getUserAdmin());
        return mapData;
    }


    @Override
    public List<Map<String, Object>> getMapDatas(Iterable<HospitalEntity> notifications) {
        List<Map<String, Object>> mapResults = new ArrayList<>();
        for (HospitalEntity notification : notifications) {
            mapResults.add(getMapData(notification));
        }
        return mapResults;
    }
    @Transactional
    public ResultEntity getAll(int type) throws Exception {
        init();
        Iterable<HospitalEntity> hospitals = hospitalRepository.findAll();
        return ok(HOSPITALS, hospitals);
    }

    @Transactional
    public ResultEntity create(Object object) throws Exception {
        init(object);
        HospitalEntity entity = getObject(HOSPITAL, HospitalEntity.class);
        String email= getString("email");
        String phone =getString("phone");
        String username= getString("username");
        String password = getString("password");
        int role = getInteger("role");
        UserEntity userEntity = new UserEntity();

        userEntity.setPassword(password);
        userEntity.setRole(role);
        if ((!StringUtils.isEmpty(phone)) && (!StringUtils.isEmpty(email)) && (!StringUtils.isEmpty(entity.getTaxCode())) && (!StringUtils.isEmpty(username))) {
            UserEntity checkUserName= userRepository.findByUsername(username);
            if(checkUserName != null){
                throw getException(12, "Username đã được sử dụng trong hệ thống. Vui lòng sử dụng Username khác.");
            }
            UserEntity checkPhone= userRepository.findByPhone(role, phone);
            if(checkPhone != null){
                throw getException(2, "Số điện thoại đã được sử dụng trong hệ thống. Vui lòng sử dụng Username khác.");
            }
            UserEntity checkEmail = userRepository.findByEmail(role, email);
            if (checkEmail != null) {
                throw getException(3, "Email đã được sử dụng trong hệ thống. Vui lòng sử dụng email khác.");
            }
            HospitalEntity hospitalCheck = hospitalRepository.findTaxCode(entity.getTaxCode());
            if(hospitalCheck != null){
                throw getException(11, "Mã số thuế đã được sử dụng trong hệ thống. Vui lòng sử dụng mã số thuế khác.");
            }
        } else {
            throw getException(10,"Các trường bắt buộc không được để trống!");
        }
        userEntity.setUsername(username);
        userEntity.setTaxCode(entity.getTaxCode());
        userEntity.setEmail(email);
        userEntity.setPhone(phone);
        userEntity.setVerify(1);
        save(userEntity);

        entity.setUserAdmin(userEntity);
        entity.setCreatedPerson(getCurrentUser());
        save(entity);
        Thread thread = new Thread(() -> {
            try {
                actionProfileVendor(entity, true);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
        thread.start();
        return ok(getMapData(entity));
    }

    @Transactional
    public ResultEntity update(Object object, long id) throws Exception {
        init(object);
        HospitalEntity data = getObject(HOSPITAL, HospitalEntity.class);
        String email= getString("email");
        String phone =getString("phone");
        String password = getString("password");
        int role = getInteger("role");
        int active = getInteger("active");
        HospitalEntity entity = getHospital(id);
        entity.setName(data.getName());
        entity.setAddress(data.getAddress());
        entity.setServiceUrl(data.getServiceUrl());
        entity.setUpdatedPerson(getCurrentUser());
        entity.setImages(data.getImages());
        entity.setLogo(data.getLogo());
        if ((!StringUtils.isEmpty(phone)) && (!StringUtils.isEmpty(email))) {
            UserEntity checkPhone= userRepository.findByPhone(role, phone);
            if(checkPhone != null && checkPhone.getId() != entity.getUserAdmin().getId()){
                throw getException(2, "Số điện thoại đã được sử dụng trong hệ thống. Vui lòng sử dụng Username khác.");
            }
            UserEntity checkEmail = userRepository.findByEmail(role, email);
            if(checkEmail != null && checkEmail.getId() != entity.getUserAdmin().getId()){
                throw getException(3, "Email đã được sử dụng trong hệ thống. Vui lòng sử dụng email khác.");
            }
        } else {
            throw getException(10,"Số điện thoại và mail không được để trống!");
        }
        UserEntity userEntity = entity.getUserAdmin();
        userEntity.setPhone(phone);
        userEntity.setEmail(email);
        userEntity.setPassword(password);
        userEntity.setRole(role);
        userEntity.setActive(active);
        entity.setUserAdmin(userEntity);
        save(userEntity);
        save(entity);
        Thread thread = new Thread(() -> {
            try {
                actionProfileVendor(entity, false);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
        thread.start();
        return ok(getMapData(entity));
    }

    @Transactional
    public ResultEntity delete(long id) throws Exception {
        init();
        hospitalRepository.deleteById(id);
        return ok();
    }

    @Transactional
    public ResultEntity search(Integer page, Integer size, String stringQuyery, Integer active, Integer type) throws Exception {
        init();
        Pageable pageable = null;
        switch (type) {
            case 1:
                pageable = PageRequest.of(page - 1, size, Sort.by("col_name").descending());
                break;

            case 2:
                pageable = PageRequest.of(page - 1, size, Sort.by("u.username").descending());
                break;

            case 3:
                pageable = PageRequest.of(page - 1, size, Sort.by("col_name").ascending());
                break;

            case 4:
                pageable = PageRequest.of(page - 1, size, Sort.by("u.username").ascending());
                break;
            case 5:
                pageable = PageRequest.of(page - 1, size, Sort.by("created_date").ascending());
                break;
            case 6:
                pageable = PageRequest.of(page - 1, size, Sort.by("created_date").descending());
                break;
            default:
                pageable = getDefaultPage(page, size);
        }
        Page<HospitalEntity> queryResults = hospitalRepository.search(stringQuyery, active, pageable);
        return ok(queryResults.getTotalElements(), getMapDatas(queryResults));
    }


    @Transactional
    public ResultEntity getDetail(long id) {
        init();
        HospitalEntity entity = getHospital(id);
        return ok(getMapData(entity));
    }

    @Transactional
    public ResultEntity setActive(Object object, long id) throws Exception {
        init(object);
        HospitalEntity entity = getHospital(id);
        Integer active = getInteger("active");
        entity.setActive(active);
        UserEntity admin = entity.getUserAdmin();
        if(admin != null) {
            UserEntity userEntity = getUser(entity.getUserAdmin().getId());
            if (userEntity != null) {
                userEntity.setActive(active);
                save(userEntity);
            }
        } else{
            throw  getException(2,"ko co tai khoan quan tri benh vien!");
        }
        save(entity);
        return ok(getMapData(entity));
    }

    @Transactional
    public ResultEntity getHospitalByDoctor(long doctorId) {
        init();
        List<HospitalEntity> entity = hospitalRepository.findHospitalByDoctor(doctorId);
        return ok(getMapDatas(entity));
    }

}
