package com.isofh.service.service;

import com.isofh.service.model.AdvertiseEntity;
import com.isofh.service.model.ResultEntity;
import com.isofh.service.model.TestEntity;
import com.isofh.service.result.IMapData;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AdvertiseService extends BaseService implements IMapData<TestEntity> {

    @Override
    public Map<String, Object> getMapData(TestEntity entity) {
        Map<String, Object> mapData = new HashMap<>();
        mapData.put(ADVERTISE, entity);
        return mapData;
    }

    @Transactional
    public ResultEntity create(Object object) throws Exception {
        init(object);
        AdvertiseEntity entity = getObject(ADVERTISE, AdvertiseEntity.class);
        advertiseRepository.save(entity);
        return ok();
    }

    @Transactional
    public ResultEntity delete(long id) throws Exception {
        init();
        advertiseRepository.deleteById(id);
        return ok();
    }

    @Transactional
    public ResultEntity search(String title, String content, Integer type, Integer page, Integer size) throws Exception {
        init();
        Page<AdvertiseEntity> queryResults = advertiseRepository.search(title, content, type, getDefaultPage(page, size));
        List<Map<String, Object>> mapResults = new ArrayList<>();
        for (AdvertiseEntity entity : queryResults.getContent()) {
            Map<String, Object> mapResult = new HashMap<>();
            mapResults.add(mapResult);
            mapResult.put(ADVERTISE, entity);
        }
        return ok(queryResults.getTotalElements(), mapResults);
    }
}
