package com.isofh.service.service;

import com.isofh.service.model.FileEntity;
import com.isofh.service.model.ResultEntity;
import com.isofh.service.result.IMapData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.Map;

@Service
public class FileService extends BaseService implements IMapData<FileEntity> {

    @Autowired
    protected FileStorageService fileStorageService;

    @Override
    public Map<String, Object> getMapData(FileEntity entity) {
        Map<String, Object> mapData = new HashMap<>();
        mapData.put(FILE, entity);
        return mapData;
    }

    @Transactional
    public ResultEntity uploadFile(MultipartFile file) throws Exception {
        init();
        String fileName = fileStorageService.storeFile(file);
        ResultEntity resultEntity = ok("file", fileName);
        return resultEntity;
    }
}
