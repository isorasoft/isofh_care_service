package com.isofh.service.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.springframework.stereotype.Service;

import com.isofh.service.constant.ServiceConst;
import com.isofh.service.enums.HttpMethodType;
import com.isofh.service.model.HospitalEntity;
import com.isofh.service.model.ResultEntity;
import com.isofh.service.model.ResultHisEntity;
import com.isofh.service.model.RoomEntity;
import com.isofh.service.model.ScheduleEntity;
import com.isofh.service.result.IMapData;
import com.isofh.service.utils.CollectionUtils;
import com.isofh.service.utils.GsonUtils;
import com.isofh.service.utils.NetworkUtils;

@Service
public class RoomService extends BaseService implements IMapData<String> {

    @Override
    public Map<String, Object> getMapData(String entity) {
        Map<String, Object> mapData = new HashMap<>();
        return mapData;
    }

    public ResultEntity sync(long hospitalId) throws Exception {
        try {
            init();
            List<Long> oldRoomIds = new ArrayList<>();
            Iterable<RoomEntity> rooms = roomRepository.getAll(hospitalId);
            for (RoomEntity room : rooms) {
                oldRoomIds.add(room.getId());
            }
            HospitalEntity hospitalEntity = getHospital(hospitalId);
            if (hospitalEntity != null) {
                String queryData = NetworkUtils.callService(hospitalEntity.getServiceUrl() + ServiceConst.SYNC_ROOM,
                        HttpMethodType.GET);
                ResultHisEntity data = GsonUtils.toObject(queryData, ResultHisEntity.class);
                if (data == null || data.getCode() != 0) {
                    throw getException(1, "His tra ve loi ");
                }
                for (int i = 0; i < data.getData().getRooms().length; i++) {
                    try {
                        RoomEntity entity = data.getData().getRooms()[i];
                        entity.setHospital(hospitalEntity);
                        if (Objects.isNull(entity.getHisRoomId())) {
                            continue;
                        }

                        RoomEntity roomEntity = roomRepository.findByHisRoomId(entity.getHisRoomId(), hospitalId);
                        if (roomEntity != null) {
                            entity = roomEntity;
                            oldRoomIds.remove(roomEntity.getId());
                        }
                        save(entity);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
                for (Long id : oldRoomIds) {
                    RoomEntity roomEntity = getRoom(id);
                    roomEntity.setDeleted(1);
                    save(roomEntity);
                    List<ScheduleEntity> deletedSchedules = scheduleRepository.findByRoom(id);
                    if (!CollectionUtils.isNullOrEmpty(deletedSchedules)) {
                        for (ScheduleEntity schedule : deletedSchedules) {
                            schedule.setDeleted(1);
                        }
                        scheduleRepository.saveAll(deletedSchedules);
                    }
                }
            } else {
                throw getException(2, "Nhap sai id hospital");
            }

            return ok();
        } catch (Exception ex) {
            throw ex;
        }
    }

    public ResultEntity getAll(long hospitalId) throws Exception {
        try {
            init();
            Iterable<RoomEntity> rooms = roomRepository.getAll(hospitalId);
            return ok(ROOMS, rooms);
        } catch (Exception ex) {
            throw ex;
        }
    }
}
