package com.isofh.service.service;

import com.isofh.service.enums.NotificationType;
import com.isofh.service.enums.NumberType;
import com.isofh.service.enums.UserType;
import com.isofh.service.model.*;
import com.isofh.service.result.IMapData;
import com.isofh.service.utils.CollectionUtils;
import com.isofh.service.utils.CustomException;
import com.isofh.service.utils.GsonUtils;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.*;

@Service
@Configuration
@EnableScheduling
public class PostService extends BaseService implements IMapData<PostEntity> {

    @Override
    public Map<String, Object> getMapData(PostEntity entity) {
        Map<String, Object> mapData = new HashMap<>();
        mapData.put(POST, entity);
        mapData.put(AUTHOR, entity.getAuthor() != null ? getMapDataUser(entity.getAuthor()) : null);
        mapData.put(ASIGNEE, entity.getAssignee() != null ? getMapDataUser(entity.getAssignee()) : null);
        mapData.put(SPECIALIST, entity.getSpecialist());
        return mapData;
    }

    public Map<String, Object> getMapDataUser(UserEntity entity) {
        Map<String, Object> mapData = new HashMap<>();
        mapData.put("name", entity.getName());
        mapData.put("id", entity.getId());
        mapData.put("numberRejectPost", entity.getNumberRejectPost());
        return mapData;
    }

    @Override
    public List<Map<String, Object>> getMapDatas(Iterable<PostEntity> entities) {
        List<Map<String, Object>> mapResults = new ArrayList<>();
        for (PostEntity entity : entities) {
            mapResults.add(getMapData(entity));
        }
        return mapResults;
    }

    @Transactional
    public ResultEntity create(Object object) {
        init(object);
        Long specialistId = getLong("specialistId");
        PostEntity post = getObject("post", PostEntity.class);
        post.setLinkAlias(getTextUrl(post.getTitle()));
        post.setAuthor(getCurrentUser());
        if (specialistId != null) {
            CommonSpecialistEntity specialist = getEntityById(commonSpecialistRepository, specialistId);
            post.setSpecialist(specialist);
        }
        save(post);
        UserEntity user = post.getAuthor();
        // init post
        if (user != null && user.getRole() == UserType.USER.getValue() && user.getJsonPost() == null) {
            user.setJsonPost(GsonUtils.toString(post));
            save(user);
        }
        Thread thread = new Thread(() -> {
            try {
                sendNotification(NumberType.POST.getValue(), "Bạn", NotificationType.SEND_SUCCESS.getValue(),
                        getUserId(), post.getId());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
        thread.start();

        return ok(getMapData(post));
    }

    public ResultEntity search(Integer page, Integer size, Integer isAnswered, Integer isAssigned, Integer isPublished,
            Long authorId, Long specialistId, Long departmentId, Long assigneeId, Integer status, String queryString,
            LocalDateTime startTime, Integer type) throws Exception {
        init();
        Pageable pageable = null;
        switch (type) {
        case 1:
            pageable = PageRequest.of(page - 1, size, Sort.by("u.name").descending());
            break;

        case 2:
            pageable = PageRequest.of(page - 1, size, Sort.by("t.name").descending());
            break;

        case 3:
            pageable = PageRequest.of(page - 1, size, Sort.by("u.name").ascending());
            break;

        case 4:
            pageable = PageRequest.of(page - 1, size, Sort.by("t.name").ascending());
            break;
        case 5:
            pageable = PageRequest.of(page - 1, size, Sort.by("created_date").ascending());
            break;
        case 6:
            pageable = PageRequest.of(page - 1, size, Sort.by("created_date").descending());
            break;
        case 7:
            pageable = PageRequest.of(page - 1, size, Sort.by("s.name").descending());
            break;
        case 8:
            pageable = PageRequest.of(page - 1, size, Sort.by("s.name").ascending());
            break;
        case 9:
            pageable = PageRequest.of(page - 1, size, Sort.by("updated_date").ascending());
            break;
        case 10:
            pageable = PageRequest.of(page - 1, size, Sort.by("updated_date").descending());
            break;
        default:
            pageable = getDefaultPage(page, size);
        }
        Page<PostEntity> results = postRepository.search(isAnswered, isAssigned, isPublished, authorId, specialistId,
                departmentId, assigneeId, status, queryString, startTime, pageable);
        ResultEntity result = ok(results.getTotalElements(), getMapDatas(results));
        return result;
    }

    @Transactional
    public ResultEntity update(Object object, long id) {
        init(object);
        PostEntity post = getObject("post", PostEntity.class);
        Long specialistId = getLong("specialistId");
        PostEntity entity = getPost(id);
        entity.setTitle(post.getTitle());
        entity.setContent(post.getContent());
        entity.setLinkAlias(getTextUrl(post.getTitle()));
        entity.setImages(post.getImages());
        entity.setIsPrivate(post.getIsPrivate());
        entity.setAge(post.getAge());
        entity.setDiseaseHistory(post.getDiseaseHistory());
        entity.setGender(post.getGender());
        entity.setOtherContent(post.getOtherContent());
        entity.setDiagnose(post.getDiagnose());
        if (specialistId != null) {
            CommonSpecialistEntity specialist = getEntityById(commonSpecialistRepository, specialistId);
            post.setSpecialist(specialist);
        } else {
            post.setSpecialist(null);
        }
        save(entity);
        return ok(getMapData(entity));
    }

    @Transactional
    public ResultEntity delete(long id) throws Exception {
        init();
        postRepository.deleteById(id);
        return ok();
    }

    @Transactional
    public ResultEntity approvedPost(Object object, long id) throws Exception {
        init(object);
        Integer isPublished = getInteger("isPublished");
        String reject = getString("reject");
        PostEntity entity = getPost(id);
        entity.setReject(reject);
        entity.setIsPublished(isPublished);
        Boolean isCheck = true;
        if (isPublished == 2) {
            if (entity.getAssignee() != null) {
                UserEntity doctor = entity.getAssignee();
                if (doctor.getNumberRejectPost() == null) {
                    doctor.setNumberRejectPost(1);
                } else {
                    doctor.setNumberRejectPost(doctor.getNumberRejectPost() + 1);
                }
                if (doctor.getNumberWaitingPost() != null) {
                    doctor.setNumberWaitingPost(doctor.getNumberWaitingPost() - 1);
                }
                save(doctor);
                entity.setStatus(5);
                isCheck = false;
            } else {
                entity.setStatus(4);
            }

        }
        save(entity);
        if (isCheck) {
            Thread thread = new Thread(() -> {
                try {
                    if (isPublished == 2) {
                        sendNotification(NumberType.POST.getValue(), "Câu hỏi của bạn",
                                NotificationType.NOT_ACCEPT_NEWS.getValue(), entity.getAuthor().getId(), id);
                    } else if (isPublished == 1) {
                        sendNotification(NumberType.POST.getValue(), "Bạn", NotificationType.ASSIGN_POST.getValue(),
                                entity.getAuthor().getId(), id);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            });
            thread.start();
        }

        return ok();
    }

    @Transactional
    public ResultEntity assignPost(Object object, long id) {
        init(object);
        PostEntity entity = getPost(id);
        entity.setIsPublished(1);
        Long doctorId = getLong("assigneeId");
        UserEntity doctorAssignee = getUser(doctorId);

        if (doctorAssignee.getNumberWaitingPost() == null) {
            doctorAssignee.setNumberWaitingPost(1);
        } else {
            doctorAssignee.setNumberWaitingPost(doctorAssignee.getNumberWaitingPost() + 1);
        }

        save(doctorAssignee);

        Long departmentId = getLong("departmentId");
        DoctorDepartmentEntity department = getDoctorDepartment(departmentId);

        entity.setAssignee(doctorAssignee);
        entity.setDepartment(department);
        entity.setIsAssigned(1);
        entity.setStatus(2);
        save(entity);
        Thread thread = new Thread(() -> {
            try {
                sendNotification(NumberType.POST.getValue(), "Bạn", NotificationType.ASSIGN_POST.getValue(), doctorId,
                        id);
                sendNotification(NumberType.POST.getValue(), "Câu hỏi của bạn",
                        NotificationType.ASSIGN_SEND_USER.getValue(),
                        entity.getAuthor() != null ? entity.getAuthor().getId() : null, id);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
        thread.start();

        return ok(getMapData(entity));
    }

    @Transactional
    public ResultEntity like(Object object, long id) throws Exception {
        init(object);
        Integer isLiked = getInteger("isLiked");

        PostEntity entity = getPost(id);
        Long[] arrLikedUserIds = GsonUtils.toObject(entity.getLikedUserIds(), Long[].class);
        List<Long> likedUserIds = new ArrayList<>();
        if (arrLikedUserIds != null) {
            likedUserIds.addAll(Arrays.asList(arrLikedUserIds));
        }
        if (isLiked == 1) {
            Long userId = getUserId();
            if (!likedUserIds.contains(userId)) {
                likedUserIds.add(userId);
            }
        } else if (isLiked == 0) {
            likedUserIds.remove(getUserId());
        }
        entity.setLikeCount(likedUserIds.size());
        entity.setLikedUserIds(GsonUtils.toStringCompact(likedUserIds));
        save(entity);
        return ok();
    }

    @Transactional
    public ResultEntity follow(Object object, long id) throws Exception {
        init(object);
        Integer isFollowed = getInteger("isFollowed");
        PostEntity entity = getPost(id);
        Long[] arrFollowedUserIds = GsonUtils.toObject(entity.getLikedUserIds(), Long[].class);
        List<Long> followedUserIds = new ArrayList<>();
        if (arrFollowedUserIds != null) {
            followedUserIds.addAll(Arrays.asList(arrFollowedUserIds));
        }
        if (isFollowed == 1) {
            if (!followedUserIds.contains(id)) {
                followedUserIds.add(id);
            }
        } else if (isFollowed == 0) {
            followedUserIds.remove(id);
        }
        UserEntity user = getCurrentUser();
        entity.getFollowedUsers().add(user);
        save(entity);
        return ok();
    }

    @Transactional
    public ResultEntity getDetail(long id) throws CustomException {
        init();
        try {
            PostEntity entity = getPost(id);
            return ok(getMapData(entity));
        } catch (Exception e) {
            throw getException(2, "khong ton tai bai post");
        }
    }

    @Transactional
    public ResultEntity getByAlias(String alias) {
        init();
        PostEntity entity = postRepository.findFirstByLinkAlias(alias);
        return ok(getMapData(entity));
    }

    @Transactional
    public ResultEntity getByTag(Integer page, Integer size, Long tagId) throws Exception {
        init();
        Page<PostEntity> results = postRepository.getByTag(tagId, getDefaultPage(page, size));
        return ok(results.getTotalElements(), getMapDatas(results));
    }

    public ResultEntity setPostApprovedStatus(long postId, Object object) {
        init(object);
        Integer isPublished = getInteger("isPublished");
        PostEntity postEntity = getPost(postId);
        postEntity.setIsPublished(isPublished);
        postEntity.setIsAssigned(isPublished);
        save(postEntity);
        return ok(getMapData(postEntity));
    }

    @Transactional
    public ResultEntity getClassifyCount() {
        init();
        long count = postRepository.getClassifyCount();
        Map<String, Object> mapResult = new HashMap<>();
        mapResult.put("unclassifiedCount", count);
        return ok(mapResult);
    }

    public ResultEntity getHighlightPost() {
        init();
        List<PostEntity> results = postRepository.getHighlightPost();
        return ok(getMapDatas(results));
    }

//    public ResultEntity setReject(Long postId, Object object) throws Exception {
//        try {
//            init(object);
//            String reject1 = getString("reject");
//            PostEntity postEntity = getPost(postId);
//            if (postEntity != null) {
//                postEntity.setReject(reject1);
//            } else {
//                throw getException(2, "bai post k ton tai");
//            }
//
//            save(postEntity);
//            return ok(getMapData(postEntity));
//        } catch (Exception e) {
//            throw e;
//        }
//
//    }

    @Transactional
    public ResultEntity getUnassigned(Integer page, Integer size) {
        init();
        Page<PostEntity> results = postRepository.getUnassigned(getDefaultPage(page, size));
        return ok(getMapDatas(results));
    }

    @Transactional
    public ResultEntity reviewPost(Object object, long id) {
        init(object);
        Integer review = getInteger("review");
        PostEntity entity = getPost(id);
        entity.setReview(review);
        entity.setStatus(6);
        save(entity);
        Thread thread = new Thread(() -> {
            try {
                sendNotification(NumberType.REVIEW.getValue(), "Bạn", NotificationType.VOTE.getValue(), getUserId(),
                        entity.getId());
                sendNotification(NumberType.REVIEW.getValue(), "Bạn", NotificationType.VOTE_SEND_DOCTOR.getValue(),
                        entity.getAssignee().getId(), entity.getId());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
        thread.start();

        return ok(getMapData(entity));
    }

    @Transactional
    public ResultEntity getAssigned(Integer page, Integer size) {
        init();
        Long assigneeId = getUserId();
        Page<PostEntity> entities = postRepository.getAssigned(assigneeId, getDefaultPage(page, size));
        return (ok(getMapDatas(entities)));
    }

    @Transactional
    public ResultEntity getResultReview(long doctorId) {
        init();
        float rating = 0;
        int countE = 0;
        float rate = 0;
        List<PostEntity> postEntities = postRepository.getListPost(doctorId);
        if (!CollectionUtils.isNullOrEmpty(postEntities)) {
            for (PostEntity postEntity : postEntities) {
                if (postEntity.getReview() != null) {
                    rating = (rating + postEntity.getReview());
                    countE++;
                }
            }
            rate = rating / countE;
        } else {
            rate = 0;
        }
        rate = (float) (Math.floor(rate * 100) / 100);
        Map<String, Object> mapData = new HashMap<>();
        mapData.put("ratingCount", rate);
        mapData.put("ratingNumber", countE);

        return ok(mapData);
    }

    @Transactional
    public ResultEntity updateDiagnose(Object object, long id) {
        init(object);
        String postDiagnose = getString("diagnose");
        PostEntity postEntity = getPost(id);
        if (postDiagnose != null) {
            postEntity.setDiagnose(postDiagnose);
        }
        save(postEntity);
        return ok(getMapData(postEntity));
    }

    @Transactional
    public ResultEntity setStatus(Object object, long id) {
        init(object);
        Integer status = getInteger("status");
        PostEntity entity = getPost(id);
        entity.setStatus(status);
        save(entity);
        return ok(getMapData(entity));
    }

    @Transactional
    public ResultEntity assignListPost(Object object) throws Exception {
        init(object);
        Long[] postIds = getObject("postIds", Long[].class);
        for (Long postId : postIds) {
            PostEntity entity = getPost(postId);
            entity.setIsPublished(1);
            Long doctorId = getLong("assigneeId");
            UserEntity doctorAssignee = getUser(doctorId);

            if (doctorAssignee.getNumberWaitingPost() == null) {
                doctorAssignee.setNumberWaitingPost(1);
            } else {
                doctorAssignee.setNumberWaitingPost(doctorAssignee.getNumberWaitingPost() + 1);
            }

            save(doctorAssignee);

            Long departmentId = getLong("departmentId");
            DoctorDepartmentEntity department = getDoctorDepartment(departmentId);

            entity.setAssignee(doctorAssignee);
            entity.setDepartment(department);
            entity.setIsAssigned(1);
            entity.setStatus(2);
            save(entity);
            Thread thread = new Thread(() -> {
                try {
                    sendNotification(NumberType.POST.getValue(), "Bạn", NotificationType.ASSIGN_POST.getValue(),
                            doctorId, postId);
                    sendNotification(NumberType.POST.getValue(), "Câu hỏi của bạn",
                            NotificationType.ASSIGN_SEND_USER.getValue(),
                            entity.getAuthor() != null ? entity.getAuthor().getId() : null, postId);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            });
            thread.start();
        }
        return ok();
    }

    @Transactional
    public ResultEntity check2DayAgo() throws Exception {
        // init();
        // System.out.print("huong");
        LocalDate today = LocalDate.now();
        LocalDate goTimeOut = today.minusDays(2);
        List<PostEntity> postEntityList = postRepository.getListPostTime(goTimeOut);
        for (PostEntity postEntity : postEntityList) {
            CommentEntity commentEntity = commentRepository.searchComment2Day(postEntity.getId());
            if (commentEntity != null) {
                UserEntity userEntity = commentEntity.getUser();
                if (userEntity != null) {
                    if (commentEntity.getUser().getRole() == UserType.DOCTOR.getValue()) {
                        postEntity.setStatus(7);
                        save(postEntity);
                    }
                }
            }
        }
        return ok();
    }

    @Scheduled(fixedRate = 60000)
    public void demoServiceMethod() throws Exception {

        check2DayAgo();
    }

}
