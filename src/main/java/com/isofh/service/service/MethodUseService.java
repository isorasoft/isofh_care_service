package com.isofh.service.service;

import com.isofh.service.model.MethodUseEntity;
import com.isofh.service.model.ResultEntity;
import com.isofh.service.result.IMapData;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MethodUseService extends BaseService implements IMapData<MethodUseEntity> {

    @Override
    public Map<String, Object> getMapData(MethodUseEntity entity) {
        Map<String, Object> mapData = new HashMap<>();
        mapData.put(METHOD_USE, entity);
        return mapData;
    }

    @Transactional
    public ResultEntity create(Object object) throws Exception {
        init(object);
        String[] methodUses = getObject("methods", String[].class);
        List<MethodUseEntity> methods = new ArrayList<>();
        for (String method : methodUses) {
            MethodUseEntity methodUseEntity = new MethodUseEntity();
            methodUseEntity.setName(method);
            methodUseRepository.save(methodUseEntity);
            methods.add(methodUseEntity);
        }
        return ok(METHOD_USES, methods);
    }

    @Transactional
    public ResultEntity getAll() throws Exception {
        init();
        Iterable<MethodUseEntity> methods = methodUseRepository.findAll();
        return ok(METHOD_USES, methods);
    }
}
