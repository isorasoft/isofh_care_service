package com.isofh.service.service;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.google.gson.Gson;
import com.isofh.service.ServiceConnector;
import com.isofh.service.constant.AppConst;
import com.isofh.service.enums.LogType;
import com.isofh.service.enums.NotificationType;
import com.isofh.service.enums.UserType;
import com.isofh.service.model.AdvertiseEntity;
import com.isofh.service.model.ApiLogEntity;
import com.isofh.service.model.BookingEntity;
import com.isofh.service.model.CacheHisEntity;
import com.isofh.service.model.CommentEntity;
import com.isofh.service.model.CommonSpecialistEntity;
import com.isofh.service.model.CountryEntity;
import com.isofh.service.model.DepartmentEntity;
import com.isofh.service.model.DeviceEntity;
import com.isofh.service.model.DiseaseEntity;
import com.isofh.service.model.DistrictEntity;
import com.isofh.service.model.DoctorDepartmentEntity;
import com.isofh.service.model.DoctorEntity;
import com.isofh.service.model.DoctorInfEntity;
import com.isofh.service.model.DrugEntity;
import com.isofh.service.model.EmailEntity;
import com.isofh.service.model.FacilityEntity;
import com.isofh.service.model.FileEntity;
import com.isofh.service.model.HisArray;
import com.isofh.service.model.HisObject;
import com.isofh.service.model.HospitalEntity;
import com.isofh.service.model.HospitalProfileEntity;
import com.isofh.service.model.ImageEntity;
import com.isofh.service.model.LogEntity;
import com.isofh.service.model.MenuEntity;
import com.isofh.service.model.MessageRabbitProfile;
import com.isofh.service.model.MethodUseEntity;
import com.isofh.service.model.NotificationEntity;
import com.isofh.service.model.PatientHistoryEntity;
import com.isofh.service.model.PermissionEntity;
import com.isofh.service.model.PostEntity;
import com.isofh.service.model.ProfileEntity;
import com.isofh.service.model.ProfileMessage;
import com.isofh.service.model.ProvinceEntity;
import com.isofh.service.model.RolesEntity;
import com.isofh.service.model.RoomEntity;
import com.isofh.service.model.ScheduleEntity;
import com.isofh.service.model.ServiceEntity;
import com.isofh.service.model.SlideEntity;
import com.isofh.service.model.SlideItemEntity;
import com.isofh.service.model.SlidePlaceEntity;
import com.isofh.service.model.SmsEntity;
import com.isofh.service.model.SpecialistEntity;
import com.isofh.service.model.SymptomEntity;
import com.isofh.service.model.TokenPasswordEntity;
import com.isofh.service.model.UserEntity;
import com.isofh.service.model.WalletEntity;
import com.isofh.service.model.ZoneEntity;
import com.isofh.service.rabbit.publisher.PublisherProfileUser;
import com.isofh.service.rabbit.publisher.PublisherProfileVendor;
import com.isofh.service.repository.AdvertiseRepository;
import com.isofh.service.repository.ApiLogRepository;
import com.isofh.service.repository.BookingRepository;
import com.isofh.service.repository.CacheHisRepository;
import com.isofh.service.repository.CommentRepository;
import com.isofh.service.repository.CommonSpecialistRepository;
import com.isofh.service.repository.CountryRepository;
import com.isofh.service.repository.DepartmentRepository;
import com.isofh.service.repository.DeviceRepository;
import com.isofh.service.repository.DiseaseRepository;
import com.isofh.service.repository.DistrictRepository;
import com.isofh.service.repository.DoctorDepartmentRepository;
import com.isofh.service.repository.DoctorInfRepository;
import com.isofh.service.repository.DoctorRepository;
import com.isofh.service.repository.DrugRepository;
import com.isofh.service.repository.EmailRepository;
import com.isofh.service.repository.FacilityRepository;
import com.isofh.service.repository.FileRepository;
import com.isofh.service.repository.HospitalRepository;
import com.isofh.service.repository.HospitalProfileRepository;
import com.isofh.service.repository.ImageRepository;
import com.isofh.service.repository.MenuRepository;
import com.isofh.service.repository.MethodUseRepository;
import com.isofh.service.repository.NotificationRepository;
import com.isofh.service.repository.PatientHistoryRepository;
import com.isofh.service.repository.PermissionRepository;
import com.isofh.service.repository.PostRepository;
import com.isofh.service.repository.ProfileRepository;
import com.isofh.service.repository.ProvinceRepository;
import com.isofh.service.repository.RolesRepository;
import com.isofh.service.repository.RoomRepository;
import com.isofh.service.repository.ScheduleRepository;
import com.isofh.service.repository.ServiceRepository;
import com.isofh.service.repository.SlideItemRepository;
import com.isofh.service.repository.SlidePlaceRepository;
import com.isofh.service.repository.SlideRepository;
import com.isofh.service.repository.SmsRepository;
import com.isofh.service.repository.SpecialistRepository;
import com.isofh.service.repository.SymptomRepository;
import com.isofh.service.repository.TokenPasswordRepository;
import com.isofh.service.repository.UserRepository;
import com.isofh.service.repository.ZoneRepository;
import com.isofh.service.result.IPaging;
import com.isofh.service.result.IResult;
import com.isofh.service.utils.CustomException;
import com.isofh.service.utils.DateTimeUtils;
import com.isofh.service.utils.DigitalSignatureTokenUtils;
import com.isofh.service.utils.GsonUtils;
import com.isofh.service.utils.LogUtils;
import com.isofh.service.utils.NotiUtils;
import com.isofh.service.utils.StrUtils;

@Service
public class BaseService implements IResult, IPaging {

    protected HttpServletRequest request;

    @Autowired
    protected DigitalSignatureTokenUtils digitalSignature;

    @Autowired
    PublisherProfileUser publisherUser;

    @Autowired
    PublisherProfileVendor publisherVendor;

    public void init() {
        init(null);
    }

    /**
     * The time the service is called, used to calculate "service running time"
     */

    protected Long userId;

    /**
     * 1: mobi <br>
     * 0: web
     */
    protected Integer typeBase;

    protected String hospitalUid;

    protected UserEntity user;

    protected Integer roleBase;

    public Long getUserId() {
        return userId;
    }

    protected JSONObject jsonData;

    public void init(Object object) {
        log = LogUtils.getInstance();
        request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();

        String url = request.getMethod() + " " + request.getRequestURI();
        String queryString = request.getQueryString();
        if (!StringUtils.isEmpty(queryString)) {
            url += "?" + queryString;
        }
        log.info(url);
        log.info("getRemoteAddr: " + request.getRemoteAddr());

        String token = request.getHeader("Authorization");
        String mobileMode = request.getHeader("MobileMode");
        hospitalUid = request.getHeader("HospitalUid");
        if (mobileMode == null) {
            roleBase = UserType.ADMIN.getValue();
        } else if (mobileMode.equals("user")) {
            roleBase = UserType.USER.getValue();
        } else if (mobileMode.equals("vender")) {
            roleBase = UserType.DOCTOR.getValue();
        } else if (mobileMode.equals("vendorPortal")) {
            roleBase = UserType.ADMIN_FACILITY.getValue();
        } else {
            roleBase = 0;
        }

        log.info("roleBase " + roleBase);
        if (!StrUtils.isNullOrWhiteSpace(token)) {
            log.info("Token " + token);
            userId = digitalSignature.getUserIdFromToken(token);
            typeBase = digitalSignature.getTypeFromToken(token);
            log.info("userId " + userId);
            log.info("type " + typeBase);
            user = getCurrentUser();
            if (user.getRole() == UserType.DOCTOR.getValue() && user.getActive() != 1) {
                try {
                    throw getException(2, "Tk bi inactive");
                } catch (Exception e) {
                    log.error("Error: " + e);
                }
            }
        } else {
            log.info("khong co token Token ");
            userId = null;
        }

        if (object != null) {
            String strObj = GsonUtils.toStringCompact(object);
            log.info(strObj);
            jsonData = new JSONObject(GsonUtils.toString(object));
        }
    }

    protected void save(Object o) {
        // save user
        if (o instanceof SymptomEntity) {
            symptomRepository.save((SymptomEntity) o);
        } else if (o instanceof DiseaseEntity) {
            diseaseRepository.save((DiseaseEntity) o);
        } else if (o instanceof UserEntity) {
            userRepository.save((UserEntity) o);
        } else if (o instanceof FacilityEntity) {
            facilityRepository.save((FacilityEntity) o);
        } else if (o instanceof CommonSpecialistEntity) {
            commonSpecialistRepository.save((CommonSpecialistEntity) o);
        } else if (o instanceof ProvinceEntity) {
            provinceRepository.save((ProvinceEntity) o);
        } else if (o instanceof MenuEntity) {
            menuRepository.save((MenuEntity) o);
        } else if (o instanceof SlideEntity) {
            slideRepository.save((SlideEntity) o);
        } else if (o instanceof SlideItemEntity) {
            slideItemRepository.save((SlideItemEntity) o);
        } else if (o instanceof AdvertiseEntity) {
            advertiseRepository.save((AdvertiseEntity) o);
        } else if (o instanceof MethodUseEntity) {
            methodUseRepository.save((MethodUseEntity) o);
        } else if (o instanceof SmsEntity) {
            smsRepository.save((SmsEntity) o);
        } else if (o instanceof DeviceEntity) {
            deviceRepository.save((DeviceEntity) o);
        } else if (o instanceof TokenPasswordEntity) {
            tokenPasswordRepository.save((TokenPasswordEntity) o);
        } else if (o instanceof FileEntity) {
            fileRepository.save((FileEntity) o);
        } else if (o instanceof DrugEntity) {
            drugRepository.save((DrugEntity) o);
        } else if (o instanceof CountryEntity) {
            countryRepository.save((CountryEntity) o);
        } else if (o instanceof SlidePlaceEntity) {
            slidePlaceRepository.save((SlidePlaceEntity) o);
        } else if (o instanceof EmailEntity) {
            emailRepository.save((EmailEntity) o);
        } else if (o instanceof HospitalProfileEntity) {
            hospitalProfileRepository.save((HospitalProfileEntity) o);
        } else if (o instanceof HospitalEntity) {
            hospitalRepository.save((HospitalEntity) o);
        } else if (o instanceof ApiLogEntity) {
            apiLogRepository.save((ApiLogEntity) o);
        } else if (o instanceof PostEntity) {
            postRepository.save((PostEntity) o);
        } else if (o instanceof CommentEntity) {
            commentRepository.save((CommentEntity) o);
        } else if (o instanceof DoctorDepartmentEntity) {
            doctorDepartmentRepository.save((DoctorDepartmentEntity) o);
        } else if (o instanceof RolesEntity) {
            rolesRepository.save((RolesEntity) o);
        } else if (o instanceof PermissionEntity) {
            permissionRepository.save((PermissionEntity) o);
        } else if (o instanceof DoctorInfEntity) {
            doctorInfRepository.save((DoctorInfEntity) o);
        } else if (o instanceof NotificationEntity) {
            notificationRepository.save((NotificationEntity) o);
        } else if (o instanceof DepartmentEntity) {
            departmentRepository.save((DepartmentEntity) o);
        } else if (o instanceof DoctorEntity) {
            doctorRepository.save((DoctorEntity) o);
        } else if (o instanceof RoomEntity) {
            roomRepository.save((RoomEntity) o);
        } else if (o instanceof ScheduleEntity) {
            scheduleRepository.save((ScheduleEntity) o);
        } else if (o instanceof ServiceEntity) {
            serviceRepository.save((ServiceEntity) o);
        } else if (o instanceof SpecialistEntity) {
            specialistRepository.save((SpecialistEntity) o);
        } else if (o instanceof BookingEntity) {
            bookingRepository.save((BookingEntity) o);
        } else if (o instanceof ProfileEntity) {
            profileRepository.save((ProfileEntity) o);
        } else if (o instanceof DistrictEntity) {
            districtRepository.save((DistrictEntity) o);
        } else if (o instanceof ZoneEntity) {
            zoneRepository.save((ZoneEntity) o);
        } else if (o instanceof CacheHisEntity) {
            cacheHisRepository.save((CacheHisEntity) o);
        } else if (o instanceof PatientHistoryEntity) {
            patientHistoryRepository.save((PatientHistoryEntity) o);
        }
    }

    /**
     * Log controller
     */
    protected LogUtils log;

    /**
     * Get Object with key in jsonData
     */
    public <U> U getObject(String key, Class<U> cls) {
        if (jsonData.has(key)) {
            return GsonUtils.toObject(jsonData.get(key).toString(), cls);
        }
        return null;
    }

    public JSONObject getJSONObject(String key) {
        if (jsonData.has(key)) {
            return jsonData.getJSONObject(key);
        }
        return null;
    }

    public JSONArray getJSONArray(String key) {
        if (jsonData.has(key)) {
            return jsonData.getJSONArray(key);
        }
        return null;
    }

    /**
     * Get param input as Integer
     */
    public Integer getInteger(String key) {
        if (!jsonData.has(key))
            return null;
        return jsonData.getInt(key);
    }

    /**
     * Get param input as Integer
     */
    public Integer getInteger(String key, Integer defaultValue) {
        if (jsonData.has(key)) {
            return jsonData.getInt(key);
        }
        return defaultValue;

    }

    /**
     * Get param input as Long
     */
    public Long getLong(String key) {
        if (jsonData.has(key))
            return jsonData.getLong(key);
        return null;
    }

    /**
     * Get param input as LocalDate
     */
    public LocalDate getDate(String key) {
        if (jsonData.has(key)) {
            String date = jsonData.getString(key);
            return LocalDate.parse(date, AppConst.DATE_FORMATTER);
        }
        return null;
    }

    /**
     * Get param input as LocalDate
     */
    public LocalDateTime getDateTime(String key) {
        if (jsonData.has(key)) {
            String date = jsonData.getString(key);
            return LocalDateTime.parse(date, AppConst.DATE_TIME_FORMATTER);
        }
        return null;
    }

    public Long getLong(String key, Long defaultValue) {
        if (jsonData.has(key)) {
            return jsonData.getLong(key);
        }
        return defaultValue;
    }

    /**
     * Get param input as String, neu khong co properties nay thi tra ve null
     */
    public String getString(String key) {
        if (jsonData.has(key)) {
            return jsonData.getString(key);
        }
        return null;
    }

    public String getString(String key, String defaultValue) {
        if (jsonData.has("key")) {
            return jsonData.getString(key);
        }
        return defaultValue;
    }

    protected <T> T getEntityById(CrudRepository repository, Long id) {
        if (id == null || Objects.equals(0L, id))
            return null;

        Optional entityOpt = repository.findById(id);
        if (!entityOpt.isPresent()) {
            return null;
        }
        return (T) entityOpt.get();
    }

    /**
     * format du lieu
     *
     * @param format
     * @param args
     * @return
     */
    protected String format(String format, Object... args) {
        return String.format(format, args);
    }

    /**
     * Tao HisObject tu string data
     *
     * @param data du lieu dau vao
     * @return HisObject
     */
    protected HisObject getHisObject(String data) {
        try {
            JSONObject jsonObject = new JSONObject(data);
            HisObject hisObject = new HisObject();
            hisObject.setCode(jsonObject.getInt("code"));
            if (hisObject.isSuccess()) {
                hisObject.setData(jsonObject.getJSONObject("data"));
            } else {
                hisObject.setComment(jsonObject.getString("comment"));
                Object object = jsonObject.get("data");
                if (object instanceof JSONObject) {
                    hisObject.setData(jsonObject.getJSONObject("data"));
                }
            }
            return hisObject;
        } catch (Exception ex) {
            return null;
        }

    }

    /**
     * Tao HisArray tu string
     *
     * @param data String Object
     * @return HisArray
     */
    protected HisArray getHisArray(String data) {
        try {
            JSONObject jsonObject = new JSONObject(data);
            HisArray hisArray = new HisArray();
            hisArray.setCode(jsonObject.getInt("code"));
            if (hisArray.isSuccess()) {
                hisArray.setData(jsonObject.getJSONArray("data"));
            } else {
                hisArray.setComment(jsonObject.getString("comment"));
            }
            return hisArray;
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * Tao text hien thi tren adress trinh duyet (linkAlias)
     *
     * @param text text muon chuyen thanh linkAlias
     * @return linkAlias
     */
    public String getTextUrl(String text) {
        return StrUtils.createUrlFromString(text);
    }

    private void saveLog(Object body) {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes())
                .getRequest();
        String method = request.getMethod();
        String uri = request.getRequestURI();
        String queryString = request.getQueryString();
        String remoteAddr = request.getRemoteAddr();
        String userAgent = request.getHeader("user-agent");
        String cookie = request.getHeader("cookie");
        String forward = request.getHeader("X-Forwarded-For");

        ApiLogEntity entity = new ApiLogEntity();
        entity.setMethod(method);
        entity.setUri(uri);
        entity.setQueryString(queryString);
        entity.setRemoteAddr(remoteAddr);
        entity.setUserAgent(userAgent);
        entity.setCookie(cookie);
        if (body != null)
            entity.setBody(GsonUtils.toString(body));
        entity.setForward(forward);

        new Thread(() -> {
            try {
                save(entity);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();
    }

    public UserEntity getCurrentUser() {
        return getUser(userId);
    }

    private UserEntity currentUser;

    boolean isAdmin() {
        boolean result = false;
        if (currentUser == null) {
            currentUser = getCurrentUser();
        }
        if (currentUser != null && (currentUser.getRole() & (UserType.ADMIN.getValue())) != 0) {
            result = true;
        }
        return result;
    }

    /**
     * Kiem tra xem User co trong khong
     *
     * @throws CustomException
     */

    void validateUser() throws CustomException {
        currentUser = getCurrentUser();
        if (currentUser == null) {
            throw getUnauthentication("user null");
        }
    }

    CustomException getUnauthentication(String message) {
        return new CustomException(HttpServletResponse.SC_UNAUTHORIZED, message);
    }

    CustomException getUnauthentication() {
        return new CustomException(HttpServletResponse.SC_UNAUTHORIZED, "Invalid Token ");
    }

    protected CustomException getException(String message, Object... objects) {
        return new CustomException(String.format(message, objects));
    }

    /**
     * Tra ve loi voi cac thong so
     *
     * @param code    ma loi
     * @param message Noi dung loi
     */
    protected CustomException getException(int code, String message, Object... objects) {
        return new CustomException(code, String.format(message, objects));
    }

    // region AutoWired Repository
    @Autowired
    UserRepository userRepository;

    @Autowired
    FacilityRepository facilityRepository;

    @Autowired
    ImageRepository imageRepository;

    @Autowired
    CommonSpecialistRepository commonSpecialistRepository;

    @Autowired
    ProvinceRepository provinceRepository;

    @Autowired
    DiseaseRepository diseaseRepository;

    @Autowired
    SymptomRepository symptomRepository;

    @Autowired
    MenuRepository menuRepository;

    @Autowired
    SlideRepository slideRepository;

    @Autowired
    SlideItemRepository slideItemRepository;

    @Autowired
    AdvertiseRepository advertiseRepository;

    @Autowired
    MethodUseRepository methodUseRepository;

    @Autowired
    SmsRepository smsRepository;

    @Autowired
    DeviceRepository deviceRepository;

    @Autowired
    TokenPasswordRepository tokenPasswordRepository;

    @Autowired
    FileRepository fileRepository;

    @Autowired
    DrugRepository drugRepository;

    @Autowired
    CountryRepository countryRepository;

    @Autowired
    SlidePlaceRepository slidePlaceRepository;

    @Autowired
    EmailRepository emailRepository;

    @Autowired
    HospitalProfileRepository hospitalProfileRepository;

    @Autowired
    protected HospitalRepository hospitalRepository;

    @Autowired
    ApiLogRepository apiLogRepository;

    @Autowired
    ServiceConnector serviceConnector;

    @Autowired
    PostRepository postRepository;

    @Autowired
    CommentRepository commentRepository;

    @Autowired
    DoctorDepartmentRepository doctorDepartmentRepository;

    @Autowired
    PermissionRepository permissionRepository;

    @Autowired
    RolesRepository rolesRepository;

    @Autowired
    DoctorInfRepository doctorInfRepository;

    @Autowired
    NotificationRepository notificationRepository;

    @Autowired
    DepartmentRepository departmentRepository;

    @Autowired
    DoctorRepository doctorRepository;

    @Autowired
    RoomRepository roomRepository;

    @Autowired
    ScheduleRepository scheduleRepository;

    @Autowired
    ServiceRepository serviceRepository;

    @Autowired
    SpecialistRepository specialistRepository;

    @Autowired
    BookingRepository bookingRepository;

    @Autowired
    ProfileRepository profileRepository;

    @Autowired
    DistrictRepository districtRepository;

    @Autowired
    ZoneRepository zoneRepository;

    @Autowired
    CacheHisRepository cacheHisRepository;

    @Autowired
    PatientHistoryRepository patientHistoryRepository;
    // endregion

    // region Fast Get Entity
    UserEntity getUser(Long id) {
        return getEntityById(userRepository, id);
    }

    FacilityEntity getFacility(Long id) {
        return getEntityById(facilityRepository, id);
    }

    ImageEntity getImage(Long id) {
        return getEntityById(imageRepository, id);
    }

    CommonSpecialistEntity getCommonSpecialist(Long id) {
        return getEntityById(commonSpecialistRepository, id);
    }

    ProvinceEntity getProvince(Long id) {
        return getEntityById(provinceRepository, id);
    }

    DiseaseEntity getDisease(Long id) {
        return getEntityById(diseaseRepository, id);
    }

    SymptomEntity getSymptom(Long id) {
        return getEntityById(symptomRepository, id);
    }

    MenuEntity getMenu(Long id) {
        return getEntityById(menuRepository, id);
    }

    SlideEntity getSlide(Long id) {
        return getEntityById(slideRepository, id);
    }

    SlideItemEntity getSlideItem(Long id) {
        return getEntityById(slideItemRepository, id);
    }

    AdvertiseEntity getAdvertise(Long id) {
        return getEntityById(advertiseRepository, id);
    }

    MethodUseEntity getMethodUse(Long id) {
        return getEntityById(methodUseRepository, id);
    }

    SmsEntity getSms(Long id) {
        return getEntityById(smsRepository, id);
    }

    DeviceEntity getDevice(Long id) {
        return getEntityById(deviceRepository, id);
    }

    TokenPasswordEntity getTokenPassword(Long id) {
        return getEntityById(tokenPasswordRepository, id);
    }

    FileEntity getFile(Long id) {
        return getEntityById(fileRepository, id);
    }

    DrugEntity getDrug(Long id) {
        return getEntityById(drugRepository, id);
    }

    CountryEntity getCountry(Long id) {
        return getEntityById(countryRepository, id);
    }

    SlidePlaceEntity getSlidePlace(Long id) {
        return getEntityById(slidePlaceRepository, id);
    }

    protected HospitalEntity getHospital(Long id) {
        return getEntityById(hospitalRepository, id);
    }

    PostEntity getPost(Long id) {
        return getEntityById(postRepository, id);
    }

    DoctorDepartmentEntity getDoctorDepartment(Long id) {
        return getEntityById(doctorDepartmentRepository, id);
    }

    CommentEntity getComment(Long id) {
        return getEntityById(commentRepository, id);
    }

    PermissionEntity getPermission(Long id) {
        return getEntityById(permissionRepository, id);
    }

    RolesEntity getRoles(Long id) {
        return getEntityById(rolesRepository, id);
    }

    NotificationEntity getNotification(Long id) {
        return getEntityById(notificationRepository, id);
    }

    DepartmentEntity getDepartment(Long id) {
        return getEntityById(departmentRepository, id);
    }

    DoctorEntity getDoctor(Long id) {
        return getEntityById(doctorRepository, id);
    }

    RoomEntity getRoom(Long id) {
        return getEntityById(roomRepository, id);
    }

    ServiceEntity getService(Long id) {
        return getEntityById(serviceRepository, id);
    }

    SpecialistEntity getSpecialist(Long id) {
        return getEntityById(specialistRepository, id);
    }

    ScheduleEntity getSchedule(Long id) {
        return getEntityById(scheduleRepository, id);
    }

    ProfileEntity getProfile(Long id) {
        return getEntityById(profileRepository, id);
    }

    BookingEntity getBooking(Long id) {
        return getEntityById(bookingRepository, id);
    }

    DistrictEntity getDistrict(Long id) {
        return getEntityById(districtRepository, id);
    }

    ZoneEntity getZone(Long id) {
        return getEntityById(zoneRepository, id);
    }
    // endregion

    // region khai bao cacs constance name of database
    protected static final String USER = "user";
    protected static final String USER_CREATE = "userCreate";
    protected static final String USER_UPDATE = "userUpdate";
    protected static final String USERS = "users";
    protected static final String USER_CONFERENCE = "userConference";
    protected static final String CONFERENCE = "conference";
    protected static final String DEVICE = "device";
    protected static final String DEVICES = "devices";
    protected static final String IMAGE = "image";
    protected static final String IMAGES = "images";
    protected static final String KEY_VALUE = "keyValue";
    protected static final String SCHEDULE = "schedule";
    protected static final String SUPPORT = "support";
    protected static final String CONFERENCE_TOPIC = "conferenceTopic";
    protected static final String CONFERENCE_SESSION = "conferenceSession";
    protected static final String CONFERENCE_QUESTION = "conferenceQuestion";
    protected static final String PROVINCE = "province";
    protected static final String ADMIN_SALE = "adminSale";
    protected static final String ADVERTISE = "advertise";
    protected static final String USER_SESSION = "userSession";
    protected static final String SURVEY = "survey";
    protected static final String ANSWER = "answer";
    protected static final String USER_ANSWER = "userAnswer";
    protected static final String CONFERENCE_NOTIFICATION = "conferenceNotification";
    protected static final String SPECIALIST = "specialist";
    protected static final String SPECIALISTS = "specialists";
    protected static final String SYMPTOM = "symptom";
    protected static final String SYMPTOMS = "symptoms";
    protected static final String DISEASE = "disease";
    protected static final String DISEASES = "diseases";
    protected static final String FACILITY = "facility";
    protected static final String FACILITIES = "facilities";
    protected static final String DRUG = "drug";
    protected static final String COUNTRY = "country";
    protected static final String METHOD_USE = "methodUse";
    protected static final String METHOD_USES = "methodUses";
    protected static final String MENU = "menu";
    protected static final String MENUS = "menus";
    protected static final String PHARMACY = "pharmacy";
    protected static final String SLIDE_ITEM = "slideItem";
    protected static final String SLIDE_ITEMS = "slideItems";
    protected static final String SLIDE = "slide";
    protected static final String SLIDE_PLACE = "slidePlace";
    protected static final String TOKEN_PASSWORD = "tokenPassword";
    protected static final String DRUG_FACILITY = "drug_facility";
    protected static final String FILE = "file";
    protected static final String HOSPITAL = "hospital";
    protected static final String HOSPITALS = "hospitals";
    protected static final String POST = "post";
    protected static final String AUTHOR = "author";
    protected static final String COMMENT = "comment";
    protected static final String ASIGNEE = "assignee";
    protected static final String DOCTOR_DEPARTMENT = "doctorDepartment";
    protected static final String DOCTORS = "doctors";
    protected static final String DOCTOR = "doctor";
    protected static final String DOCTOR_DEPARTMENTS = "doctorDepartments";
    protected static final String TEMP_DEPARTMENT = "tempDepartment";
    protected static final String TEMP_DEPARTMENTS = "tempDepartments";
    protected static final String TEMP_DOCTOR = "tempDoctor";
    protected static final String TEMP_DOCTORS = "tempDoctors";
    protected static final String TEMP_SERVICE = "tempService";
    protected static final String TEMP_SERVICES = "tempServices";
    protected static final String TEMP_SPECIALIST = "tempSpecialist";
    protected static final String TEMP_SPECIALISTS = "tempSpecialists";

    protected static final String TEMP_BOOKING = "tempBooking";
    protected static final String TEMP_BOOKINGS = "tempBookings";
    protected static final String ROLES = "roles";
    protected static final String ROLE = "role";
    protected static final String PERMISSION = "permission";
    protected static final String CREATE_PERSON = "createPerson";
    protected static final String UPDATE_PERSON = "updatePerson";
    protected static final String DOCTOR_INF = "doctorInf";
    protected static final String NOTIFICATION = "notification";

    protected static final String BOOKING = "booking";
    protected static final String BOOKINGS = "bookings";

    protected static final String DEPARTMENTS = "departments";
    protected static final String DEPARTMENT = "department";

    protected static final String ROOMS = "rooms";
    protected static final String ROOM = "room";

    protected static final String SERVICES = "services";
    protected static final String SERVICE = "service";

    protected static final String SCHEDULES = "schedules";
    protected static final String PROFILE = "profile";
    protected static final String PROFILES = "profiles";

    protected static final String PROVINCES = "provinces";
    protected static final String DISTRICTS = "districts";
    protected static final String ZONES = "zones";
    protected static final String DISTRICT = "district";
    protected static final String ZONE = "zone";
    protected static final String PATIENT_HISTORY = "patientHistory";
    protected static final String PATIENT_HISTORYS = "patientHistorys";

    // endregion

    protected void sendNotification(int style, String type, Integer status, Long userId, Long id) {
        try {
            StringBuffer title = new StringBuffer();
            UserEntity user = getEntityById(userRepository, userId);
            PostEntity postEntity = getPost(id);
            if (type.equals("Bác sĩ")) {
                title.append("Bác sĩ " + postEntity.getAssignee().getName());

            } else if (type.equals("Người hỏi")) {
                title.append("Người hỏi");
            } else if (type.equals("Admin")) {
                title.append("Admin");
            } else if (type.equals("Bạn")) {
                title.append("Bạn");
            } else if (type.equals("Câu hỏi của bạn")) {
                title.append("Câu hỏi của bạn");
            } else if (type.equals("Câu hỏi")) {
                title.append("Câu hỏi " + postEntity.getId());
            }

            if (status == NotificationType.COMMENT.getValue()) {
                title.append(" đã trả lời câu hỏi của bạn");
            } else if (status == NotificationType.ASSIGN_POST.getValue()) {
                title.append(" nhận được một câu hỏi mới. ");
            } else if (status == NotificationType.NOT_ACCEPT_NEWS.getValue()) {
                title.append(" đã bị từ chối vì lý do " + postEntity.getReject());
            } else if (status == NotificationType.VOTE.getValue()) {
                title.append(" đã gửi đánh giá thành công! ");
            } else if (status == NotificationType.VOTE_SEND_DOCTOR.getValue()) {
                title.append(" vừa nhận được đánh giá cho lượt tư vấn " + postEntity.getId());
            } else if (status == NotificationType.SEND_SUCCESS.getValue()) {
                title.append(" đã gửi câu hỏi thành công! ");
            } else if (status == NotificationType.ASSIGN_SEND_USER.getValue()) {
                title.append(" đã được gửi đến bác sĩ " + postEntity.getAssignee().getName());
            } else if (status == NotificationType.COMMENT_USER.getValue()) {
                title.append(" có thêm một phản hồi mới.");
            }
            List<DeviceEntity> deviceEntities = deviceRepository.findAllByUser(userId);

            NotificationEntity object = new NotificationEntity(null, title.toString(), user);
            save(object);
            Map<String, Object> mapData = new HashMap<>();
            mapData.put("id", id);
            mapData.put("type", style);
            mapData.put("notificationId", object.getId());
            Gson gson = new Gson();
            String json = gson.toJson(mapData);
            object.setValue(json);
            save(object);
            NotiUtils.sendNotification(deviceEntities, id, type, title.toString());

        } catch (Exception ex) {
            try {
                throw getException(2, "error!");
            } catch (CustomException e) {
                e.printStackTrace();
            }
        }
    }

    protected void saveLog(LogType logType, String doctor, String user, String note, LocalDate effectiveDate) {
        runInThread(() -> {
            try {
                log.info("save log");
                UserEntity currentUser = getCurrentUser();

                LogEntity logEntity = new LogEntity();
                if (currentUser == null) {
                    log.info("currentUser is null");
                    return;
                } else {
                    logEntity.setAdmin(currentUser.getUsername());
                    logEntity.setAdminName(currentUser.getName());
                }
                if (Objects.equals(currentUser.getRole(), UserType.USER.getValue())) {
                    logEntity.setNote("nguoi dung khong phai admin");
                }
                logEntity.setType(logType.getValue());

                if (doctor != null) {
                    logEntity.setDoctor(doctor);
                }

                if (user != null) {
                    logEntity.setUser(user);
                }
                logEntity.setNote(note);
                if (effectiveDate == null) {
                    logEntity.setEffectiveDate(LocalDate.now());
                } else {
                    logEntity.setEffectiveDate(effectiveDate);
                }
                save(logEntity);
                log.info("end save log");
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        });
    }

    /**
     * Run a runnable Object in other thread
     *
     * @param runnable
     */
    void runInThread(Runnable runnable) {
        try {
            Thread thread = new Thread(runnable::run);
            thread.start();
        } catch (Exception ex) {
            log.info(ex.getMessage());
        }

    }

    protected void validateUser(Integer value) throws CustomException {
        if (typeBase == 0) {
            user = getCurrentUser();
            if (user == null) {
                throw getUnauthentication("user null");
            }
            if (user.getRoleUser() != null) {
                if (user.getRoleUser().getValueRole() != null) {
                    if ((user.getRoleUser().getValueRole() & value) == 0) {
                        throw getException(6, "User khong co quen thuc hien thao tac nay");
                    }
                } else {
                    throw getException(6, "User chua duoc gan voi permission");
                }
            } else {
                throw getException(6, "User chua duoc gan voi role");
            }
        }

    }

    protected void actionProfileUser(UserEntity user, Boolean isCheck) {
        try {
            ProfileEntity profile = null;
            if (isCheck) {
                profile = new ProfileEntity();
                profile.setName(user.getName());
                profile.setAddress(user.getAddress());
                profile.setDob(user.getDob());
                profile.setPassport(user.getPassport());
                profile.setGender(user.getGender());
                profile.setPhone(user.getPhone());
                profile.setUser(user);
            } else {
                profile = profileRepository.findFirstByUserId(user.getId());
                profile.setName(user.getName());
                profile.setAddress(user.getAddress());
                profile.setDob(user.getDob());
                profile.setPassport(user.getPassport());
                profile.setGender(user.getGender());
                profile.setPhone(user.getPhone());
            }

            save(profile);
            actionRabbitMQUser(profile, isCheck);

            // TODO
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    protected void actionRabbitMQUser(ProfileEntity profileEntity, Boolean isCheck) {
        try {
            MessageRabbitProfile message = new MessageRabbitProfile();
            String status = profileEntity.getActive() == 0 ? "active" : "inActive";
            if (isCheck) {
                ProfileMessage profile = new ProfileMessage(profileEntity.getName(), profileEntity.getGender(),
                        DateTimeUtils.convertLocalDateTimeToString(profileEntity.getDob(), AppConst.DATE_TIME_FORMAT),
                        profileEntity.getPassport(), profileEntity.getPhone(), profileEntity.getAddress(),
                        String.valueOf(profileEntity.getUser().getId()), status);
                message.setAction("create");
                message.setData(profile);
            } else {
                ProfileMessage profile = new ProfileMessage(profileEntity.getName(), profileEntity.getGender(),
                        DateTimeUtils.convertLocalDateTimeToString(profileEntity.getDob(), AppConst.DATE_TIME_FORMAT),
                        profileEntity.getPassport(), profileEntity.getPhone(), profileEntity.getAddress(),
                        String.valueOf(profileEntity.getUser().getId()), status);
                message.setAction("update");
                message.setData(profile);
            }

            publisherUser.produceMsg(GsonUtils.toString(message));
            // TODO
        } catch (Exception e) {
        }
    }

    protected void actionProfileVendor(HospitalEntity user, Boolean isCheck) {
        try {
            ProfileEntity profile = null;
            if (isCheck) {
                profile = new ProfileEntity();
                profile.setName(user.getName());
                profile.setAddress(user.getAddress());
                profile.setPhone(user.getUserAdmin().getPhone());
                profile.setUser(user.getUserAdmin());
                profile.setTaxCode(user.getTaxCode());
            } else {
                profile = profileRepository.findFirstByUserId(user.getUserAdmin().getId());
                profile.setName(user.getName());
                profile.setAddress(user.getAddress());
                profile.setPhone(user.getUserAdmin().getPhone());
                profile.setTaxCode(user.getTaxCode());
            }

            save(profile);
            actionRabbitMQVendor(profile, isCheck, user.getId());

            // TODO
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    protected void actionRabbitMQVendor(ProfileEntity profileEntity, Boolean isCheck, Long id) {
        try {
            MessageRabbitProfile message = new MessageRabbitProfile();
            String status = profileEntity.getActive() == 0 ? "active" : "inActive";
            if (isCheck) {
                ProfileMessage profile = new ProfileMessage(profileEntity.getName(), profileEntity.getTaxCode(),
                        profileEntity.getAddress(), status, String.valueOf(id),
                        new WalletEntity(profileEntity.getWalletId()));
                message.setAction("create");
                message.setData(profile);
            } else {
                ProfileMessage profile = new ProfileMessage(profileEntity.getName(), profileEntity.getTaxCode(),
                        profileEntity.getAddress(), status, String.valueOf(id),
                        new WalletEntity(profileEntity.getWalletId()));
                message.setAction("update");
                message.setData(profile);
            }

            publisherVendor.produceMsg(GsonUtils.toString(message));
            // TODO
        } catch (Exception e) {
            LogUtils.getInstance().error(e);
        }
    }

    public static String getMD5Hex(final String inputString) throws NoSuchAlgorithmException {

        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(inputString.getBytes());

        byte[] digest = md.digest();

        return convertByteToHex(digest);
    }

    private static String convertByteToHex(byte[] byteData) {

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < byteData.length; i++) {
            sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }

        return sb.toString();
    }
}
