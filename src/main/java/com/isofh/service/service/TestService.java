package com.isofh.service.service;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.isofh.service.constant.AppConst;
import com.isofh.service.model.MessageRabbitProfile;
import com.isofh.service.model.ProfileMessage;
import com.isofh.service.model.ResultEntity;
import com.isofh.service.model.TestEntity;
import com.isofh.service.model.UserEntity;
import com.isofh.service.rabbit.publisher.PublisherHuong;
import com.isofh.service.rabbit.publisher.PublisherProfileUser;
import com.isofh.service.result.IMapData;
import com.isofh.service.utils.DateTimeUtils;
import com.isofh.service.utils.GsonUtils;

@Service
public class TestService extends BaseService implements IMapData<TestEntity> {

//	@Autowired
//	Publisher publishe1r;
	@Autowired
    PublisherProfileUser publisher;
	
	@Autowired
	PublisherHuong publisherHuong;
    @Override
    public Map<String, Object> getMapData(TestEntity entity) {
        Map<String, Object> mapData = new HashMap<>();
        mapData.put("test", entity);
        return mapData;
    }
    
    @Transactional
    public ResultEntity getDetail(Long id, Integer type) throws Exception {
        init();
        Map<String, String> tokenMap = new HashMap<String, String>();
        UserEntity user = getEntityById(userRepository, id);
        String tonken = digitalSignature.generateToken(id, type, user.getHospital() != null ? user.getHospital().getId():null);
        tokenMap.put("token", tonken);
        return ok(tokenMap);
    }

    public ResultEntity testMQ() throws Exception{
        MessageRabbitProfile message = new MessageRabbitProfile();
        ProfileMessage profile = new ProfileMessage("hung", 1, DateTimeUtils.convertLocalDateToString(LocalDate.now(), AppConst.DATE_FORMAT), "123123124", "0988454626", "asdassdasd", "1","active");
        message.setAction("create");
        message.setData(profile);
        publisher.produceMsg(GsonUtils.toString(message));
    	return ok();
    }
}
