package com.isofh.service.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.springframework.stereotype.Service;

import com.isofh.service.constant.ServiceConst;
import com.isofh.service.enums.HttpMethodType;
import com.isofh.service.model.HospitalEntity;
import com.isofh.service.model.ResultEntity;
import com.isofh.service.model.ResultHisEntity;
import com.isofh.service.model.ScheduleEntity;
import com.isofh.service.model.SpecialistEntity;
import com.isofh.service.result.IMapData;
import com.isofh.service.utils.CollectionUtils;
import com.isofh.service.utils.GsonUtils;
import com.isofh.service.utils.NetworkUtils;

@Service
public class SpecialistService extends BaseService implements IMapData<String> {

    @Override
    public Map<String, Object> getMapData(String entity) {
        Map<String, Object> mapData = new HashMap<>();
        mapData.put("test", entity);
        return mapData;
    }

    public ResultEntity sync(long hospitalId) throws Exception {
        try {
            init();

            List<Long> oldSpecialistIds = new ArrayList<>();
            Iterable<SpecialistEntity> specialists = specialistRepository.getAll(hospitalId);
            for (SpecialistEntity specialist : specialists) {
                oldSpecialistIds.add(specialist.getId());
            }

            HospitalEntity hospitalEntity = getHospital(hospitalId);
            if (hospitalEntity != null) {
                String queryData = NetworkUtils.callService(hospitalEntity.getServiceUrl() + ServiceConst.SYNC_SPECIALIST,
                        HttpMethodType.GET);
                ResultHisEntity data = GsonUtils.toObject(queryData, ResultHisEntity.class);
                if (data == null || data.getCode() != 0) {
                    throw getException(1, "His tra ve loi ");
                }

                for (int i = 0; i < data.getData().getSpecialists().length; i++) {
                    try {
                        SpecialistEntity entity = data.getData().getSpecialists()[i];
                        entity.setHospital(hospitalEntity);
                        if (Objects.isNull(entity.getHisSpecialistId())) {
                            continue;
                        }

                        SpecialistEntity specialistEntity = specialistRepository.findByHisSpecialistId(entity.getHisSpecialistId(),
                                hospitalId);
                        if (specialistEntity != null) {
                            entity = specialistEntity;
                            oldSpecialistIds.remove(specialistEntity.getId());
                        }
                        save(entity);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
                for (Long id : oldSpecialistIds) {
                    SpecialistEntity specialistEntity = getSpecialist(id);
                    specialistEntity.setDeleted(1);
                    save(specialistEntity);

                    List<ScheduleEntity> deletedSchedules = scheduleRepository.findBySpecialist(id);
                    if (!CollectionUtils.isNullOrEmpty(deletedSchedules)) {
                        for (ScheduleEntity schedule : deletedSchedules) {
                            schedule.setDeleted(1);
                        }
                        scheduleRepository.saveAll(deletedSchedules);
                    }
                }
            } else {
                throw getException(2, "Nhap sai id hospital");
            }

            return ok();
        } catch (Exception ex) {
            throw ex;
        }

    }

    public ResultEntity getAll(long hospitalId) throws Exception {
        try {
            init();
            Iterable<SpecialistEntity> specialists = specialistRepository.getAll(hospitalId);
            return ok(SPECIALISTS, specialists);
        } catch (Exception ex) {
            throw ex;
        }
    }

    public ResultEntity getByDepartmentSpecialist(Long departmentId, Long doctorId) throws Exception {
        try {
            init();
            List<SpecialistEntity> specialists = specialistRepository.findByDepartmentDoctor(departmentId, doctorId);
            return ok(SPECIALISTS, specialists);
        } catch (Exception ex) {
            throw ex;
        }
    }
}
