package com.isofh.service.service;

import com.isofh.service.model.ResultEntity;
import com.isofh.service.model.SmsEntity;
import com.isofh.service.result.IMapData;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SmsService extends BaseService implements IMapData<SmsEntity> {

    @Override
    public Map<String, Object> getMapData(SmsEntity entity) {
        Map<String, Object> mapData = new HashMap<>();
        return mapData;
    }

    @Transactional
    public ResultEntity search() throws Exception {
        init();
        List<SmsEntity> smsEntities = smsRepository.getNotSent();
        return ok("sms", smsEntities);
    }

    @Transactional
    public ResultEntity setSent(Long id) throws Exception {
        init();
        SmsEntity smsEntity = getSms(id);
        smsEntity.setSent(1);
        smsRepository.save(smsEntity);
        return ok();
    }
}
