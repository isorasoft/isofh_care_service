package com.isofh.service.service;

import com.isofh.service.model.ResultEntity;
import com.isofh.service.model.SlideEntity;
import com.isofh.service.model.SlidePlaceEntity;
import com.isofh.service.result.IMapData;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SlidePlaceService extends BaseService implements IMapData<SlidePlaceEntity> {

    @Override
    public Map<String, Object> getMapData(SlidePlaceEntity entity) {
        Map<String, Object> mapData = new HashMap<>();
        mapData.put(SLIDE_PLACE, entity);
        return mapData;
    }

    @Transactional
    public ResultEntity create(Object object) throws Exception {
        init(object);
        SlidePlaceEntity slidePlace = getObject(SLIDE_PLACE, SlidePlaceEntity.class);
        Long slideId = getLong("slideId");
        SlideEntity slide = getSlide(slideId);
        slidePlace.setSlide(slide);
        slidePlaceRepository.save(slidePlace);
        return ok(SLIDE_PLACE, slidePlace);
    }

    @Transactional
    public ResultEntity update(Object object, long id) throws Exception {
        init(object);
        SlidePlaceEntity data = getObject(SLIDE_PLACE, SlidePlaceEntity.class);
        SlidePlaceEntity entity = getSlidePlace(id);
        entity.setName(data.getName());
        Long slideId = getLong("slideId");
        SlideEntity slide = getSlide(slideId);
        entity.setSlide(slide);
        slidePlaceRepository.save(entity);
        return ok(SLIDE_PLACE, entity);
    }

    @Transactional
    public ResultEntity delete(long id) throws Exception {
        init();
        slidePlaceRepository.deleteById(id);
        return ok();
    }

    @Transactional
    public ResultEntity search(Integer page, Integer size, String name) throws Exception {
        init();
        Page<SlidePlaceEntity> queryResults = slidePlaceRepository.search(name, getDefaultPage(page, size));
        List<Map<String, Object>> mapResults = new ArrayList<>();
        for (SlidePlaceEntity entity : queryResults.getContent()) {
            Map<String, Object> mapResult = new HashMap<>();
            mapResult.put(SLIDE_PLACE, entity);
            mapResult.put(SLIDE, entity.getSlide());
            mapResult.put(SLIDE_ITEMS, entity.getSlide().getSlideItems());
            mapResults.add(mapResult);
        }

        return ok(queryResults.getTotalElements(), mapResults);
    }
}
