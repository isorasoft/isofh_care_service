package com.isofh.service.service;

import com.isofh.service.model.PermissionEntity;
import com.isofh.service.model.ResultEntity;
import com.isofh.service.result.IMapData;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PermissionService extends BaseService implements IMapData<PermissionEntity> {

    @Override
    public Map<String, Object> getMapData(PermissionEntity entity) {
        Map<String, Object> mapData = new HashMap<>();
        mapData.put(PERMISSION, entity);
        return mapData;
    }

    @Override
    public List<Map<String, Object>> getMapDatas(Iterable<PermissionEntity> notifications) {
        List<Map<String, Object>> mapResults = new ArrayList<>();
        for (PermissionEntity notification : notifications) {
            mapResults.add(getMapData(notification));
        }
        return mapResults;
    }


    @Transactional
    public ResultEntity create(Object object) throws Exception {
        init(object);
        PermissionEntity entity = getObject(PERMISSION, PermissionEntity.class);
        save(entity);
        return ok(getMapData(entity));
    }

    @Transactional
    public ResultEntity update(Object object, long id) throws Exception {
        init(object);
        PermissionEntity data = getObject(PERMISSION, PermissionEntity.class);
        PermissionEntity entity = getPermission(id);

        entity.setName(data.getName());

        save(entity);
        return ok(getMapData(entity));
    }

    @Transactional
    public ResultEntity delete(long id) throws Exception {
        init();
        permissionRepository.deleteById(id);
        return ok();
    }

    @Transactional
    public ResultEntity search(Integer page, Integer size, String name) throws Exception {
        init();
        Page<PermissionEntity> queryResults = permissionRepository.search(name, getDefaultPage(page, size));
        return ok(getMapDatas(queryResults));
    }


}

