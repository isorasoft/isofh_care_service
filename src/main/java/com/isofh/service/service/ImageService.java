package com.isofh.service.service;

import com.isofh.service.model.ImageEntity;
import com.isofh.service.model.ResultEntity;
import com.isofh.service.result.IMapData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Service
public class ImageService extends BaseService implements IMapData<ImageEntity> {

    @Autowired
    private ImageStorageService imageStorageService;

    @Override
    public Map<String, Object> getMapData(ImageEntity entity) {
        Map<String, Object> mapData = new HashMap<>();
        mapData.put(IMAGE, entity);
        return mapData;
    }

    @Transactional
    public ResultEntity upload(MultipartFile file) throws Exception {
        init();
        log.info("upload " + file.getName());
        Map<String, String> images = imageStorageService.storeImage(file);

        if (!images.containsKey("image")) {
            throw getException("import khong thanh cong");
        }
        String imageLocation = images.get("image");
        String thumbnailLocation = imageLocation;

        if (images.containsKey("thumbnail")) {
            thumbnailLocation = images.get("thumbnail");
        }

        log.info("imageLocation " + imageLocation);
        log.info("thumbnailLocation " + thumbnailLocation);
        Map<String, Object> map = new HashMap<>();
        map.put("image", imageLocation);
        map.put("thumbnail", thumbnailLocation);

        ResultEntity resultEntity = ok("images", Collections.singletonList(map));
        return resultEntity;
    }


}
