package com.isofh.service.service;

import com.isofh.service.enums.NotificationType;
import com.isofh.service.enums.NumberType;
import com.isofh.service.enums.UserType;
import com.isofh.service.model.CommentEntity;
import com.isofh.service.model.PostEntity;
import com.isofh.service.model.ResultEntity;
import com.isofh.service.model.UserEntity;
import com.isofh.service.result.IMapData;
import com.isofh.service.utils.GsonUtils;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CommentService extends BaseService implements IMapData<CommentEntity> {

    @Override
    public Map<String, Object> getMapData(CommentEntity entity) {
        Map<String, Object> mapData = new HashMap<>();
//        mapData.put(AUTHOR, entity.getAuthor());
        mapData.put(COMMENT, entity);
        mapData.put(USER, entity.getUser() != null ? getMapDataUser(entity.getUser()) : null);
        return mapData;
    }

    public Map<String, Object> getMapDataUser(UserEntity entity) {
        Map<String, Object> mapData = new HashMap<>();
        mapData.put("name", entity.getName());
        mapData.put("id", entity.getId());
        mapData.put("avatar", entity.getAvatar());
        return mapData;
    }

    @Transactional
    public ResultEntity create(Object object) throws Exception {
        init(object);
        CommentEntity comment = getObject("comment", CommentEntity.class);
//        comment.setAuthor(getCurrentUser());

        Long postId = getLong("postId");
        String diagnose = getString("diagnose");
        PostEntity postEntity = getPost(postId);
        postEntity.setUpdatedDate(LocalDateTime.now());
        if (postEntity == null) {
            throw getException(2, "khong ton tai bao post nay!");
        }
        comment.setUser(getCurrentUser());
        if (diagnose == null && postEntity.getIsAnswered() == 0
                && getCurrentUser().getRole() == UserType.DOCTOR.getValue()) {
            throw getException(2, "phai co chan doan benh khi bac si comment dau tien");
        }

        if (postEntity.getIsAnswered() == 0 && getCurrentUser().getRole() == UserType.DOCTOR.getValue()) {
            UserEntity doctor = getCurrentUser();
            if (doctor.getNumberAnsweredPost() == null) {
                doctor.setNumberAnsweredPost(1);
            } else {
                doctor.setNumberAnsweredPost(doctor.getNumberAnsweredPost() + 1);
            }
            doctor.setNumberWaitingPost(doctor.getNumberWaitingPost() - 1);
            save(doctor);
        }
        if (diagnose != null && getCurrentUser().getRole() == UserType.DOCTOR.getValue()) {
            postEntity.setDiagnose(diagnose);

        }
        int dem = postEntity.getCommentCount() + 1;
        postEntity.setCommentCount(dem);

//        postEntity.setCommentCount(postEntity.getComments().size() + 1);
        comment.setPost(postEntity);
        if (getCurrentUser().getRole() == UserType.DOCTOR.getValue()) {
            postEntity.setStatus(3);
            postEntity.setIsAnswered(1);
        }
        save(comment);

        if (getCurrentUser().getRole() == UserType.DOCTOR.getValue()) {
            postEntity.setDoctorComment(GsonUtils.toString(getMapData(comment)));
        } else {
            if (postEntity.getNumberCommentUser() != null) {
                postEntity.setNumberCommentUser(postEntity.getNumberCommentUser() + 1);
            } else {
                postEntity.setNumberCommentUser(1);
            }
        }

        save(postEntity);

        Thread thread = new Thread(() -> {
            try {
                if (getCurrentUser().getRole() == UserType.DOCTOR.getValue()) {
                    List<CommentEntity> commentEntities = commentRepository.searchList(postId);
                    if(commentEntities.size()>= 2) {
                        List<CommentEntity> commentEntity = commentRepository.searchEach(postId);
                        CommentEntity commentEntity1 = commentEntity.get(1);
                        if (commentEntity1 != null && (commentEntity1.getUser().getRole() != UserType.DOCTOR.getValue())) {
                            sendNotification(NumberType.COMMENT.getValue(), "Bác sĩ", NotificationType.COMMENT.getValue(),
                                    postEntity.getAuthor().getId(), postId);
                        }
                    } else{
                        sendNotification(NumberType.COMMENT.getValue(), "Bác sĩ", NotificationType.COMMENT.getValue(),
                                postEntity.getAuthor().getId(), postId);
                    }
                } else {
                    sendNotification(NumberType.COMMENT.getValue(),"Câu hỏi", NotificationType.COMMENT_USER.getValue(), postEntity.getAssignee().getId(),postId);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
        thread.start();
        //runInThread(() -> );
        return ok(getMapData(comment));
    }

    @Transactional
    public ResultEntity search(Integer page, Integer size, Integer role, Integer postId) throws Exception {
        init();
        Page<CommentEntity> results = commentRepository.search(postId, role, getDefaultPage(page, size));
        return ok(results.getTotalElements(), getMapDatas(results));
    }

    @Transactional
    public ResultEntity update(Object object, long id) throws Exception {
        init(object);
        CommentEntity comment = getObject("comment", CommentEntity.class);
        CommentEntity entity = getComment(id);
        PostEntity postEntity = getPost(getLong("postId"));
        postEntity.setCommentCount(postEntity.getCommentCount() + 1);
        save(postEntity);
        entity.setContent(comment.getContent());
//        entity.setImages(comment.getImages());
        save(entity);
        commentRepository.save(entity);
        return ok(getMapData(entity));
    }

    @Transactional
    public ResultEntity delete(long id) throws Exception {
        init();
        commentRepository.deleteById(id);
        return ok();
    }

    @Transactional
    public ResultEntity acceptAsSolution(Object object, long id) throws Exception {
        init(object);
        Integer isSolution = getInteger("isSolution");
        CommentEntity entity = getComment(id);
//        entity.setIsSolution(isSolution);
        save(entity);
        return ok();
    }

//    @Transactional
//    public ResultEntity like(Object object, long id) throws Exception {
//        init(object);
//        Integer isLiked = getInteger("isLiked");
//        CommentEntity entity = getComment(id);
//        if (isLiked == 1) {
//            entity.setLikeCount(entity.getLikeCount() + 1);
//        } else if (isLiked == 0 && entity.getLikeCount() != 0) {
//            entity.setLikeCount(entity.getLikeCount() - 1);
//        } else if (isLiked == 0 && entity.getLikeCount() == 0) {
//            entity.setLikeCount(0);
//        }
//        save(entity);
//        return ok();
//    }
}
