package com.isofh.service.service;

import com.isofh.service.model.MenuEntity;
import com.isofh.service.model.ResultEntity;
import com.isofh.service.result.IMapData;
import com.isofh.service.utils.CollectionUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class MenuService extends BaseService implements IMapData<MenuEntity> {

    @Override
    public Map<String, Object> getMapData(MenuEntity entity) {
        Map<String, Object> mapData = new HashMap<>();
        mapData.put(MENU, entity);
        return mapData;
    }

    @Transactional
    public ResultEntity create(Object object) throws Exception {
        init(object);
        MenuEntity entity = getObject(MENU, MenuEntity.class);
        long parentId = getLong("parentId", -1L);
        if (parentId != -1) {
            MenuEntity parent = getMenu(parentId);
            entity.setParent(parent);
        }
        menuRepository.save(entity);
        return ok(Arrays.asList(MENU, "parent"), Arrays.asList(entity, entity.getParent()));
    }

    @Transactional
    public ResultEntity update(Object object, Long id) throws Exception {
        init(object);

        MenuEntity entity = getMenu(id);
        MenuEntity data = getObject(MENU, MenuEntity.class);

        entity.setName(data.getName());
        entity.setHref(data.getHref());
        entity.setLevel(data.getLevel());
        entity.setRole(data.getRole());
        entity.setIsActive(data.getIsActive());
        entity.setMenuIndex(data.getMenuIndex());

        long parentId = getLong("parentId", -1L);
        if (parentId != -1) {
            MenuEntity parent = getMenu(parentId);
            entity.setParent(parent);
        } else {
            entity.setParent(null);
        }
        menuRepository.save(entity);
        return ok(Arrays.asList(MENU, "parent"), Arrays.asList(entity, entity.getParent()));
    }

    @Transactional
    public ResultEntity delete(Long id) throws Exception {
        init();
        menuRepository.deleteById(id);
        return ok();
    }

    @Transactional
    public ResultEntity search(Integer page, Integer size, String name, Long level, Long role, Integer isActive) throws Exception {
        init();
        Sort.Order order = new Sort.Order(Sort.Direction.DESC, "menu_index");
        Page<MenuEntity> menus = menuRepository.search(name, level, role, isActive, getPaging(page, size, Sort.by(order)));
        List<Map<String, Object>> mapResults = new ArrayList<>();
        for (MenuEntity entity : menus.getContent()) {
            Map<String, Object> mapResult = new HashMap<>();
            mapResults.add(mapResult);
            mapResult.put(MENU, entity);
            List<Map<String, Object>> menuLevel2 = new ArrayList<>();
            if (!CollectionUtils.isNullOrEmpty(entity.getMenus())) {
                for (MenuEntity menu2 : entity.getMenus()) {
                    if (role != -1 && (menu2.getRole() & role) == 0) {
                        continue;
                    }
                    if (isActive != -1 && !Objects.equals(menu2.getIsActive(), isActive)) continue;
                    Map<String, Object> mapMenu2 = new HashMap<>();
                    mapMenu2.put(MENU, menu2);
                    List<Object> menu3s = new ArrayList<>();
                    if (!CollectionUtils.isNullOrEmpty(menu2.getMenus())) {
                        for (MenuEntity menu3 : menu2.getMenus()) {
                            if (isActive != -1 && !Objects.equals(menu3.getIsActive(), isActive)) continue;
                            if (role != -1 && (menu3.getRole() & role) == 0) {
                                continue;
                            }
                            menu3s.add(menu3);
                        }
                    }
                    mapMenu2.put(MENUS, menu3s);
                    menuLevel2.add(mapMenu2);
                }
            }
            mapResult.put(MENUS, menuLevel2);
        }

        return ok(menus.getTotalElements(), mapResults);
    }

    @Transactional
    public ResultEntity getDetail(long id) throws Exception {
        init();
        MenuEntity entity = getMenu(id);
        List<Map<String, Object>> mapResults = new ArrayList<>();
        if (!CollectionUtils.isNullOrEmpty(entity.getMenus())) {
            for (MenuEntity menu : entity.getMenus()) {
                Map<String, Object> mapResult = new HashMap<>();
                mapResults.add(mapResult);
                mapResult.put(MENU, menu);
                mapResult.put(MENUS, menu.getMenus());
            }
        }
        return ok(Arrays.asList(MENU, MENUS), Arrays.asList(entity, mapResults));
    }

    @Transactional
    public ResultEntity setActive(Object object, Long id) throws Exception {
        init(object);
        MenuEntity entity = getMenu(id);
        Integer isActive = getInteger("isActive");
        entity.setIsActive(isActive);
        menuRepository.save(entity);
        return ok(Arrays.asList(MENU, "parent"), Arrays.asList(entity, entity.getParent()));
    }
}
