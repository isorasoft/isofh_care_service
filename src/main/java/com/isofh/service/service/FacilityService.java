package com.isofh.service.service;

import com.isofh.service.constant.field.FacilityField;
import com.isofh.service.constant.type.QueueType;
import com.isofh.service.model.*;
import com.isofh.service.result.IMapData;
import com.isofh.service.utils.ArrayUtils;
import com.isofh.service.utils.RabbitmqUtils;
import org.apache.poi.ss.usermodel.*;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.time.LocalDate;
import java.util.*;

@Service
public class FacilityService extends BaseService implements IMapData<FacilityEntity> {

    @Value("${project.data}")
    private String dataPath;

    @Override
    public Map<String, Object> getMapData(FacilityEntity entity) {
        Map<String, Object> mapData = new HashMap<>();
        mapData.put(FACILITY, entity);
        return mapData;
    }

    public ResultEntity importFile(Object object) throws Exception {
        init(object);
        String fileName = getString("file");
        String file = dataPath + fileName;
        List<Integer> datas = importFile(file);
        return ok(Arrays.asList("total", "update", "insert"), Arrays.asList(datas.get(0), datas.get(1), datas.get(2)));
    }

    public List<Integer> importFile(String file) {
        int total = 0;
        int update = 0;
        int insert = 0;
        try {
            // Creating a Workbook from an Excel file (.xls or .xlsx)
            Workbook workbook = WorkbookFactory.create(new File(file));

            // Getting the Sheet at index zero

            for (int countSheet = 0; countSheet < 6; countSheet++) {
                Sheet sheet = workbook.getSheetAt(countSheet);
                // Create a DataFormatter to format and get each cell's value as String
                DataFormatter dataFormatter = new DataFormatter();

                Iterator<Row> rowIterator = sheet.rowIterator();
//            Iterable<StaffEntity> staffs = staffRepository.findAll();
//            Map<String, Long> mapUsers = new HashMap<>();
//            for (StaffEntity entity : staffs) {
//                mapUsers.put(entity.getType(), entity.getId());
//            }


                // bo qua line dau tien
                int ignoreRow = 2;
                boolean finish = false;
                while (rowIterator.hasNext() && !finish) {
                    try {
                        Row row = rowIterator.next();
                        if (ignoreRow > 0) {
                            ignoreRow--;
                            continue;
                        }

                        // Now let's iterate over the columns of the current row
                        Iterator<Cell> cellIterator = row.cellIterator();
                        int currentCol = 0;
                        if (!cellIterator.hasNext()) {
                            finish = true;
                            break;
                        }
                        FacilityEntity entity = new FacilityEntity();
                        LocalDate dateValue = null;
                        while (cellIterator.hasNext() && !finish) {
                            Cell cell = cellIterator.next();
                            String cellValue = dataFormatter.formatCellValue(cell).trim();
                            if (dateValue != null) {
                                break;
                            }
                            switch (currentCol) {
                                // name
                                case 0:
                                    String nameFacility = cellValue;
                                    entity.setName(nameFacility);
                                    break;
                                case 1:
                                    String province = cellValue;
//                                    ProvinceEntity provinceEntity = provinceRepository.findByName("Hà Nội");
//                                    entity.setProvince(provinceEntity);
                                    break;
                                case 2:
                                    String type = cellValue;
                                    if (type.equals("Phòng khám") || type.equals("phong kham")) {
                                        entity.setType(2);
                                    } else if (type.equals("Bệnh viện") || type.equals("benh vien")) {
                                        entity.setType(1);
                                    } else if (type.equals("Trung tâm y tế") || type.equals("trung tam y te")) {
                                        entity.setType(4);
                                    } else if (type.equals("Nha thuoc") || type.equals("Nhà thuốc")) {
                                        entity.setType(8);
                                    } else {
                                        entity.setType(2);
                                    }

                                    break;
                                case 3:
                                    String special = cellValue;
                                    CommonSpecialistEntity commonSpecialistEntity = commonSpecialistRepository.findFirstByName(special);
                                    if (commonSpecialistEntity != null) {
                                        entity.getSpecialists().add(commonSpecialistEntity);
                                    } else {
                                        CommonSpecialistEntity commonSpecialistEntity1 = new CommonSpecialistEntity();
                                        commonSpecialistEntity1.setName(special);
                                        commonSpecialistEntity1.getFacilities().add(entity);
                                        entity.getSpecialists().add(commonSpecialistEntity1);
                                    }
                                    break;
                                // chuc vu
                                case 4:
                                    String address = cellValue;
                                    entity.setAddress(address);
                                    break;
                                case 5:
                                    String address1 = entity.getAddress() + " " + cellValue;
                                    entity.setAddress(address1);
                                    break;
                                case 6:
                                    String address2 = entity.getAddress() + " " + cellValue;
                                    entity.setAddress(address2);
                                    break;
                                case 7:
                                    break;
                                case 8:
                                    break;
                                case 9:
                                    break;
                                default: {
                                    break;
                                }
                            }
                            currentCol++;
                            save(entity);
                        }

                        if (finish) {
                            break;
                        }

                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
            workbook.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return Arrays.asList(total, update, insert);
    }

    @Transactional
    public ResultEntity create(Object object) throws Exception {
        init(object);
        FacilityEntity entity = getObject(FACILITY, FacilityEntity.class);
        if (facilityRepository.checkNameAndAddress(entity.getName(), entity.getAddress()) != null) {
            throw getException(2, "Nha thuoc nay da ton tai");
        }

        long provinceId = getLong("provinceId", -1L);
        long userId = getLong("userId", -1L);
        if (!Objects.equals(userId, -1L)) {
            UserEntity userEntity = getUser(userId);
            entity.setUser(userEntity);
        }

        if (provinceId != -1) {
            ProvinceEntity province = getProvince(provinceId);
            entity.setProvince(province);
        }

        Long[] specialists = getObject("specialistIds", Long[].class);
        if (!ArrayUtils.isNullOrEmpty(specialists)) {
            for (Long specialistId : specialists) {
                CommonSpecialistEntity commonSpecialistEntity = getCommonSpecialist(specialistId);
                if (commonSpecialistEntity != null) {
                    entity.getSpecialists().add(commonSpecialistEntity);
                }
            }
        }

        facilityRepository.save(entity);
        String[] imageUrls = getObject("imageUrls", String[].class);
        if (!ArrayUtils.isNullOrEmpty(imageUrls)) {
            for (String url : imageUrls) {
                ImageEntity imageEntity = new ImageEntity();
                imageEntity.setUrl(url);
                imageEntity.setFacility(entity);
                imageRepository.save(imageEntity);
                entity.getImages().add(imageEntity);
            }
        }
        sendCreatedToQueue(entity);
        return ok(Arrays.asList(FACILITY, IMAGES, SPECIALISTS, PROVINCE), Arrays.asList(entity, entity.getImages(), entity.getSpecialists(), entity.getProvince()));
    }

    public ResultEntity update(Object object, long id) throws Exception {
        init(object);
        FacilityEntity data = getObject(FACILITY, FacilityEntity.class);
        FacilityEntity entity = getFacility(id);

        if (!Objects.equals(data.getName(), entity.getName())) {
            if (facilityRepository.checkName(data.getName()) != null) {
                throw getException(2, "Ten nha thuoc nay da ton tai");
            }
        }

        entity.setName(data.getName());
        entity.setCode(data.getCode());
        entity.setType(data.getType());
        entity.setLevelId(data.getLevelId());
        entity.setRankId(data.getRankId());
        entity.setWebsite(data.getWebsite());
        entity.setReview(data.getReview());
        entity.setBelongIsofh(data.getBelongIsofh());
        entity.setAddress(data.getAddress());
        entity.setPhone(data.getPhone());
        entity.setIntroduction(data.getIntroduction());
        entity.setApproval(data.getApproval());
        entity.setLatitude(data.getLatitude());
        entity.setLongitude(data.getLongitude());
        entity.setGpp(data.getGpp());
        entity.setAddress(data.getAddress());
        entity.setLicenseNumber(data.getLicenseNumber());
        entity.setLogo(data.getLogo());
        entity.setEmergencyContact(data.getEmergencyContact());
        entity.setPharmacist(data.getPharmacist());

        long provinceId = getLong("provinceId", -1L);
        if (provinceId != -1) {
            ProvinceEntity province = getProvince(provinceId);
            entity.setProvince(province);
        }

        String[] imageUrls = getObject("imageUrls", String[].class);
        entity.getImages().clear();
        if (!ArrayUtils.isNullOrEmpty(imageUrls)) {
            for (String url : imageUrls) {
                ImageEntity imageEntity = new ImageEntity();
                imageEntity.setUrl(url);
                imageEntity.setFacility(entity);
                entity.getImages().add(imageEntity);
            }
        }

        Long[] specialistIds = getObject("specialistIds", Long[].class);
        entity.getSpecialists().clear();
        if (!ArrayUtils.isNullOrEmpty(specialistIds)) {
            for (Long specialistId : specialistIds) {
                CommonSpecialistEntity commonSpecialistEntity = getCommonSpecialist(specialistId);
                entity.getSpecialists().add(commonSpecialistEntity);
            }
        }
        facilityRepository.save(entity);
        sendUpdatedToQueue(entity);
        return ok(Arrays.asList(FACILITY, "images", "specialists"), Arrays.asList(data, data.getImages(), data.getSpecialists()));
    }

    @Transactional
    public ResultEntity delete(Long id) throws Exception {
        init();
        facilityRepository.deleteById(id);
        sendDeletedToQueue(id);
        return ok();
    }

    @Transactional
    public ResultEntity search(Integer page, Integer size, String name, String code, Long levelId, Long approval, Integer type, Long rankId, Long provinceId, String website, Float latitude, Float longitude, Integer sortType,
                               String licenseNumber, String gpp, String address, String phone, LocalDate fromUpdatedDate, LocalDate toUpdatedDate, Long userId, Integer review, String emergencyContact,
                               String username) throws Exception {
        init();
        Page<FacilityEntity> diseases = facilityRepository.search(name, levelId, rankId, provinceId, website, code, approval, latitude, longitude, sortType, fromUpdatedDate, toUpdatedDate, type, licenseNumber, gpp, address, phone, userId
                , review, emergencyContact, username, PageRequest.of(page - 1, size));
        List<Map<String, Object>> mapResults = new ArrayList<>();
        for (FacilityEntity facilityEntity : diseases.getContent()) {
            Map<String, Object> mapResult = new HashMap<>();
            mapResults.add(mapResult);
            mapResult.put(FACILITY, facilityEntity);
            mapResult.put(SPECIALISTS, facilityEntity.getSpecialists());
            List<ImageEntity> imageEntities = new ArrayList<>(facilityEntity.getImages());
            imageEntities.sort(Comparator.comparing(ImageEntity::getId));
            mapResult.put(IMAGES, imageEntities);
            mapResult.put(PROVINCE, facilityEntity.getProvince());
            mapResult.put(USER, facilityEntity.getUser());
        }
        return ok(diseases.getTotalElements(), mapResults);
    }

    @Transactional
    public ResultEntity updateApproval(Object object, long id) throws Exception {
        init(object);
        FacilityEntity entity = getFacility(id);
        int approval = getInteger(FacilityField.APPROVAL, 0);
        entity.setApproval(approval);
        facilityRepository.save(entity);
        sendUpdatedToQueue(entity);
        return ok(Arrays.asList(FACILITY, IMAGES, SPECIALISTS), Arrays.asList(entity, entity.getImages(), entity.getSpecialists()));
    }

    @Transactional
    public ResultEntity importCsv(Object object) throws Exception {
        init(object);
        JSONArray drugs = getJSONArray("facilities");
        Iterable<ProvinceEntity> provinces = provinceRepository.findAll();
        Map<String, ProvinceEntity> mapProvinces = new HashMap<>();
        for (ProvinceEntity entity : provinces) {
            mapProvinces.put(entity.getName(), entity);
        }

        Map<String, Integer> mapLevel = new HashMap<>();
        mapLevel.put("Tuyến 1", 1);
        mapLevel.put("Tuyến 2", 2);
        mapLevel.put("Tuyến 3", 4);
        mapLevel.put("Tuyến 4", 8);

        Map<String, Integer> mapRank = new HashMap<>();
        mapRank.put("Hạng đặc biệt", 1);
        mapRank.put("Hạng 1", 2);
        mapRank.put("Hạng 2", 4);
        mapRank.put("Hạng 3", 8);
        mapRank.put("Hạng 4", 16);

        Map<String, Integer> mapType = new HashMap<>();
        mapType.put("Bệnh viện", 1);
        mapType.put("Phòng khám", 2);
        mapType.put("Trung tâm y tế", 4);


        List<FacilityEntity> facilityEntities = new ArrayList<>();
        if (drugs != null) {
            for (int i = 0; i < drugs.length(); i++) {
                JSONObject facilityObj = drugs.getJSONObject(i);
                FacilityEntity facilityEntity = new FacilityEntity();
                facilityEntity.setCode(facilityObj.getString("Code"));
                facilityEntity.setName(facilityObj.getString("name"));
                facilityEntity.setWebsite(facilityObj.getString("website"));

                String level = facilityObj.getString("Levelid");
                if (mapLevel.containsKey(level)) {
                    facilityEntity.setLevelId(mapLevel.get(level));
                }

                String rank = facilityObj.getString("rankId");
                if (mapRank.containsKey(rank)) {
                    facilityEntity.setRankId(mapRank.get(rank));
                }

                String type = facilityObj.getString("Type");
                if (mapType.containsKey(type)) {
                    facilityEntity.setType(mapType.get(type));
                }
                facilityEntity.setAddress(facilityObj.getString("address"));
                String province = facilityObj.getString("Province");
                if (mapProvinces.containsKey(province)) {
                    facilityEntity.setProvince(mapProvinces.get(province));
                }
                facilityEntity.setPhone(facilityObj.getString("phone"));
                facilityRepository.save(facilityEntity);
//                    facilityEntities.add(facilityEntity);
            }
        }

        return ok("drugs", facilityEntities);
    }

    @Transactional
    public ResultEntity searchByQuery(Integer page, Integer size, String query, Long specialistId, Integer approval) throws Exception {
        init();
        Sort.Order order = new Sort.Order(Sort.Direction.DESC, "review");
        Page<FacilityEntity> queryResuls = facilityRepository.searchByQuery(query, specialistId, approval, getPaging(page, size, Sort.by(order)));
        List<Map<String, Object>> mapResults = new ArrayList<>();
        for (FacilityEntity facilityEntity : queryResuls.getContent()) {
            Map<String, Object> mapResult = new HashMap<>();
            mapResults.add(mapResult);
            mapResult.put(FACILITY, facilityEntity);
            mapResult.put(SPECIALISTS, facilityEntity.getSpecialists());
            mapResult.put(IMAGES, facilityEntity.getImages());
            mapResult.put(PROVINCE, facilityEntity.getProvince());
            mapResult.put(USER, facilityEntity.getUser());
        }
        return ok(queryResuls.getTotalElements(), mapResults);
    }

    @Transactional
    public ResultEntity getDetail(long id) {
        init();
        FacilityEntity facilityEntity = getFacility(id);
        Map<String, Object> mapResult = new HashMap<>();
        mapResult.put(FACILITY, facilityEntity);
        mapResult.put(SPECIALISTS, facilityEntity.getSpecialists());
        mapResult.put(IMAGES, facilityEntity.getImages());
        mapResult.put(PROVINCE, facilityEntity.getProvince());
        mapResult.put(USER, facilityEntity.getUser());
        return ok(mapResult);
    }

    @Transactional
    public ResultEntity review(Object object, long id) throws Exception {
        init(object);
        FacilityEntity entity = getFacility(id);
        int review = getInteger(FacilityField.REVIEW, -1);
        if (review == -1) {
            throw getException(1, "tham so khong hop le");
        }
        float newReview = (entity.getReview() * entity.getReviewCount() + review) / (entity.getReviewCount() + 1);
        entity.setReview(newReview);
        entity.setReviewCount(entity.getReviewCount() + 1);
        facilityRepository.save(entity);
        return ok(Arrays.asList(FACILITY, IMAGES, SPECIALISTS), Arrays.asList(entity, entity.getImages(), entity.getSpecialists()));
    }

    @Transactional
    public ResultEntity searchFacilityFromDrug(Integer page, Integer size, Long drugId) throws Exception {
        init();
        Page<FacilityEntity> facilitys = facilityRepository.findByDrug(drugId, getDefaultPage(page, size));
        List<Map<String, Object>> mapResults = new ArrayList<>();
        for (FacilityEntity facility : facilitys) {
            Map<String, Object> mapResult = new HashMap<>();
            mapResults.add(mapResult);
            mapResult.put(FACILITY, facility);
        }
        return ok(facilitys.getTotalElements(), mapResults);
    }

    /**
     * day thong tin tao moivao Rbmq xu ly
     *
     * @param entity
     */
    private void sendCreatedToQueue(FacilityEntity entity) {
        QueueEntity queueEntity = new QueueEntity(entity, QueueType.CREATE.getValue());
        sendMessage(queueEntity);
    }

    /**
     * day thong tin cap nhap vao Rbmq xu ly
     *
     * @param entity
     */
    private void sendUpdatedToQueue(FacilityEntity entity) {
        QueueEntity queueEntity = new QueueEntity(entity, QueueType.UPDATE.getValue());
        sendMessage(queueEntity);
    }

    /**
     * day thong tin tao xoa vao Rbmq xu ly
     *
     * @param id
     */
    private void sendDeletedToQueue(long id) {
        Map<String, Object> mapId = new HashMap<>();
        mapId.put("id", id);
        QueueEntity queueEntity = new QueueEntity(mapId, QueueType.DELETE.getValue());
        sendMessage(queueEntity);
    }

    private void sendMessage(QueueEntity queue) {
        RabbitmqUtils.sendMessages(facilityQueue, Arrays.asList(queue));
    }

    @Value("${facility.queue}")
    private String facilityQueue;

}
