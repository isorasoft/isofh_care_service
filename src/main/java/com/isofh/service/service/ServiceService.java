package com.isofh.service.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.springframework.stereotype.Service;

import com.isofh.service.constant.ServiceConst;
import com.isofh.service.enums.HttpMethodType;
import com.isofh.service.model.HospitalEntity;
import com.isofh.service.model.ResultEntity;
import com.isofh.service.model.ResultHisEntity;
import com.isofh.service.model.ScheduleEntity;
import com.isofh.service.model.ServiceEntity;
import com.isofh.service.result.IMapData;
import com.isofh.service.utils.CollectionUtils;
import com.isofh.service.utils.GsonUtils;
import com.isofh.service.utils.NetworkUtils;

@Service
public class ServiceService extends BaseService implements IMapData<String> {

    @Override
    public Map<String, Object> getMapData(String entity) {
        Map<String, Object> mapData = new HashMap<>();
        mapData.put("test", entity);
        return mapData;
    }

    public ResultEntity sync(long hospitalId) throws Exception {
        try {
            init();

            List<Long> oldServiceIds = new ArrayList<>();
            Iterable<ServiceEntity> services = serviceRepository.getAll(hospitalId);
            for (ServiceEntity service : services) {
                oldServiceIds.add(service.getId());
            }
            HospitalEntity hospitalEntity = getHospital(hospitalId);
            if(hospitalEntity!=null) {
                String queryData = NetworkUtils.callService(hospitalEntity.getServiceUrl() + ServiceConst.SYNC_SERVICE, HttpMethodType.GET);
                ResultHisEntity data = GsonUtils.toObject(queryData, ResultHisEntity.class);
                if (data == null || data.getCode() != 0) {
                    throw getException(1, "His tra ve loi ");
                }

                for (int i = 0; i < data.getData().getServices().length; i++) {
                    try {
                        ServiceEntity entity = data.getData().getServices()[i];
                        entity.setHospital(hospitalEntity);
                        if (Objects.isNull(entity.getHisServiceId())) {
                            continue;
                        }

                        ServiceEntity serviceEntity = serviceRepository.findByHisServiceId(entity.getHisServiceId(), hospitalId);
                        if (serviceEntity != null) {
                            entity = serviceEntity;
                            oldServiceIds.remove(serviceEntity.getId());
                        }
                        
                        save(entity);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
                for (Long id : oldServiceIds) {
                    ServiceEntity serviceEntity = getService(id);
                    serviceEntity.setDeleted(1);
                    save(serviceEntity);

                    List<ScheduleEntity> deletedSchedules = scheduleRepository.findByService(id);
                    if (!CollectionUtils.isNullOrEmpty(deletedSchedules)) {
                        for (ScheduleEntity schedule : deletedSchedules) {
                            schedule.setDeleted(1);
                        }
                        scheduleRepository.saveAll(deletedSchedules);
                    }
                }
            } else {
                throw getException(2, "Nhap sai id hospital");
            }
            
            return ok();
        } catch (Exception ex) {
            throw ex;
        }
    }

    public ResultEntity getAll(long hospitalId) throws Exception {
        try {
            init();
            Iterable<ServiceEntity> services = serviceRepository.getAll(hospitalId);
            return ok(BaseService.SERVICES, services);
        } catch (Exception ex) {
            throw ex;
        }
    }
}
