package com.isofh.service.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.isofh.service.ServiceConnector;
import com.isofh.service.constant.DateTimeConst;
import com.isofh.service.constant.ServiceConst;
import com.isofh.service.enums.BookingHospitalStatusType;
import com.isofh.service.enums.BookingStatusType;
import com.isofh.service.enums.GenderType;
import com.isofh.service.enums.HttpMethodType;
import com.isofh.service.enums.LogType;
import com.isofh.service.model.BookingEntity;
import com.isofh.service.model.EmailEntity;
import com.isofh.service.model.HisObject;
import com.isofh.service.model.HospitalEntity;
import com.isofh.service.model.HospitalProfileEntity;
import com.isofh.service.model.PatientHistoryEntity;
import com.isofh.service.model.ProfileEntity;
import com.isofh.service.model.ResultEntity;
import com.isofh.service.model.ResultHisEntity;
import com.isofh.service.model.ScheduleEntity;
import com.isofh.service.model.UserEntity;
import com.isofh.service.result.IMapData;
import com.isofh.service.utils.CollectionUtils;
import com.isofh.service.utils.CustomException;
import com.isofh.service.utils.DateTimeUtils;
import com.isofh.service.utils.GsonUtils;
import com.isofh.service.utils.JSONUtils;
import com.isofh.service.utils.NetworkUtils;
import com.isofh.service.utils.StrUtils;

@Service
public class BookingService extends BaseService implements IMapData<BookingEntity> {

    @Value("${environment}")
    private String environment;

    @Value("${secret.key}")
    private String secretKey;
    
    @Value("${time.init.booking}")
    private Integer timeInitBooking;

    @Autowired
    ServiceConnector serviceConnector;

    @Autowired
    PatientHistoryService patientService;

    @Override
    public Map<String, Object> getMapData(BookingEntity entity) {
        Map<String, Object> mapData = new HashMap<>();
        mapData.put(BOOKING, entity);
        mapData.put(ROOM, entity.getRoom());
        mapData.put(SPECIALIST, entity.getSpecialist());
        mapData.put(SERVICE, entity.getService());
        mapData.put(DOCTOR, entity.getDoctor());
        mapData.put(DEPARTMENT, entity.getDepartment());
        mapData.put(SCHEDULE, entity.getSchedule());
        mapData.put(PROFILE, entity.getProfile());
        return mapData;
    }

    @Override
    public List<Map<String, Object>> getMapDatas(Iterable<BookingEntity> entities) {
        List<Map<String, Object>> mapResults = new ArrayList<>();
        for (BookingEntity entity : entities) {
            mapResults.add(getMapData(entity));
        }
        return mapResults;
    }

    public ResultEntity create(Object object, long hospitalId) throws Exception {
        try {
            init(object);
            validateUser();
            Long profileId = getLong("profileId");
            String admin = "";
            if (!isAdmin()) {
                if (!Objects.equals(getCurrentUser().getProfile().getId(), profileId)) {
                    throw getUnauthentication();
                }
            } else {
                admin = getCurrentUser().getUsername();
            }

            ProfileEntity profile = getProfile(profileId);
            Long scheduleId = getLong("scheduleId");
            ScheduleEntity schedule = getSchedule(scheduleId);
            if (profile == null) {
                throw getException(1, "profileId invalid");
            }

            BookingEntity bookingEntity = getObject("booking", BookingEntity.class);
            HospitalEntity hospitalEntity = getHospital(hospitalId);
            bookingEntity.setHospital(hospitalEntity);

            bookingEntity.setId(0);
            if (!bookingEntity.getBookingTime().toLocalDate().equals(schedule.getScheduleDate())) {
                throw getException(1, "dat kham phai cung ngay voi lich lam viec ");
            }
            bookingEntity.setSchedule(schedule);
            bookingEntity.setProfile(profile);
            bookingEntity.setStatus(BookingStatusType.WAIT_PAY.getValue());
            bookingEntity.setRoom(schedule.getRoom());
            bookingEntity.setDoctor(schedule.getDoctor());
            bookingEntity.setService(schedule.getService());
            bookingEntity.setSpecialist(schedule.getSpecialist());
            bookingEntity.setDepartment(schedule.getDepartment());
            bookingEntity.setAdmin(admin);
            if (StrUtils.isNullOrWhiteSpace(bookingEntity.getPhoneCall())
                    && !StrUtils.isNullOrWhiteSpace(profile.getPhone())) {
                bookingEntity.setPhoneCall(profile.getPhone());
            }
            List<BookingEntity> existedBookings = bookingRepository.findByScheduleAndTime(scheduleId,
                    bookingEntity.getBookingTime());
            List<Long> sequenceNos = new ArrayList<>();
            for (BookingEntity entity : existedBookings) {
                sequenceNos.add(entity.getSequenceNo());
            }

            if (sequenceNos.size() >= schedule.getNumberCase()) {
                throw getException(2, "Da kin lich trong khung gio nay");
            }

            int timeInMinute = bookingEntity.getBookingTime().getHour() * 60
                    + bookingEntity.getBookingTime().getMinute();
            if (timeInMinute < schedule.getStartWorking()) {
                throw getException(4, "Thoi gian dat lich khong duoc nho hon thoi giat bat dau lich lam viec bac si");
            }
            if (timeInMinute > schedule.getEndWorking()) {
                throw getException(5, "Thoi gian dat lich khong duoc lon hon thoi giat bat dau lich lam viec bac si");
            }
            long startNumber = getMorningBookCount(schedule, bookingEntity) + (schedule.getNumberCase()
                    * (timeInMinute - schedule.getStartWorking()) / (DateTimeConst.ONE_HOUR_IN_MINUTE / 2));
            long sequenceNo = 0;
            for (int i = 0; i < schedule.getNumberCase(); i++) {
                if (!sequenceNos.contains(startNumber + i)) {
                    sequenceNo = startNumber + i;
                    break;
                }
            }
            if (sequenceNo == 0) {
                throw new CustomException(3, "co loi trong qua trinh sinh so kham");
            }

            bookingEntity.setSequenceNo(sequenceNo);
            save(bookingEntity);
            String hash = secretKey + "&" + String.valueOf(schedule.getService().getPrice()) + "&"
                    + String.valueOf(bookingEntity.getId()) + "&" + String.valueOf(bookingEntity.getDoctor().getId());

            bookingEntity.setHash(getMD5Hex(hash));
            save(bookingEntity);
            saveLog(LogType.CREATE_BOOKING, null, profile.getUser().getName(), "",
                    bookingEntity.getBookingTime().toLocalDate());
            return ok(getResult(Arrays.asList("book", "index", "checkinResult", "doctor", "service"),
                    Arrays.asList(bookingEntity, sequenceNo, "", bookingEntity.getDoctor(), schedule.getService())));
        } catch (Exception ex) {
            throw ex;
        }
    }

    private int getMorningBookCount(ScheduleEntity scheduleEntity, BookingEntity bookingEntity) {
        int initValue = 6;
        // Neu lich la buoi sang thi k check chua
        if (Objects.equals(0, scheduleEntity.getTimeOfDay())) {
            return initValue;
        }
        // tim lich lam viec buoi sang cua phong nay
        ScheduleEntity schedule = scheduleRepository.findByRoom(scheduleEntity.getRoom().getId(),
                scheduleEntity.getScheduleDate(), 0);
        if (schedule == null) {
            return initValue;
        }
        return (int) (initValue + (schedule.getNumberCase() * (schedule.getEndWorking() - schedule.getStartWorking())
                / (DateTimeConst.ONE_HOUR_IN_MINUTE / 2)));

    }

    public ResultEntity getByDoctorSpecialistDepartment(Long doctorId, Long specialistId, LocalDate startDate,
            LocalDate endDate) throws Exception {
        try {
            init();
            List<BookingEntity> scheduleQueries = bookingRepository.findByDoctorSpecialist(doctorId, specialistId,
                    startDate, endDate);
            return ok(BOOKINGS, getMapDatas(scheduleQueries));
        } catch (Exception ex) {
            throw ex;
        }
    }

    public ResultEntity search(String patientName, Long doctorId, Long serviceId, Long specialistId, Long roomId,
            String phone, String address, Integer status, String value, LocalDate startDate, LocalDate endDate,
            Integer page, Integer size, Long departmentId, String admin, String phoneCall, Integer arrival,
            Integer requestDoctor) throws Exception {
        try {
            init();
            validateUser();
            Page<BookingEntity> scheduleQueries = bookingRepository.search(patientName, doctorId, serviceId, phone,
                    address, status, value, startDate, endDate, departmentId, specialistId, admin, roomId, phoneCall,
                    arrival, requestDoctor, getDefaultPage(page, size));
            return ok(scheduleQueries.getTotalElements(), getMapDatas(scheduleQueries.getContent()));
        } catch (Exception ex) {
            throw ex;
        }
    }

    public ResultEntity delete(long id) throws Exception {
        try {
            init();
            BookingEntity bookingEntity = getBooking(id);
            validateUser();
            if (!isAdmin()) {
                if (getCurrentUser().getProfile() == null || bookingEntity.getProfile() == null
                        || !Objects.equals(getCurrentUser().getProfile().getId(), bookingEntity.getProfile().getId())) {
                    throw getUnauthentication();
                }
            }

            bookingEntity.setStatus(BookingStatusType.CANCEL.getValue());
            save(bookingEntity);
            runInThread(() -> saveLog(LogType.CANCEL_BOOKING, bookingEntity.getDoctor().getName(),
                    bookingEntity.getProfile().getUser().getName(), "", bookingEntity.getBookingTime().toLocalDate()));
            return ok();
        } catch (Exception ex) {
            throw ex;
        }
    }

    public ResultEntity update(Object object, long id) throws Exception {
        try {
            init(object);
            BookingEntity oldBooking = getBooking(id);
            validateUser();
            if (!isAdmin()) {
                if (getCurrentUser().getProfile() == null || oldBooking.getProfile() == null
                        || !Objects.equals(getCurrentUser().getProfile().getId(), oldBooking.getProfile().getId())) {
                    throw getUnauthentication();
                }
            }

            oldBooking.setDeleted(1);
            oldBooking.setStatus(BookingStatusType.CANCEL.getValue());
            save(oldBooking);

            try {
                Long profileId = getLong("profileId");
                ProfileEntity profile = getProfile(profileId);
                Long scheduleId = getLong("scheduleId");
                ScheduleEntity schedule = getSchedule(scheduleId);
                if (profile == null) {
                    throw getException(1, "profileId invalid");
                }

                BookingEntity bookingEntity = getObject("booking", BookingEntity.class);
                if (!bookingEntity.getBookingTime().toLocalDate().equals(schedule.getScheduleDate())) {
                    throw getException(1, "dat kham phai cung ngay voi lich lam viec ");
                }
                bookingEntity.setSchedule(schedule);
                bookingEntity.setProfile(profile);
                bookingEntity.setStatus(BookingStatusType.WAIT_CONFIRM.getValue());
                bookingEntity.setRoom(schedule.getRoom());
                bookingEntity.setDoctor(schedule.getDoctor());
                bookingEntity.setService(schedule.getService());
                bookingEntity.setSpecialist(schedule.getSpecialist());
                bookingEntity.setDepartment(schedule.getDepartment());

                List<BookingEntity> existedBookings = bookingRepository.findByScheduleAndTime(scheduleId,
                        bookingEntity.getBookingTime());
                List<Long> sequenceNos = new ArrayList<>();
                for (BookingEntity entity : existedBookings) {
                    sequenceNos.add(entity.getSequenceNo());
                }

                if (sequenceNos.size() >= schedule.getNumberCase()) {
                    throw getException(2, "Da kin lich trong khung gio nay");
                }

                int timeInMinute = bookingEntity.getBookingTime().getHour() * 60
                        + bookingEntity.getBookingTime().getMinute();
                if (timeInMinute < schedule.getStartWorking()) {
                    throw getException(4,
                            "Thoi gian dat lich khong duoc nho hon thoi giat bat dau lich lam viec bac si");
                }
                if (timeInMinute > schedule.getEndWorking()) {
                    throw getException(5,
                            "Thoi gian dat lich khong duoc lon hon thoi giat bat dau lich lam viec bac si");
                }
                long startNumber = getMorningBookCount(schedule, bookingEntity) + (schedule.getNumberCase()
                        * (timeInMinute - schedule.getStartWorking()) / (DateTimeConst.ONE_HOUR_IN_MINUTE / 2));
                long sequenceNo = 0;
                for (int i = 0; i < schedule.getNumberCase(); i++) {
                    if (!sequenceNos.contains(startNumber + i)) {
                        sequenceNo = startNumber + i;
                        break;
                    }
                }
                if (sequenceNo == 0) {
                    throw new CustomException(3, "co loi trong qua trinh sinh so kham");
                }

                bookingEntity.setSequenceNo(sequenceNo);

                String admin = "";
                if (!isAdmin()) {
                    if (!Objects.equals(getCurrentUser().getProfile().getId(), profileId)) {
                        throw getUnauthentication();
                    }
                } else {
                    admin = getCurrentUser().getUsername();
                }
                bookingEntity.setAdmin(admin);
                if (StrUtils.isNullOrWhiteSpace(bookingEntity.getPhoneCall())
                        && !StrUtils.isNullOrWhiteSpace(profile.getPhone())) {
                    bookingEntity.setPhoneCall(profile.getPhone());
                }

                String hash = secretKey + "&" + String.valueOf(schedule.getService().getPrice()) + "&"
                        + String.valueOf(bookingEntity.getId()) + "&"
                        + String.valueOf(bookingEntity.getDoctor().getId());

                bookingEntity.setHash(DigestUtils.md5Hex(hash));
                save(bookingEntity);
                saveLog(LogType.UPDATE_BOOKING, null, profile.getUser().getName(), "",
                        bookingEntity.getBookingTime().toLocalDate());
                return ok(getResult(Arrays.asList("book", "index"), Arrays.asList(bookingEntity, sequenceNo)));
            } catch (Exception ex) {
                oldBooking.setDeleted(0);
                save(oldBooking);
                throw ex;
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public ResultEntity syncData(long hospitalId) {
        try {
            init();
            String serviceUrl = "http://localhost:8080";
            String department = serviceUrl + "/department/sync/" + hospitalId;
            String doctors = serviceUrl + "/doctor/sync/" + hospitalId;
            String rooms = serviceUrl + "/room/sync/" + hospitalId;
            String services = serviceUrl + "/service/sync/" + hospitalId;
            String syncSpecialist = serviceUrl + "/specialist/sync/" + hospitalId;

            NetworkUtils.callService(department, HttpMethodType.GET);
            NetworkUtils.callService(rooms, HttpMethodType.GET);
            NetworkUtils.callService(doctors, HttpMethodType.GET);
            NetworkUtils.callService(services, HttpMethodType.GET);
            NetworkUtils.callService(syncSpecialist, HttpMethodType.GET);
            return ok("Dong bo thanh cong");
        } catch (Exception ex) {
            throw ex;
        }
    }

    public ResultEntity updateArrival(Object object, Long id) throws Exception {
        try {
            init(object);
            BookingEntity booking = getBooking(id);
            Integer arrival = getInteger("arrival", 0);
            booking.setArrival(arrival);
            save(booking);
            return ok(BOOKING, booking);
        } catch (Exception ex) {
            throw ex;
        }
    }

    public ResultEntity updateReminder(Object object, Long id) throws Exception {
        try {
            init(object);
            BookingEntity booking = getBooking(id);
            String reminder = getString("reminder");
            booking.setReminder(reminder);
            save(booking);
            return ok(BOOKING, booking);
        } catch (Exception ex) {
            throw ex;
        }
    }

    public ResultEntity updatePhoneCall(Object object, Long id) throws Exception {
        try {
            init(object);
            BookingEntity booking = getBooking(id);
            String phoneCall = getString("phoneCall");
            booking.setPhoneCall(phoneCall);
            save(booking);
            return ok(BOOKING, booking);
        } catch (Exception ex) {
            throw ex;
        }
    }

    public ResultEntity getDetail(long id) throws Exception {
        try {
            init();
            if (!isAdmin()) {
                throw getUnauthentication();
            }
            BookingEntity booking = getBooking(id);
            return ok(BOOKING, booking);
        } catch (Exception ex) {
            throw ex;
        }
    }

    public ResultEntity findDuplicateInRoom() throws Exception {
        try {
            init();
            List<BookingEntity> bookings = bookingRepository.findDuplicateInRoom();
            if (!CollectionUtils.isNullOrEmpty(bookings)) {
                bookings.forEach(bookingEntity -> {
                    save(new EmailEntity("ledangtuanbk@gmail.com,thao.dt@isofh.com ,lenhu1188@gmail.com",
                            environment + " Trùng số khám ",
                            " phòng " + bookingEntity.getRoom().getId() + " Ngày: " + bookingEntity.getBookingTime()));
                });
                throw getException(1, "có " + bookings.size() + " lỗi");
            }
            return ok();
        } catch (Exception ex) {
            throw ex;
        }
    }

    /**
     * Day thong tin benh nhan len HIS
     *
     * @return tra ve ket qua
     */
    private String checkInHospital(List<BookingEntity> bookingEntities, ProfileEntity bookingProfileEntity,
            long hospitalId) throws CustomException {

        String body = "";
        String hisResult = "";
        try {

            Map<String, Object> requestBody = new HashMap<>();

            List<Map<String, Object>> listServices = new ArrayList<>();
            StringBuilder sbReason = new StringBuilder();
            for (BookingEntity entity : bookingEntities) {
                Map<String, Object> mapListService = new HashMap<>();
                mapListService.put("DoctorId", entity.getDoctor().getHisDoctorId());
                mapListService.put("ServiceId", entity.getService().getHisServiceId());
                mapListService.put("DepartmentId", entity.getDepartment().getHisDepartmentId());
                mapListService.put("RoomId", entity.getRoom().getHisRoomId());
                mapListService.put("SequenceNo", entity.getSequenceNo());
                sbReason.append(" " + entity.getNote());
                listServices.add(mapListService);
            }

            requestBody.put("Reason", sbReason.toString());
            requestBody.put("ListService", listServices);

            HospitalProfileEntity hospitalUser = hospitalProfileRepository
                    .findByProfileHospital(bookingProfileEntity.getId(), hospitalId);
            HospitalEntity hospitalEntity = getHospital(hospitalId);
            if (hospitalUser == null) {
                hospitalUser = new HospitalProfileEntity();
                hospitalUser.setHospital(hospitalEntity);
                hospitalUser.setProfile(bookingProfileEntity);
                save(hospitalUser);
            }

            requestBody.put("Value", hospitalUser.getHisPatientId());
            requestBody.put("TimeCheckIn", DateTimeUtils.getTimeInMillis(bookingEntities.get(0).getBookingTime()));
            // TODO: 3/21/19 cho thay doi api thi sua tiep
            requestBody.put("CountryId",
                    bookingProfileEntity.getCountry() != null ? bookingProfileEntity.getCountry().getCode() : 0);
            requestBody.put("ProvinceId",
                    bookingProfileEntity.getProvince() != null ? bookingProfileEntity.getProvince().getCode() : 0);
            requestBody.put("DistrictId",
                    bookingProfileEntity.getDistrict() != null ? bookingProfileEntity.getDistrict().getCode() : 0);
            requestBody.put("ZoneId",
                    bookingProfileEntity.getZone() != null ? bookingProfileEntity.getZone().getCode() : 0);
            int age = DateTimeUtils.getAge(bookingProfileEntity.getDob());
            if (age <= 0) {
                throw getException(9, "ngay sinh khong hop le " + bookingProfileEntity.getDob());
            }
            requestBody.put("Age", DateTimeUtils.getAge(bookingProfileEntity.getDob()));
            requestBody.put("PatientName", bookingProfileEntity.getName());
            requestBody.put("GenderId", GenderType.MALE.getType().equals(bookingProfileEntity.getGender()) ? "M" : "F");
            requestBody.put("Birthday", DateTimeUtils.getTimeInMillis(bookingProfileEntity.getDob()));
            requestBody.put("PhoneNumber",
                    !StrUtils.isNullOrWhiteSpace(bookingProfileEntity.getPhone()) ? bookingProfileEntity.getPhone()
                            : "0");
            requestBody.put("Address", bookingProfileEntity.getAddress());
            requestBody.put("Cmt", "");
            requestBody.put("GuardianName", bookingProfileEntity.getGuardianName());
            requestBody.put("PhoneGuardian", bookingProfileEntity.getGuardianPhone());
            requestBody.put("DepartmentId", bookingEntities.get(0).getDepartment().getHisDepartmentId());
            String result = NetworkUtils.callService(ServiceConst.CHECK_IN, null, requestBody, null);
            log.info("checkin result " + result);
            hisResult = result;
            body = GsonUtils.toString(requestBody);
            HisObject hisObject = getHisObject(result);
            if (hisObject == null) {
                throw getException(2, "Checkin fail");
            } else if (hisObject.getCode() == 1) {
                throw getException(6, hisObject.getComment());
            } else if (hisObject.getCode() == 6) {
                throw getException(7, "So kham khong hop le");
            } else if (!hisObject.isSuccess()) {
                throw getException(8, hisObject.getComment());
            }

            String value = JSONUtils.getStringByPath(hisObject.getData(), "Patient.Value");
            Long hisPatientHistoryId = JSONUtils.getLongByPath(hisObject.getData(), "HIS_PatientHistory_ID");

            bookingEntities.forEach(bookingEntity -> {
                bookingEntity.setHisPatientHistoryId(hisPatientHistoryId);
                bookingEntity.setCheckInResult(result);
                bookingEntity.setStatus(BookingHospitalStatusType.HAVE_NUMBER.getValue());
                save(bookingEntity);
            });

            if (!value.equals(hospitalUser.getHisPatientId())) {
                hospitalUser.setHisPatientId(value);
                save(hospitalUser);
            }
            return hisObject.getData().toString();
        } catch (Exception ex) {
            save(new EmailEntity("ledangtuanbk@gmail.com,thao.dt@isofh.com ,lenhu1188@gmail.com",
                    environment + " Checkin lỗi ",
                    ex.getMessage() + " \n" + "Checkin lỗi người dùng " + bookingProfileEntity.getName()
                            + " bookingProfileId: " + bookingProfileEntity.getId() + " \nrequest " + body + " \nresult"
                            + hisResult));
            log.error(ex.getMessage());
            throw ex;
        }
    }

    public ResultEntity autoCheckin(long hospitalId) {
        init();
        List<BookingEntity> bookings = bookingRepository.findByDateaAndStatus(LocalDate.now(),
                BookingStatusType.WAIT_CONFIRM.getValue());
        long lastProfileId = 0;
        List<BookingEntity> books = new ArrayList<>();
        ProfileEntity profileEntity = null;
        StringBuilder sb = new StringBuilder();
        sb.append(", Danh sach profile failed: ");
        for (int i = 0; i < bookings.size(); i++) {
            BookingEntity entity = bookings.get(i);
            if (entity.getProfile().getId() != lastProfileId) {
                // ton tai list du lieu truoc do
                if (books.size() > 0) {
                    try {
                        checkInHospital(books, profileEntity, hospitalId);
                    } catch (Exception ex) {
                        sb.append(", " + profileEntity.getId() + " " + ex.getMessage());
                    }
                }
                books.clear();
                books.add(entity);
                profileEntity = entity.getProfile();
                lastProfileId = profileEntity.getId();
            } else {
                books.add(entity);
            }
        }
        if (books.size() > 0) {
            try {
                checkInHospital(books, profileEntity, hospitalId);
            } catch (Exception ex) {
                sb.append(", " + profileEntity.getId() + " " + ex.getMessage());
            }
        }
        return ok("Dong bo thanh cong " + sb.toString());

    }

    public ResultEntity autoReject() {
        init();
        List<BookingEntity> bookings = bookingRepository.findByCreateDateaAndStatus(
                LocalDateTime.now().plusMinutes(-30), BookingStatusType.WAIT_PAY.getValue());
        for (BookingEntity booking : bookings) {
            booking.setStatus(BookingStatusType.CANCEL.getValue());
            save(booking);
        }
        return ok("reject done " + GsonUtils.toString(bookings));

    }

    public ResultEntity getListPatientHistoryByProfileHospital(long profileId, long hospitalId) throws Exception {
        init();
        validateUser();
        ProfileEntity profileEntity = getProfile(profileId);

        if (profileEntity == null || Objects.equals(profileEntity.getDeleted(), 1)) {
            throw getException(2, "profile khong ton tai hoac da bi xoa");
        }
        // lay cac booking chua day len his, trang thai cho xac nhan (1)
        List<BookingEntity> books = bookingRepository.getByProfile(profileId,
                BookingStatusType.WAIT_CONFIRM.getValue());
        List<Map<String, Object>> mapDatas = getMapDatas(books);

        List<PatientHistoryEntity> patientHistoryEntity = new ArrayList<>();
        try {

            if (!StrUtils.isNullOrWhiteSpace(profileEntity.getUid()) && profileEntity.getHisPatientId() != null) {
                patientHistoryEntity = patientHistoryRepository.findByProfileHospital(profileId, hospitalId);

                // ton tai trong cache
                if (patientHistoryEntity.size() == 0) {
                    patientService.sync(hospitalId, profileEntity.getUid(), false);
                    patientHistoryEntity = patientHistoryRepository.findByProfileHospital(profileId, hospitalId);
                } else {
                    new Thread(() -> {
                        try {
                            patientService.sync(hospitalId, profileEntity.getUid(), false);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }).start();
                }

            } else {
                throw getException(2, "profile chua link voi HIS");
            }
        } catch (Exception ex) {

        }
        return ok(getResult(Arrays.asList("bookingNotInHis", "patientHistorys"),
                Arrays.asList(mapDatas, getMapData(patientHistoryEntity))));
    }

    public ResultEntity getListPatientHistoryByProfile() throws Exception {
        init();
        UserEntity user = getCurrentUser();
        ProfileEntity profileEntity = getProfile(user.getProfile().getId());
        Long profileId = profileEntity.getId();

        if (profileEntity == null || Objects.equals(profileEntity.getDeleted(), 1)) {
            throw getException(2, "profile khong ton tai hoac da bi xoa");
        }
        // lay cac booking chua day len his, trang thai cho xac nhan (1)
        List<BookingEntity> books = bookingRepository.getByProfile(profileId,
                BookingStatusType.WAIT_CONFIRM.getValue());
        List<Map<String, Object>> mapDatas = getMapDatas(books);

        List<PatientHistoryEntity> patientHistoryEntity = new ArrayList<>();
        try {

            if (!StrUtils.isNullOrWhiteSpace(profileEntity.getUid()) && profileEntity.getHisPatientId() != null) {
                patientHistoryEntity = patientHistoryRepository.findByProfile(profileId);

                // ton tai trong cache
                if (patientHistoryEntity.size() == 0) {
                    patientService.sync(profileId);
                    patientHistoryEntity = patientHistoryRepository.findByProfile(profileId);
                } else {
                    new Thread(() -> {
                        try {
                            patientService.sync(profileId);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }).start();
                }

            } else {
                throw getException(2, "profile chua link voi HIS");
            }
        } catch (Exception ex) {

        }

        return ok(getResult(Arrays.asList("bookingNotInHis", "patientHistorys"),
                Arrays.asList(mapDatas, getMapData(patientHistoryEntity))));
    }

    private List<Map<String, Object>> getMapData(List<PatientHistoryEntity> entities) {
        List<Map<String, Object>> mapResults = new ArrayList<>();
        Map<String, Object> map;
        for (PatientHistoryEntity entity : entities) {
            map = new HashMap<String, Object>();
            map.put(PATIENT_HISTORY, entity);
            HospitalEntity hospital = getEntityById(hospitalRepository, entity.getHospitalId());
            map.put(HOSPITAL, hospital);
            mapResults.add(map);
        }
        return mapResults;
    }

    private ResultEntity getArrayBookingInHis(String value, long profileId, long hospitalId) {
        HospitalEntity hospital = getEntityById(hospitalRepository, hospitalId);
        String url = ServiceConst.GET_BY_VALUE;
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("isofhCareValue", value);
        String result = NetworkUtils.callService(hospital.getServiceUrl() + url, null, null, null, uriVariables);
        // luu tru danh sach patientHistory
        try {
            ResultHisEntity hisResult = GsonUtils.toObject(result, ResultHisEntity.class);
            String patientHistoryIds = "";
            for (int i = 0; i < hisResult.getData().getPatientHistoryIds().length; i++) {
                if (!StrUtils.isNullOrWhiteSpace(hisResult.getData().getPatientHistoryIds()[i].getPatientHistoryId())) {
                    patientHistoryIds += ",";
                }
                patientHistoryIds += String
                        .valueOf(hisResult.getData().getPatientHistoryIds()[i].getPatientHistoryId());
            }
            ProfileEntity profile = getProfile(profileId);
            profile.setPatientHistoryIds(patientHistoryIds);
            save(profile);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return GsonUtils.toObject(result, ResultEntity.class);
    }

    public ResultEntity getDetailPatientHistory(long patientHistoryId, long hospitalId) throws Exception {
        init();
        PatientHistoryEntity patientHistoryEntity = patientHistoryRepository.findByHospitalPatientHistory(hospitalId,
                String.valueOf(patientHistoryId));
//        CacheHisEntity cacheHisEntity = cacheHisRepository.findFirstByTypeAndPatientHistoryIdAndHospitalId(
//                CacheHisType.DETAIL_PATIENT_HISTORY.getValue(), patientHistoryId, hospitalId);
        // Chua ton tai cache
        HospitalEntity hospital = getEntityById(hospitalRepository, hospitalId);
        if (patientHistoryEntity != null) {
            if (patientHistoryEntity.getUpdatedDate().plusMinutes(hospital.getTimeInit())
                    .isAfter(LocalDateTime.now())) {
                new Thread(() -> {
                    try {
                        patientService.updateResult(hospitalId, String.valueOf(patientHistoryId), false);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }).start();
            }

        } else {
            patientService.updateResult(hospitalId, String.valueOf(patientHistoryId), false);
            patientHistoryEntity = patientHistoryRepository.findByHospitalPatientHistory(hospitalId,
                    String.valueOf(patientHistoryId));
        }
        return ok("data", patientHistoryEntity);
    }

    public ResultEntity getObjectDetailPatientHistory(long patientHistoryId, long hospitalId) {

        String url = ServiceConst.GET_DETAIL_PATIENT_HISTORY;
        HospitalEntity hospital = getEntityById(hospitalRepository, hospitalId);
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("patientHistoryId", patientHistoryId);
        String result = NetworkUtils.callService(hospital.getServiceUrl() + url, HttpMethodType.GET, null, null, null,
                uriVariables);
        return GsonUtils.toObject(result, ResultEntity.class);
    }

    private HospitalProfileEntity getHospitalUserByCurrentUser(long hospitalId) {
        return hospitalProfileRepository.findByProfileHospital(getCurrentUser().getProfile().getId(), hospitalId);
    }

    public ResultEntity getResultPatientHistory(long patientHistoryId, long hospitalId) throws Exception {
        init();
        PatientHistoryEntity patientHistoryEntity = patientHistoryRepository.findByHospitalPatientHistory(hospitalId,
                String.valueOf(patientHistoryId));
        HospitalEntity hospital = getEntityById(hospitalRepository, hospitalId);
        if (patientHistoryEntity != null) {
            if (patientHistoryEntity.getUpdatedDate().plusMinutes(hospital.getTimeInit())
                    .isAfter(LocalDateTime.now())) {
                new Thread(() -> {
                    try {
                        patientService.updateResult(hospitalId, String.valueOf(patientHistoryId), false);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }).start();
            }

        } else {
            patientService.updateResult(hospitalId, String.valueOf(patientHistoryId), false);
            patientHistoryEntity = patientHistoryRepository.findByHospitalPatientHistory(hospitalId,
                    String.valueOf(patientHistoryId));
        }
        return ok("data", patientHistoryEntity);

    }

    public ResultEntity getObjectResultPatientHistory(long patientHistoryId, long hospitalId) {
        String url = ServiceConst.GET_RESULT_PATIENT_HISTORY;
        HospitalEntity hospital = getEntityById(hospitalRepository, hospitalId);
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("patientHistoryId", patientHistoryId);
        String result = NetworkUtils.callService(hospital.getServiceUrl() + url, HttpMethodType.GET, null, null, null,
                uriVariables);
        return GsonUtils.toObject(result, ResultEntity.class);
    }

    public String isPayWallet(Long bookingId) throws Exception {
        BookingEntity booking = getEntityById(bookingRepository, bookingId);
        if (booking.getStatus() == BookingStatusType.WAIT_PAY.getValue()) {
            if(booking.getCreatedDate().plusMinutes(timeInitBooking).isAfter(LocalDateTime.now())) {
                booking.setStatus(BookingStatusType.WAIT_CONFIRM.getValue());
                save(booking);
                serviceConnector.sendSMS(booking.getProfile().getPhone(),
                        "Ban đã đặt lịch thành công với số thứ tự: " + booking.getSequenceNo());
                return "True";
            } else {
                return "Booking time out with create date: " + booking.getCreatedDate();
            }
        } else if (booking.getStatus() == BookingStatusType.WAIT_CONFIRM.getValue()) {
            return "True";
        } else {
            return "Booking with status: " + booking.getStatus();
        }
    }
}
