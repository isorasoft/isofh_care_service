package com.isofh.service.repository;

import com.isofh.service.model.MethodUseEntity;
import org.springframework.data.repository.CrudRepository;

public interface MethodUseRepository extends CrudRepository<MethodUseEntity, Long> {
}
