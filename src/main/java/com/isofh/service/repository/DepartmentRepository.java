package com.isofh.service.repository;

import com.isofh.service.model.DepartmentEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface DepartmentRepository extends CrudRepository<DepartmentEntity, Long> {

    @Query(value = "select n.* from department n "
            + " where true "
            + " and n.his_department_id = :hisDepartmentId"
            + " and n.col_hospital_id = :hospitalId"
            + " and n.deleted = 0"
            , nativeQuery = true)
    DepartmentEntity findByHisDepartmentId(
            @Param("hisDepartmentId") Long hisDepartmentId,
            @Param("hospitalId") Long hospitalId);

    @Query(value = "select n.* from department n "
            + " where true "
            + " and n.col_hospital_id = :hospitalId"
            + " and n.deleted = 0 order by index_value desc"
            , nativeQuery = true)
    List<DepartmentEntity> getAll(@Param("hospitalId") long hospitalId);

}
