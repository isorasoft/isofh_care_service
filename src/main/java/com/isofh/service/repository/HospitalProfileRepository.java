package com.isofh.service.repository;

import com.isofh.service.model.HospitalProfileEntity;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface HospitalProfileRepository extends CrudRepository<HospitalProfileEntity, Long> {

    @Query(value = "select * from hospital_profile n" +
            " where true"
            + " and (n.col_hospital_id = :hospitalId )"
            + " and (n.col_profile_id = :profileId )"
            + " limit 1 "
            , nativeQuery = true)
    HospitalProfileEntity findByProfileHospital(
            @Param("profileId") Long profileId,
            @Param("hospitalId") Long hospitalId);
    
    @Query(value = "select * from hospital_profile n" +
            " where true"
            + " and (n.col_profile_id = :profileId )"
            , nativeQuery = true)
    List<HospitalProfileEntity> findByProfile(
            @Param("profileId") Long profileId);
}
