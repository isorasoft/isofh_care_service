package com.isofh.service.repository;

import com.isofh.service.model.HospitalEntity;
import com.isofh.service.model.HospitalProfileEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface HospitalRepository extends CrudRepository<HospitalEntity, Long> {
    @Query(value = "select n.* from hospital n "
            + " where true "
            + " and (n.col_type = :type or :type= -1) "
            , nativeQuery = true)
    List<HospitalEntity> getAll(@Param("type") int type);

    @Query(value = "select n.* from hospital n left join user u on n.col_admin_id = u.id"
            + " where true "
            + " and ((n.col_name like concat('%',:stringQuyery,'%') or :stringQuyery = '' ) "
            + " or (n.col_tax_code like concat('%',:stringQuyery,'%') or :stringQuyery = '') "
            + " or (u.email like concat('%',:stringQuyery,'%') or :stringQuyery = '') "
            + " or (u.phone like concat('%',:stringQuyery,'%') or :stringQuyery = '')) "
            + " and (n.col_active =:active or :active = '-1') ",
            countQuery = "select count(n.id) from hospital n left join user u on n.col_admin_id = u.id"
                    + " where true "
                    + " and ((n.col_name like concat('%',:stringQuyery,'%') or :stringQuyery = '' ) "
                    + " or (n.col_tax_code like concat('%',:stringQuyery,'%') or :stringQuyery = '') "
                    + " or (u.email like concat('%',:stringQuyery,'%') or :stringQuyery = '') "
                    + " or (u.phone like concat('%',:stringQuyery,'%') or :stringQuyery = '')) "
                    + " and (n.col_active =:active or :active = '-1') "
            , nativeQuery = true)
    Page<HospitalEntity> search(
            @Param("stringQuyery") String stringQuyery,
            @Param("active") Integer active,
            Pageable defaultPage
    );

    @Query(value = "select n.* from hospital n "
            + " where true "
            + " and (n.col_tax_code = :taxCode or :taxCode= -1) limit 1"
            , nativeQuery = true)
    HospitalEntity findTaxCode(@Param("taxCode") String taxCode);

    @Query(value = "select n.* from hospital n "
            + " where true "
            + " and (n.col_doctor_id = :doctorId or :doctorId= -1)"
            , nativeQuery = true)
    List<HospitalEntity> findHospitalByDoctor(@Param("doctorId") Long doctorId);

    @Query(value = "select * from hospital n join hospital_profile hp on n.id = hp.col_hospital_id" +
            " where true"
            + " and (hp.col_profile_id = :profileId )"
            , nativeQuery = true)
    List<HospitalEntity> findByProfileHospital(
            @Param("profileId") Long profileId);
    
    @Query(value = "select n.* from hospital n "
            + " where true "
            + " and (n.uid = :uid) limit 1"
            , nativeQuery = true)
    HospitalEntity findUid(@Param("uid") String uid);

}
