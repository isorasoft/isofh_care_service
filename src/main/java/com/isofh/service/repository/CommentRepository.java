package com.isofh.service.repository;

import com.isofh.service.model.CommentEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CommentRepository extends CrudRepository<CommentEntity, Long> {

    @Query(value = "select n.* from tbl_comment n left join user u on n.col_user_id = u.id" + " where true "
            + " and (n.col_post_id =:postId or :postId = '-1') " + " and (u.role =:role or :role = '-1') "
            + " ORDER BY  n.created_date DESC", countQuery = "select count(n.id) from tbl_comment n left join user u on n.col_user_id = u.id"
            + " where true " + " and (n.col_post_id =:postId or :postId = '-1') "
            + " and (u.role =:role or :role = '-1') " + " ORDER BY  n.created_date DESC", nativeQuery = true)
    Page<CommentEntity> search(@Param("postId") Integer postId, @Param("role") Integer role, Pageable defaultPage);

    @Query(value = "select n.* from tbl_comment n "
            + " where true "
            + " and (n.col_post_id =:postId or :postId = '-1') "
            + " ORDER BY  n.created_date DESC limit 2", nativeQuery = true)
    List<CommentEntity> searchEach(@Param("postId") Long postId);

    @Query(value = "select n.* from tbl_comment n "
            + " where true "
            + " and (n.col_post_id =:postId or :postId = '-1') "
            + " ORDER BY  n.created_date DESC", nativeQuery = true)
    List<CommentEntity> searchList(@Param("postId") Long postId);

    @Query(value = "select n.* from tbl_comment n"
            + " where true "
            + " and (n.col_post_id =:postId or :postId = '-1') "
            + " ORDER BY  n.created_date DESC limit 1", nativeQuery = true)
    CommentEntity searchComment2Day(@Param("postId") Long postId);

    CommentEntity findFirstByUid(String uid);

}
