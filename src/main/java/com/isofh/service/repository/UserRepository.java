package com.isofh.service.repository;

import com.isofh.service.model.UserEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends CrudRepository<UserEntity, Long> {

    @Query(value = "select n.* from user n left join specialist u on n.specialist_id = u.id" + " where true "
            + " and (n.name like concat('%',:name,'%') or :name = '') "
            + " and (n.phone like concat('%',:phone,'%') or :phone = '') "
            + " and (n.email like concat('%',:email,'%') or :email = '') "
            + " and (n.certificate_code like concat('%',:certificateCode,'%') or :certificateCode = '') "
            + " and (u.id & :specialistId =:specialistId or :specialistId= -1) "

            , countQuery = "select count(n.id) from user n left join specialist u on n.specialist_id = u.id "
            + " where true " + " and (n.name like concat('%',:name,'%') or :name = '') "
            + " and (n.phone like concat('%',:phone,'%') or :phone = '') "
            + " and (n.email like concat('%',:email,'%') or :email = '') "
            + " and (n.certificate_code like concat('%',:certificateCode,'%') or :certificateCode = '') "
            + " and (u.id & :specialistId =:specialistId or :specialistId= -1) ", nativeQuery = true)
    Page<UserEntity> search(@Param("name") String name, @Param("phone") String phone, @Param("email") String email,
                            @Param("certificateCode") String certificateCode, @Param("specialistId") Long specialistId,
                            Pageable pageable);

    @Query(value = "select n.* from user n left join common_specialist u on n.specialist_id = u.id"
            + " where true " + " and ((n.name like concat('%',:queryString,'%') or :queryString = '' ) "
            + " or (u.name like concat('%',:queryString,'%') or :queryString = '' ) "
            + " or (n.email like concat('%',:queryString,'%') or :queryString = '' ) "
            + " or (n.phone like concat('%',:queryString,'%') or :queryString = '' ) "
            + " or (n.certificate_code like concat('%',:queryString,'%') or :queryString = '' )) "
            + " and n.role = 2 " + " and (n.active = :active  or :active = '-1') "
            + " and (n.role_id = :roleId  or :roleId = '-1') ORDER BY FIELD(n.specialist_id, :specialistId) DESC"

            , countQuery = "select count(n.id) from user n left join common_specialist u on n.specialist_id = u.id"
            + " where true " + " and ((n.name like concat('%',:queryString,'%') or :queryString = '' ) "
            + " or (u.name like concat('%',:queryString,'%') or :queryString = '' ) "
            + " or (n.email like concat('%',:queryString,'%') or :queryString = '' ) "
            + " or (n.phone like concat('%',:queryString,'%') or :queryString = '' ) "
            + " or (n.certificate_code like concat('%',:queryString,'%') or :queryString = '' )) "
            + " and n.role = 2 " + " and (n.active = :active  or :active = '-1') "
            + " and (n.role_id = :roleId  or :roleId = '-1') ORDER BY FIELD(n.specialist_id, :specialistId) DESC", nativeQuery = true)
    Page<UserEntity> searchByDoctor(@Param("queryString") String queryString, @Param("active") Integer active,@Param("specialistId") Long specialistId, @Param("roleId") Long roleId,
                                    Pageable pageable);

    @Query(value = "select n.* from user n " + " where true "
            + " and ((n.name like concat('%',:queryString,'%') or :queryString = '' ) "
            + " or (n.email like concat('%',:queryString,'%') or :queryString = '' ) "
            + " or (n.phone like concat('%',:queryString,'%') or :queryString = '' ) "
            + " or (n.passport like concat('%',:queryString,'%') or :queryString = '' )) " + " and n.role = 1 "
            + " and (n.active = :active  or :active = '-1') " + " and (n.role_id = :roleId  or :roleId = '-1') "

            , countQuery = "select count(n.id) from user n " + " where true "
            + " and ((n.name like concat('%',:queryString,'%') or :queryString = '' ) "
            + " or (n.email like concat('%',:queryString,'%') or :queryString = '' ) "
            + " or (n.phone like concat('%',:queryString,'%') or :queryString = '' ) "
            + " or (n.passport like concat('%',:queryString,'%') or :queryString = '' )) " + " and n.role = 1 "
            + " and (n.active = :active  or :active = '-1') "
            + " and (n.role_id = :roleId  or :roleId = '-1') ", nativeQuery = true)
    Page<UserEntity> searchByUser(@Param("queryString") String queryString, @Param("active") Integer active, @Param("roleId") Long roleId,
                                  Pageable pageable);

    @Query(value = "select n.* from user n " + " where true "
            + " and ((n.name like concat('%',:queryString,'%') or :queryString = '' ) "
            + " or (n.email like concat('%',:queryString,'%') or :queryString = '' ) "
            + " or (n.phone like concat('%',:queryString,'%') or :queryString = '' ) "
            + " or (n.username like concat('%',:queryString,'%') or :queryString = '' )) "
            + " and (n.role = 4) " + " and (n.active = :active  or :active = '-1') "
            + " and (n.role_id = :roleId  or :roleId = '-1') "

            , countQuery = "select count(n.id) from user n " + " where true "
            + " and ((n.name like concat('%',:queryString,'%') or :queryString = '' ) "
            + " or (n.email like concat('%',:queryString,'%') or :queryString = '' ) "
            + " or (n.phone like concat('%',:queryString,'%') or :queryString = '' ) "
            + " or (n.username like concat('%',:queryString,'%') or :queryString = '' )) "
            + " and (n.role = 4) " + " and (n.active = :active  or :active = '-1') "
            + " and (n.role_id = :roleId  or :roleId = '-1') ", nativeQuery = true)
    Page<UserEntity> searchByAdmin(@Param("queryString") String queryString, @Param("active") Integer active, @Param("roleId") Long roleId,
                                   Pageable pageable);

    @Query(value = "select u.* from user u where u.email = :email"
            + " and (u.role = :role or :role = -1)  limit 1", nativeQuery = true)
    UserEntity findByEmail(@Param("role") Integer role,
                           @Param("email") String email);

    @Query(value = "select u.* from user u where u.certificate_code = :certificateCode limit 1", nativeQuery = true)
    UserEntity findByCertificateCode(@Param("certificateCode") String certificateCode);

    @Query(value = "select u.* from user u where u.username = :username limit 1", nativeQuery = true)
    UserEntity findByUsername(@Param("username") String username);

    @Query(value = "select u.* from user u where u.phone = :phone"
            + " and (u.role = :role or :role = -1)  limit 1", nativeQuery = true)
    UserEntity findByPhone(@Param("role") Integer role,
                           @Param("phone") String phone);

    @Query(value = "select u.* from user u where (u.email = :phoneOrMail or u.phone = :phoneOrMail) and (u.role = :role or :role = -1) limit 1"
            , nativeQuery = true)
    UserEntity findByPhoneOrMail(@Param("phoneOrMail") String phoneOrMail, @Param("role") Integer role);

    @Query(value = "select u.* from user u where u.email_token = :emailToken limit 1", nativeQuery = true)
    UserEntity findByEmailToken(@Param("emailToken") String emailToken);

    @Query(value = "select u.* from user u where ((u.email = :emailOrPhone or u.phone = :emailOrPhone or u.username = :emailOrPhone) and u.password = :password and (u.role = :role or :role = -1))  limit 1", nativeQuery = true)
    UserEntity login(@Param("emailOrPhone") String emailOrPhone, @Param("password") String password, @Param("role") Integer role);

    UserEntity findByGoogleIdAndRole(String googleId, Integer role);

    UserEntity findByFacebookIdAndRole(String facebookId, Integer role);

    /**
     * Tim kiem theo email
     *
     * @param email
     * @return
     */
    UserEntity findFirstByEmail(String email);

    UserEntity findFirstByUid(String uid);

    @Query(value = "select n.* from user n" + " where true "
            + " and (n.role_id = :roleId or :roleId = -1) limit 1 ", nativeQuery = true)
    UserEntity checkRoles(@Param("roleId") Long roleId);


    @Query(value = "select n.* from user n left join common_specialist u on n.specialist_id = u.id"
            + " where true "
            + " and (n.specialist_id = :specialistId or :specialistId = -1)"
            + " and (n.role = 2)",
            countQuery = "select count(n.id) from user n left join common_specialist u on n.specialist_id = u.id"
                    + " where true "
                    + " and (n.specialist_id = :specialistId or :specialistId = -1)"
                    + " and (n.role = 2)"
            , nativeQuery = true)
    Page<UserEntity> getListBySpecialist(@Param("specialistId") Long specialistId,
                                         Pageable pageable);
}
