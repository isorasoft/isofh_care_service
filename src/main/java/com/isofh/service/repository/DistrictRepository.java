package com.isofh.service.repository;

import com.isofh.service.model.DistrictEntity;
import org.springframework.data.repository.CrudRepository;

public interface DistrictRepository extends CrudRepository<DistrictEntity, Long> {

    /**
     * Lay District theo hisDistrictId
     * @param hisDistrictId
     * @return
     */
    DistrictEntity findFirstByCode(String code);
}
