package com.isofh.service.repository;

import com.isofh.service.model.ScheduleEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;

public interface ScheduleRepository extends CrudRepository<ScheduleEntity, Long> {

    /**
     * Tim lich lam theo phong, ngay
     *
     * @param roomId
     * @param startDate
     * @param endDate
     * @param dayOfWeek
     * @param timeOfDay
     * @return
     */
    @Query(value = "select  n.* from schedule n "
            + " where true "
            + " and (n.room_id = :roomId) "
            + " and (n.schedule_date >= :startDate) "
            + " and (n.schedule_date <= :endDate) "
            + " and (n.day_of_week = :dayOfWeek ) "
            + " and (n.time_of_day = :timeOfDay ) "
            + " and (n.deleted = 0) "
            , nativeQuery = true)
    List<ScheduleEntity> findByRoom(
            @Param("roomId") long roomId,
            @Param("startDate") LocalDate startDate,
            @Param("endDate") LocalDate endDate,
            @Param("dayOfWeek") Integer dayOfWeek,
            @Param("timeOfDay") Integer timeOfDay);


    @Query(value = "select  n.* from schedule n "
            + " where true "
            + " and (n.department_id = :departmentId) "
            + " and (n.schedule_date >= :startDate) "
            + " and (n.schedule_date <= :endDate) "
            + " and (n.deleted = 0) "
            + " order by n.specialist_id asc, n. schedule_date asc"
            , nativeQuery = true)
    List<ScheduleEntity> findByDepartmentDate(
            @Param("startDate") LocalDate startDate,
            @Param("endDate") LocalDate endDate,
            @Param("departmentId") Long departmentId);

    @Query(value = "select  n.* from schedule n "
            + " where true "
            + " and (n.schedule_chain = :scheduleChain) "
            + " and (n.schedule_date >= :scheduleDate or :scheduleDate = '1970-01-01') "
            + " and (n.deleted = 0) "
            , nativeQuery = true)
    List<ScheduleEntity> findByChainDate(
            @Param("scheduleChain") String scheduleChain,
            @Param("scheduleDate") LocalDate scheduleDate);


    @Query(value = "select  n.* from schedule n "
            + " where true "
            + " and (n.doctor_id = :doctorId) "
            + " and (n.schedule_date >= :startDate) "
            + " and (n.schedule_date <= :endDate) "
            + " and (n.deleted = 0) "
            + " order by n.department_id asc, n. schedule_date asc"
            , nativeQuery = true)
    List<ScheduleEntity> findByDoctorDate(
            @Param("startDate") LocalDate startDate,
            @Param("endDate") LocalDate endDate,
            @Param("doctorId") Long doctorId);

    @Query(value = "select  n.* from schedule n "
            + " where true "
            + " and n.schedule_date = date(now()) "
            + " and (n.time_of_day = :timeOfDay) "
            + " and (n.end_working > :timeInMinute) "
            + " and (n.deleted = 0) "
            , nativeQuery = true)
    List<ScheduleEntity> findTodaySchedule(
            @Param("timeOfDay") int timeOfDay,
            @Param("timeInMinute") int timeInMinute);

    @Query(value = "select  n.* from schedule n "
            + " where true "
            + " and (n.doctor_id = :doctorId or :doctorId = -1) "
            + " and (n.specialist_id = :specialistId or :specialistId = -1) "
            + " and (n.department_id = :departmentId or :departmentId = -1) "
            + " and (n.schedule_date >= :startDate or :startDate = '1970-01-01') "
            + " and (n.schedule_date <= :endDate or :endDate = '1970-01-01') "
            + " and (n.day_of_week = :dayOfWeek or :dayOfWeek = -1) "
            + " and (n.deleted = 0) "
            , nativeQuery = true)
    List<ScheduleEntity> findByDoctorSpecialistDepartment(
            @Param("doctorId") Long doctorId,
            @Param("specialistId") Long specialistId,
            @Param("departmentId") Long departmentId,
            @Param("startDate") LocalDate startDate,
            @Param("endDate") LocalDate endDate,
            @Param("dayOfWeek") Integer dayOfWeek);

    /**
     * Tim lich lam theo phong, ngay
     *
     * @param roomId
     * @param scheduleDate
     * @param timeOfDay
     * @return
     */
    @Query(value = "select  n.* from schedule n "
            + " where true "
            + " and (n.room_id = :roomId) "
            + " and (n.schedule_date = :scheduleDate) "
            + " and (n.time_of_day = :timeOfDay ) "
            + " and (n.deleted = 0) limit 1 "
            , nativeQuery = true)
    ScheduleEntity findByRoom(
            @Param("roomId") long roomId,
            @Param("scheduleDate") LocalDate scheduleDate,
            @Param("timeOfDay") Integer timeOfDay);

    @Query(value = "select  n.* from schedule n "
            + " where true "
            + " and (n.department_id = :departmentId) "
            + " and (n.deleted = 0)"
            , nativeQuery = true)
    List<ScheduleEntity> findByDepartment(
            @Param("departmentId") Long departmentId);

    @Query(value = "select  n.* from schedule n "
            + " where true "
            + " and (n.service_id = :serviceId) "
            + " and (n.deleted = 0)"
            , nativeQuery = true)
    List<ScheduleEntity> findByService(
            @Param("serviceId") Long serviceId);

    @Query(value = "select  n.* from schedule n "
            + " where true "
            + " and (n.specialist_id = :specialistId) "
            + " and (n.deleted = 0)"
            , nativeQuery = true)
    List<ScheduleEntity> findBySpecialist(
            @Param("specialistId") Long specialistId);

    @Query(value = "select  n.* from schedule n "
            + " where true "
            + " and (n.doctor_id = :doctorId) "
            + " and (n.deleted = 0)"
            , nativeQuery = true)
    List<ScheduleEntity> findByDoctor(
            @Param("doctorId") Long doctorId);

    @Query(value = "select  n.* from schedule n "
            + " where true "
            + " and (n.room_id = :roomId) "
            + " and (n.deleted = 0)"
            , nativeQuery = true)
    List<ScheduleEntity> findByRoom(
            @Param("roomId") Long roomId);
}
