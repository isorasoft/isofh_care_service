package com.isofh.service.repository;

import com.isofh.service.model.SlideEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface SlideRepository extends CrudRepository<SlideEntity, Long> {
    @Query(value = "select n.* from slide n "
            + " where true "
            + " and (n.active =:active or :active = '-1') "
            + " and (n.interval_time =:intervalTime or :intervalTime = '-1') "
            + " and (n.auto_play =:autoPlay or :autoPlay = '-1') "
            + " and (n.name like concat('%',:name,'%') or :name = '') "

            , countQuery = "select count(n.id) from slide n "
            + " where true "
            + " and (n.active =:active or :active = '-1') "
            + " and (n.interval_time =:intervalTime or :intervalTime = '-1') "
            + " and (n.auto_play =:autoPlay or :autoPlay = '-1') "
            + " and (n.name like concat('%',:name,'%') or :name = '') "

            , nativeQuery = true)
    Page<SlideEntity> search(
            @Param("active") Integer active,
            @Param("intervalTime") Long intervalTime,
            @Param("autoPlay") Integer autoPlay,
            @Param("name") String name,
            Pageable pageable);

    @Query(value = "select n.* from slide n "
            + " where true "
            + " and (n.active = 1) "
            , nativeQuery = true)
    List<SlideEntity> findAllActive();
}
