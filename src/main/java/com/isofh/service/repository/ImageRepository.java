package com.isofh.service.repository;

import com.isofh.service.model.ImageEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ImageRepository extends CrudRepository<ImageEntity, Long> {
    @Query(value = "select i.* from image i where i.object_id=:objectId"
            , countQuery = "select count(i.id) from image i where i.object_id=:objectId"
            , nativeQuery = true)
    Page<ImageEntity> getImages(@Param("objectId") long objectId, Pageable pageable);

    List<ImageEntity> findAllByObjectId(Long objectId);
}
