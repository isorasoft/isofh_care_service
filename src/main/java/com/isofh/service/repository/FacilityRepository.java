package com.isofh.service.repository;

import com.isofh.service.model.FacilityEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;

public interface FacilityRepository extends CrudRepository<FacilityEntity, Long> {
    @Query(value = "select distinct n.* from facility n left join user u on n.user_id = u.id"
            + " where true "
            + " and (n.name like concat('%',:name,'%') or :name = '') "
            + " and (n.level_id = :levelId or :levelId= -1) "
            + " and (n.rank_id = :rankId or :rankId= -1) "
            + " and (n.province_id = :provinceId or :provinceId= -1) "
            + " and (n.approval = :approval or :approval= -1) "
            + " and (n.type & :type != 0 or :type= -1) "
            + " and (n.website like concat('%',:website,'%') or :website = '') "
            + " and (n.code like concat('%',:code,'%') or :code = '')"
            + " and (date(n.updated_date) >= :fromUpdatedDate or :fromUpdatedDate = '1970-01-01') "
            + " and (date(n.updated_date) <= :toUpdatedDate or :toUpdatedDate = '1970-01-01') "
            + " and (n.license_number like concat('%',:licenseNumber,'%') or :licenseNumber = '')"
            + " and (n.gpp like concat('%',:gpp,'%') or :gpp = '')"
            + " and (n.address like concat('%',:address,'%') or :address = '')"
            + " and (n.phone like concat('%',:phone,'%') or :phone = '')"
            + " and (n.user_id =:userId or :userId = '-1')"
            + " and (n.review <=:review or :review = '-1')"
            + " and (n.emergency_contact like concat('%',:emergencyContact,'%') or :emergencyContact = '')"
            + " and (u.username like concat('%',:username,'%') or :username = '')"
            + " order by "
            + " case when (:sortType = -1) then n.id end desc, "
            + " case when (:sortType = 1) then (:latitude - n.latitude) * (:latitude - n.latitude) + (:longitude - n.longitude) * (:longitude - n.longitude) end asc,"
            + " case when (:sortType = 2) then review end desc"

            , countQuery = "select count(distinct n.id) from facility n left join user u on n.user_id = u.id"
            + " where true "
            + " and (n.name like concat('%',:name,'%') or :name = '') "
            + " and (n.level_id = :levelId or :levelId= -1) "
            + " and (n.rank_id = :rankId or :rankId= -1) "
            + " and (n.province_id = :provinceId or :provinceId= -1) "
            + " and (n.approval = :approval or :approval= -1) "
            + " and (n.type & :type != 0 or :type= -1) "
            + " and (n.website like concat('%',:website,'%') or :website = '') "
            + " and (n.code like concat('%',:code,'%') or :code = '') "
            + " and (date(n.updated_date) >= :fromUpdatedDate or :fromUpdatedDate = '1970-01-01') "
            + " and (date(n.updated_date) <= :toUpdatedDate or :toUpdatedDate = '1970-01-01') "
            + " and (n.license_number like concat('%',:licenseNumber,'%') or :licenseNumber = '')"
            + " and (n.gpp like concat('%',:gpp,'%') or :gpp = '')"
            + " and (n.address like concat('%',:address,'%') or :address = '')"
            + " and (n.phone like concat('%',:phone,'%') or :phone = '')"
            + " and (n.user_id =:userId or :userId = '-1')"
            + " and (n.review <=:review or :review = '-1')"
            + " and (n.emergency_contact like concat('%',:emergencyContact,'%') or :emergencyContact = '')"
            + " and (u.username like concat('%',:username,'%') or :username = '')"
            , nativeQuery = true)
    Page<FacilityEntity> search(
            @Param("name") String name,
            @Param("levelId") Long levelId,
            @Param("rankId") Long rankId,
            @Param("provinceId") Long provinceId,
            @Param("website") String website,
            @Param("code") String code,
            @Param("approval") Long approval,
            @Param("latitude") Float latitude,
            @Param("longitude") Float longitude,
            @Param("sortType") Integer sortType,
            @Param("fromUpdatedDate") LocalDate fromUpdatedDate,
            @Param("toUpdatedDate") LocalDate toUpdatedDate,
            @Param("type") Integer type,
            @Param("licenseNumber") String licenseNumber,
            @Param("gpp") String gpp,
            @Param("address") String address,
            @Param("phone") String phone,
            @Param("userId") Long userId,
            @Param("review") Integer review,
            @Param("emergencyContact") String emergencyContact,
            @Param("username") String username,
            Pageable pageable);

    @Query(value = "select distinct n.* from facility n left join facility_specialist fs on n.id = fs.facility_id left join specialist s on s.id = fs.specialist_id"
            + " where true "
            + " and (n.name like concat('%',:query,'%') or n.address like concat('%',:query,'%') or s.name like concat('%',:query,'%') or :query = '') "
            + " and (s.id = :specialistId or :specialistId = -1) "
            + " and (n.approval = :approval or :approval = -1) "

            , countQuery = "select count(distinct n.id) from facility n left join facility_specialist fs on n.id = fs.facility_id left join specialist s on s.id = fs.specialist_id"
            + " where true "
            + " and (n.name like concat('%',:query,'%') or n.address like concat('%',:query,'%') or s.name like concat('%',:query,'%') or :query = '') "
            + " and (s.id = :specialistId or :specialistId = -1) "
            + " and (n.approval = :approval or :approval = -1) "
            , nativeQuery = true)
    Page<FacilityEntity> searchByQuery(
            @Param("query") String query,
            @Param("specialistId") Long specialistId,
            @Param("approval") Integer approval,
            Pageable pageable);

    @Query(value = "select f.* from facility f join drug d on f.id=d.facility_id" +
            " where d.origin_id=:drugId"
            , nativeQuery = true)
    Page<FacilityEntity> findByDrug(
            @Param("drugId") Long drugId,
            Pageable pageable
    );

    @Query(value = " select * from facility n" +
            " where n.name = :name" +
            " limit 1", nativeQuery = true)
    FacilityEntity checkName(
            @Param("name") String name
    );

    @Query(value = " select * from facility n" +
            " where n.name = :name" +
            " and n.address = :address" +
            " limit 1", nativeQuery = true)
    FacilityEntity checkNameAndAddress(
            @Param("name") String name,
            @Param("address") String address
    );
}
