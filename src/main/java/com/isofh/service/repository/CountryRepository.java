package com.isofh.service.repository;

import com.isofh.service.model.CountryEntity;
import org.springframework.data.repository.CrudRepository;

public interface CountryRepository extends CrudRepository<CountryEntity, Long> {

    CountryEntity findFirstByCode(String code);
}
