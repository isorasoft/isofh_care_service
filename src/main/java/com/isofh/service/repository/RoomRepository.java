package com.isofh.service.repository;

import com.isofh.service.model.RoomEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RoomRepository extends CrudRepository<RoomEntity, Long> {

    @Query(value = "select n.* from room n "
            + " where true "
            + " and n.his_room_id = :hisRoomId"
            + " and n.col_hospital_id = :hospitalId"
            + " and n.deleted = 0"
            , nativeQuery = true)
    RoomEntity findByHisRoomId(
            @Param("hisRoomId") String hisRoomId,
            @Param("hospitalId") long hospitalId);

    @Query(value = "select n.* from room n "
            + " where true "
            + " and n.col_hospital_id = :hospitalId"
            + " and n.deleted = 0"
            , nativeQuery = true)
    List<RoomEntity> getAll(@Param("hospitalId") long hospitalId);

}
