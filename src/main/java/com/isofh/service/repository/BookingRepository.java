package com.isofh.service.repository;

import com.isofh.service.model.BookingEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public interface BookingRepository extends CrudRepository<BookingEntity, Long> {

    @Query(value = "select n.* from booking n "
            + " where true "
            + " and n.schedule_id=:scheduleId"
            + " and n.booking_time=:bookingTime"
            // chua bi cancel
            + " and n.status != -1"
            + " and n.deleted != 1"
            , nativeQuery = true)
    List<BookingEntity> findByScheduleAndTime(
            @Param("scheduleId") Long scheduleId,
            @Param("bookingTime") LocalDateTime bookingTime
    );


    @Query(value = "select  n.* from booking n "
            + " where true "
            + " and (n.doctor_id = :doctorId or :doctorId = -1) "
            + " and (n.specialist_id = :specialistId or :specialistId = -1) "
            + " and (date(n.booking_time)  >= :startDate or :startDate = '1970-01-01') "
            + " and (date(n.booking_time) <= :endDate or :endDate = '1970-01-01') "
            + " and (n.deleted = 0) "
            , nativeQuery = true)
    List<BookingEntity> findByDoctorSpecialist(
            @Param("doctorId") Long doctorId,
            @Param("specialistId") Long specialistId,
            @Param("startDate") LocalDate startDate,
            @Param("endDate") LocalDate endDate);


    @Query(value = "select  n.* from booking n left join profile p on p.id = n.profile_id"
            + " where true "
            + " and (p.name like concat('%',:patientName,'%') or :patientName = '') "
            + " and (n.doctor_id = :doctorId or :doctorId = -1) "
            + " and (n.service_id = :serviceId or :serviceId = -1) "
            + " and (p.phone like concat('%',:phone,'%') or :phone = '') "
            + " and (p.address like concat('%',:address,'%') or :address = '') "
            + " and (n.status = :status or :status = -2) "
            + " and (p.value like concat('%',:value,'%') or :value = '') "
            + " and (date(n.booking_time)  >= :startDate or :startDate = '1970-01-01') "
            + " and (date(n.booking_time) <= :endDate or :endDate = '1970-01-01') "
            + " and (n.department_id = :departmentId or :departmentId = -1) "
            + " and (n.specialist_id = :specialistId or :specialistId = -1) "
            + " and (n.admin like concat('%',:admin,'%') or :admin = '') "
            + " and (n.deleted = 0) "
            + " and (n.room_id = :roomId or :roomId = -1) "
            + " and (n.phone_call like concat('%',:phoneCall,'%') or :phoneCall = '') "
            + " and (n.arrival = :arrival or :arrival = -1) "
            + " and (n.request_doctor = :requestDoctor or :requestDoctor = -1) "

            , countQuery = "select count(n.id) from booking n left join profile p on p.id = n.profile_id"
            + " where true "
            + " and (p.name like concat('%',:patientName,'%') or :patientName = '') "
            + " and (n.doctor_id = :doctorId or :doctorId = -1) "
            + " and (n.service_id = :serviceId or :serviceId = -1) "
            + " and (p.phone like concat('%',:phone,'%') or :phone = '') "
            + " and (p.address like concat('%',:address,'%') or :address = '') "
            + " and (n.status = :status or :status = -2) "
            + " and (p.value like concat('%',:value,'%') or :value = '') "
            + " and (date(n.booking_time)  >= :startDate or :startDate = '1970-01-01') "
            + " and (date(n.booking_time) <= :endDate or :endDate = '1970-01-01') "
            + " and (n.department_id = :departmentId or :departmentId = -1) "
            + " and (n.specialist_id = :specialistId or :specialistId = -1) "
            + " and (n.admin like concat('%',:admin,'%') or :admin = '') "
            + " and (n.deleted = 0) "
            + " and (n.room_id = :roomId or :roomId = -1) "
            + " and (n.phone_call like concat('%',:phoneCall,'%') or :phoneCall = '') "
            + " and (n.arrival = :arrival or :arrival = -1) "
            + " and (n.request_doctor = :requestDoctor or :requestDoctor = -1) "
            , nativeQuery = true)
    Page<BookingEntity> search(
            @Param("patientName") String patientName,
            @Param("doctorId") Long doctorId,
            @Param("serviceId") Long serviceId,
            @Param("phone") String phone,
            @Param("address") String address,
            @Param("status") Integer status,
            @Param("value") String value,
            @Param("startDate") LocalDate startDate,
            @Param("endDate") LocalDate endDate,
            @Param("departmentId") Long departmentId,
            @Param("specialistId") Long specialistId,
            @Param("admin") String admin,
            @Param("roomId") Long roomId,
            @Param("phoneCall") String phoneCall,
            @Param("arrival") Integer arrival,
            @Param("requestDoctor") Integer requestDoctor,
            Pageable defaultPage);

    @Query(value = "select  n.* from booking n "
            + " where true "
            + " and (n.profile_id = :profileId) "
            + " and (n.status = :status) "
            + " and (n.deleted = 0) "
            , nativeQuery = true)
    List<BookingEntity> getByProfile(
            @Param("profileId") long profileId,
            @Param("status") int status);

    @Query(value = "select  n.* from booking n\n" +
            "where true  \n" +
            "and (date(n.booking_time) = date(:curDate))  \n" +
            "and n.status = :status\n" +
            "and (n.deleted = 0)\n" +
            "order by n.profile_id desc "
            , nativeQuery = true)
    List<BookingEntity> findByDateaAndStatus(
            @Param("curDate") LocalDate curDate,
            @Param("status") Integer status);
    
    @Query(value = "select  n.* from booking n\n" +
            "where true  \n" +
            "and n.created_date <= :curDate  \n" +
            "and n.status = :status\n" +
            "and (n.deleted = 0)\n" +
            "order by n.profile_id desc "
            , nativeQuery = true)
    List<BookingEntity> findByCreateDateaAndStatus(
            @Param("curDate") LocalDateTime curDate,
            @Param("status") Integer status);

    @Query(value = "select n.* from booking n "
            + " where true "
            + " and n.schedule_id in (:scheduleIds)"
            + " and n.status != -1"
            + " and n.deleted != 1"
            , nativeQuery = true)
    List<BookingEntity> findBySchedules(
            @Param("scheduleIds") List<Long> scheduleIds
    );

    @Query(value = "select n.*, count(n.sequence_no) c" +
            " from booking n" +
            " where date(n.booking_time) > date(now())" +
            "  and n.status != -1" +
            "  and n.deleted = 0" +
            " group by n.sequence_no, n.room_id, date(n.booking_time)" +
            " having c > 1"
            , nativeQuery = true)
    List<BookingEntity> findDuplicateInRoom();
}
