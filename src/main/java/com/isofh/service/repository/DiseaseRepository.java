package com.isofh.service.repository;

import com.isofh.service.model.DiseaseEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;

public interface DiseaseRepository extends CrudRepository<DiseaseEntity, Long> {

    @Query(value = "select n.* from disease n "
            + " where true "
            + " and (n.name like concat('%',:name,'%') or :name = '') "
            + " and (n.from_age >= :fromAge or :fromAge= -1) "
            + " and (n.to_age = :toAge or :toAge= -1) "
            + " and (n.specialist_id = :specialistId or :specialistId= -1) "
            + " and (n.gender = :gender or :gender= -1) "
            + " and (date(n.updated_date) >= :fromUpdatedDate or :fromUpdatedDate = '1970-01-01') "
            + " and (date(n.updated_date) <= :toUpdatedDate or :toUpdatedDate = '1970-01-01') "
            , countQuery = "select count(n.id) from disease n "
            + " where true "
            + " and (n.name like concat('%',:name,'%') or :name = '') "
            + " and (n.from_age >= :fromAge or :fromAge= -1) "
            + " and (n.to_age = :toAge or :toAge= -1) "
            + " and (n.specialist_id = :specialistId or :specialistId= -1) "
            + " and (n.gender = :gender or :gender= -1) "
            + " and (date(n.updated_date) >= :fromUpdatedDate or :fromUpdatedDate = '1970-01-01') "
            + " and (date(n.updated_date) <= :toUpdatedDate or :toUpdatedDate = '1970-01-01') "

            , nativeQuery = true)
    Page<DiseaseEntity> search(
            @Param("name") String name,
            @Param("fromAge") Integer fromAge,
            @Param("toAge") Integer toAge,
            @Param("specialistId") Integer specialistId,
            @Param("gender") Integer gender,
            @Param("fromUpdatedDate") LocalDate fromUpdatedDate,
            @Param("toUpdatedDate") LocalDate toUpdatedDate,
            Pageable pageable);

    @Query(value = "select distinct n.* from disease n "
            + " left join disease_symptom ds on n.id = ds.disease_id "
            + " left join symptom s on ds.symptom_id = s.id"
            + " where true "
            + " and (n.name like concat('%',:name,'%') or s.name like concat('%',:name,'%') or :name = '') "

            , countQuery = "select count(distinct n.id) from disease n "
            + " left join disease_symptom ds on n.id = ds.disease_id "
            + " left join symptom s on ds.symptom_id = s.id"
            + " where true "
            + " and (n.name like concat('%',:name,'%') or s.name like concat('%',:name,'%') or :name = '') "
            , nativeQuery = true)
    Page<DiseaseEntity> searchByDiseaseSymptom(@Param("name") String name, Pageable paging);

    @Query(value = "select distinct n.* from disease n "
            + " join disease_symptom ds on n.id = ds.disease_id and ds.symptom_id=:symptomId"

            , countQuery = "select count(distinct n.id) from disease n "
            + " join disease_symptom ds on n.id = ds.disease_id and ds.symptom_id=:symptomId"
            , nativeQuery = true)
    Page<DiseaseEntity> searchDiseaseBySymptom(
            @Param("symptomId") long symptomId,
            Pageable paging);
}
