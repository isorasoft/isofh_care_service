package com.isofh.service.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.isofh.service.model.PatientHistoryEntity;

public interface PatientHistoryRepository extends CrudRepository<PatientHistoryEntity, Long> {

    @Query(value = "select * from patient_history n" + " where true" + " and (n.hospital_id = :hospitalId )"
            + " and (n.profile_id = :profileId )" + " and (n.patient_history_id = :patientHistoryId )"
            + " limit 1 ", nativeQuery = true)
    PatientHistoryEntity findByProfileHospitalPatientHistory(@Param("profileId") Long profileId,
            @Param("hospitalId") Long hospitalId, @Param("patientHistoryId") String patientHistoryId);

    @Query(value = "select * from patient_history n" + " where true" + " and (n.hospital_id = :hospitalId )"
            + " and (n.profile_id = :profileId )" + " ORDER BY  n.time_go_in DESC", nativeQuery = true)
    List<PatientHistoryEntity> findByProfileHospital(@Param("profileId") Long profileId,
            @Param("hospitalId") Long hospitalId);

    @Query(value = "select * from patient_history n" + " where true" + " and (n.profile_id = :profileId )"
            + " ORDER BY  n.time_go_in DESC", nativeQuery = true)
    List<PatientHistoryEntity> findByProfile(@Param("profileId") Long profileId);

    @Query(value = "select * from patient_history n" + " where true" + " and (n.hospital_id = :hospitalId )"
            + " and (n.patient_history_id = :patientHistoryId )" + " limit 1 ", nativeQuery = true)
    PatientHistoryEntity findByHospitalPatientHistory(@Param("hospitalId") Long hospitalId,
            @Param("patientHistoryId") String patientHistoryId);
}
