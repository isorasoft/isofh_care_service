package com.isofh.service.repository;

import com.isofh.service.model.SlidePlaceEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface SlidePlaceRepository extends CrudRepository<SlidePlaceEntity, Long> {

    @Query(value = "select  n.* from slide_place n "
            + " where true "
            + " and (n.name like concat('%',:name,'%') or :name = '') "

            , countQuery = "select count(n.id) from slide_place n "
            + " where true "
            + " and (n.name like concat('%',:name,'%') or :name = '') "

            , nativeQuery = true)
    Page<SlidePlaceEntity> search(
            @Param("name") String name, Pageable pageable);
}
