package com.isofh.service.repository;

import com.isofh.service.model.ServiceEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ServiceRepository extends CrudRepository<ServiceEntity, Long> {

    @Query(value = "select n.* from service n "
            + " where true "
            + " and n.his_service_id = :hisServiceId"
            + " and n.col_hospital_id = :hospitalId"
            + " and n.deleted = 0"
            , nativeQuery = true)
    ServiceEntity findByHisServiceId(@Param("hisServiceId") String hisServiceId,
                                     @Param("hospitalId") long hospitalId);

    @Query(value = "select n.* from service n "
            + " where true "
            + " and n.deleted = 0"
            + " and n.col_hospital_id = :hospitalId"
            , nativeQuery = true)
    List<ServiceEntity> getAll(@Param("hospitalId") long hospitalId);
}
