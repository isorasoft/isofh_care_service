package com.isofh.service.repository;

import com.isofh.service.model.ProfileEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProfileRepository extends CrudRepository<ProfileEntity, Long> {

    /**
     * Lay cac profile cua 1 user
     *
     * @param userId
     * @return
     */
    @Query(value = "select * from profile n" +
            " where true" +
            " and n.user_id = :userId" +
            " and n.deleted != 1"
            , nativeQuery = true)
    List<ProfileEntity> findByUserId(@Param("userId") long userId);
    
    @Query(value = "select * from profile n" +
            " where true" +
            " and n.user_id = :userId" +
            " and n.deleted != 1 limit 1"
            , nativeQuery = true)
    ProfileEntity findFirstByUserId(@Param("userId") long userId);
    
    @Query(value = "select * from profile n" +
            " where true" +
            " and n.uid = :uid" +
            " and n.deleted != 1 limit 1"
            , nativeQuery = true)
    ProfileEntity findFirstByUid(@Param("uid") String uid);

    /**
     * Lay cac profile cua 1 user
     *
     * @param value
     * @return
     */
    @Query(value = "select * from profile n" +
            " where true" +
            " and n.his_patient_id = :hisPatientId" +
            " and n.deleted != 1 limit 1"
            , nativeQuery = true)
    ProfileEntity findFirstByHisPatientId(@Param("hisPatientId") String hisPatientId);

    @Query(value = "select * from profile n" +
            " where true" +
            " and date(n.dob) > date(now())"
            , nativeQuery = true)
    List<ProfileEntity> getInvalidProfile();

}
