package com.isofh.service.repository;

import com.isofh.service.model.SpecialistEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface SpecialistRepository extends CrudRepository<SpecialistEntity, Long> {

    @Query(value = "select n.* from specialist n "
            + " where true "
            + " and n.his_specialist_id = :hisSpecialistId"
            + " and n.col_hospital_id = :hospitalId"
            + " and n.deleted = 0"
            , nativeQuery = true)
    SpecialistEntity findByHisSpecialistId(
            @Param("hisSpecialistId") Long hisSpecialistId
            , @Param("hospitalId") long hospitalId);

    @Query(value = "select n.* from specialist n "
            + " where true "
            + " and n.deleted = 0"
            + " and n.col_hospital_id = :hospitalId"
            , nativeQuery = true)
    List<SpecialistEntity> getAll(@Param("hospitalId") long hospitalId);

    @Query(value = "select n.* from specialist n" +
            " where n.id in" +
            " (select distinct(s.specialist_id) from schedule s" +
            " where s.department_id = :departmentId" +
            " and (s.doctor_id = :doctorId or :doctorId = '-1') " +
            " and date(s.schedule_date) >= date(now())" +
            " and s.deleted = 0)"
            ,
            nativeQuery = true)
    List<SpecialistEntity> findByDepartmentDoctor(
            @Param("departmentId") Long departmentId,
            @Param("doctorId") Long doctorId);
}
