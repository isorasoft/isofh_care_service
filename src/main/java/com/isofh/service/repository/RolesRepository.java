package com.isofh.service.repository;

import com.isofh.service.model.RolesEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface RolesRepository extends CrudRepository<RolesEntity, Long> {
    @Query(value = "select distinct n.* from tbl_role n "
            + " where ((n.col_name like concat('%',:stringQuery,'%') or :stringQuery = '') "
            + " or (n.col_code like concat('%',:stringQuery,'%') or :stringQuery = '')) "
            + " and (n.col_blocked =:blocked or :blocked = '-1') "
            + " and (n.col_deleted = 0 ) "
            + " and (n.col_value_role & :value != 0 or :value = -1) "
            ,
            countQuery = "select count(distinct n.id) from tbl_role n "
                    + " where ((n.col_name like concat('%',:stringQuery,'%') or :stringQuery = '') "
                    + " or (n.col_code like concat('%',:stringQuery,'%') or :stringQuery = '')) "
                    + " and (n.col_blocked =:blocked or :blocked = '-1') "
                    + " and (n.col_deleted = 0) "
                    + " and (n.col_value_role & :value != 0 or :value = -1) "
            ,
            nativeQuery = true)
    Page<RolesEntity> search(
            @Param("stringQuery") String stringQuery,
            @Param("value") Long value,
            @Param("blocked") int blocked,
            Pageable pageable
    );

    @Query(value = "select n.* from tbl_role n "
            + " where true "
            + " and (n.col_code = :code)"
            + " and (n.col_deleted = 0) limit 1 "
            , nativeQuery = true)
    RolesEntity checkCode(
            @Param("code") String code);
}
