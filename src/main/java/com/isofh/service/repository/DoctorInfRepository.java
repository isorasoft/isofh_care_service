package com.isofh.service.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.isofh.service.model.DoctorInfEntity;

public interface DoctorInfRepository extends CrudRepository<DoctorInfEntity, Long> {
    @Query(value = "select n.* from tbl_doctor_inf n "
            + " where true "
            + " and (n.col_email like concat('%',:email,'%') or :email = '')"
            + " and (n.block = 0)  limit 1"
            , nativeQuery = true)
    DoctorInfEntity checkEmail(
            @Param("email") String email);

    @Query(value = "select n.* from tbl_doctor_inf n "
            + " where true "
            + " and (n.col_phone like concat('%',:phone,'%') or :phone = '')"
            + " and (n.block = 0)  limit 1"
            , nativeQuery = true)
    DoctorInfEntity checkPhone(
            @Param("phone") String phone);

    @Query(value = "select n.* from tbl_doctor_inf n "
            + " where true "
            + " and (n.col_certificate_code like concat('%',:certificateCode,'%') or :certificateCode = '')"
            + " and (n.block = 0)  limit 1"
            , nativeQuery = true)
    DoctorInfEntity checkcertificateCode(
            @Param("certificateCode") String certificateCode);

    @Query(value = "select n.* from user n "
            + " where true "
            + " and (n.phone like concat('%',:stringQuery,'%') or :stringQuery = '')"
            + " or (n.email like concat('%',:stringQuery,'%') or :stringQuery = '') limit 1"
            , nativeQuery = true)
    DoctorInfEntity checkPhoneOrMail(
            @Param("stringQuery") String stringQuery);



    @Query(value = "select n.* from tbl_doctor_inf n left join tbl_specialist s on n.specialist_id = s.id"
            + " where true "
            + " and ((n.col_name like concat('%',:queryString,'%') or :queryString = '' ) "
            + " or (n.col_phone like concat('%',:queryString,'%') or :queryString = '' ) "
            + " or (n.col_email like concat('%',:queryString,'%') or :queryString = '' )) "
            + " and (n.specialist_id =:specialistId or :specialistId = '-1') "
            + " and (n.col_active =:active or :active = '-1') "
            + " and (n.block = 0) "

            , countQuery = "select count(n.id) from tbl_doctor_inf n left join tbl_specialist s on n.specialist_id = s.id"
            + " where true "
            + " and ((n.col_name like concat('%',:queryString,'%') or :queryString = '' ) "
            + " or (n.col_phone like concat('%',:queryString,'%') or :queryString = '' ) "
            + " or (n.col_email like concat('%',:queryString,'%') or :queryString = '' )) "
            + " and (n.specialist_id =:specialistId or :specialistId = '-1') "
            + " and (n.col_active =:active or :active = '-1') "
            + " and (n.block = 0) "
            , nativeQuery = true)
    Page<DoctorInfEntity> search(
            @Param("queryString") String queryString,
            @Param("specialistId") Long specialistId,
            @Param("active") Integer active,
            Pageable defaultPage);

}
