package com.isofh.service.repository;

import com.isofh.service.model.SymptomEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;

public interface SymptomRepository extends CrudRepository<SymptomEntity, Long> {


    @Query(value = "select n.* from symptom n "
            + " where true "
            + " and (n.name like concat('%',:name,'%') or :name = '') "
            + " and ((:head = -2 and n.head != 0) or (n.head & :head =:head) or :head= -1) "
            + " and ((:neck = -2 and n.neck != 0) or (n.neck & :neck =:neck) or :neck= -1) "
            + " and ((:shoulder = -2 and n.shoulder != 0) or (n.shoulder & :shoulder =:shoulder) or :shoulder= -1) "
            + " and ((:chest = -2 and n.chest != 0) or (n.chest & :chest =:chest) or :chest= -1) "
            + " and ((:arm = -2 and n.arm != 0) or (n.arm & :arm =:arm) or :arm= -1) "
            + " and ((:abdomen = -2 and n.abdomen != 0) or (n.abdomen & :abdomen =:abdomen) or :abdomen= -1) "
            + " and ((:flank = -2 and n.flank != 0) or (n.flank & :flank =:flank) or :flank= -1) "
            + " and ((:pelvis = -2 and n.pelvis != 0) or (n.pelvis & :pelvis =:pelvis) or :pelvis= -1) "
            + " and ((:leg = -2 and n.leg != 0) or (n.leg & :leg =:leg) or :leg= -1) "
            + " and ((:foot = -2 and n.foot != 0) or (n.foot & :foot =:foot) or :foot= -1) "
            + " and ((:back = -2 and n.back != 0) or (n.back & :back =:back) or :back= -1) "
            + " and ((:waist = -2 and n.waist != 0) or (n.waist & :waist =:waist) or :waist= -1) "
            + " and ((:ass = -2 and n.ass != 0) or (n.ass & :ass =:ass) or :ass= -1) "
            + " and ((:hand = -2 and n.shoulder != 0) or (n.hand & :hand =:hand) or :hand= -1) "
            + " and (date(n.updated_date) >= :fromUpdatedDate or :fromUpdatedDate = '1970-01-01') "
            + " and (date(n.updated_date) <= :toUpdatedDate or :toUpdatedDate = '1970-01-01') "

            , countQuery = "select count(n.id) from symptom n "
            + " where true "
            + " and (n.name like concat('%',:name,'%') or :name = '') "
            + " and ((:head = -2 and n.head != 0) or (n.head & :head =:head) or :head= -1) "
            + " and ((:neck = -2 and n.neck != 0) or (n.neck & :neck =:neck) or :neck= -1) "
            + " and ((:shoulder = -2 and n.shoulder != 0) or (n.shoulder & :shoulder =:shoulder) or :shoulder= -1) "
            + " and ((:chest = -2 and n.chest != 0) or (n.chest & :chest =:chest) or :chest= -1) "
            + " and ((:arm = -2 and n.arm != 0) or (n.arm & :arm =:arm) or :arm= -1) "
            + " and ((:abdomen = -2 and n.abdomen != 0) or (n.abdomen & :abdomen =:abdomen) or :abdomen= -1) "
            + " and ((:flank = -2 and n.flank != 0) or (n.flank & :flank =:flank) or :flank= -1) "
            + " and ((:pelvis = -2 and n.pelvis != 0) or (n.pelvis & :pelvis =:pelvis) or :pelvis= -1) "
            + " and ((:leg = -2 and n.leg != 0) or (n.leg & :leg =:leg) or :leg= -1) "
            + " and ((:foot = -2 and n.foot != 0) or (n.foot & :foot =:foot) or :foot= -1) "
            + " and ((:back = -2 and n.back != 0) or (n.back & :back =:back) or :back= -1) "
            + " and ((:waist = -2 and n.waist != 0) or (n.waist & :waist =:waist) or :waist= -1) "
            + " and ((:ass = -2 and n.ass != 0) or (n.ass & :ass =:ass) or :ass= -1) "
            + " and ((:hand = -2 and n.shoulder != 0) or (n.hand & :hand =:hand) or :hand= -1) "
            + " and (date(n.updated_date) >= :fromUpdatedDate or :fromUpdatedDate = '1970-01-01') "
            + " and (date(n.updated_date) <= :toUpdatedDate or :toUpdatedDate = '1970-01-01') "
            , nativeQuery = true)
    Page<SymptomEntity> search(@Param("name") String name,
                               @Param("head") Integer head,
                               @Param("neck") Integer neck,
                               @Param("shoulder") Integer shoulder,
                               @Param("chest") Integer chest,
                               @Param("arm") Integer arm,
                               @Param("abdomen") Integer abdomen,
                               @Param("flank") Integer flank,
                               @Param("pelvis") Integer pelvis,
                               @Param("leg") Integer leg,
                               @Param("foot") Integer foot,
                               @Param("back") Integer back,
                               @Param("waist") Integer waist,
                               @Param("ass") Integer ass,
                               @Param("hand") Integer hand,
                               @Param("fromUpdatedDate") LocalDate fromUpdatedDate,
                               @Param("toUpdatedDate") LocalDate toUpdatedDate,
                               Pageable pageable);
}
