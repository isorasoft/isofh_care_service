package com.isofh.service.repository;

import com.isofh.service.model.AdvertiseEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface AdvertiseRepository extends CrudRepository<AdvertiseEntity, Long> {

    @Query(value = "select  n.* from advertise n "
            + " where true "
            + " and (n.title like concat('%',:title,'%') or :title = '') "
            + " and (n.content like concat('%',:content,'%') or :content = '') "
            + " and (n.type  = :type or :type = '-1') "

            , countQuery = "select count(n.id) from advertise n "
            + " where true "
            + " and (n.title like concat('%',:title,'%') or :title = '') "
            + " and (n.content like concat('%',:content,'%') or :content = '') "
            + " and (n.type  = :type or :type = '-1') "

            , nativeQuery = true)
    Page<AdvertiseEntity> search(
            @Param("title") String title,
            @Param("content") String content,
            @Param("type") Integer type,
            Pageable pageable);
}
