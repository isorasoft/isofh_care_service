package com.isofh.service.repository;

import com.isofh.service.model.ProvinceEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface ProvinceRepository extends CrudRepository<ProvinceEntity, Long> {


    /**
     * Lay Province theo hisProvinceId
     * @param hisProvinceId
     * @return
     */
    ProvinceEntity findFirstByCode(String code);
}
