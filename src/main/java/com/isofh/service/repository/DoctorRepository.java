package com.isofh.service.repository;

import com.isofh.service.model.DoctorEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface DoctorRepository extends CrudRepository<DoctorEntity, Long> {

    @Query(value = "select n.* from doctor n "
            + " where true "
            + " and n.his_doctor_id = :hisDoctorId"
            + " and n.col_hospital_id = :hospitalId"
            + " and n.deleted = 0"
            , nativeQuery = true)
    DoctorEntity findByHisDoctorId(
            @Param("hisDoctorId") String hisDoctorId,
            @Param("hospitalId") long hospitalId);

    @Query(value = "select n.* from doctor n "
            + " where true "
            + " and n.deleted = 0"
            + " and n.col_hospital_id = :hospitalId"
            , nativeQuery = true)
    List<DoctorEntity> getAll(@Param("hospitalId") long hospitalId);

    @Query(value = "select n.* from doctor n " +
            " where n.id in" +
            " (select distinct(s.doctor_id) from schedule s" +
            " where s.department_id = :departmentId" +
            " and (s.specialist_id = :specialistId or :specialistId = '-1')" +
            " and date(s.schedule_date) > date(now())" +
            " and date(s.schedule_date) <= date((adddate(now(), s.num_allowed_date)))" +
            " and s.deleted = 0)" +
            " order by n.name asc"

            , nativeQuery = true)
    List<DoctorEntity> findByDepartmentSpecialist(
            @Param("departmentId") Long departmentId,
            @Param("specialistId") Long specialistId);
}
