package com.isofh.service.repository;

import com.isofh.service.model.SlideItemEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface SlideItemRepository extends CrudRepository<SlideItemEntity, Long> {

    @Query(value = "select n.* from slide_item n "
            + " where true "
            + " and (n.slide_id = :slideId  or :slideId = '-1') "
            + " and (n.name like concat('%',:name,'%') or :name = '') "

            , countQuery = "select count(n.id) from slide_item n "
            + " where true "
            + " and (n.slide_id = :slideId  or :slideId = '-1') "
            + " and (n.name like concat('%',:name,'%') or :name = '') "

            , nativeQuery = true)
    Page<SlideItemEntity> search(
            @Param("slideId") Long slideId,
            @Param("name") String name,
            Pageable pageable);
}
