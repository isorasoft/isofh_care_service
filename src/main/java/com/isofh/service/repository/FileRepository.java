package com.isofh.service.repository;

import com.isofh.service.model.FileEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface FileRepository extends CrudRepository<FileEntity, Long> {

    @Query(value = "select i.* from file i where i.object_id=:objectId"
            , countQuery = "select count(i.id) from file i where i.object_id=:objectId"
            , nativeQuery = true)
    Page<FileEntity> getFiles(@Param("objectId") long objectId, Pageable pageable);
}
