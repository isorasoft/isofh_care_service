package com.isofh.service.repository;

import com.isofh.service.model.NotificationEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface NotificationRepository extends CrudRepository<NotificationEntity, Long> {
    @Query(value = "select n.* from tbl_notification n " + " where (n.user_id = :userId)"
            + " and (n.watched = :watched or :watched = '-1')", countQuery = "select count(n.id) from tbl_notification n "
                    + " where (n.user_id = :userId) "
                    + " and (n.watched = :watched or :watched = '-1')", nativeQuery = true)
    Page<NotificationEntity> search(@Param("userId") Long userId, @Param("watched") Integer watched, Pageable pageable);

    @Query(value = "select n.* from tbl_notification n " + " where true " + " and (n.col_code = :code)"
            + " and (n.col_deleted = 0) limit 1 ", nativeQuery = true)
    NotificationEntity checkCode(@Param("code") String code);

    @Query(value = "select d.* from tbl_notification d where d.user_id = :userId "
            + " and (d.watched = 0)", nativeQuery = true)
    List<NotificationEntity> findByUser(@Param("userId") Long userId);

    @Query(value = "select d.* from tbl_notification d where d.user_id = :userId ", nativeQuery = true)
    List<NotificationEntity> findByUserDelete(@Param("userId") Long userId);
}
