package com.isofh.service.repository;

import com.isofh.service.model.SmsEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface SmsRepository extends CrudRepository<SmsEntity, Long> {

    @Query(value = "select u.* from sms u where u.sent = 0", nativeQuery = true)
    List<SmsEntity> getNotSent();
}
