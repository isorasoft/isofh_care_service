package com.isofh.service.repository;


import com.isofh.service.model.DeviceEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface UserDeviceRepository extends CrudRepository<DeviceEntity, Long> {

    @Transactional
    List<DeviceEntity> deleteByDeviceId(String deviceId);

    @Query(value = "select d.* from device d where d.user_id = :userId ", nativeQuery = true)
    List<DeviceEntity> findByUser(@Param("userId") Long userId);
}
