package com.isofh.service.repository;

import com.isofh.service.model.CacheHisEntity;
import org.springframework.data.repository.CrudRepository;

public interface CacheHisRepository extends CrudRepository<CacheHisEntity, Long> {

    CacheHisEntity findFirstByTypeAndPatientHistoryIdAndHospitalId(int type, long patientHistoryId, long hospitalId);
    CacheHisEntity findFirstByTypeAndValueAndHospitalId(int type, String value, long hospitalId);
}
