package com.isofh.service.repository;

import com.isofh.service.model.PermissionEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface PermissionRepository extends CrudRepository<PermissionEntity, Long> {
    @Query(value = "select n.* from tbl_permission n "
            + " where (n.col_name like concat('%',:name,'%') or :name = '') "
            ,
            countQuery = "select count(n.id) from tbl_permission n "
                    + " where (n.col_name like concat('%',:name,'%') or :name = '') "
            ,

            nativeQuery = true)
    Page<PermissionEntity> search(
            @Param("name") String name,
            Pageable pageable

    );
}
