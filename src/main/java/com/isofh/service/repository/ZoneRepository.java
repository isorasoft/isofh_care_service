package com.isofh.service.repository;

import com.isofh.service.model.ZoneEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ZoneRepository extends CrudRepository<ZoneEntity, Long> {

    /**
     * Lay Zone theo hisZoneId
     * @param hisZoneId
     * @return
     */
    ZoneEntity findFirstByCode(String code);

    /**
     * Lay Zone theo hisDistrictId
     * @param hisDistrictId
     * @return
     */
    List<ZoneEntity> findBycode(String code);
    
    List<ZoneEntity> findByDistrictCode(String code);
}
