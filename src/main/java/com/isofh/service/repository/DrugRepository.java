package com.isofh.service.repository;

import com.isofh.service.model.DrugEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;

public interface DrugRepository extends CrudRepository<DrugEntity, Long> {

    @Query(value = "select n.* from drug n "
            + " where true "
            + " and (n.name like concat('%',:name,'%') or :name = '') "
            + " and (n.active_substances like concat('%',:activeSubstances,'%') or :activeSubstances = '') "
            + " and (n.category & :category =:category or :category= -1) "
            + " and (n.method_use=:methodUse or :methodUse= -1) "
            + " and (n.standard like concat('%',:standard,'%') or :standard = '') "
            + " and (n.manufacturer like concat('%',:manufacturer,'%') or :manufacturer = '') "
            + " and (n.price >= :fromPrice or :fromPrice = -1) "
            + " and (n.price <= :toPrice or :toPrice = -1) "
            + " and (date(n.updated_date) >= :fromUpdatedDate or :fromUpdatedDate = '1970-01-01') "
            + " and (date(n.updated_date) <= :toUpdatedDate or :toUpdatedDate = '1970-01-01') "
            + " and (n.facility_id =:facilityId or (:facilityId = '-1' and n.facility_id is null))  "

            , countQuery = "select count(n.id) from drug n "
            + " where true "
            + " and (n.name like concat('%',:name,'%') or :name = '') "
            + " and (n.active_substances like concat('%',:activeSubstances,'%') or :activeSubstances = '') "
            + " and (n.category & :category =:category or :category= -1) "
            + " and (n.method_use=:methodUse or :methodUse= -1) "
            + " and (n.standard like concat('%',:standard,'%') or :standard = '') "
            + " and (n.manufacturer like concat('%',:manufacturer,'%') or :manufacturer = '') "
            + " and (n.price >= :fromPrice or :fromPrice = -1) "
            + " and (n.price <= :toPrice or :toPrice = -1) "
            + " and (date(n.updated_date) >= :fromUpdatedDate or :fromUpdatedDate = '1970-01-01') "
            + " and (date(n.updated_date) <= :toUpdatedDate or :toUpdatedDate = '1970-01-01') "
            + " and (n.facility_id =:facilityId or (:facilityId = '-1' and n.facility_id is null))  "
            ,
            nativeQuery = true)
    Page<DrugEntity> search(@Param("name") String name,
                            @Param("activeSubstances") String activeSubstances,
                            @Param("category") Long category,
                            @Param("methodUse") Long methodUse,
                            @Param("standard") String standard,
                            @Param("manufacturer") String manufacturer,
                            @Param("fromPrice") Long fromPrice,
                            @Param("toPrice") Long toPrice,
                            @Param("fromUpdatedDate") LocalDate fromUpdatedDate,
                            @Param("toUpdatedDate") LocalDate toUpdatedDate,
                            @Param("facilityId") Integer facilityId,
                            Pageable pageable);

    @Query(value = " select * from drug n" +
            " where n.origin_id = :originId" +
            " and n.facility_id = :facilityId" +
            " limit 1", nativeQuery = true)
    DrugEntity findByFacilityDrug(
            @Param("facilityId") Long facilityId,
            @Param("originId") Long originId
    );

    @Query(value = " select * from drug n" +
            " where n.name = :drugName" +
            " limit 1", nativeQuery = true)
    DrugEntity checkName(
            @Param("drugName") String drugName
    );

    List<DrugEntity> findTop10ByNameContains(String name);

}
