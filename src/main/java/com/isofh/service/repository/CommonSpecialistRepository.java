package com.isofh.service.repository;

import com.isofh.service.model.CommonSpecialistEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;

public interface CommonSpecialistRepository extends CrudRepository<CommonSpecialistEntity, Long> {

    @Query(value = "select n.* from common_specialist n "
            + " where true "
            + " and (n.name like concat('%',:name,'%') or :name = '') "
            + " and (date(n.updated_date) >= :fromUpdatedDate or :fromUpdatedDate = '1970-01-01') "
            + " and (date(n.updated_date) <= :toUpdatedDate or :toUpdatedDate = '1970-01-01') "

            , countQuery = "select count(n.id) from common_specialist n "
            + " where true "
            + " and (n.name like concat('%',:name,'%') or :name = '') "
            + " and (date(n.updated_date) >= :fromUpdatedDate or :fromUpdatedDate = '1970-01-01') "
            + " and (date(n.updated_date) <= :toUpdatedDate or :toUpdatedDate = '1970-01-01') "

            , nativeQuery = true)
    Page<CommonSpecialistEntity> search(
            @Param("name") String name,
            @Param("fromUpdatedDate") LocalDate fromUpdatedDate,
            @Param("toUpdatedDate") LocalDate toUpdatedDate,
            Pageable pageable);

    @Query(value = "select n.* from common_specialist n "
            + " where true "
            + " and (n.name like concat('%',:name,'%') or :name = '') limit 1 "

            , countQuery = "select count(n.id) from common_specialist n "
            + " where true "
            + " and (n.name like concat('%',:name,'%') or :name = '') limit 1 "

            , nativeQuery = true)
    CommonSpecialistEntity findFirstByName(@Param("name") String name);
}
