package com.isofh.service.repository;

import com.isofh.service.model.MenuEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface MenuRepository extends CrudRepository<MenuEntity, Long> {

    @Query(value = "select n.* from menu n "
            + " where true "
            + " and (n.name like concat('%',:name,'%') or :name = '') "
            + " and (n.level & :level =:level or :level= -1) "
            + " and (n.role & :role =:role or :role= -1) "
            + " and (n.is_active =:isActive or :isActive= -1) "

            , countQuery = "select count(n.id) from menu n "
            + " where true "
            + " and (n.name like concat('%',:name,'%') or :name = '') "
            + " and (n.level & :level =:level or :level= -1) "
            + " and (n.role & :role =:role or :role= -1) "
            + " and (n.is_active =:isActive or :isActive= -1) "

            , nativeQuery = true)
    Page<MenuEntity> search(@Param("name") String name,
                            @Param("level") Long level,
                            @Param("role") Long role,
                            @Param("isActive") Integer isActive,
                            Pageable paging);
}
