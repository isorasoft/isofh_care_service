package com.isofh.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.isofh.service.enums.HttpMethodType;
import com.isofh.service.model.FacebookResultEntity;
import com.isofh.service.model.ResultEntity;
import com.isofh.service.utils.GsonUtils;
import com.isofh.service.utils.NetworkUtils;
import com.isofh.service.utils.StrUtils;

@Service
public class ServiceConnector {

    @Value("${fb.url}")
    private String urlFB;

    @Value("${fb.version}")
    private String versionFB;

    @Value("${fb.access.token}")
    private String accessToken;

    @Value("${sms.url}")
    private String urlSMS;
    
    @Value("${sms.path}")
    private String pathSMS;

    /**
     * access token facebook
     *
     * @return
     * @throws Exception
     */
    public FacebookResultEntity accessTokenFB(String tonken) throws Exception {
        String url = urlFB + versionFB;

        if (StrUtils.isNullOrWhiteSpace(tonken)) {
            throw new Exception("Khong co token");
        } else {
            url = url + String.format(accessToken, tonken);
        }
        String result = NetworkUtils.callService(url, HttpMethodType.GET, null, null, null, null);

        return GsonUtils.toObject(result, FacebookResultEntity.class);
    }
    
    public ResultEntity sendSMS(String phone, String content) throws Exception {
        String url = urlSMS + pathSMS;
        Map<String, String> body = new HashMap<String, String>();
        body.put("phone", phone);
        body.put("content", content);
        String result = NetworkUtils.callService(url, HttpMethodType.POST, null, body, null, null);

        return GsonUtils.toObject(result, ResultEntity.class);
    }

}
