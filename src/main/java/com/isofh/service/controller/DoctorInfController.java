package com.isofh.service.controller;

import com.isofh.service.constant.AppConst;
import com.isofh.service.constant.DefaultConst;
import com.isofh.service.constant.field.CommonField;
import com.isofh.service.model.ResultEntity;
import com.isofh.service.service.DoctorInfService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(path = "/doctorInf")
public class DoctorInfController extends BaseController {

    @Autowired
    DoctorInfService doctorInfService;

    @ApiOperation(value = "Create a new doctor info", notes = "{\"doctorInf\":{\"image\":\"gggggg\",\"name\":\"nguyen thi huong\",\"email\":\"kkelanggthasndg12345@gmail.com\",\"phone\":\"134545\",\"certificateCode\":\"BS1s23\"}," +
            "\"specialistId\":1}")
    @PostMapping(path = "/create")
    public ResponseEntity<?> create(@RequestBody Object object) {
        try {
            return response(doctorInfService.create(object));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }


    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token",
                    required = true, dataType = "string", paramType = "header")})
    @ApiOperation(value = "active user with doctor info", notes = "{}")
    @PutMapping(path = "/active/{id}")
    public ResponseEntity<?> active(@PathVariable("id") Long id) {
        try {
            return response(doctorInfService.active(id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    
    @ApiOperation(value = "reject doctor info", notes = "{\"reject\":\"aaa\"}")
    @PutMapping(path = "/reject/{id}")
    public ResponseEntity<?> block(@PathVariable("id") Long id, @RequestBody Object object) {
        try {
            return response(doctorInfService.reject(id, object));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }


    @ApiOperation(value = "Search post", notes = "")
    @GetMapping(path = "/search")
    public ResponseEntity<?> search(
            @RequestParam(value = CommonField.PAGE, required = false, defaultValue = DefaultConst.PAGE) Integer page,
            @RequestParam(value = CommonField.SIZE, required = false, defaultValue = DefaultConst.SIZE) Integer size,
            @RequestParam(value = "queryString", required = false, defaultValue = DefaultConst.STRING) String queryString,
            @RequestParam(value = "specialistId", required = false, defaultValue = DefaultConst.NUMBER) Long specialistId,
            @RequestParam(value = "active", required = false, defaultValue = DefaultConst.NUMBER) Integer active,
            @RequestParam(value = "type", required = false, defaultValue = DefaultConst.NUMBER) Integer type
    ) {
        try {
            ResultEntity result = doctorInfService.search(page, size, queryString, specialistId, active, type);
            return response(result);
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

}
