package com.isofh.service.controller;

import com.isofh.service.service.MethodUseService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/method-use")
public class MethodUseController extends BaseController {

    @Autowired
    MethodUseService methodUseService;

    @ApiOperation(value = "sync data", notes = "{\"methods\":[]}")
    @PostMapping(path = "/sync-data")
    public ResponseEntity<?> create(@RequestBody Object object) {
        try {
            return response(methodUseService.create(object));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "get all", notes = "")
    @GetMapping(path = "/get-all")
    public ResponseEntity<?> getAll() {
        try {
            return response(methodUseService.getAll());
        } catch (Exception ex) {
            return response(error(ex));
        }
    }


}
