package com.isofh.service.controller;

import com.isofh.service.constant.DefaultConst;
import com.isofh.service.service.SpecialistService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/specialist-booking")
public class SpecialistController extends BaseController {

    @Autowired
    SpecialistService service;

    public SpecialistController() {
        super();
    }

    @ApiOperation(value = "Sync his data", notes = "{}")
    @GetMapping(path = "/sync/{hospitalId}")
    public ResponseEntity<?> sync(@PathVariable("hospitalId") long hospitalId) {
        try {
            return response(service.sync(hospitalId));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "get all", notes = "{}")
    @GetMapping(path = "/get-all/{hospitalId}")
    public ResponseEntity<?> getAll(@PathVariable("hospitalId") long hospitalId) {
        try {
            return response(service.getAll(hospitalId));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "Lay tat ca cac chuyen khoa theo khoa va bac si", notes = "{}")
    @GetMapping(path = "/get-by-department-doctor")
    public ResponseEntity<?> getByDepartmentSpecialist(
            @RequestParam(value = "departmentId") Long departmentId,
            @RequestParam(value = "doctorId", required = false, defaultValue = DefaultConst.NUMBER) Long doctorId
    ) {
        try {
            return response(service.getByDepartmentSpecialist(departmentId, doctorId));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }
}
