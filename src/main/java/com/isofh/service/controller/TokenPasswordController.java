package com.isofh.service.controller;

import com.isofh.service.service.TokenPasswordService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/token-password")
public class TokenPasswordController extends BaseController {

    @Autowired
    TokenPasswordService tokenPasswordService;

    /**
     * Lay thong tin user tu token <br>
     * code = 2 token da het han
     *
     * @param token
     * @return
     */
    @ApiOperation(value = "Get Detail token", notes = "{}")
    @GetMapping(path = "/get-detail/{token}")
    public ResponseEntity<?> getDetail(@PathVariable("token") String token) {
        try {
            return response(tokenPasswordService.getDetail(token));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

}
