package com.isofh.service.controller;


import com.isofh.service.utils.EmailUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.GetMapping;

import com.isofh.service.service.TestService;

import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.isofh.service.service.TestService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(path = "/test")
public class TestController extends BaseController {

    @Autowired
    TestService testService;

    /**
     * type: 0 web <br>
     * 1: app <br>
     *
     */
    @ApiOperation(value = "Lay token cua 1 user", notes = "")
    @GetMapping(path = "/get-token/{id}/{type}")
    public ResponseEntity<?> getToken(@PathVariable("id") Long id, @PathVariable("type") Integer type) {
        try {
            return response(testService.getDetail(id, type));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "test", notes = "{}")
    @GetMapping(path = "/test")
    public ResponseEntity<?> testMQ() {
        try {
            for (int i = 0; i < 1; i++) {
                Thread.sleep(10);
                EmailUtils.sendEmail("huong.nt@isofh.com", "subject " + i , "content " + i);
            }
            return response(testService.testMQ());
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

}

