package com.isofh.service.controller;

import com.isofh.service.model.ProfileEntity;
import com.isofh.service.service.ProfileService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping(path = "/profile")
public class ProfileController extends BaseController {


    @Autowired
    ProfileService service;

    @ApiOperation(value = "Create profile", notes = "{\"userId\":1,\"countryId\":1,\"provinceId\":1,\"districtId\":1,\"zoneId\":1,\"profile\":{\"name\":\"Le Dang Tuan\",\"gender\":0,\"dob\":\"1989-04-26\",\"phone\":\"0968245523\"," +
            "\"guardianName\":\"Tuan\",\"guardianPhone\":\"09829358146\",\"guardianPassport\":\"21313\"}}")
    @PostMapping(path = "/create")
    public ResponseEntity<?> create(@RequestBody Object object) {
        try {
            return response(service.create(object));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    /**
     * Admin update patient profile
     *
     * @param userId
     * @return
     */
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token",
                    required = true, dataType = "string", paramType = "header")})
    @ApiOperation(value = "Get profiles of user", notes = "")
    @GetMapping(path = "/get-by-user/{userId}")
    public ResponseEntity<?> getByUser(@PathVariable("userId") long userId) {
        try {
            return response(service.getByUser(userId));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    /**
     * Lay thong tin chi tiet cua profile
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "Get profiles of user", notes = "")
    @GetMapping(path = "/get-detail/{id}")
    public ResponseEntity<?> getDetail(@PathVariable("id") long id) {
        try {
            return response(service.getDetail(id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }
}
