package com.isofh.service.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.isofh.service.service.DistrictService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(path = "/district")
public class DistrictController extends BaseController {

    @Autowired
    DistrictService service;

    @ApiOperation(value = "Get all", notes = "")
    @GetMapping(path = "/get-all")
    public ResponseEntity<?> getAll() {
        try {
            return response(service.getAll());
        } catch (Exception ex) {
            return response(error(ex));
        }
    }
}
