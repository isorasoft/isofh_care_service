package com.isofh.service.controller;

import com.isofh.service.constant.DefaultConst;
import com.isofh.service.constant.field.CommonField;
import com.isofh.service.service.SlidePlaceService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/slide-place")
public class SlidePlaceController extends BaseController {

    @Autowired
    SlidePlaceService slidePlaceService;

    @ApiOperation(value = "Create a new slide place", notes = "{\"slidePlace\":{\"name\":\"top\"},\"slideId\":1}")
    @PostMapping(path = "/create")
    public ResponseEntity<?> create(@RequestBody Object object) {
        try {
            return response(slidePlaceService.create(object));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "Update slide place", notes = "{\"slidePlace\":{\"name\":\"top\"},\"slideId\":1}")
    @PutMapping(path = "/update/{id}")
    public ResponseEntity<?> update(@RequestBody Object object, @PathVariable("id") long id) {
        try {
            return response(slidePlaceService.update(object, id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "delete slide place", notes = "{}")
    @DeleteMapping(path = "/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") long id) {
        try {
            return response(slidePlaceService.delete(id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "Search slide place", notes = "{}")
    @GetMapping(path = "/search")
    public ResponseEntity<?> search(
            @RequestParam(value = CommonField.PAGE, required = false, defaultValue = DefaultConst.PAGE) Integer page,
            @RequestParam(value = CommonField.SIZE, required = false, defaultValue = DefaultConst.SIZE) Integer size,
            @RequestParam(value = "name", required = false, defaultValue = DefaultConst.STRING) String name) {
        try {
            return response(slidePlaceService.search(page, size, name));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

}
