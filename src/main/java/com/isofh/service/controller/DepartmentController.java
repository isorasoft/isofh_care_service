package com.isofh.service.controller;

import com.isofh.service.service.DepartmentService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/department-booking")
public class DepartmentController extends BaseController {

    @Autowired
    DepartmentService service;

    public DepartmentController() {
        super();
    }

    @ApiOperation(value = "Sync his department", notes = "{}")
    @GetMapping(path = "/sync/{hospitalId}")
    public ResponseEntity<?> sync(@PathVariable("hospitalId") long hospitalId) {
        try {
            return response(service.sync(hospitalId));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "get all department", notes = "{}")
    @GetMapping(path = "/get-all/{hospitalId}")
    public ResponseEntity<?> getAll(@PathVariable("hospitalId") long hospitalId) {
        try {
            return response(service.getAll(hospitalId));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "get detail department", notes = "{}")
    @GetMapping(path = "/get-detail/{id}")
    public ResponseEntity<?> getDetail(@PathVariable("id") long id) {
        try {
            return response(service.getDetail(id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

}
