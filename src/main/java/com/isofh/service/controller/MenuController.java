package com.isofh.service.controller;

import com.isofh.service.constant.DefaultConst;
import com.isofh.service.constant.field.CommonField;
import com.isofh.service.service.MenuService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/menu")
public class MenuController extends BaseController {


    @Autowired
    MenuService menuService;

    @ApiOperation(value = "Create a menu", notes = "{\"menu\":{}, \"parentId\": 1}")
    @PostMapping(path = "/create")
    public ResponseEntity<?> create(@RequestBody Object object) {
        try {
            return response(menuService.create(object));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "Update menu", notes = "{\"menu\":{}, \"parentId\": 1}")
    @PutMapping(path = "/update/{id}")
    public ResponseEntity<?> update(@RequestBody Object object, @PathVariable("id") Long id) {
        try {
            return response(menuService.update(object, id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "Delete menu", notes = "")
    @DeleteMapping(path = "/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") Long id) {
        try {
            return response(menuService.delete(id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "search menu", notes = "")
    @GetMapping(path = "/search")
    public ResponseEntity<?> search(
            @RequestParam(value = CommonField.PAGE, required = false, defaultValue = DefaultConst.PAGE) Integer page,
            @RequestParam(value = CommonField.SIZE, required = false, defaultValue = DefaultConst.SIZE) Integer size,
            @RequestParam(value = "name", required = false, defaultValue = DefaultConst.STRING) String name,
            @RequestParam(value = "level", required = false, defaultValue = DefaultConst.NUMBER) Long level,
            @RequestParam(value = "role", required = false, defaultValue = DefaultConst.NUMBER) Long role,
            @RequestParam(value = "isActive", required = false, defaultValue = DefaultConst.NUMBER) Integer isActive
    ) {
        try {
            return response(menuService.search(page, size, name, level, role, isActive));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "lay chi tiet cua menu", notes = "")
    @GetMapping(path = "/get-detail/{id}")
    public ResponseEntity<?> getDetail(@PathVariable("id") long id
    ) {
        try {
            return response(menuService.getDetail(id));

        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "Update menu", notes = "{\"menu\":{}, \"isActive\": 1}")
    @PutMapping(path = "/set-active/{id}")
    public ResponseEntity<?> setActive(@RequestBody Object object, @PathVariable("id") Long id) {
        try {
            return response(menuService.setActive(object, id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

}
