package com.isofh.service.controller;

import com.isofh.service.constant.AppConst;
import com.isofh.service.constant.DefaultConst;
import com.isofh.service.constant.field.CommonField;
import com.isofh.service.service.FacilityService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@RestController
@RequestMapping(path = "/facility")
public class FacilityController extends BaseController {


    @Autowired
    FacilityService facilityService;

    @ApiOperation(value = "", notes = "{\"file\":\"data.xls\"}")
    @PostMapping(path = "/import")
    public ResponseEntity<?> importFile(@RequestBody Object object) {
        try {
            return response(facilityService.importFile(object));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "Create a new facility", notes = "{\"facility\":{}, \"imageUrls\":[\"url\"], \"specialistIds\":[5], \"provinceId\":5, \"userId\":5}")
    @PostMapping(path = "/create")
    public ResponseEntity<?> create(@RequestBody Object object) {
        try {
            return response(facilityService.create(object));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "Update a facility", notes = "{\"facility\":{}, \"imageUrls\":[\"url1\"], \"specialistIds\":[5], \"provinceId\":5}")
    @PutMapping(path = "/update/{id}")
    public ResponseEntity<?> update(@RequestBody Object object, @PathVariable("id") long id) {
        try {
            return response(facilityService.update(object, id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "Delete a facility", notes = "")
    @DeleteMapping(path = "/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") Long id) {
        try {
            return response(facilityService.delete(id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }


    @ApiOperation(value = "Search disease", notes = "")
    @GetMapping(path = "/search")
    public ResponseEntity<?> search(
            @RequestParam(value = CommonField.PAGE, required = false, defaultValue = DefaultConst.PAGE) Integer page,
            @RequestParam(value = CommonField.SIZE, required = false, defaultValue = DefaultConst.SIZE) Integer size,
            @RequestParam(value = "name", required = false, defaultValue = DefaultConst.STRING) String name,
            @RequestParam(value = "code", required = false, defaultValue = DefaultConst.STRING) String code,
            @RequestParam(value = "levelId", required = false, defaultValue = DefaultConst.NUMBER) Long levelId,
            @RequestParam(value = "approval", required = false, defaultValue = DefaultConst.NUMBER) Long approval,
            @RequestParam(value = "type", required = false, defaultValue = DefaultConst.NUMBER) Integer type,
            @RequestParam(value = "rankId", required = false, defaultValue = DefaultConst.NUMBER) Long rankId,
            @RequestParam(value = "provinceId", required = false, defaultValue = DefaultConst.NUMBER) Long provinceId,
            @RequestParam(value = "website", required = false, defaultValue = DefaultConst.STRING) String website,
            @RequestParam(value = "latitude", required = false, defaultValue = DefaultConst.NUMBER) Float latitude,
            @RequestParam(value = "longitude", required = false, defaultValue = DefaultConst.NUMBER) Float longitude,
            @RequestParam(value = "sortType", required = false, defaultValue = DefaultConst.NUMBER) Integer sortType,
            @RequestParam(value = "licenseNumber", required = false, defaultValue = DefaultConst.STRING) String licenseNumber,
            @RequestParam(value = "gpp", required = false, defaultValue = DefaultConst.STRING) String gpp,
            @RequestParam(value = "address", required = false, defaultValue = DefaultConst.STRING) String address,
            @RequestParam(value = "phone", required = false, defaultValue = DefaultConst.STRING) String phone,
            @RequestParam(value = "fromUpdatedDate", required = false, defaultValue = DefaultConst.DATE) @DateTimeFormat(pattern = AppConst.DATE_FORMAT) LocalDate fromUpdatedDate,
            @RequestParam(value = "toUpdatedDate", required = false, defaultValue = DefaultConst.DATE) @DateTimeFormat(pattern = AppConst.DATE_FORMAT) LocalDate toUpdatedDate,
            @RequestParam(value = "userId", required = false, defaultValue = DefaultConst.NUMBER) Long userId,
            @RequestParam(value = "review", required = false, defaultValue = DefaultConst.NUMBER) Integer review,
            @RequestParam(value = "emergencyContact", required = false, defaultValue = DefaultConst.STRING) String emergencyContact,
            @RequestParam(value = "username", required = false, defaultValue = DefaultConst.STRING) String username
    ) {
        try {
            return response(facilityService.search(page, size, name, code, levelId, approval, type, rankId, provinceId, website, latitude, longitude, sortType, licenseNumber, gpp, address, phone, fromUpdatedDate, toUpdatedDate, userId,
                    review, emergencyContact, username));

        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "Update approval", notes = "\"approval\":1}")
    @PutMapping(path = "/update-approval/{id}")
    public ResponseEntity<?> updateApproval(@RequestBody Object object, @PathVariable("id") long id) {
        try {
            return response(facilityService.updateApproval(object, id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "Import du lieu Facility", notes = "{\"facilities\":[{\"Code\":\"10064\",\"name\":\"Trạm y tế Sín Chéng\",\"website\":\"\",\"Levelid\":\"Tuyến 4\",\"rankId\":\"\",\"Type\":\"Trung tâm y tế\",\"Review\":\"\"," +
            "\"belongIsofh\":\"\",\"address\":\"Xã Sín Chéng, Huyện Si Ma Cai\",\"Province\":\"Lào Cai\",\"phone\":\"\"}]}")
    @PostMapping(path = "/import-csv")
    public ResponseEntity<?> importCsv(@RequestBody Object object) {
        try {
            return response(facilityService.importCsv(object));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }


    /**
     * Tim kiem thong tin cua co so y te, sap xep theo review giam dan
     *
     * @return
     */
    @ApiOperation(value = "Search disease", notes = "")
    @GetMapping(path = "/search-by-query")
    public ResponseEntity<?> searchByQuery(
            @RequestParam(value = CommonField.PAGE, required = false, defaultValue = DefaultConst.PAGE) Integer page,
            @RequestParam(value = CommonField.SIZE, required = false, defaultValue = DefaultConst.SIZE) Integer size,
            @RequestParam(value = "query", required = false, defaultValue = DefaultConst.STRING) String query,
            @RequestParam(value = "specialistId", required = false, defaultValue = DefaultConst.NUMBER) Long specialistId,
            @RequestParam(value = "approval", required = false, defaultValue = DefaultConst.NUMBER) Integer approval
    ) {
        try {
            return response(facilityService.searchByQuery(page, size, query, specialistId, approval));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    /**
     * Tim kiem thong tin cua co so y te
     *
     * @return
     */
    @ApiOperation(value = "lay thong tin chi tiet facility", notes = "")
    @GetMapping(path = "/get-detail/{id}")
    public ResponseEntity<?> getDetail(@PathVariable("id") long id) {
        try {
            return response(facilityService.getDetail(id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "Review Facility", notes = "{\"review\":4}")
    @PutMapping(path = "/review/{id}")
    public ResponseEntity<?> review(@RequestBody Object object, @PathVariable("id") long id) {
        try {
            return response(facilityService.review(object, id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "Search facility from drug", notes = "{\"drugId\":1}")
    @GetMapping(path = "/search-from-drug")
    public ResponseEntity<?> searchFacilityFromDrug(
            @RequestParam(value = CommonField.PAGE, required = false, defaultValue = DefaultConst.PAGE) Integer page,
            @RequestParam(value = CommonField.SIZE, required = false, defaultValue = DefaultConst.SIZE) Integer size,
            @RequestParam(value = "drugId", required = false, defaultValue = DefaultConst.NUMBER) Long drugId
    ) {
        try {
            return response(facilityService.searchFacilityFromDrug(page, size, drugId));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

}
