package com.isofh.service.controller;

import com.isofh.service.model.ResultEntity;
import com.isofh.service.result.IErrorResult;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;

@Component
@CrossOrigin(origins = "*")
public class BaseController implements IErrorResult {

    public BaseController() {

    }

    /**
     * Log service running time and response json object
     *
     * @param entity Object data muon chuyen sang json
     * @return {"code":0,"message":"no message","data":{}}
     */
    protected ResponseEntity response(ResultEntity entity) {
        return new ResponseEntity(entity, HttpStatus.OK);
    }

}
