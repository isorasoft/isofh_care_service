package com.isofh.service.controller;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.isofh.service.constant.AppConst;
import com.isofh.service.constant.DefaultConst;
import com.isofh.service.constant.field.CommonField;
import com.isofh.service.service.CommonSpecialistService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(path = "/specialist")
public class CommonSpecialistController extends BaseController {

    @Autowired
    CommonSpecialistService commonSpecialistService;

    /**
     * Code =2 ten chuyen khoa da ton tai
     *
     * @param object
     * @return
     */
    @ApiOperation(value = "Create a new specialist", notes = "{\"specialist\":{}}")
    @PostMapping(path = "/create")
    public ResponseEntity<?> create(@RequestBody Object object) {
        try {
            return response(commonSpecialistService.create(object));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    /**
     * Code =2 ten chuyen khoa da ton tai
     *
     * @param object
     * @param id
     * @return
     */
    @ApiOperation(value = "update a new specialist", notes = "{\"specialist\":{}}")
    @PutMapping(path = "/update/{id}")
    public ResponseEntity<?> update(@RequestBody Object object, @PathVariable("id") Long id) {
        try {
            return response(commonSpecialistService.update(object, id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "delete a new specialist", notes = "{}")
    @DeleteMapping(path = "/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") Long id) {
        try {
            return response(commonSpecialistService.delete(id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    /**
     * Tim kiem chuyen khoa
     *
     * @param name
     * @param page
     * @param size
     * @param sortType 1 sap xep theo so luong view giam dan, mac dinh sap xep theo thoi gian update giam dan
     * @return
     */
    @ApiOperation(value = "delete a new specialist", notes = "{}")
    @GetMapping(path = "/search")
    public ResponseEntity<?> search(
            @RequestParam(value = "name", required = false, defaultValue = DefaultConst.STRING) String name,
            @RequestParam(value = CommonField.PAGE, required = false, defaultValue = DefaultConst.PAGE) Integer page,
            @RequestParam(value = CommonField.SIZE, required = false, defaultValue = DefaultConst.SIZE) Integer size,
            @RequestParam(value = "sortType", required = false, defaultValue = DefaultConst.NUMBER) Integer sortType,
            @RequestParam(value = "fromUpdatedDate", required = false, defaultValue = DefaultConst.DATE) @DateTimeFormat(pattern = AppConst.DATE_FORMAT) LocalDate fromUpdatedDate,
            @RequestParam(value = "toUpdatedDate", required = false, defaultValue = DefaultConst.DATE) @DateTimeFormat(pattern = AppConst.DATE_FORMAT) LocalDate toUpdatedDate

    ) {
        try {
            return response(commonSpecialistService.search(name, page, size, sortType, fromUpdatedDate, toUpdatedDate));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "update view count", notes = "{}")
    @DeleteMapping(path = "/update-view-count/{id}")
    public ResponseEntity<?> updateViewCount(@PathVariable("id") Long id) {
        try {
            return response(commonSpecialistService.updateViewCount(id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }
    
    @PostMapping(path = "/import-dictionary")
    public ResponseEntity<?> importSp(@RequestBody Object object) {
        try {
            return response(commonSpecialistService.importSp(object));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

}
