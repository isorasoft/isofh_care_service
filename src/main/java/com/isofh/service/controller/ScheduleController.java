package com.isofh.service.controller;

import com.isofh.service.constant.AppConst;
import com.isofh.service.constant.DefaultConst;
import com.isofh.service.service.ScheduleService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@RestController
@RequestMapping(path = "/schedule-booking")
public class ScheduleController extends BaseController {

    public ScheduleController() {
        super();
    }

    @Autowired
    ScheduleService service;


    @ApiOperation(value = "Create a new schedule", notes = "{\"departmentId\":1,\"roomId\":1,\"serviceId\":1,\"doctorId\":1,\"specialistId\":1,\"startDate\":\"2018-08-01\",\"endDate\":\"2018-08-30\",\"schedule\":{\"dayOfWeek\":1," +
            "\"numberCase\":3,\"timeOfDay\":1,\"startWorking\":480,\"endWorking\":600,\"numAllowedDate\":10}}")
    @PostMapping(path = "/create/{hospitalId}")
    public ResponseEntity<?> create(@RequestBody Object object, @PathVariable("hospitalId") long hospitalId) {
        try {
            return response(service.create(object, hospitalId));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    /**
     * get lich lam viec theo khoa, gop by chuyen khoa
     *
     * @param departmentId
     * @param startDate
     * @param endDate
     * @return
     */
    @ApiOperation(value = "get lich lam viec theo khoa, gop by chuyen khoa", notes = "{}")
    @GetMapping(path = "/get-by-department/{departmentId}")
    public ResponseEntity<?> getByDepartment(
            @PathVariable("departmentId") Long departmentId,
            @RequestParam(value = "startDate", required = false, defaultValue = DefaultConst.DATE) @DateTimeFormat(pattern = AppConst.DATE_FORMAT) LocalDate startDate,
            @RequestParam(value = "endDate", required = false, defaultValue = DefaultConst.DATE) @DateTimeFormat(pattern = AppConst.DATE_FORMAT) LocalDate endDate
    ) {
        try {
            return response(service.getByDepartment(departmentId, startDate, endDate));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }


    /**
     * Xoa chi schedule nay (chain = 0) hoac xoa tu schedule nay ve tuong lai<br>
     * code = 3 da ton tai lich kham cua schedule
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "Delete schedule", notes = "{\"chain\":1}")
    @DeleteMapping(path = "/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") long id) {
        try {
            return response(service.delete(id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    /**
     * Update thong tin schedule
     * code = 3 da ton tai lich kham cua schedule
     *
     * @param object
     * @param id
     * @return
     */
    @ApiOperation(value = "Update schedule", notes = "{\"chain\":1,\"status\":1,\"departmentId\":1,\"roomId\":1,\"serviceId\":1,\"doctorId\":1,\"specialistId\":1,\"schedule\":{\"numberCase\":3,\"timeOfDay\":1,\"dayOfWeek\":1," +
            "\"startWorking\":480," +
            "\"endWorking\":600,\"numAllowedDate\":10}}")
    @PutMapping(path = "/update/{id}")
    public ResponseEntity<?> update(@RequestBody Object object, @PathVariable("id") long id) {
        try {
            return response(service.update(object, id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }


    /**
     * get lich lam viec theo bac si, gop by khoa
     *
     * @param doctorId
     * @param startDate
     * @param endDate
     * @return
     */
    @ApiOperation(value = "get lich lam viec theo bac si, gop by khoa", notes = "{}")
    @GetMapping(path = "/get-by-doctor/{doctorId}")
    public ResponseEntity<?> getByDoctor(
            @PathVariable("doctorId") Long doctorId,
            @RequestParam(value = "startDate", required = false, defaultValue = DefaultConst.DATE) @DateTimeFormat(pattern = AppConst.DATE_FORMAT) LocalDate startDate,
            @RequestParam(value = "endDate", required = false, defaultValue = DefaultConst.DATE) @DateTimeFormat(pattern = AppConst.DATE_FORMAT) LocalDate endDate
    ) {
        try {
            return response(service.getByDoctor(doctorId, startDate, endDate));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    /**
     * get lich lam viec theo chuoi (chain va startDate)
     *
     * @param scheduleChain
     * @param startDate
     * @return
     */
    @ApiOperation(value = "get lich lam viec theo chuoi (chain va startDate)", notes = "{}")
    @GetMapping(path = "/get-by-chain-date")
    public ResponseEntity<?> getByChainDate(
            @RequestParam("scheduleChain") String scheduleChain,
            @RequestParam(value = "startDate", defaultValue = DefaultConst.DATE) @DateTimeFormat(pattern = AppConst.DATE_FORMAT) LocalDate startDate) {
        try {
            return response(service.getByChainDate(scheduleChain, startDate));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    /**
     * Lay lich lam viec theo bac si, chuyen khoa, khoa
     */
    @ApiOperation(value = "get lich lam viec theo chuoi (chain va startDate)", notes = "{}")
    @GetMapping(path = "/get-by-doctor-specialist-department")
    public ResponseEntity<?> getByDoctorSpecialistDepartment(
            @RequestParam(value = "doctorId", required = false, defaultValue = DefaultConst.NUMBER) Long doctorId,
            @RequestParam(value = "specialistId", required = false, defaultValue = DefaultConst.NUMBER) Long specialistId,
            @RequestParam(value = "departmentId", required = false, defaultValue = DefaultConst.NUMBER) Long departmentId,
            @RequestParam(value = "dayOfWeek", required = false, defaultValue = DefaultConst.NUMBER) Integer dayOfWeek,
            @RequestParam(value = "startDate", required = false, defaultValue = DefaultConst.DATE) @DateTimeFormat(pattern = AppConst.DATE_FORMAT) LocalDate startDate,
            @RequestParam(value = "endDate", required = false, defaultValue = DefaultConst.DATE) @DateTimeFormat(pattern = AppConst.DATE_FORMAT) LocalDate endDate
    ) {
        try {
            return response(service.getByDoctorSpecialistDepartment(doctorId, specialistId, departmentId, dayOfWeek, startDate, endDate));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "Kiem tra trang thai cac booking trong schedule " +
            "<br> 0 -> Khong co dat kham hom nay va tuong lai -> cho phep sua tat ca cac thong tin cua dat kham nay va cac ngay tiep theo" +
            "<br> 1 -> Khong co dat kham hom nay nhung co lich trong tuong lai -> cho phep sua tat ca cac thong tin cua dat kham trong ngay duoc chon" +
            "<br> 2 -> Co lich ngay duoc chon, nhung khong yeu cau dich danh bac si, chi cho phep sua thong tin bac si" +
            "<br> 3 -> Co lich ngay duoc chon va yeu cau dich danh bac si khong cho phep sua lich")
    @GetMapping(path = "/check-booking-status/{id}")
    public ResponseEntity<?> checkBookingStatus(@PathVariable("id") long id) {
        try {
            return response(service.checkBookingStatus(id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }
}
