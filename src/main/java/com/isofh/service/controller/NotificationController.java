package com.isofh.service.controller;

import com.isofh.service.constant.DefaultConst;
import com.isofh.service.constant.field.CommonField;
import com.isofh.service.service.NotificationService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/notification")
public class NotificationController extends BaseController {

    @Autowired
    NotificationService notificationService;

    @ApiOperation(value = "search notification", notes = "{}")
    @GetMapping(path = "/search")
    public ResponseEntity<?> search(
            @RequestParam(value = CommonField.PAGE, required = false, defaultValue = DefaultConst.PAGE) Integer page,
            @RequestParam(value = CommonField.SIZE, required = false, defaultValue = DefaultConst.SIZE) Integer size,
            @RequestParam(value = "watched", required = false, defaultValue = DefaultConst.NUMBER) Integer watched
    ) {
        try {
            return response(notificationService.search(watched, page, size));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "Lay chi tiet theo id", notes = "")
    @GetMapping(path = "/get-detail/{id}")
    public ResponseEntity<?> getDetail(@PathVariable("id") long id) {
        try {
            return response(notificationService.getDetail(id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }


    @ApiOperation(value = "active watched", notes = "{}")
    @PutMapping(path = "/active-watched/{id}")
    public ResponseEntity<?> activeWatched(@PathVariable("id") long id) {
        try {
            return response(notificationService.activeWatched(id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "Delete notification", notes = "")
    @DeleteMapping(path = "/delete/all")
    public ResponseEntity<?> deleteAll() {
        try {
            return response(notificationService.deleteAll());
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "count notification", notes = "")
    @GetMapping(path = "/count-notification")
    public ResponseEntity<?> countNotification() {
        try {
            return response(notificationService.countNotification());
        } catch (Exception ex) {
            return response(error(ex));
        }
    }
}
