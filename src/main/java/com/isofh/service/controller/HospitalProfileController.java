package com.isofh.service.controller;

import com.isofh.service.service.HospitalProfileService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/hospital-profile")
public class HospitalProfileController extends BaseController {


    @Autowired
    HospitalProfileService service;

    /**
     * Lay danh sach cac benh vien ma nguoi dung nay co thong tin
     */
    @ApiOperation(value = "", notes = "")
    @PostMapping(path = "/create")
    public ResponseEntity<?> create(@RequestBody Object object) {
        try {
            return response(service.create(object));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }
    
    @ApiOperation(value = "get hospital by profile", notes = "{}")
    @GetMapping(path = "/get-hospital-by-profile")
    public ResponseEntity<?> getHospitalByProfile() {
        try {
            return response(service.getHospitalByProfile());
        } catch (Exception ex) {
            return response(error(ex));
        }
    }
}
