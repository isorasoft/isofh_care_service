package com.isofh.service.controller;

import com.isofh.service.constant.AppConst;
import com.isofh.service.constant.DefaultConst;
import com.isofh.service.constant.field.CommonField;
import com.isofh.service.model.ResultEntity;
import com.isofh.service.service.PostService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@RestController
@RequestMapping(path = "/post")
public class PostController extends BaseController {

    @Autowired
    PostService postService;

    /**
     * Dat cau hoi or tao bai viet
     *
     * @param object {"departmentId": "1", "tags": ["1","2"], "post": {"title": "dfasdf dfasd", "content": "fasdf sdfasd", "images": "image1,images", "privated": 1}}
     * @return
     */
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token",
                    required = true, dataType = "string", paramType = "header")})
    @ApiOperation(value = "Create a post", notes = "{\"departmentId\": \"1\", \"tags\": [\"1\",\"2\"], \"post\": {\"title\": \"dfasdf dfasd\", \"content\": \"fasdf sdfasd\", \"images\": \"image1,images\", \"isPrivate\": 1, " +
            "\"otherContent\": \"image1,images\"}}")
    @PostMapping(path = "/create")
    public ResponseEntity<?> create(@RequestBody Object object) {
        try {
            return response(postService.create(object));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    /**
     * tim kiem cau hoi or bai viet<br>
     * type:
     * 1: des name bac si
     * 2: des ngay nguoi tao
     * 3: asc ngay bac si
     * 4: asc nguoi tao
     * 5: asc ngay tao
     * 6: des ngay tao
     *
     * @return
     */
    @ApiOperation(value = "Search post", notes = "")
    @GetMapping(path = "/search")
    public ResponseEntity<?> search(
            @RequestParam(value = CommonField.PAGE, required = false, defaultValue = DefaultConst.PAGE) Integer page,
            @RequestParam(value = CommonField.SIZE, required = false, defaultValue = DefaultConst.SIZE) Integer size,
            @RequestParam(value = "isAnswered", required = false, defaultValue = DefaultConst.NUMBER) Integer isAnswered,
            @RequestParam(value = "isAssigned", required = false, defaultValue = DefaultConst.NUMBER) Integer isAssigned,
            @RequestParam(value = "isPublished", required = false, defaultValue = DefaultConst.NUMBER) Integer isPublished,
            @RequestParam(value = "authorId", required = false, defaultValue = DefaultConst.NUMBER) Long authorId,
            @RequestParam(value = "specialistId", required = false, defaultValue = DefaultConst.NUMBER) Long specialistId,
            @RequestParam(value = "departmentId", required = false, defaultValue = DefaultConst.NUMBER) Long departmentId,
            @RequestParam(value = "assigneeId", required = false, defaultValue = DefaultConst.NUMBER) Long assigneeId,
            @RequestParam(value = "status", required = false, defaultValue = DefaultConst.NUMBER) Integer status,
            @RequestParam(value = "queryString", required = false, defaultValue = DefaultConst.STRING) String queryString,
            @RequestParam(value = "type", required = false, defaultValue = DefaultConst.NUMBER) Integer type,
            @RequestParam(value = "startTime", required = false, defaultValue = DefaultConst.DATE_TIME) @DateTimeFormat(pattern = AppConst.DATE_TIME_FORMAT) LocalDateTime startTime
    ) {
        try {
            ResultEntity result = postService.search(page, size, isAnswered, isAssigned, isPublished, authorId, specialistId, departmentId, assigneeId, status, queryString, startTime, type);
            return response(result);
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    /**
     * cap nhat bai viet or cau hoi
     *
     * @param object {"departmentId": "1", "tags": ["1","2"], "post": {"title": "dfasdf dfasd", "content": "fasdf sdfasd", "images": "image1,images", "privated": 1}}
     * @return
     */
    @ApiOperation(value = "Update a post", notes = "{\"departmentId\": \"1\", \"tags\": [\"1\",\"2\"], \"post\": {\"title\": \"dfasdf dfasd\", \"content\": \"fasdf sdfasd\", \"images\": \"image1,images\", \"privated\": 1}}")
    @PutMapping(path = "/update/{id}")
    public ResponseEntity<?> update(@RequestBody Object object, @PathVariable("id") long id) {
        try {
            return response(postService.update(object, id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    /**
     * Xoa post
     *
     * @return
     */
    @ApiOperation(value = "Delete post", notes = "")
    @DeleteMapping(path = "/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") long id) {
        try {
            return response(postService.delete(id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    /**
     * Duyet or huy duyet bai viet cua bac si
     * 1:admin da duyet
     * 2: admin tu choi
     *
     * @param object {"isPublished":1}
     * @return
     */
    @ApiOperation(value = "Change status is published", notes = "{\"isPublished\":1, \"reject\":\"cau hoi nay chua thong tin nguy hiem\"}")
    @PutMapping(path = "/approved-post/{id}")
    public ResponseEntity<?> approvedPost(@RequestBody Object object, @PathVariable("id") long id) {
        try {
            return response(postService.approvedPost(object, id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }


    /**
     * Asignee post cho bac si tra loi
     *
     * @param object {"doctorId":"8","departmentId":"1"}
     * @return
     */

    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token",
                    required = true, dataType = "string", paramType = "header")})
    @ApiOperation(value = "Assign post to a doctor", notes = "{\"assigneeId\":\"8\",\"departmentId\":\"1\"}")
    @PutMapping(path = "/assign/{id}")
    public ResponseEntity<?> assignPost(@RequestBody Object object, @PathVariable("id") long id) {
        try {
            return response(postService.assignPost(object, id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token",
                    required = true, dataType = "string", paramType = "header")})
    @ApiOperation(value = "Assign post to a doctor", notes = "{\"assigneeId\":\"3836\",\"postIds\":[6243, 6246]}")
    @PutMapping(path = "/assign-list-post")
    public ResponseEntity<?> assignListPost(@RequestBody Object object) {
        try {
            return response(postService.assignListPost(object));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    /**
     * like bai viet or cau hoi
     *
     * @param object {"isLiked":1}
     * @return
     */
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token",
                    required = true, dataType = "string", paramType = "header")})
    @ApiOperation(value = "Like post", notes = "{\"isLiked\":1}")
    @PutMapping(path = "/like/{id}")
    public ResponseEntity<?> like(@RequestBody Object object, @PathVariable("id") long id) {
        try {
            return response(postService.like(object, id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    /**
     * like bai viet or cau hoi
     *
     * @param object {"isLiked":1}
     * @return
     */
    @ApiOperation(value = "follow post", notes = "{\"isFollowed\":1}")
    @PutMapping(path = "/follow/{id}")
    public ResponseEntity<?> follow(@RequestBody Object object, @PathVariable("id") long id) {
        try {
            return response(postService.follow(object, id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "Lay chi tiet theo id", notes = "")
    @GetMapping(path = "/get-detail/{id}")
    public ResponseEntity<?> getDetail(@PathVariable("id") long id) {
        try {
            return response(postService.getDetail(id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "get-by-alias news", notes = "")
    @GetMapping(path = "/get-by-alias/{alias}")
    public ResponseEntity<?> getByAlias(@PathVariable("alias") String alias) {
        try {
            return response(postService.getByAlias(alias));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    /**
     * Get Post by Tag
     *
     * @return
     */
    @ApiOperation(value = "get-by-tag", notes = "")
    @GetMapping(path = "get-by-tag/{tagId}")
    public ResponseEntity<?> getByTag(
            @RequestParam(value = CommonField.PAGE, required = false, defaultValue = DefaultConst.PAGE) Integer page,
            @RequestParam(value = CommonField.SIZE, required = false, defaultValue = DefaultConst.SIZE) Integer size,
            @RequestParam(value = "tagId", required = false, defaultValue = DefaultConst.NUMBER) Long tagId
    ) {
        try {
            return response(postService.getByTag(page, size, tagId));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }


    /**
     * Admin change status of Post Approved/Not Approved
     * After that this post will be display as ads
     * 1- Not found Post
     * 2- Value is not valid
     *
     * @param postId id cua bai post
     * @param object {"isPublished":1}
     * @return {"code":0,"message":"no message","data":{"post":{"title":"Viêm tiết niệu","content":"Tôi thường xuyên bị tiểu buốt, liệu có phải là viêm tiết niệu ko? Bệnh này điều trị thế nào?","imageUrls":[],"thumbnailImageUrls":[],
     * "categoryId":"002519151379929889081-cda49f30b6cb4ff9bd216d7e9191f846","isClassified":1,"assignedDate":1488257041000,"isPublished":1,"likeCount":1,"commentCount":2,"linkAlias":"viem-tiet-nieu-20170214110426000.html",
     * "titleSearch":"viem tiet nieu","contentSearch":"toi thuong xuyen bi tieu buot, lieu co phai la viem tiet nieu ko? benh nay dieu tri the nao?","answered":1,"isPrivate":0,
     * "uid":"002519152557338931609-e663692c687e4e2395f5720b514a915c","createdDate":1487045066000,"updatedDate":1513911990171},"user":{"email":"ngo.giakiet@congdongyte.vn","nickname":"ngo.giakiet","fullname":"Ngô Gia Kiệt","job":"",
     * "company":"","dob":0,"address":"kim giang","gender":1,"thumbnailAvatarUrl":"http://123.24.206.9:38192/Thumbnail/14022017035154AM-002519152564857908719-da09bdce7f7046ab81389ca85bf4ea95.png","avatarUrl":"http://123.24.206
     * .9:38192/Images/14022017035154AM-002519152564857908719-da09bdce7f7046ab81389ca85bf4ea95.png","phone":"09123344","socialId":"","socialType":0,"role":0,"isVerified":0,"jobTitle":"","isBlocked":0,"loginToken":"","adminRole":0,
     * "uid":"002519152607980385500-ae030e82f81b49de84b88c53c032f8dd","createdDate":1487040001000,"updatedDate":1487040001000000},"tags":[]}}
     */
    @ApiOperation(value = "Them moi hoi vien cua Hoi Lao", notes = "{}")
    @PutMapping(path = "set-post-approved-status/{postId}")
    public ResponseEntity<?> setPostApprovedStatus(
            @PathVariable("postId") long postId,
            @RequestBody Object object) {
        try {
            return response(postService.setPostApprovedStatus(postId, object));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "Đếm số lượng câu hỏi chưa duyệt", notes = "{}")
    @GetMapping(path = "get-classify-count")
    public ResponseEntity<?> getClassifyCount() {
        try {
            return response(postService.getClassifyCount());
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "Đếm số lượng câu hỏi chưa duyệt", notes = "{}")
    @GetMapping(path = "get-highlight-post")
    public ResponseEntity<?> getHighlightPost() {
        try {
            return response(postService.getHighlightPost());
        } catch (Exception ex) {
            return response(error(ex));
        }
    }


    @ApiOperation(value = "danh sach cac bai post chua duoc asigned", notes = "{}")
    @GetMapping(path = "/get-unassigned/{postId}")
    public ResponseEntity<?> getUnassigned(
            @RequestParam(value = CommonField.PAGE, required = false, defaultValue = DefaultConst.PAGE) Integer page,
            @RequestParam(value = CommonField.SIZE, required = false, defaultValue = DefaultConst.SIZE) Integer size
    ) {
        try {
            return response(postService.getUnassigned(page, size));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "Review a post", notes = "{\"review\":1}")
    @PutMapping(path = "/review/{id}")
    public ResponseEntity<?> reviewPost(@RequestBody Object object, @PathVariable("id") long id) {
        try {
            return response(postService.reviewPost(object, id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token",
                    required = true, dataType = "string", paramType = "header")})
    @ApiOperation(value = "Lay danh sach bai post da duoc asign cho bac si", notes = "{}")
    @GetMapping(path = "get-assigned")
    public ResponseEntity<?> getAssigned(
            @RequestParam(value = CommonField.PAGE, required = false, defaultValue = DefaultConst.PAGE) Integer page,
            @RequestParam(value = CommonField.SIZE, required = false, defaultValue = DefaultConst.SIZE) Integer size
    ) {
        try {
            return response(postService.getAssigned(page, size));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "Lay danh gia trung binh theo id bac si", notes = "")
    @GetMapping(path = "/get-result-review/{doctorId}")
    public ResponseEntity<?> getResultReview(@PathVariable("doctorId") long doctorId) {
        try {
            return response(postService.getResultReview(doctorId));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "Update a post", notes = "{\"diagnose\":\"dau lung\"}")
    @PutMapping(path = "/update-diagnose/{id}")
    public ResponseEntity<?> updateDiagnose(@RequestBody Object object, @PathVariable("id") long id) {
        try {
            return response(postService.updateDiagnose(object, id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }
    
    @ApiOperation(value = "set status a post", notes = "{\"status\":7}")
    @PutMapping(path = "/set-status/{id}")
    public ResponseEntity<?> setStatus(@RequestBody Object object, @PathVariable("id") long id) {
        try {
            return response(postService.setStatus(object, id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "set status a post", notes = "{\"status\":7}")
    @PutMapping(path = "/check-2day-ago")
    public ResponseEntity<?> check2DayAgo() {
        try {
            return response(postService.check2DayAgo());
        } catch (Exception ex) {
            return response(error(ex));
        }
    }
}
