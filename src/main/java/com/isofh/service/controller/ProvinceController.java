package com.isofh.service.controller;

import com.isofh.service.constant.DefaultConst;
import com.isofh.service.service.ProvinceService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/province")
public class ProvinceController extends BaseController {

    @Autowired
    ProvinceService provinceService;

    @ApiOperation(value = "lay tat ca tinh, thanh pho", notes = "{}")
    @GetMapping(path = "/get-all")
    public ResponseEntity<?> getAll() {
        try {
            return response(provinceService.getAll());
        } catch (Exception ex) {
            return response(error(ex));
        }
    }
}
