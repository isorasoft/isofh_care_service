package com.isofh.service.controller;

import com.isofh.service.constant.DefaultConst;
import com.isofh.service.service.DoctorService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/doctor-booking")
public class DoctorController extends BaseController {

    @Autowired
    DoctorService service;

    public DoctorController() {
        super();
    }

    @ApiOperation(value = "Sync Doctor from HIS", notes = "{}")
    @GetMapping(path = "/sync/{hospitalId}")
    public ResponseEntity<?> sync(@PathVariable("hospitalId") long hospitalId) {
        try {
            return response(service.sync(hospitalId));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "get all", notes = "{}")
    @GetMapping(path = "/get-all/{hospitalId}")
    public ResponseEntity<?> getAll(@PathVariable("hospitalId") long hospitalId) {
        try {
            return response(service.getAll(hospitalId));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "Lay tat ca cac bac si theo khoa va chuyen khoa", notes = "{}")
    @GetMapping(path = "/get-by-department-specialist")
    public ResponseEntity<?> getByDepartmentSpecialist(
            @RequestParam(value = "departmentId") Long departmentId,
            @RequestParam(value = "specialistId", required = false, defaultValue = DefaultConst.NUMBER) Long specialistId
    ) {
        try {
            return response(service.getByDepartmentSpecialist(departmentId, specialistId));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

}
