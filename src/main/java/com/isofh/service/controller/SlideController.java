package com.isofh.service.controller;

import com.isofh.service.constant.DefaultConst;
import com.isofh.service.constant.field.CommonField;
import com.isofh.service.constant.field.SlideField;
import com.isofh.service.service.SlideService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/slide")
public class SlideController extends BaseController {


    @Autowired
    SlideService slideService;

    @ApiOperation(value = "Create a new slide", notes = "{\"slideItemIds\":[1,2,3],\"slide\":{\"href\":\"http\",\"image\":\"tuan.png\"}}")
    @PostMapping(path = "/create")
    public ResponseEntity<?> create(@RequestBody Object object) {
        try {
            return response(slideService.create(object));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "Update a slide", notes = "{\"slideItemIds\":[1,2,3],\"slide\":{\"href\":\"http\",\"image\":\"tuan.png\"}}")
    @PutMapping(path = "/update/{id}")
    public ResponseEntity<?> update(@RequestBody Object object, @PathVariable("id") long id) {
        try {
            return response(slideService.update(object, id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "delete a slide", notes = "{}")
    @DeleteMapping(path = "/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") long id) {
        try {
            return response(slideService.delete(id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "search slide", notes = "{}")
    @GetMapping(path = "/search")
    public ResponseEntity<?> search(
            @RequestParam(value = CommonField.PAGE, required = false, defaultValue = DefaultConst.PAGE) Integer page,
            @RequestParam(value = CommonField.SIZE, required = false, defaultValue = DefaultConst.SIZE) Integer size,
            @RequestParam(value = SlideField.ACTIVE, required = false, defaultValue = DefaultConst.NUMBER) Integer active,
            @RequestParam(value = SlideField.INTERVAL_TIME, required = false, defaultValue = DefaultConst.NUMBER) Long intervalTime,
            @RequestParam(value = SlideField.AUTO_PLAY, required = false, defaultValue = DefaultConst.NUMBER) Integer autoPlay,
            @RequestParam(value = SlideField.NAME, required = false, defaultValue = DefaultConst.STRING) String name
    ) {
        try {
            return response(slideService.search(page, size, active, intervalTime, autoPlay, name));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "Update a slide", notes = "{\"active\":1}")
    @PutMapping(path = "/set-active/{id}")
    public ResponseEntity<?> setActive(@RequestBody Object object, @PathVariable("id") long id) {
        try {
            return response(slideService.setActive(object, id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }
}
