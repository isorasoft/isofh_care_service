package com.isofh.service.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.isofh.service.service.PatientHistoryService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(path = "/patient-history-booking")
public class PatientHistoryController extends BaseController {

    @Autowired
    PatientHistoryService patientHistoryService;
    
    @ApiOperation(value = "Sync his department", notes = "{}")
    @GetMapping(path = "/sync/{hospitalId}/{profileId}")
    public ResponseEntity<?> sync(@PathVariable("hospitalId") long hospitalId, @PathVariable("profileUid") String profileUid) {
        try {
            return response(patientHistoryService.sync(hospitalId, profileUid, true));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }
    
    @ApiOperation(value = "Sync his department", notes = "{}")
    @GetMapping(path = "/sync/{profileId}")
    public ResponseEntity<?> sync(@PathVariable("profileId") Long profileId) {
        try {
            return response(patientHistoryService.sync(profileId));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }
    
    @ApiOperation(value = "Sync his department", notes = "{}")
    @PutMapping(path = "/update/{hospitalId}/{PatientHistoryId}")
    public ResponseEntity<?> updateResult(@PathVariable("hospitalId") long hospitalId, @PathVariable("patientHistoryId") String patientHistoryId) {
        try {
            return response(patientHistoryService.updateResult(hospitalId, patientHistoryId, true));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "get all department", notes = "{}")
    @GetMapping(path = "/get-all/{hospitalId}")
    public ResponseEntity<?> getAll(@PathVariable("hospitalId") long hospitalId) {
        try {
            return response(patientHistoryService.getAll(hospitalId));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "get detail", notes = "{}")
    @GetMapping(path = "/get-detail/{id}")
    public ResponseEntity<?> getDetail(@PathVariable("id") long id) {
        try {
            return response(patientHistoryService.getDetail(id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }
    
    @ApiOperation(value = "update data", notes = "{}")
    @GetMapping(path = "/update-data")
    public ResponseEntity<?> updateData() {
        try {
            return response(patientHistoryService.updateData());
        } catch (Exception ex) {
            return response(error(ex));
        }
    }
}
