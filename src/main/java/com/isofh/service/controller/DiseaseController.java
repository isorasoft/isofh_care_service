package com.isofh.service.controller;

import com.isofh.service.constant.AppConst;
import com.isofh.service.constant.DefaultConst;
import com.isofh.service.constant.field.CommonField;
import com.isofh.service.service.DiseaseService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@RestController
@RequestMapping(path = "/disease")
public class DiseaseController extends BaseController {

    @Autowired
    DiseaseService diseaseService;

    @ApiOperation(value = "Create a new disease", notes = "{\"disease\":{},\"imageUrls\":[\"url1\"],\"symptomIds\":[3],\"specialistId\":5}")
    @PostMapping(path = "/create")
    public ResponseEntity<?> create(@RequestBody Object object) {
        try {
            return response(diseaseService.create(object));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "update a disease", notes = "{\"disease\":{},\"imageUrls\":[\"url1\"],\"symptomIds\":[3],\"specialistId\":5}")
    @PutMapping(path = "/update/{id}")
    public ResponseEntity<?> update(@RequestBody Object object, @PathVariable("id") Long id) {
        try {
            return response(diseaseService.update(object, id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "Delete info a new disease", notes = "")
    @DeleteMapping(path = "/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") Long id) {
        try {
            return response(diseaseService.delete(id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    /**
     * Tim kiem benh
     * (Tên bệnh,from age - to age, chuyên khoa, giới tính, UpdateDate(từ ngày - đến ngày) )
     *
     * @param sortType 1. tim kiem theo view_count<br>
     * @return
     */
    @ApiOperation(value = "Search disease", notes = "")
    @GetMapping(path = "/search")
    public ResponseEntity<?> search(
            @RequestParam(value = CommonField.PAGE, required = false, defaultValue = DefaultConst.PAGE) Integer page,
            @RequestParam(value = CommonField.SIZE, required = false, defaultValue = DefaultConst.SIZE) Integer size,
            @RequestParam(value = "name", required = false, defaultValue = DefaultConst.STRING) String name,
            @RequestParam(value = "fromAge", required = false, defaultValue = DefaultConst.NUMBER) Integer fromAge,
            @RequestParam(value = "toAge", required = false, defaultValue = DefaultConst.NUMBER) Integer toAge,
            @RequestParam(value = "specialistId", required = false, defaultValue = DefaultConst.NUMBER) Integer specialistId,
            @RequestParam(value = "gender", required = false, defaultValue = DefaultConst.NUMBER) Integer gender,
            @RequestParam(value = "fromUpdatedDate", required = false, defaultValue = DefaultConst.DATE) @DateTimeFormat(pattern = AppConst.DATE_FORMAT) LocalDate fromUpdatedDate,
            @RequestParam(value = "toUpdatedDate", required = false, defaultValue = DefaultConst.DATE) @DateTimeFormat(pattern = AppConst.DATE_FORMAT) LocalDate toUpdatedDate,
            @RequestParam(value = "sortType", required = false, defaultValue = DefaultConst.NUMBER) Integer sortType
    ) {
        try {
            return response(diseaseService.search(page, size, name, fromAge, toAge, specialistId, gender, fromUpdatedDate, toUpdatedDate, sortType));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "Tim kiem benh theo ten cua benh hoac ten cua trieu trung", notes = "")
    @GetMapping(path = "/search-by-disease-symptom")
    public ResponseEntity<?> searchByDiseaseSymptom(
            @RequestParam(value = CommonField.PAGE, required = false, defaultValue = DefaultConst.PAGE) Integer page,
            @RequestParam(value = CommonField.SIZE, required = false, defaultValue = DefaultConst.SIZE) Integer size,
            @RequestParam(value = "name", required = false, defaultValue = DefaultConst.STRING) String name,
            @RequestParam(value = "sortType", required = false, defaultValue = DefaultConst.NUMBER) Integer sortType
    ) {
        try {
            return response(diseaseService.searchByDiseaseSymptom(page, size, name, sortType));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    /**
     * Tang so luong viewCount moi lan click vao item nay
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "update view count", notes = "")
    @PutMapping(path = "/update-view-count/{id}")
    public ResponseEntity<?> updateViewCount(@PathVariable("id") Long id) {
        try {
            return response(diseaseService.updateViewCount(id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    /**
     * lay thong tin chi tiet Benh (Disease)
     *
     * @return
     */
    @ApiOperation(value = "lay thong tin chi tiet Benh (Disease)", notes = "")
    @GetMapping(path = "/get-detail/{id}")
    public ResponseEntity<?> getDetail(@PathVariable("id") long id) {
        try {
            return response(diseaseService.getDetail(id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    /**
     * @param page
     * @param size
     * @param sortType  mac dinh search by created_date, 1 thoe view_count
     * @param symptomId
     * @return
     */
    @ApiOperation(value = "Tim kiem benh theo ten cua benh hoac ten cua trieu trung", notes = "")
    @GetMapping(path = "/search-disease-by-symptom")
    public ResponseEntity<?> searchDiseaseBySymptom(
            @RequestParam(value = CommonField.PAGE, required = false, defaultValue = DefaultConst.PAGE) Integer page,
            @RequestParam(value = CommonField.SIZE, required = false, defaultValue = DefaultConst.SIZE) Integer size,
            @RequestParam(value = "sortType", required = false, defaultValue = DefaultConst.NUMBER) Integer sortType,
            @RequestParam(value = "symptomId") long symptomId
    ) {
        try {
            return response(diseaseService.searchDiseaseBySymptom(page, size, sortType, symptomId));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }
}
