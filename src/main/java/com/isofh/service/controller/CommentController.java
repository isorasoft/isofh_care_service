package com.isofh.service.controller;

import com.isofh.service.constant.DefaultConst;
import com.isofh.service.constant.field.CommonField;
import com.isofh.service.service.CommentService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/comment")
public class CommentController extends BaseController {

    @Autowired
    CommentService commentService;


    /**
     * comment vao bai viet or cau hoi
     *
     * @param object {"postId":"6","comment":{"content":"content","images":"image1, image2"}}
     * @return
     */
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token",
                    required = true, dataType = "string", paramType = "header")})
    @ApiOperation(value = "Create a comment", notes = "{\"comment\":{\"content\":\"day la noi dung bai comment\"},\"postId\":1, \"diagnose\":\"chan doan\"}")
    @PostMapping(path = "/create")
    public ResponseEntity<?> create(@RequestBody Object object) {
        try {
            return response(commentService.create(object));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    /**
     * tim kiem comment trong post
     *
     * @return
     */
    @ApiOperation(value = "Search comment in post", notes = "")
    @GetMapping(path = "/search")
    public ResponseEntity<?> search(
            @RequestParam(value = CommonField.PAGE, required = false, defaultValue = DefaultConst.PAGE) Integer page,
            @RequestParam(value = CommonField.SIZE, required = false, defaultValue = DefaultConst.SIZE) Integer size,
            @RequestParam(value = "postId", required = false, defaultValue = DefaultConst.NUMBER) Integer postId,
            @RequestParam(value = "role", required = false, defaultValue = DefaultConst.NUMBER) Integer role
    ) {
        try {
            return response(commentService.search(page, size, role, postId));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    /**
     * cap nhat comment
     *
     * @param object {"comment":{"content":"content update","images":"image1, image2"}}
     * @return
     */
    @ApiOperation(value = "Update a comment", notes = "{\"comment\":{\"content\":\"content update\",\"images\":\"image1, image2\"}}")
    @PutMapping(path = "/update/{id}")
    public ResponseEntity<?> update(@RequestBody Object object, @PathVariable("id") long id) {
        try {
            return response(commentService.update(object, id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    /**
     * Xoa comment
     *
     * @return
     */
    @ApiOperation(value = "Delete comment", notes = "")
    @DeleteMapping(path = "/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") long id) {
        try {
            return response(commentService.delete(id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    /**
     * Duyet or huy duyet comment giai phap
     *
     * @param object {"isSolution":1}
     * @return
     */
    @ApiOperation(value = "Change status is published", notes = "{\"isSolution\":1}")
    @PutMapping(path = "/accept-as-solution/{id}")
    public ResponseEntity<?> acceptAsSolution(@RequestBody Object object, @PathVariable("id") long id) {
        try {
            return response(commentService.acceptAsSolution(object, id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    /**
     * like comment
     *
     * @param object {"isLiked":1}
     * @return
     */
//    @ApiOperation(value = "Like comment", notes = "{\"isLiked\":1}")
//    @PutMapping(path = "/like/{id}")
//    public ResponseEntity<?> like(@RequestBody Object object, @PathVariable("id") long id) {
//        try {
//            return response(commentService.like(object, id));
//        } catch (Exception ex) {
//            return response(error(ex));
//        }
//    }
}
