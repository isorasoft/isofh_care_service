package com.isofh.service.controller;

import com.isofh.service.constant.DefaultConst;
import com.isofh.service.constant.ServiceConst;
import com.isofh.service.model.CountryEntity;
import com.isofh.service.model.HisArray;
import com.isofh.service.service.CountryService;
import com.isofh.service.utils.NetworkUtils;
import io.swagger.annotations.ApiOperation;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/country")
public class CountryController extends BaseController {

    @Autowired
    CountryService countryService;

    @ApiOperation(value = "Get all", notes = "")
    @GetMapping(path = "/get-all")
    public ResponseEntity<?> getAll() {
        try {
            return response(countryService.getAll());
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

}
