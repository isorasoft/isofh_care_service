package com.isofh.service.controller;

import com.isofh.service.constant.AppConst;
import com.isofh.service.constant.DefaultConst;
import com.isofh.service.constant.field.CommonField;
import com.isofh.service.service.DrugService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@RestController
@RequestMapping(path = "/drug")
public class DrugController extends BaseController {


    @Autowired
    DrugService drugService;

    /**
     * hàm tạo 1 thuốc mới vào trong bảng thuốc chung từ các nhà thuốc và từ bên mình nhập
     *
     * @param object
     * @return
     */
    @ApiOperation(value = "Create drug", notes = "{\"drug\":{},\"diseaseIds\":[3],\"countryId\":5}")
    @PostMapping(path = "/create")
    public ResponseEntity<?> create(@RequestBody Object object) {
        try {
            return response(drugService.create(object));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }


    /**
     * hàm update các thông tin cho drug
     *
     * @param object {"drug":{},"diseaseIds":[3],"countryId":5}
     * @param id     id cua thuoc can update
     * @return
     */
    @ApiOperation(value = "update a drug", notes = "{\"drug\":{},\"diseaseIds\":[3],\"countryId\":5}")
    @PutMapping(path = "/update/{id}")
    public ResponseEntity<?> update(@RequestBody Object object, @PathVariable("id") Long id) {
        try {
            return response(drugService.update(object, id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "Delete a drug", notes = "")
    @DeleteMapping(path = "/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") Long id) {
        try {
            return response(drugService.delete(id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    /**
     * (Tên thuốc, Hoạt chất, Phân loại thuốc, Đường dùng, Quy cách, Hãng sản xuất, Giá tham khảo(với giá trị nhỏ hơn hoặc bằng), UpdateDate(từ ngày - đến ngày) )
     *
     * @param page
     * @param size
     * @param name
     * @param activeSubstances
     * @param category
     * @param methodUse
     * @param standard
     * @param manufacturer
     * @param fromPrice
     * @param toPrice
     * @param fromUpdatedDate
     * @param toUpdatedDate
     * @param sortType         Loai sap xep default theo updated_date, 1 theo view_count
     * @return
     */
    @ApiOperation(value = "Search drug", notes = "")
    @GetMapping(path = "/search")
    public ResponseEntity<?> search(
            @RequestParam(value = CommonField.PAGE, required = false, defaultValue = DefaultConst.PAGE) Integer page,
            @RequestParam(value = CommonField.SIZE, required = false, defaultValue = DefaultConst.SIZE) Integer size,
            @RequestParam(value = "name", required = false, defaultValue = DefaultConst.STRING) String name,
            @RequestParam(value = "activeSubstances", required = false, defaultValue = DefaultConst.STRING) String activeSubstances,
            @RequestParam(value = "category", required = false, defaultValue = DefaultConst.NUMBER) Long category,
            @RequestParam(value = "methodUse", required = false, defaultValue = DefaultConst.NUMBER) Long methodUse,
            @RequestParam(value = "standard", required = false, defaultValue = DefaultConst.STRING) String standard,
            @RequestParam(value = "manufacturer", required = false, defaultValue = DefaultConst.STRING) String manufacturer,
            @RequestParam(value = "fromPrice", required = false, defaultValue = DefaultConst.NUMBER) Long fromPrice,
            @RequestParam(value = "toPrice", required = false, defaultValue = DefaultConst.NUMBER) Long toPrice,
            @RequestParam(value = "fromUpdatedDate", required = false, defaultValue = DefaultConst.DATE) @DateTimeFormat(pattern = AppConst.DATE_FORMAT) LocalDate fromUpdatedDate,
            @RequestParam(value = "toUpdatedDate", required = false, defaultValue = DefaultConst.DATE) @DateTimeFormat(pattern = AppConst.DATE_FORMAT) LocalDate toUpdatedDate,
            @RequestParam(value = "sortType", required = false, defaultValue = DefaultConst.NUMBER) Integer sortType,
            @RequestParam(value = "facilityId", required = false, defaultValue = DefaultConst.NUMBER) Integer facilityId
    ) {
        try {
            return response(drugService.search(page, size, name, activeSubstances, category, methodUse, standard, manufacturer, fromPrice, toPrice, fromUpdatedDate, toUpdatedDate, sortType, facilityId));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "update view count", notes = "")
    @PutMapping(path = "/update-view-count/{id}")
    public ResponseEntity<?> updateViewCount(@PathVariable("id") Long id) {
        try {
            return response(drugService.updateViewCount(id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "import drug", notes = "{\"drugs\":[{\"Name\":\"MORIHEPAMIN\",\"activeSubstances\":\"Acid amin*\",\"category\":\"\",\"Methoduser\":\"Tiêm truyền\",\"component\":\"\",\"standard\":\"túi\"," +
            "\"manufacturer\":\"AJINOMOTO- Japan\",\"Price\":\"\",\"useness\":\"\",\"avoidUseness\":\"\",\"country\":\"Japan\"}]}")
    @PostMapping(path = "/import-csv")
    public ResponseEntity<?> importCsv(@RequestBody Object object) {
        try {
            return response(drugService.importCsv(object));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "lay thong tin chi tiet thuoc", notes = "")
    @GetMapping(path = "/get-detail/{id}")
    public ResponseEntity<?> getDetail(@PathVariable("id") long id) {
        try {
            return response(drugService.getDetail(id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "Sync data elastic search", notes = "")
    @GetMapping(path = "/sync-data")
    public ResponseEntity<?> synsData() {
        try {
            return response(drugService.syncData());
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    /**
     * Them moi thuoc vao nha thuoc
     *
     * @param object
     * @return
     */
    @ApiOperation(value = "Review Facility", notes = "{\"facilityId\":1,\"drugId\":2}")
    @PostMapping(path = "/add-facility-to-drug")
    public ResponseEntity<?> addDrugToFacility(@RequestBody Object object) {
        try {
            return response(drugService.addDrugToFacility(object));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "import drug", notes = "")
    @PostMapping(path = "/import-thuoc-net")
    public ResponseEntity<?> importThuocNet(@RequestBody Object object) {
        try {
            return response(drugService.importThuocNet(object));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

}
