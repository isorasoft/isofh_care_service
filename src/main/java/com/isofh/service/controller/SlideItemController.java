package com.isofh.service.controller;

import com.isofh.service.constant.DefaultConst;
import com.isofh.service.constant.field.CommonField;
import com.isofh.service.constant.field.SlideItemField;
import com.isofh.service.service.SlideItemService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/slide-item")
public class SlideItemController extends BaseController {


    @Autowired
    SlideItemService slideItemService;

    @ApiOperation(value = "Create a new slide-item", notes = "{\"slideItem\":{\"href\":\"http\",\"image\":\"tuan.png\"}}")
    @PostMapping(path = "/create")
    public ResponseEntity<?> create(@RequestBody Object object) {
        try {
            return response(slideItemService.create(object));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "Update a slide-item", notes = "{\"slideItem\":{\"href\":\"http\",\"image\":\"tuan.png\"}}")
    @PutMapping(path = "/update/{id}")
    public ResponseEntity<?> update(@RequestBody Object object, @PathVariable("id") long id) {
        try {
            return response(slideItemService.update(object, id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "delete a slide-item", notes = "{}")
    @DeleteMapping(path = "/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") long id) {
        try {
            return response(slideItemService.delete(id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "search slide-item", notes = "{}")
    @GetMapping(path = "/search")
    public ResponseEntity<?> search(
            @RequestParam(value = CommonField.PAGE, required = false, defaultValue = DefaultConst.PAGE) Integer page,
            @RequestParam(value = CommonField.SIZE, required = false, defaultValue = DefaultConst.SIZE) Integer size,
            @RequestParam(value = "slideId", required = false, defaultValue = DefaultConst.NUMBER) Long slideId,
            @RequestParam(value = SlideItemField.NAME, required = false, defaultValue = DefaultConst.STRING) String name
    ) {
        try {
            return response(slideItemService.search(page, size, slideId, name));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }
}
