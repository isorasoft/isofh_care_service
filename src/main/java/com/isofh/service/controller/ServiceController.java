package com.isofh.service.controller;

import com.isofh.service.service.ServiceService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/service-booking")
public class ServiceController extends BaseController {

    @Autowired
    ServiceService service;

    public ServiceController() {
        super();
    }

    @ApiOperation(value = "Sync service from HIS", notes = "{}")
    @GetMapping(path = "/sync/{hospitalId}")
    public ResponseEntity<?> sync(@PathVariable("hospitalId") long hospitalId) {
        try {
            return response(service.sync(hospitalId));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "get all service", notes = "{}")
    @GetMapping(path = "/get-all/{hospitalId}")
    public ResponseEntity<?> getAll(@PathVariable("hospitalId") long hospitalId) {
        try {
            return response(service.getAll(hospitalId));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }
}
