package com.isofh.service.controller;

import com.isofh.service.constant.DefaultConst;
import com.isofh.service.constant.field.CommonField;
import com.isofh.service.service.DoctorDepartmentService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/doctor-department")
public class DoctorDepartmentController extends BaseController {

    @Autowired
    DoctorDepartmentService doctorDepartmentService;

    @ApiOperation(value = "Create a news", notes = "{\"department\":{\"name\":\"Khoa khám bệnh\",\"isPublic\":9,\"index\":9}}")
    @PostMapping(path = "/create")
    public ResponseEntity<?> create(@RequestBody Object object) {
        try {
            return response(doctorDepartmentService.create(object));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "Get all", notes = "")
    @GetMapping(path = "/get-all")
    public ResponseEntity<?> getAll() {
        try {
            return response(doctorDepartmentService.getAll());
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "Search department", notes = "")
    @GetMapping(path = "/search")
    public ResponseEntity<?> search(
            @RequestParam(value = CommonField.PAGE, required = false, defaultValue = DefaultConst.PAGE) Integer page,
            @RequestParam(value = CommonField.SIZE, required = false, defaultValue = DefaultConst.SIZE) Integer size,
            @RequestParam(value = "linkAlias", required = false, defaultValue = DefaultConst.STRING) String linkAlias
    ) {
        try {
            return response(doctorDepartmentService.search(page, size, linkAlias));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "Search department", notes = "")
    @GetMapping(path = "/get-list-doctor-department")
    public ResponseEntity<?> getListGroupByDepartment() {
        try {
            return response(doctorDepartmentService.getListGroupByDepartment());
        } catch (Exception ex) {
            return response(error(ex));
        }
    }
}
