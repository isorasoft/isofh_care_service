package com.isofh.service.controller;

import com.isofh.service.service.RoomService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/room-booking")
public class RoomController extends BaseController {

    @Autowired
    RoomService service;

    public RoomController() {
        super();
    }

    @ApiOperation(value = "Sync room from HIS", notes = "{}")
    @GetMapping(path = "/sync/{hospitalId}")
    public ResponseEntity<?> sync(@PathVariable("hospitalId") long hospitalId) {
        try {
            return response(service.sync(hospitalId));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "get all", notes = "{}")
    @GetMapping(path = "/get-all/{hospitalId}")
    public ResponseEntity<?> getAll(@PathVariable("hospitalId") long hospitalId) {
        try {
            return response(service.getAll(hospitalId));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

}
