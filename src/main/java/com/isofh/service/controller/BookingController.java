package com.isofh.service.controller;

import com.isofh.service.constant.AppConst;
import com.isofh.service.constant.DefaultConst;
import com.isofh.service.constant.field.CommonField;
import com.isofh.service.service.BookingService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@RestController
@RequestMapping(path = "/booking")
public class BookingController extends BaseController {

    @Autowired
    BookingService service;

    @ApiOperation(value = "Create booking", notes = "{\"scheduleId\":50,\"profileId\":6,\"booking\":{\"bookingTime\":\"2018-09-18 07:30:00\",\"note\":\"note\"}}")
    @PostMapping(path = "/create/{hospitalId}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token", required = true, dataType = "string", paramType = "header") })
    public ResponseEntity<?> create(@RequestBody Object object, @PathVariable("hospitalId") long hospitalId) {
        try {
            return response(service.create(object, hospitalId));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    /**
     * Lay lich lam viec theo bac si, chuyen khoa, khoa
     */
    @ApiOperation(value = "Lay lich dat kham theo bac si, chuyen khoa", notes = "{}")
    @GetMapping(path = "/get-by-doctor-specialist-department")
    public ResponseEntity<?> getByDoctorSpecialistDepartment(
            @RequestParam(value = "doctorId", required = false, defaultValue = DefaultConst.NUMBER) Long doctorId,
            @RequestParam(value = "specialistId", required = false, defaultValue = DefaultConst.NUMBER) Long specialistId,
            @RequestParam(value = "startDate", required = false, defaultValue = DefaultConst.DATE) @DateTimeFormat(pattern = AppConst.DATE_FORMAT) LocalDate startDate,
            @RequestParam(value = "endDate", required = false, defaultValue = DefaultConst.DATE) @DateTimeFormat(pattern = AppConst.DATE_FORMAT) LocalDate endDate) {
        try {
            return response(service.getByDoctorSpecialistDepartment(doctorId, specialistId, startDate, endDate));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    /**
     * Lay lich lam viec theo bac si, chuyen khoa, khoa
     */
    @ApiOperation(value = "Tim kiem danh sach booking", notes = "{}")
    @GetMapping(path = "/search")
    public ResponseEntity<?> search(
            @RequestParam(value = "patientName", required = false, defaultValue = DefaultConst.STRING) String patientName,
            @RequestParam(value = "doctorId", required = false, defaultValue = DefaultConst.NUMBER) Long doctorId,
            @RequestParam(value = "serviceId", required = false, defaultValue = DefaultConst.NUMBER) Long serviceId,
            @RequestParam(value = "specialistId", required = false, defaultValue = DefaultConst.NUMBER) Long specialistId,
            @RequestParam(value = "roomId", required = false, defaultValue = DefaultConst.NUMBER) Long roomId,
            @RequestParam(value = "phone", required = false, defaultValue = DefaultConst.STRING) String phone,
            @RequestParam(value = "address", required = false, defaultValue = DefaultConst.STRING) String address,
            @RequestParam(value = "status", required = false, defaultValue = "-2") Integer status,
            @RequestParam(value = "value", required = false, defaultValue = DefaultConst.STRING) String value,
            @RequestParam(value = "startDate", required = false, defaultValue = DefaultConst.DATE) @DateTimeFormat(pattern = AppConst.DATE_FORMAT) LocalDate startDate,
            @RequestParam(value = "endDate", required = false, defaultValue = DefaultConst.DATE) @DateTimeFormat(pattern = AppConst.DATE_FORMAT) LocalDate endDate,
            @RequestParam(value = CommonField.PAGE, required = false, defaultValue = DefaultConst.PAGE) Integer page,
            @RequestParam(value = CommonField.SIZE, required = false, defaultValue = DefaultConst.SIZE) Integer size,
            @RequestParam(value = "departmentId", required = false, defaultValue = DefaultConst.NUMBER) Long departmentId,
            @RequestParam(value = "admin", required = false, defaultValue = DefaultConst.STRING) String admin,
            @RequestParam(value = "phoneCall", required = false, defaultValue = DefaultConst.STRING) String phoneCall,
            @RequestParam(value = "arrival", required = false, defaultValue = DefaultConst.NUMBER) Integer arrival,
            @RequestParam(value = "requestDoctor", required = false, defaultValue = DefaultConst.NUMBER) Integer requestDoctor) {
        try {
            return response(service.search(patientName, doctorId, serviceId, specialistId, roomId, phone, address,
                    status, value, startDate, endDate, page, size, departmentId, admin, phoneCall, arrival,
                    requestDoctor));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    /**
     * Xoa lich dat kham, set deleted=1
     */
    @ApiOperation(value = "Xoa lich dat kham, set deleted=1", notes = "{}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token", required = true, dataType = "string", paramType = "header") })
    @DeleteMapping(path = "/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") long id) {
        try {
            return response(service.delete(id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token", required = true, dataType = "string", paramType = "header") })
    @ApiOperation(value = "Update booking", notes = "{\"scheduleId\":50,\"profileId\":6,\"booking\":{\"bookingTime\":\"2018-09-18 07:30:00\",\"note\":\"note\"}}")
    @PutMapping(path = "/update/{id}")
    public ResponseEntity<?> update(@RequestBody Object object, @PathVariable("id") long id) {
        try {
            return response(service.update(object, id));
        } catch (Exception ex) {
            return response(error(ex));
        }

    }

    @ApiOperation(value = "Sync booking info")
    @GetMapping(path = "/sync-data/{hospitalId}")
    public ResponseEntity<?> syncData(@PathVariable("hospitalId") long hospitalId) {
        try {
            return response(service.syncData(hospitalId));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "Update trang thai la da den benh vien", notes = "{\"arrival\":1}}")
    @PutMapping(path = "/update-arrival/{id}")
    public ResponseEntity<?> updateArrival(@RequestBody Object object, @PathVariable("id") Long id) {
        try {
            return response(service.updateArrival(object, id));
        } catch (Exception ex) {
            return response(error(ex));
        }

    }

    @ApiOperation(value = "Update reminder", notes = "{\"reminder\":\"reminder\"}")
    @PutMapping(path = "/update-reminder/{id}")
    public ResponseEntity<?> updateReminder(@RequestBody Object object, @PathVariable("id") Long id) {
        try {
            return response(service.updateReminder(object, id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "Update phone call", notes = "{\"phoneCall\":\"phoneCall\"}")
    @PutMapping(path = "/update-phone-call/{id}")
    public ResponseEntity<?> updatePhoneCall(@RequestBody Object object, @PathVariable("id") Long id) {
        try {
            return response(service.updatePhoneCall(object, id));
        } catch (Exception ex) {
            return response(error(ex));
        }

    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token", required = true, dataType = "string", paramType = "header") })
    @ApiOperation(value = "Get detail, chi admin moi co quyen dung api nay", notes = "")
    @GetMapping(path = "/get-detail/{id}")
    public ResponseEntity<?> getDetail(@PathVariable("id") long id) {
        try {
            return response(service.getDetail(id));
        } catch (Exception ex) {
            return response(error(ex));
        }

    }

    @ApiOperation(value = "Kiem tra xem co phong nao bi trung so kham trong ngay khong", notes = "")
    @GetMapping(path = "/find-duplicate-in-room")
    public ResponseEntity<?> findDuplicateInRoom() {
        try {
            return response(service.findDuplicateInRoom());
        } catch (Exception ex) {
            return response(error(ex));
        }

    }

    @ApiOperation(value = "Auto checkin")
    @GetMapping(path = "/auto-checkin/{hospitalId}")
    public ResponseEntity<?> autoCheckin(@PathVariable("hospitalId") long hospitalId) {
        try {
            return response(service.autoCheckin(hospitalId));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "Auto checkin")
    @GetMapping(path = "/auto-reject")
    public ResponseEntity<?> autoReject() {
        try {
            return response(service.autoReject());
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    /**
     * Get List patient history by user, bao gom 2 loai, da day len his va chua day
     * len his
     */
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token", required = true, dataType = "string", paramType = "header") })
    @ApiOperation(value = "Get List patient history by profile, bao gom 2 loai, da day len his va chua day len his", notes = "{}")
    @GetMapping(path = "/get-list-patient-history-by-profile/{profileId}")
    public ResponseEntity<?> getListPatientHistoryByProfileHospital(@PathVariable("profileId") long profileId,
            @RequestParam(name = "hospitalId") long hospitalId) {
        try {
            return response(service.getListPatientHistoryByProfileHospital(profileId, hospitalId));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    /**
     * Get List patient history by user, bao gom 2 loai, da day len his va chua day
     * len his
     */
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token", required = true, dataType = "string", paramType = "header") })
    @ApiOperation(value = "Get List patient history by profile, bao gom 2 loai, da day len his va chua day len his", notes = "{}")
    @GetMapping(path = "/get-list-patient-history-by-profile")
    public ResponseEntity<?> getListPatientHistoryByProfile() {
        try {
            return response(service.getListPatientHistoryByProfile());
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    /**
     * Lay chi tiet lich su vao vien
     */
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token", required = true, dataType = "string", paramType = "header") })
    @ApiOperation(value = "Lay chi tiet lich su vao vien", notes = "{}")
    @GetMapping(path = "/get-detail-patient-history/{patientHistoryId}")
    public ResponseEntity<?> getDetailPatientHistory(@PathVariable("patientHistoryId") long patientHistoryId,
            @RequestParam(name = "hospitalId") long hospitalId) {
        try {
            return response(service.getDetailPatientHistory(patientHistoryId, hospitalId));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    /**
     * Lay chi tiet lich su vao vien
     */
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token", required = true, dataType = "string", paramType = "header") })
    @ApiOperation(value = "Lay ket qua lich su vao vien", notes = "{}")
    @GetMapping(path = "/get-result-patient-history/{patientHistoryId}")
    public ResponseEntity<?> getResultPatientHistory(@PathVariable("patientHistoryId") long patientHistoryId,
            @RequestParam(name = "hospitalId") long hospitalId) {
        try {
            return response(service.getResultPatientHistory(patientHistoryId, hospitalId));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }
}
