package com.isofh.service.controller;

import com.isofh.service.constant.AppConst;
import com.isofh.service.constant.DefaultConst;
import com.isofh.service.constant.field.CommonField;
import com.isofh.service.service.SymptomService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@RestController
@RequestMapping(path = "/symptom")
public class SymptomController extends BaseController {

    @Autowired
    SymptomService symptomService;

    @ApiOperation(value = "Create a new Symptom", notes = "{\"symptom\":{}}")
    @PostMapping(path = "/create")
    public ResponseEntity<?> create(@RequestBody Object object) {
        try {
            return response(symptomService.create(object));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "Create a new Symptom", notes = "{\"symptom\":{}}")
    @PostMapping(path = "/update/{id}")
    public ResponseEntity<?> update(@RequestBody Object object, @PathVariable("id") Long id) {
        try {
            return response(symptomService.update(object, id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "delete a new Symptom", notes = "{\"symptom\":{}}")
    @PostMapping(path = "/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") Long id) {
        try {
            return response(symptomService.delete(id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }


    /**
     * Tim kiem theo trieu chung, neu
     *
     * @param sortType 1. tim kiem theo view_count
     * @return
     */
    @ApiOperation(value = "search a new specialist", notes = "{}")
    @GetMapping(path = "/search")
    public ResponseEntity<?> search(
            @RequestParam(value = "name", required = false, defaultValue = DefaultConst.STRING) String name,
            @RequestParam(value = "head", required = false, defaultValue = DefaultConst.NUMBER) Integer head,
            @RequestParam(value = "neck", required = false, defaultValue = DefaultConst.NUMBER) Integer neck,
            @RequestParam(value = "shoulder", required = false, defaultValue = DefaultConst.NUMBER) Integer shoulder,
            @RequestParam(value = "chest", required = false, defaultValue = DefaultConst.NUMBER) Integer chest,
            @RequestParam(value = "arm", required = false, defaultValue = DefaultConst.NUMBER) Integer arm,
            @RequestParam(value = "abdomen", required = false, defaultValue = DefaultConst.NUMBER) Integer abdomen,
            @RequestParam(value = "flank", required = false, defaultValue = DefaultConst.NUMBER) Integer flank,
            @RequestParam(value = "pelvis", required = false, defaultValue = DefaultConst.NUMBER) Integer pelvis,
            @RequestParam(value = "leg", required = false, defaultValue = DefaultConst.NUMBER) Integer leg,
            @RequestParam(value = "foot", required = false, defaultValue = DefaultConst.NUMBER) Integer foot,
            @RequestParam(value = "back", required = false, defaultValue = DefaultConst.NUMBER) Integer back,
            @RequestParam(value = "waist", required = false, defaultValue = DefaultConst.NUMBER) Integer waist,
            @RequestParam(value = "ass", required = false, defaultValue = DefaultConst.NUMBER) Integer ass,
            @RequestParam(value = "hand", required = false, defaultValue = DefaultConst.NUMBER) Integer hand,
            @RequestParam(value = CommonField.PAGE, required = false, defaultValue = DefaultConst.PAGE) Integer page,
            @RequestParam(value = CommonField.SIZE, required = false, defaultValue = DefaultConst.SIZE) Integer size,
            @RequestParam(value = "sortType", required = false, defaultValue = DefaultConst.NUMBER) Integer sortType,
            @RequestParam(value = "fromUpdatedDate", required = false, defaultValue = DefaultConst.DATE) @DateTimeFormat(pattern = AppConst.DATE_FORMAT) LocalDate fromUpdatedDate,
            @RequestParam(value = "toUpdatedDate", required = false, defaultValue = DefaultConst.DATE) @DateTimeFormat(pattern = AppConst.DATE_FORMAT) LocalDate toUpdatedDate

    ) {
        try {
            return response(symptomService.search(name, head, neck, shoulder, chest, arm, abdomen, flank, pelvis, leg, foot, back, waist, ass, hand, page, size, sortType, fromUpdatedDate, toUpdatedDate));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    /**
     * Tang so luong viewCount moi lan click vao item nay
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "update view count", notes = "")
    @PutMapping(path = "/update-view-count/{id}")
    public ResponseEntity<?> updateViewCount(@PathVariable("id") Long id) {
        try {
            return response(symptomService.updateViewCount(id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

}
