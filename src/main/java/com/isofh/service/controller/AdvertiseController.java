package com.isofh.service.controller;

import com.isofh.service.constant.DefaultConst;
import com.isofh.service.constant.field.CommonField;
import com.isofh.service.service.AdvertiseService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/advertise")
public class AdvertiseController extends BaseController {

    @Autowired
    AdvertiseService advertiseService;

    public AdvertiseController() {
        super();
    }

    @ApiOperation(value = "Create a new advertise", notes = "{\"advertise\":{}}")
    @PostMapping(path = "/create")
    public ResponseEntity<?> create(@RequestBody Object object) {
        try {
            return response(advertiseService.create(object));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "Delete advertise", notes = "")
    @DeleteMapping(path = "/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") long id) {
        try {
            return response(advertiseService.delete(id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @GetMapping(path = "/search")
    public ResponseEntity<?> search(
            @RequestParam(value = CommonField.PAGE, required = false, defaultValue = DefaultConst.PAGE) Integer page,
            @RequestParam(value = CommonField.SIZE, required = false, defaultValue = DefaultConst.SIZE) Integer size,
            @RequestParam(value = "title", required = false, defaultValue = DefaultConst.STRING) String title,
            @RequestParam(value = "content", required = false, defaultValue = DefaultConst.STRING) String content,
            @RequestParam(value = "type", required = false, defaultValue = DefaultConst.NUMBER) Integer type
    ) {
        try {
            return response(advertiseService.search(title, content, type, page, size));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }


}
