package com.isofh.service.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.isofh.service.constant.DefaultConst;
import com.isofh.service.constant.field.CommonField;
import com.isofh.service.service.RolesService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(path = "/roles")
public class RolesController extends BaseController {

    @Autowired
    RolesService rolesService;

    @ApiOperation(value = "Create a new slide", notes = "{\"roles\":{\"name\":\"cham soc khach hang\",\"code\":\"CSKH\"},\"permissionIds\":[1,2,3], \"createPersonId\":3,\"updatePersonId\":7 }")
    @PostMapping(path = "/create")
    public ResponseEntity<?> create(@RequestBody Object object) {
        try {
            return response(rolesService.create(object));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "Update a slide", notes = "")
    @PutMapping(path = "/update/{id}")
    public ResponseEntity<?> update(@RequestBody Object object, @PathVariable("id") long id) {
        try {
            return response(rolesService.update(object, id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "delete a slide", notes = "{}")
    @DeleteMapping(path = "/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") long id) {
        try {
            return response(rolesService.delete(id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "search slide", notes = "{}")
    @GetMapping(path = "/search")
    public ResponseEntity<?> search(
            @RequestParam(value = CommonField.PAGE, required = false, defaultValue = DefaultConst.PAGE) Integer page,
            @RequestParam(value = CommonField.SIZE, required = false, defaultValue = DefaultConst.SIZE) Integer size,
            @RequestParam(value = "stringQuery", required = false, defaultValue = DefaultConst.STRING) String stringQuery,
            @RequestParam(value = "value", required = false, defaultValue = DefaultConst.NUMBER) Long value,
            @RequestParam(value = "blocked", required = false, defaultValue = DefaultConst.NUMBER) int blocked,
            @RequestParam(value = "type", required = false, defaultValue = DefaultConst.NUMBER) int type
    ) {
        try {
            return response(rolesService.search(page, size, stringQuery, value, type, blocked));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "lay thong tin chi tiet roles", notes = "")
    @GetMapping(path = "/get-detail/{id}")
    public ResponseEntity<?> getDetail(@PathVariable("id") long id) {
        try {
            return response(rolesService.getDetail(id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "block a role", notes = "{\"blocked\":1}")
    @PutMapping(path = "/block/{id}")
    public ResponseEntity<?> block(@RequestBody Object object, @PathVariable("id") long id) {
        try {
            return response(rolesService.block(object, id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }
    
    @ApiOperation(value = "Update role hr (all)", notes = "{}")
    @PutMapping(path = "/update-role/{id}")
    public ResponseEntity<?> updateRoleHR(@PathVariable("id") long id) {
        try {
            return response(rolesService.updateRoleHR(id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }


}
