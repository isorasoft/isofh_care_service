package com.isofh.service.controller;

import com.isofh.service.constant.DefaultConst;
import com.isofh.service.constant.field.CommonField;
import com.isofh.service.service.HospitalService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/hospital")
public class HospitalController extends BaseController {

    @Autowired
    HospitalService hospitalService;

    @ApiOperation(value = "Create a new permission", notes = "{\"hospital\":{\"name\":\"benh vien phoi trung uong\",\"address\":\"ha noi\", \"taxCode\":\"155sd523\"},\"username\":\"huong1233 nguyen\",\"email\":\"kkelasnddhuongnguyenthang12345@gmail.com\",\"phone\":\"0ddd2ds0000099\",\"role\":8,\"password\":\"123333\",\"active\":1}")
    @PostMapping(path = "/create")
    public ResponseEntity<?> create(@RequestBody Object object) {
        try {
            return response(hospitalService.create(object));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }


    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token",
                    required = true, dataType = "string", paramType = "header")})
    @ApiOperation(value = "Update a slide", notes = "{\"permission\":{\"name\":\"xem nhan vien\",\"value\":8}}")
    @PutMapping(path = "/update/{id}")
    public ResponseEntity<?> update(@RequestBody Object object, @PathVariable("id") long id) {
        try {
            return response(hospitalService.update(object, id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    /**
     * Lay danh sach benh vien
     * @param type
     * @return
     */
    @ApiOperation(value = "", notes = "{}")
    @PostMapping(path = "/get-all")
    public ResponseEntity<?> getAll(@RequestParam(name = "type", required = false, defaultValue = DefaultConst.NUMBER) int type) {
        try {
            return response(hospitalService.getAll(type));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "search hospital", notes = "{}")
    @GetMapping(path = "/search")
    public ResponseEntity<?> search(
            @RequestParam(value = CommonField.PAGE, required = false, defaultValue = DefaultConst.PAGE) Integer page,
            @RequestParam(value = CommonField.SIZE, required = false, defaultValue = DefaultConst.SIZE) Integer size,
            @RequestParam(value = "stringQuyery", required = false, defaultValue = DefaultConst.STRING) String stringQuyery,
            @RequestParam(value = "active", required = false, defaultValue = DefaultConst.NUMBER) Integer active,
            @RequestParam(value = "type", required = false, defaultValue = DefaultConst.NUMBER) Integer type
    ) {
        try {
            return response(hospitalService.search(page, size, stringQuyery, active, type));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }



    @ApiOperation(value = "delete a slide", notes = "{}")
    @DeleteMapping(path = "/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") long id) {
        try {
            return response(hospitalService.delete(id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "lay thong tin chi tiet csyt", notes = "")
    @GetMapping(path = "/get-detail/{id}")
    public ResponseEntity<?> getDetail(@PathVariable("id") long id) {
        try {
            return response(hospitalService.getDetail(id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "active", notes = "{\"active\":1}")
    @PutMapping(path = "/set-active/{id}")
    public ResponseEntity<?> setActive(@RequestBody Object object, @PathVariable("id") long id) {
        try {
            return response(hospitalService.setActive(object, id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }


    @ApiOperation(value = "lay thong tin csyt cua 1 bac si", notes = "")
    @GetMapping(path = "/get-hospital-by-doctor/{doctorId}")
    public ResponseEntity<?> getHospitalByDoctor(@PathVariable("doctorId") long doctorId) {
        try {
            return response(hospitalService.getHospitalByDoctor(doctorId));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }
}
