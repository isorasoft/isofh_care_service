package com.isofh.service.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.isofh.service.service.ZoneService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(path = "/zone")
public class ZoneController extends BaseController {

    @Autowired
    ZoneService zoneService;

    @ApiOperation(value = "Lay zones of district", notes = "")
    @GetMapping(path = "/get-by-district/{districtId}")
    public ResponseEntity<?> getByDistrict(@PathVariable("districtId") Long districtId) {
        try {
            return response(zoneService.getByDistrict(districtId));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }
}
