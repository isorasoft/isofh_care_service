package com.isofh.service.controller;

import com.isofh.service.constant.DefaultConst;
import com.isofh.service.constant.field.CommonField;
import com.isofh.service.model.ResultEntity;
import com.isofh.service.service.UserService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/user")
public class UserController extends BaseController {

    @Autowired
    UserService userService;

    /**
     * Them moi nguoi dung, nguoi dung quan ly cac khoa
     *
     * @param object
     * @return
     */
    @ApiOperation(value = "Create a new user by admin", notes = "{\"user\":{}, \"facilityIds\":[1, 2, 3], \"roleId\":1}")
    @PostMapping(path = "/create")
    public ResponseEntity<?> create(@RequestBody Object object) {
        try {
            return response(userService.create(object));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "Delete a user", notes = "")
    @DeleteMapping(path = "/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") Long id) {
        try {
            return response(userService.delete(id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    /**
     * style: 1: des name 2: des ngay tao 3: asc ngay tao 4: asc name 5: asc
     * username 6: des username
     *
     * @param page
     * @param size
     * @param queryString
     * @param type
     * @param style
     * @return
     */
    @ApiOperation(value = "Search user", notes = "")
    @GetMapping(path = "/search")
    public ResponseEntity<?> search(
            @RequestParam(value = CommonField.PAGE, required = false, defaultValue = DefaultConst.PAGE) Integer page,
            @RequestParam(value = CommonField.SIZE, required = false, defaultValue = DefaultConst.SIZE) Integer size,
            @RequestParam(value = "queryString", required = false, defaultValue = DefaultConst.STRING) String queryString,
            @RequestParam(value = "active", required = false, defaultValue = DefaultConst.NUMBER) Integer active,
            @RequestParam(value = "specialistId", required = false, defaultValue = DefaultConst.NUMBER) Long specialistId,
            @RequestParam(value = "type", required = false, defaultValue = DefaultConst.NUMBER) Integer type,
            @RequestParam(value = "roleId", required = false, defaultValue = DefaultConst.NUMBER) Long roleId,
            @RequestParam(value = "style", required = false, defaultValue = DefaultConst.NUMBER) Integer style

    ) {
        try {
            return response(userService.search(queryString, active, specialistId, type, style, roleId, page, size));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "block/unblock a user", notes = "{\"block\":1}")
    @PutMapping(path = "/block/{id}")
    public ResponseEntity<?> block(@RequestBody Object object, @PathVariable("id") Long id) {
        try {
            return response(userService.block(object, id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    /**
     * @param object cho phep dang nhap bang username, phone, email
     * @return code = 2 tai khoan bi khoa <br>
     * 3 thong tin dang nhap khong dung <br>
     * 4 Tai khoan chua duoc kich hoat <br>
     */
    @ApiOperation(value = "login with email and password", notes = "{\"emailOrPhone\":\"admin\",\"password\":\"e10adc3949ba59abbe56e057f20f883e\", \"device\":{\"os\":0,\"deviceId\":\"123456\",\"token\":\"11212\"}}")
    @PutMapping(path = "/login")
    public ResponseEntity<ResultEntity> login(@RequestBody Object object) {
        try {
            return response(userService.login(object));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "logout with email and password", notes = "{\"emailOrPhone\":\"admin\",\"password\":\"e10adc3949ba59abbe56e057f20f883e\", \"device\":{\"os\":0,\"deviceId\":\"123456\",\"token\":\"11212\"}}")
    @PutMapping(path = "/logout/{userId}")
    public ResponseEntity<ResultEntity> logout(@PathVariable("userId") Long userId) {
        try {
            return response(userService.logout(userId));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "reset password a user", notes = "{}")
    @PutMapping(path = "/reset-password/{id}")
    public ResponseEntity<?> resetPassword(@RequestBody Object object, @PathVariable("id") Long id) {
        try {
            return response(userService.resetPassword(object, id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "Dang ky tai khoan", notes = "{\"user\":{\"email\":\"\",\"password\":\"25d55ad283aa400af464c76d713c07ad\",\"name\":\"Đjrj\",\"gender\":1,\"phone\":\"0981111300\",\"dob\":null,\"role\":1},\"applicationId\":\"457683741386685\",\"accessToken\":\"EMAWcX8oXp7HsVEraQHggeTrg04x6Cuu3BdFFy5ZBPBD8vbzBYxNwX0xD0ro5S4rIJuBkQt5a4g16ihRe8EkQ6t2Xuk4ZCPZBvYv23YauUAZDZD\",\"socialId\":\"0\",\"socialType\":1,\"device\":{\"os\":0,\"deviceId\":\"\",\"token\":\"\"}}")
    @PostMapping(path = "/register")
    public ResponseEntity<?> register(@RequestBody Object object) {
        try {
            return response(userService.register(object));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "Active tai khoan", notes = "{\"verifyCode\":\"123456\"}")
    @PostMapping(path = "/active/{id}")
    public ResponseEntity<?> active(@RequestBody Object object, @PathVariable("id") long id) {
        try {
            return response(userService.active(object, id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    /**
     * Active tu email
     *
     * @param token
     * @return
     */
    @ApiOperation(value = "Active tai khoan theo token gui tu email ve", notes = "{\"verifyCode\":\"123456\"}")
    @GetMapping(path = "/active-email/{token}")
    public ResponseEntity<?> activeEmail(@PathVariable("token") String token) {
        try {
            return response(userService.activeEmail(token));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    /**
     * @param object {"socialType":1,"socialId":"123456781","fullname":"Tuan dep
     *               zai","email":"ledangtuanbk2@gmail.com","device":{"os":1,"deviceId":"8453cbaf464b1ec","token":"efgGY55itgM:APA91bG1wpO7EBfasJ4NV"}}
     * @return code = 2 tai khoan bi khoa <br>
     * 3 thong tin dang nhap khong dung <br>
     */
    @ApiOperation(value = "login with email and password", notes = "{\"socialType\":1,\"socialId\":\"123456781\",\"name\":\"Tuan dep zai\",\"email\":\"ledangtuanbk2@gmail.com\",\"device\":{\"os\":1,\"deviceId\":\"8453cbaf464b1ec\","
            + "\"token\":\"efgGY55itgM:APA91bG1wpO7EBfasJ4NV\"}}")
    @PutMapping(path = "/login-social")
    public ResponseEntity<?> loginSocial(@RequestBody Object object) {
        try {
            return response(userService.loginSocial(object));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    /**
     * Nguoi dung lay lai mat khau
     *
     * @return
     */
    @ApiOperation(value = "Nguoi dung lay lai mat khau", notes = "{\"emailOrPhone\":\"ledangtuanbk@gmail.com\", \"type\":1}")
    @PutMapping(path = "/forget-password")
    public ResponseEntity<?> forgetPassword(@RequestBody Object object) {
        try {
            return response(userService.forgetPassword(object));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    /**
     * Nguoi dung lay lai mat khau by fb acount kit
     *
     * @return
     */
    @ApiOperation(value = "Nguoi dung lay lai mat khau ty fb acount kit", notes = "{\"accessToken\":\"\", \"applicationId\":, \"newPassword\":\"ádasdasd\"}")
    @PutMapping(path = "/refresh-password-by-token")
    public ResponseEntity<?> refreshPasswordByToken(@RequestBody Object object) {
        try {
            return response(userService.refreshPasswordByToken(object));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }
    
    /**
     * Nguoi dung sua sdt by fb acount kit
     *
     * @return
     */
    @ApiOperation(value = "Nguoi dung lay lai mat khau ty fb acount kit", notes = "{\"accessToken\":\"\", \"applicationId\":, \"newPhone\":\"0988456789\"}")
    @PutMapping(path = "/refresh-phone-by-token")
    public ResponseEntity<?> refreshPhoneByToken(@RequestBody Object object) {
        try {
            return response(userService.refreshPhoneByToken(object));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "Nguoi dung confirm forget password code", notes = "{\"code\":\"123456\", \"phone\":\"0982938146\"}")
    @PutMapping(path = "/confirm-code")
    public ResponseEntity<?> confirmCode(@RequestBody Object object) {
        try {
            return response(userService.confirmCode(object));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "Nguoi dung cap nhap password ", notes = "{\"passwordOld\":\"encodePassword\",\"passwordNew\":\"encodePassword\"}")
    @PutMapping(path = "/update-password/{id}")
    public ResponseEntity<?> updatePassword(@PathVariable("id") long id, @RequestBody Object object) {
        try {
            return response(userService.updatePassword(id, object));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    /**
     * Chinh sua danh sach co so y te duoc quan ly boi nguoi dung
     *
     * @param object
     * @return
     */
    @ApiOperation(value = "them danh sach co so y te duoc quan ly boi nguoi dung", notes = "{\"facilityIds\":[1, 2, 3]}")
    @PutMapping(path = "/add-facility/{id}")
    public ResponseEntity<?> addFacility(@RequestBody Object object, @PathVariable("id") long id) {
        try {
            return response(userService.addFacility(object, id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    /**
     * Chinh sua danh sach co so y te duoc quan ly boi nguoi dung
     *
     * @param object
     * @return
     */
    @ApiOperation(value = "xoa danh sach co so y te duoc quan ly boi nguoi dung", notes = "{\"facilityIds\":[1, 2, 3]}")
    @PutMapping(path = "/remove-facility/{id}")
    public ResponseEntity<?> removeFacility(@RequestBody Object object, @PathVariable("id") long id) {
        try {
            return response(userService.removeFacility(object, id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "Lay thong tin chi tiet cua 1 user", notes = "")
    @GetMapping(path = "/get-detail/{id}")
    public ResponseEntity<?> getDetail(@PathVariable("id") Long id) {
        try {
            return response(userService.getDetail(id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "xoa mail", notes = "{}")
    @GetMapping(path = "/clear-email/{email}")
    public ResponseEntity<?> clearEmail(@PathVariable("email") String email) {
        try {
            return response(userService.clearEmail(email));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }
    
    @ApiOperation(value = "check used phone", notes = "{}")
    @GetMapping(path = "/check-used-phone/{phone}")
    public ResponseEntity<?> checkUsedPhone(@PathVariable("phone") String phone) {
        try {
            return response(userService.checkUsedPhone(phone));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "Verify SMS Code", notes = "{\"verifyCode\":\"1\"}")
    @PutMapping(path = "/verify-sms-code/{id}")
    public ResponseEntity<?> verifySmsCode(@RequestBody Object object, @PathVariable("id") Long id) {
        try {
            return response(userService.verifySmsCode(object, id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    /**
     * update thong tin email cho tai khoan
     *
     * @param object
     * @param id
     * @return
     */
    @ApiOperation(value = "update email", notes = "{\"email\":\"ledangtuanbk@gmail.com\"}")
    @PutMapping(path = "/update-email/{id}")
    public ResponseEntity<?> updateEmail(@RequestBody Object object, @PathVariable("id") long id) {
        try {
            return response(userService.updateEmail(object, id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    /**
     * update thong tin cho tai khoan hien tai chi update name
     *
     * @param object
     * @param id
     * @return
     */
    @ApiOperation(value = "update thong tin tai khoan", notes = "{\"user\":{\"name\":\"ldt\"}}")
    @PutMapping(path = "/update/{id}")
    public ResponseEntity<?> update(@RequestBody Object object, @PathVariable("id") long id) {
        try {
            return response(userService.update(object, id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "", notes = "{}")
    @GetMapping(path = "/token/{userId}")
    public ResponseEntity<?> token(@PathVariable("userId") Long userId) {
        try {
            return response(userService.token(userId));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    // ham duoc them

    @ApiOperation(value = "Create a new doctor by admin", notes = "{\"doctor\":{\"name\":\"nguyen thi huong\",\"email\":\"kkelangtddhang12345@gmail.com\",\"phone\":\"134d545\",\"certificateCode\":\"BSff123\",\"tiles\":\"Bac si\"," +
            "\"adress\":\"khoai chau, hung yen\",\"gender\":0,\"degree\":\"giam doc\",\"introduct\":\"la sinh vien da tot nghiep loai xuat sac\"},\"specialistId\":521}")
    @PostMapping(path = "/create-doctor-by-admin")
    public ResponseEntity<?> createDoctor(@RequestBody Object object) {
        try {
            return response(userService.createDoctor(object));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "update doctor by admin", notes = "{\"doctor\":{\"name\":\"nguyen thi huong\",\"email\":\"kkelangtddhang12345@gmail.com\",\"phone\":\"134d545\",\"certificateCode\":\"BSff123\",\"tiles\":\"Bac si\",\"adress\":\"khoai chau, hung yen\",\"gender\":0,\"degree\":\"giam doc\",\"introduct\":\"la sinh vien da tot nghiep loai xuat sac\"},\"specialistId\":521}")
    @PutMapping(path = "/update-doctor-by-admin/{id}")
    public ResponseEntity<?> updateDoctor(@RequestBody Object object, @PathVariable("id") Long id) {
        try {
            return response(userService.updateDoctor(id, object));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "Lay thong tin chi tiet cua 1 user", notes = "")
    @GetMapping(path = "/get-detail-doctor/{id}")
    public ResponseEntity<?> getDetailDoctor(@PathVariable("id") Long id) {
        try {
            return response(userService.getDetailDoctor(id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "active user", notes = "{\"active\":1}")
    @PutMapping(path = "/set-active/{id}")
    public ResponseEntity<?> setActive(@RequestBody Object object, @PathVariable("id") long id) {
        try {
            return response(userService.setActive(object, id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "Search user", notes = "")
    @GetMapping(path = "/getListBySpecialist")
    public ResponseEntity<?> search(
            @RequestParam(value = CommonField.PAGE, required = false, defaultValue = DefaultConst.PAGE) Integer page,
            @RequestParam(value = CommonField.SIZE, required = false, defaultValue = DefaultConst.SIZE) Integer size,
            @RequestParam(value = "specialistId", required = false, defaultValue = DefaultConst.NUMBER) Long specialistId,
            @RequestParam(value = "type", required = false, defaultValue = DefaultConst.NUMBER) Integer type

    ) {
        try {
            return response(userService.getListBySpecialist(specialistId, page, size, type));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    
    @ApiOperation(value = "His create new user and profile", notes = "{\"hisPatientId\":\"hisPatientId\",\"countryCode\":1,\"provinceCode\":1,\"districtCode\":1,\"zoneCode\":1,\"email\":\"ledangtuanbk@gmail.com\",\"profile\":{\"patientName\":\"Le " +
            "Dang Tuan\",\"gender\":0,\"dob\":\"1989-06-23\",\"phone\":\"0968245523\",\"address\":\"123 duong ABC\",\"guardianName\":\"Ten nguoi bao lanh\",\"guardianPhone\":\"So dien thoai\"}}")
    @PostMapping(path = "/create-user-by-his")
    public synchronized ResponseEntity<?> createByHis(@RequestBody Object object) {
        try {
            return response(userService.createByHis(object));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiOperation(value = "active user", notes = "{\"hospitalIds\":[1]}")
    @PutMapping(path = "/update-hospital/{userId}")
    public ResponseEntity<?> updateHospital(@RequestBody Object object, @PathVariable("userId") long userId) {
        try {
            return response(userService.updateHospital(object, userId));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Authorization token",
                    required = true, dataType = "string", paramType = "header")})
    @ApiOperation(value = "lay cac vien ma user nay da co thong tin", notes = "")
    @GetMapping(path = "/get-hospital-by-current-user")
    public ResponseEntity<?> getHospitalByCurrentUser() {
        try {
            return response(userService.getHospitalByCurrentUser());
        } catch (Exception ex) {
            return response(error(ex));
        }
    }
}
