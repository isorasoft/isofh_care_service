package com.isofh.service.controller;

import com.isofh.service.service.SmsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/sms")
public class SmsController extends BaseController {

    @Autowired
    SmsService smsService;

    @GetMapping(path = "/get-not-sent")
    public ResponseEntity<?> search() {
        try {
            return response(smsService.search());
        } catch (Exception ex) {
            return response(error(ex));
        }
    }

    @PutMapping(path = "/set-sent/{id}")
    public ResponseEntity<?> setSent(@PathVariable("id") Long id) {
        try {
            return response(smsService.setSent(id));
        } catch (Exception ex) {
            return response(error(ex));
        }
    }


}
