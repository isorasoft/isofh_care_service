package test;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class TestCls {
    private String name = "tuan";

    @JsonIgnore
    private int age = 0;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
