FROM ledangtuanbk/tomcat:8
# Take the war and copy to webapps of tomcat
RUN rm -rf /usr/local/tomcat/webapps/ROOT /usr/local/tomcat/webapps/docs
# copy build file
#COPY target/*.war /usr/local/tomcat/webapps/ROOT.war
COPY target/*.war /usr/local/tomcat/webapps/isofh-care.war

# copy docs
COPY docs /usr/local/tomcat/webapps/docs

EXPOSE 8080