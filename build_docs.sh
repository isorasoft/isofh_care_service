#!/usr/bin/env bash
javadoc -private -d docs -splitindex com.isofh.service.model com.isofh.service.controller com.isofh.service.enums -sourcepath "src/main/java"